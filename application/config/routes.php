<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['/'] = 'welcome';
$route['admin'] = 'admin/user';
$route['login'] = 'user/login/$1';

$route['login/(:any)'] = 'user/login/$1';
$route['login/(:any)/(:num)'] = 'user/login/$1/$2';

$route['register'] = 'user/register';
$route['register_action'] = 'user/register_user_action';

$route['register_handyman'] = 'user/register_handyman';

$route['forget_password'] = 'user/forget_password';


$route['registerHandyman_action'] = 'user/register_handyman_action';
$route['login_action'] = 'user/login_action';
$route['forget_password_action'] = 'user/forget_password_action';
$route['logout'] = 'user/logout';
$route['services/(:any)'] = 'category/services/$1';


/* admin routes*/
//$route['admin/loginAction'] = 'admin/welcome/login_action';
//$route['admin/adminLogout'] = 'admin/welcome/logout';
//$route['admin/adminDashboard'] = 'admin/welcome/dashboard';

$route['404_override'] = 'my404';
$route['translate_uri_dashes'] = FALSE;

/* user routes */
$route['home_maintenance'] = 'category/home_maintenance';
$route['home_renovation'] = 'category/home_renovation';
$route['maintenance_list/(:any)'] = 'category/maintenance_list/$1';
$route['renovation_list/(:any)'] = 'category/renovation_list/$1';
$route['do_login'] = 'user/do_login';
$route['dashboard'] = 'user/do_login';
$route['change_password'] = 'user/change_password';
$route['change_password_action'] = 'user/change_password_action';
$route['edit_profile'] = 'user/edit_profile';
$route['edit_profile_action'] = 'user/edit_profile_action';
$route['paypal_ipn'] = 'paypal/ipn';
$route['user_profile/(:any)/(:num)'] = 'user/user_profile/$1/$2';



/* task routes*/
$route['post_task'] = 'task/post_task';
$route['make_offer'] = 'task/make_offer';
$route['apply_task/(:any)'] = 'task/apply_maintenance_task/$1';
$route['edit_task/(:any)'] = 'task/edit_task/$1';
$route['edit_task_action'] = 'task/edit_task_action';
$route['task_detail'] = 'category/detail';
$route['check_task_response/(:any)'] = 'task/check_task_response/$1';
$route['handyman_detail/(:num)/(:num)'] = 'user/handyman_detail/$1/$2';
$route['hire_handyman/(:num)/(:num)'] = 'user/hire_handyman/$1/$2';
$route['check_task_detail/(:any)'] = 'task/check_task_detail/$1';
$route['check_task_detail'] = 'task/check_task_detail';
$route['apply_task'] = 'task/apply_maintenance_task';
$route['add_task_action'] = 'task/add_task_action';
$route['follow_task'] = 'task/follow_task';
$route['get_price'] = 'category/get_price';
$route['posted_task'] = 'task/posted_task';
$route['assigned_task'] = 'task/assigned_task';
$route['applied_task'] = 'task/applied_task';
$route['following_tasks'] = 'task/following_tasks';
$route['switch_user/(:any)'] = 'user/switch_user/$1';
$route['switchTo_registerHandyman_action'] = 'user/switchTo_registerHandyman_action';
$route['switchTo_registerUser_action'] = 'user/switchTo_registerUser_action';
$route['ratingToCustomer'] = 'task/ratingToCustomer';
$route['ratingToHandyman'] = 'task/ratingToHandyman';

$route['update_workarea'] = 'user/update_workarea';
$route['store_credit'] = 'paypal/store_credit';
$route['store_credit_action'] = 'paypal/store_credit_action';


$route['login_fb'] = 'user/login_fb';
$route['search_task'] = 'category/search_task';



<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller 
{ 
   var $template  = array();
   var $data      = array();
   public function __construct() 
   {
	    date_default_timezone_set('Asia/Kolkata');
	 parent::__construct();
	 $this->load->helper(array('url', 'file', 'string', 'html','form','utility'));
	 $this->load->library(array('auth','session','email'));
	  $this->load->Model('page_model');
	 
	 

   }
   
   
   public function layoutDefault() 
   {
	
	 // making temlate and send data to view.
     $this->template['header'] = $this->load->view('layout/header', $this->data, true);
     //$this->template['left']   = $this->load->view('layout/left', $this->data, true);
     $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
     $this->data['footer']  = $this->page_model->footer_data();
     $this->template['footer'] = $this->load->view('layout/footer', $this->data, true);
     $this->load->view('layout/default', $this->template);
   }
   
   public function layoutLogin() 
   {
     // making temlate and send data to view.
     $this->template['header'] = $this->load->view('layout/header', $this->data, true);
     //$this->template['left']   = $this->load->view('layout/left', $this->data, true);
     $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
     $this->data['footer']  = $this->page_model->footer_data();
     $this->template['footer'] = $this->load->view('layout/footer', $this->data, true);
     $this->load->view('layout/login', $this->template);
   }
   
   
   public function layoutDashboard() 
   {
	  
     $this->template['header'] = $this->load->view('layout/header', $this->data, true);
     $this->template['leftbar'] = $this->load->view('layout/dashboard_leftsidebar', $this->data, true);
     $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
     $this->data['footer']  = $this->page_model->footer_data();
     $this->template['footer'] = $this->load->view('layout/footer', $this->data, true);
     $this->load->view('layout/dashboard', $this->template);
   }
   public function layoutCustomerProfile() 
   {
	  
     $this->template['header'] = $this->load->view('layout/header', $this->data, true);
     //$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
     $this->data['footer']  = $this->page_model->footer_data();
     $this->template['footer'] = $this->load->view('layout/footer', $this->data, true);
     $this->load->view('layout/profile_customer', $this->template);
   }
    public function layoutContentPages() 
   {
	  
     $this->template['header'] = $this->load->view('layout/header', $this->data, true);
     //$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
     $this->data['footer']  = $this->page_model->footer_data();
     $this->template['footer'] = $this->load->view('layout/footer', $this->data, true);
     $this->load->view('layout/cms', $this->template);
   }
   public function layoutHandymanProfile() 
   {
	  
     $this->template['header'] = $this->load->view('layout/header', $this->data, true);
     //$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
     $this->data['footer']  = $this->page_model->footer_data();
     $this->template['footer'] = $this->load->view('layout/footer', $this->data, true);
     $this->load->view('layout/profile_handyman', $this->template);
   }
    public function layoutError() 
   {
     $this->template['header'] = $this->load->view('layout/header', $this->data, true);
     $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
     $this->data['footer']  = $this->page_model->footer_data();
     $this->template['footer'] = $this->load->view('layout/footer', $this->data, true);
     $this->load->view('layout/error', $this->template);
   }
   
   
   
   //ADMIN
   public function layoutAdmin() 
   {
     // making temlate and send data to view.
     $this->template['header'] = $this->load->view('layout/admin/header', $this->data, true);
     $this->template['left']   = $this->load->view('layout/admin/left', $this->data, true);
     $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
     $this->template['footer'] = $this->load->view('layout/admin/footer',$this->data , true);
     $this->load->view('layout/admin/default', $this->template);
   }
   public function layoutAdminlogin() 
   {
     // making temlate and send data to view.
     //$this->template['header'] = $this->load->view('layout/admin/header', $this->data, true);
     //$this->template['left']   = $this->load->view('layout/left', $this->data, true);
     $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
     //$this->template['footer'] = $this->load->view('layout/admin/footer', $this->data, true);
     $this->load->view('layout/admin/login', $this->template);
   }
  
}

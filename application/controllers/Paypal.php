<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paypal extends MY_Controller {

    function  __construct(){
        parent::__construct();
        
            $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->load->library('paypal_lib');
        $this->load->model('task_model');
        $this->load->model('payment_model');
        if($this->session->userdata('user'))
		{
			  $user_detail=$this->session->userdata('user');
			  $this->send_data['user_detail']=$user_detail;
		}
		
     }
     
     function success(){
        //get the transaction data
        $paypalInfo = $_REQUEST;//$this->input->get();
       
        //$data['item_number'] = $paypalInfo['item_number']; 
        //$data['txn_id'] = $paypalInfo["tx"];
        //$data['payment_amt'] = $paypalInfo["amt"];
        //$data['currency_code'] = $paypalInfo["cc"];
        //$data['status'] = $paypalInfo["st"];
        
        //pass the transaction data to view
        //$this->load->view('paypal/success', $data);
           $this->session->set_flashdata('success','Congratulations,you just completed your payment.');
		  redirect('/do_login');	
		        
     }
     
     function success_credit(){
       
        $paypalInfo = $_REQUEST;
        
         $this->load->model('customer_model');


		$data=array('user_type'=>'user','email'=>$this->session->userdata('user')['email']);
		$res=$this->customer_model->is_email_exist($data);
		
		
	  	unset($this->session->userdata['user']['credit']);
	    $this->session->userdata['user']['credit']=$res['credit'];
		     
        $this->session->set_flashdata('success','Congratulations,You have made your transaction successfully.');
		redirect('do_login');
		        
     }
     
     function cancel(){
          $this->session->set_flashdata('success','You have cancelled transaction.');
		  redirect('/post_task');	
     }
     
     function ipn(){
        //paypal return transaction details array
        $paypalInfo    = $this->input->post();

        $data['user_id'] = $paypalInfo['custom'];
        $data['task_id']    = $paypalInfo["item_number"];
        $data['txn_id']    = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["payment_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status']    = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;        
        $result    = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
        
        //check whether the payment is verified
        if(preg_match("/VERIFIED/i",$result)){
            //insert the transaction data into the database
            $this->task_model->insertTransaction($data);
        }
    }
    
     function ipn_store_credit(){
        //paypal return transaction details array
        $paypalInfo    = $this->input->post();

        $data['user_id'] = $paypalInfo['custom'];
        $data['txn_id']    = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["payment_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status']    = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;        
        $result    = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
        
        //check whether the payment is verified
        if(preg_match("/VERIFIED/i",$result)){
            //insert the transaction data into the database
            $this->payment_model->insertCredit($data);
        }
    }
    

    
    function store_credit()
	{	
		if(!$this->session->userdata('user'))
		{
			//redirect to corresponding dashboard
             redirect('login');
						  			 
		}
		else
		{			
                  $this->data=$this->send_data;
				  $this->middle = 'store_credit';     
				  $this->layoutDashboard();
	    }	
		
	}
	
	  function store_credit_action(){

        $this->form_validation->set_rules('amount', 'amount', 'trim|required|numeric|callback_validate_amount'); 
        if ($this->form_validation->run() == FALSE) 
        {
			
                  $this->data=$this->send_data;
				  $this->middle = 'store_credit';     
				  $this->layoutDashboard();
		}
		else
		{
			
				 $data = $this->input->post();
				//Set variables for paypal form
				$returnURL = base_url().'paypal/success_credit'; //payment success url
				$cancelURL = base_url().'paypal/cancel'; //payment cancel url
				$notifyURL = base_url().'paypal/ipn_store_credit'; //ipn url
				//get particular product data
				
				$logo = base_url().'assets/images/logo.png';
				
				$this->paypal_lib->add_field('return', $returnURL);
				$this->paypal_lib->add_field('cancel_return', $cancelURL);
				$this->paypal_lib->add_field('notify_url', $notifyURL);
				$this->paypal_lib->add_field('item_name', 'Store Credit');
				$this->paypal_lib->add_field('custom',$this->session->userdata('user')['id']);
				$this->paypal_lib->add_field('rm','2');
				$this->paypal_lib->add_field('cbt', 'Return to The Store');
					  
				$this->paypal_lib->add_field('amount',  $data['amount']);        
				$this->paypal_lib->image($logo);
				
				$this->paypal_lib->paypal_auto_form();
			   //	$this->paypal_lib->paypal_form();
       
			
		}
    }
    
    function testt()
    {
	

		     $data=array('task_id'=>15,'user_id'=>29);
		     $this->task_model->insertTransaction($data);
		     //get credit for this customer

	}
    
     function validate_amount($str)
	{				
	   
			if ($str*1 ==0)
			{
				$this->form_validation->set_message('validate_amount', 'Amount can not be zero !');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
    
    
    
	 
	}
	
	
}

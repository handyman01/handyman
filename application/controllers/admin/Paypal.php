<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paypal extends MY_Controller {

    function  __construct(){
        parent::__construct();
        $this->load->library('paypal_lib');
        $this->load->model('task_model');
        $this->load->model('payment_model');
       
		
     }
     
     function success_store_credit_user(){
        //get the transaction data
        $paypalInfo = $_REQUEST;//$this->input->get();
       
        //$data['item_number'] = $paypalInfo['item_number']; 
        //$data['txn_id'] = $paypalInfo["tx"];
        //$data['payment_amt'] = $paypalInfo["amt"];
        //$data['currency_code'] = $paypalInfo["cc"];
        //$data['status'] = $paypalInfo["st"];
        
        //pass the transaction data to view
        //$this->load->view('paypal/success', $data);
          $this->session->set_flashdata('success','You have just send credit to user successfully.');
		  redirect('/admin/task/detail/'.$paypalInfo['item_number']);	
		        
     }
     
  
     
     function cancel_store_credit_user(){
          $this->session->set_flashdata('error','You have cancelled transaction.');
		  redirect('/admin/task/manage');
     }
     
     function ipn_store_credit_user(){
        //paypal return transaction details array
        $paypalInfo    = $this->input->post();

        $data['user_id'] = $paypalInfo['custom'];
        $data['task_id']    = $paypalInfo["item_number"];
        $data['txn_id']    = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["payment_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status']    = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;        
        $result    = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
        
        //check whether the payment is verified
        if(preg_match("/VERIFIED/i",$result)){
            //insert the transaction data into the database
            $this->task_model->insertStore_credit_user($data);
        }
    }
    
    
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

  	function __construct()
	{
		parent::__construct();

        $this->load->library('form_validation');
		$this->load->model('category_model');
        $this->load->library('upload');
        $this->auth->is_admin_logged_in();
    
	}
	
	
	public function add()
	{
		$cats=$this->category_model->get_categories(-1);
		$arr_categories[-1]='Select Parent Category';
		if(!empty($cats))
		{
			foreach($cats as $var)
			{
			   $arr_categories[$var['id']]=$var['name'];
			}
	    }

		$this->data=array('categories'=>$arr_categories);
		$this->middle = 'admin/categories/add';  
        $this->layoutAdmin(); 
	}
	
	public function add_category_action()
	{
			
         $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
         $this->form_validation->set_rules('hourly_price', 'Hourly Price', 'trim|required|numeric'); 
         $this->form_validation->set_rules('overtime_charge', 'Overtime Charge', 'trim|required|numeric'); 
         $this->form_validation->set_rules('parent_id', 'Parent Category', 'trim|required|callback_validate_category'); 
			
         if ($this->form_validation->run() == FALSE) { 
			$cats=$this->category_model->get_categories(-1);
			$arr_categories[-1]='Select Parent Category';
			if(!empty($cats))
			{
				foreach($cats as $var)
				{
				   $arr_categories[$var['id']]=$var['name'];
				}
			}

			$this->data=array('categories'=>$arr_categories);
		
         		$this->middle = 'admin/categories/add';  
                $this->layoutAdmin(); 
         } 
         else
         { 

            if(!empty($_FILES['picture']['name']))
            {
				
                $config['upload_path'] = 'assets/uploads/categories/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtotime(date('Y-m-d H:i:s')).'_'.preg_replace('/\s+/', '',$_FILES['picture']['name']);
                
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('picture')){
                    $uploadData = $this->upload->data();
                    $picture = $config['file_name'];
                }else{
                    $picture = '';
                }
            }else{
                $picture = '';
            }
            
            if(!empty($_FILES['icon_big']['name']))
            {
				
                $config['upload_path'] = 'assets/uploads/categories/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtotime(date('Y-m-d H:i:s')).'_'.preg_replace('/\s+/', '',$_FILES['icon_big']['name']);
                
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('icon_big')){
                    $uploadData = $this->upload->data();
                    $icon_big = $config['file_name'];
                }else{
                    $icon_big = '';
                }
            }else{
                $icon_big = '';
            }
            
            //Prepare array of user data
            $userData = array(
                'name' => $this->input->post('name'),
                'image' => $picture,
                'icon_big' => $icon_big,
                'parent_id'=>$this->input->post('parent_id'),
                'hourly_price'=>$this->input->post('hourly_price'),
                'overtime_charge'=>$this->input->post('overtime_charge')
            );
            
           
            
            $data['query']  = $this->category_model->save($userData);                        	
			if($data['query'] != NULL)
			{
				//success
				$this->session->set_flashdata('success','Record has been added successfully.');
		        redirect('/admin/category/add');
			
			}
			else
			{
                //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again!');
		        redirect('/admin/category/add');
			}
         } 
	}
	
public function manage($id=-1)
	{
		$this->data=array('categories'=>$this->category_model->get_categories($id));
		$this->middle = 'admin/categories/manage';  
        $this->layoutAdmin(); 
	}
	
	public function delete()
	{
		$post=$this->input->post();
		$arr_prev_img=$this->category_model->get_imageNameById($post['id']);
					
        if(file_exists(FCPATH."assets/uploads/categories/".$arr_prev_img['image']))
        {
			unlink(FCPATH."assets/uploads/categories/".$arr_prev_img['image']);
		}					
			
		if($this->category_model->delete($post['id']))
		{

					
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
	}
	
	public function edit($id)
	{
		
		if($details=$this->category_model->get_category_detail($id))
		{
				$cats=$this->category_model->get_categories(-1);
				$arr_categories['']='Select Parent Category';
				if(!empty($cats))
				{
					foreach($cats as $var)
					{
					   $arr_categories[$var['id']]=$var['name'];
					}
				}

			     $this->data=array('details'=>$details,'categories'=>$arr_categories);
	             $this->middle = 'admin/categories/edit';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/category/manage');
		}
		
	}
	
	public function edit_action()
	{
		
		
		$post=$this->input->post();
		
		    if(!empty($_FILES['picture']['name']))
            {
                $config['upload_path'] = 'assets/uploads/categories/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtotime(date('Y-m-d H:i:s')).'_'.$_FILES['picture']['name'];
                
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('picture')){
					
					//unlink prev image
					$arr_prev_img=$this->category_model->get_imageNameById($post['id']);
					
					unlink(FCPATH."assets/uploads/categories/".$arr_prev_img['image']);										
                    $uploadData = $this->upload->data();
                    $picture = $config['file_name'];
                    $post['image'] = $picture;
                }else{
                 unset($post['image']);
                }
            }else{
                 unset($post['image']);
            }
            
		    if(!empty($_FILES['icon_big']['name']))
            {
                $config['upload_path'] = 'assets/uploads/categories/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = strtotime(date('Y-m-d H:i:s')).'_'.$_FILES['icon_big']['name'];
                
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('icon_big')){
					
				    $arr_prev_img=$this->category_model->getIconNameById($post['id']);
					//unlink prev image
					if(file_exists(FCPATH."assets/uploads/categories/".$arr_prev_img['big_icon']))
					{
					 // unlink(FCPATH."assets/uploads/categories/".$arr_prev_img['big_icon']);
					}	
		
					
                    $uploadData = $this->upload->data();
                    $icon_big = $config['file_name'];
                    $post['icon_big'] = $icon_big;
                }else{
                 unset($post['icon_big']);
                }
            }else{
                 unset($post['icon_big']);
            }
            

            
		if($this->category_model->update($post))
		{
                $this->session->set_flashdata('success','Record has been updated successfully.');
		        redirect('/admin/category/manage');		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/category/manage');
		}
		exit;
	}
	
	public function change_status()
	{
		$post=$this->input->post();
		if($this->category_model->change_status($post['id'],$post['status']))
		{
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
	}
	
	public function change_popularity()
	{
		$post=$this->input->post();
		if($this->category_model->change_popularity($post['id'],$post['status']))
		{
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
	}
	
	
	
	function validate_category($str)
	{
		
		
	  
	   if($str!=-1)
	   {
		 return TRUE;
	   }
	   else
	   {
		   $this->form_validation->set_message('validate_category','Please choose valid parent category !');
		   return false;
	   }
	}
	
	function get_subcateg_bySlug()
	{
		$post=$this->input->post();
		if($res=$this->category_model->get_categories_bySlug($post['slug']))
		{
			$response=array('msg'=>'success','response'=>$res);
			echo json_encode($response);
		}
		else
		{
			$response=array('msg'=>'error','response'=>'');
			echo json_encode($response);
		}
		exit;
		
	}
	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
  	function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
		
	}

	public function index()
	{		
          $this->load->view('/admin/login', $this->template);
	}  
	public function notifications()
	{
		$return_array = array();
		$return_array['count'] = 0;
		$return_array['notification'] ='';
		$this->load->model('notification_model');
		$notify=$this->notification_model->notifications();
		if(!empty($notify))
		{
			$return_array['count'] = count($notify);
			$now  = strtotime(date('Y-m-d h:i:s A'));
			
			foreach($notify as $noti)
			{
				
				$difference = $now -  strtotime($noti['updated']);
				if($difference < 3570) $output = round($difference / 60).' minutes ago ';
				elseif ($difference < 86370) $output = round($difference / 3600).' hours ago';
				elseif ($difference < 604770) $output = round($difference / 86400).' days ago';
				elseif ($difference < 31535970) $output = round($difference / 604770).' week ago';
				else $output = round($difference / 31536000).' years ago';
				if($noti['type'] == 'cancel_request') $url = '<a href="'. site_url('/admin/task/cancel_detail/'.$noti['id']).'">';
				else if($noti['type'] == 'complete_request') $url = '<a href="'. site_url('/admin/task/detail/'.$noti['id']).'/1">';
				else if($noti['type'] == 'payment') $url = '<a href="'. site_url('/admin/payment/detail/'.$noti['id']).'">';
				$return_array['notification'] .= '<li>'.$url.'<span><span>'.$noti['title'].'</span><span class="time">'.$output.'</span></span><span class="message">'.$noti['message'].'</span></a></li>';
			}
		
		}
		
		echo json_encode($return_array);
		exit;
			
	
	
	}
	public function login_action()
	{

		if(isset($this->session->userdata['user']))
		{
			unset($this->session->userdata['user']);
			//$this->session->unset_userdata('user');
        } 		
        
		$post=$this->input->post();
		if(isset($post['submitted']))
		{
			
			$email	= $post['email'];
			$password	= $post['password'];
			$login		= $this->auth->login_admin($email, $password);
			
			if ($login)
			{	
			
			$param_service_type="home_maintenance";
			$param_service_type1="home_renovation";
		    $param_yearly="Y";
		    $param_monthly="M";
		     $this->load->model('customer_model');
		     $this->load->model('task_model');

			$this->data=array('customers'=>$this->customer_model->get_totalUsers('user'),'handymen'=>$this->customer_model->get_totalUsers('handyman'),'maintenance_submitted_year'=>$this->task_model->getTotalSubmitted('home_maintenance',$param_yearly),'renovation_submitted_year'=>$this->task_model->getTotalSubmitted('home_renovation',$param_yearly),'maintenance_amount_year'=>$this->task_model->getTotalTaskAmount('home_maintenance',$param_yearly),'renovation_amount_year'=>$this->task_model->getTotalTaskAmount('home_renovation',$param_yearly));
			
					  $this->middle = 'admin/dashboard';  
                      $this->layoutAdmin(); 
			}
			else
			{
				
				$this->session->set_flashdata('message', 'Invalid email or password');
		         redirect('/admin/user/index');
			}
		}
		else
		{
		         redirect('/admin/user/index');
		}
	}	 
	
	
    public function do_login()
	{
		if(isset($this->session->userdata['admin']))
		{
			$this->middle = 'admin/dashboard';  
             $this->layoutAdmin(); 
        } 	
        else{
			  redirect('/admin/user/index');
		}
        			
	}
	
	
	
	public function dashboard()
	{
 
		if(isset($this->session->userdata['admin']))
		{

			$param_service_type="home_maintenance";
			$param_service_type1="home_renovation";
		    $param_yearly="Y";
		    $param_monthly="M";
		     $this->load->model('customer_model');
		     $this->load->model('task_model');

			$this->data=array('customers'=>$this->customer_model->get_totalUsers('user'),'handymen'=>$this->customer_model->get_totalUsers('handyman'),'maintenance_submitted_year'=>$this->task_model->getTotalSubmitted('home_maintenance',$param_yearly),'renovation_submitted_year'=>$this->task_model->getTotalSubmitted('home_renovation',$param_yearly),'maintenance_amount_year'=>$this->task_model->getTotalTaskAmount('home_maintenance',$param_yearly),'renovation_amount_year'=>$this->task_model->getTotalTaskAmount('home_renovation',$param_yearly));
			
			$this->middle = 'admin/dashboard';  
            $this->layoutAdmin(); 
        } 	
        else{
			
			
			  redirect('/admin/user/index');
		}
        			
	
     
	}
    
    public function logout()
	{		
		$this->auth->logout();		
		$this->session->set_flashdata('message','You have logged out successfully');
		redirect('/admin/user/index');
	}
	
	public function add_customer()
	{
		$this->auth->is_admin_logged_in();
		$this->middle = 'admin/users/add_customer';  
        $this->layoutAdmin(); 
	}
	
	public function add_customer_action()
	{
	     $this->auth->is_admin_logged_in();	
         $this->form_validation->set_rules('first_name', 'Firstname', 'trim|required'); 
         $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required'); 
         $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_validate_member'); 
         $this->form_validation->set_rules('password', 'Password', 'trim|required'); 
			
         if ($this->form_validation->run() == FALSE) { 

         		$this->middle = 'admin/users/add_customer';  
                $this->layoutAdmin(); 
         } 
         else
         { 
			$this->load->model('customer_model');
			$data=$this->input->post();
			$data['user_type']=1;
			$data['password']=md5($data['password']);
			
			$data['query'] = $this->customer_model->save_customer($data);
			if($data['query'] != NULL)
			{
				//success
				$this->session->set_flashdata('success','Customer has been added successfully.');
		        redirect('/admin/user/add_customer');
			
			}
			else
			{
                //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again!');
		        redirect('/admin/user/add_customer');
			}
         } 
	}
	
	public function manage_customers()
	{
	    $this->auth->is_admin_logged_in();
		$this->load->model('customer_model');
		
		
		$this->data=array('customers'=>$this->customer_model->get_customers(1),'total_users'=>$this->customer_model->get_totalUsers('user'),'active_users'=>$this->customer_model->get_totalActiveUsers('user'),'online_users'=>$this->customer_model->get_onlineUsers('user'),'new_users'=>$this->customer_model->get_newUsers('user'),'inactive_users'=>$this->customer_model->get_totalInactiveUsers('user'));
		
		$this->middle = 'admin/users/manage_customers';  
        $this->layoutAdmin(); 
	}
	
	public function filter_customers($filter_type)
	{
	    $this->auth->is_admin_logged_in();
		$this->load->model('customer_model');
		
		
		$this->data=array('customers'=>$this->customer_model->get_FilteredCustomers($filter_type,'user'),'total_users'=>$this->customer_model->get_totalUsers('user'),'active_users'=>$this->customer_model->get_totalActiveUsers('user'),'online_users'=>$this->customer_model->get_onlineUsers('user'),'new_users'=>$this->customer_model->get_newUsers('user'),'inactive_users'=>$this->customer_model->get_totalInactiveUsers('user'));
		
		$this->middle = 'admin/users/manage_customers';  
        $this->layoutAdmin(); 
	}
	
	public function delete_customer()
	{
		$this->auth->is_admin_logged_in();
		$post=$this->input->post();
		$this->load->model('customer_model');
		if($this->customer_model->delete_customer($post['id'],$post['user_type']))
		{
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
	}
	
	public function edit_customer($id)
	{
		$this->auth->is_admin_logged_in();
		$this->load->model('customer_model');
		if($details=$this->customer_model->get_customer_detail($id))
		{
			     $this->data=array('details'=>$details);
	             $this->middle = 'admin/users/edit_customer';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/user/manage_customers');
		}
		
	}
	
	public function edit_customer_action()
	{
	
		$this->auth->is_admin_logged_in();
		$post=$this->input->post();
		$this->load->model('customer_model');
		
		$post['other_table'] = 'handyman';
		if($this->customer_model->update_customer($post))
		{
                $this->session->set_flashdata('success','Record has been updated successfully.');
		        redirect('/admin/user/manage_customers');
		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/user/manage_customers');
		}
		exit;
	}
	
	public function change_status_customer()
	{
		$this->auth->is_admin_logged_in();
		$post=$this->input->post();
		$this->load->model('customer_model');
		if($this->customer_model->change_status_customer($post['id'],$post['status'],$post['user_type']))
		{
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
	}
	
	
	public function add_handyman()
	{
		$this->auth->is_admin_logged_in();
		$this->middle = 'admin/users/add_handyman';  
        $this->layoutAdmin(); 
	}
	
	public function add_handyman_action()
	{
		 $this->auth->is_admin_logged_in();		
         $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
         $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_validate_member'); 
         $this->form_validation->set_rules('password', 'Password', 'trim|required'); 
         $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric'); 
         $this->form_validation->set_rules('company', 'Company', 'trim|required'); 
         $this->form_validation->set_rules('address', 'Address', 'trim|required'); 
			
         if ($this->form_validation->run() == FALSE) { 

         		$this->middle = 'admin/users/add_handyman';  
                $this->layoutAdmin(); 
         } 
         else
         { 
			$this->load->model('customer_model');
			$data=$this->input->post();
			$data['password']=md5($data['password']);
			
			$data['query'] = $this->customer_model->save_handyman($data,array());
			if($data['query'] != NULL)
			{
				//success
				$this->session->set_flashdata('success','Handyman has been added successfully.');
		        redirect('/admin/user/add_handyman');
			
			}
			else
			{
                //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again!');
		        redirect('/admin/user/add_handyman');
			}
         } 
	}
	
	public function manage_handymen()
	{
		$this->auth->is_admin_logged_in();
		$this->load->model('customer_model');
									
				$handyman_working=$this->customer_model->get_handyman_workings();
				$arr_handyman_working_hrs=array();
				$arr_handyman_working_amt=array();
							
				if(!empty($handyman_working))
				{
					foreach($handyman_working as $var)
					{
					 $arr_handyman_working_hrs[$var['handyman_id']]=$var['working_hrs'];
					 $arr_handyman_working_amt[$var['handyman_id']]=$var['working_amt'];
					}
				}	
				
		$this->data=array('customers'=>$this->customer_model->get_handyman(),'total_users'=>$this->customer_model->get_totalUsers('handyman'),'active_users'=>$this->customer_model->get_totalActiveUsers('handyman'),'online_users'=>$this->customer_model->get_onlineUsers('handyman'),'new_users'=>$this->customer_model->get_newUsers('handyman'),'inactive_users'=>$this->customer_model->get_totalInactiveUsers('handyman'),'arr_handyman_working_hrs'=>$arr_handyman_working_hrs,'arr_handyman_working_amt'=>$arr_handyman_working_amt);
				
		//$this->data=array('customers'=>$this->customer_model->get_handyman());
		$this->middle = 'admin/users/manage_handymen';  
        $this->layoutAdmin(); 
	}
    
    public function filter_handyman($filter_type)
	{
	    $this->auth->is_admin_logged_in();
		$this->load->model('customer_model');
		
		
		$this->data=array('customers'=>$this->customer_model->get_FilteredCustomers($filter_type,'handyman'),'total_users'=>$this->customer_model->get_totalUsers('handyman'),'active_users'=>$this->customer_model->get_totalActiveUsers('handyman'),'online_users'=>$this->customer_model->get_onlineUsers('handyman'),'new_users'=>$this->customer_model->get_newUsers('handyman'),'inactive_users'=>$this->customer_model->get_totalInactiveUsers('handyman'));
		
		$this->middle = 'admin/users/manage_handymen';  
        $this->layoutAdmin(); 
	}
	
	
	
	public function delete_handyman()
	{
		$this->auth->is_admin_logged_in();
		$post=$this->input->post();
		$this->load->model('customer_model');
		if($this->customer_model->delete_handyman($post['id']))
		{
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
	}
	
	public function edit_handyman($id)
	{
		$this->auth->is_admin_logged_in();		
		$this->load->model('customer_model');
		if($details=$this->customer_model->get_handyman_detail($id))
		{
			     $this->data=array('details'=>$details);
	             $this->middle = 'admin/users/edit_handyman';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/user/manage_handymen');
		}
		
	}
	
		public function block_user($id)
	{
		$this->auth->is_admin_logged_in();		
		$this->load->model('customer_model');
		if($details=$this->customer_model->get_customer_detail($id))
		{
			
			     $this->data=array('details'=>$details);
	             $this->middle = 'admin/users/block_user';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/user/manage_customers');
		}
		
	}
	
	public function block_user_action()
	{
		$this->auth->is_admin_logged_in();
		
		$post=$this->input->post();
		$this->load->model('customer_model');
		if($this->customer_model->block_user($post))
		{
                $this->session->set_flashdata('success','Record has been updated successfully.');
		        redirect('/admin/user/manage_customers');
		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/user/manage_customers');
		}
		exit;
	}
	
	
	
	public function block_handyman($id)
	{
		$this->auth->is_admin_logged_in();		
		$this->load->model('customer_model');
		if($details=$this->customer_model->get_handyman_detail($id))
		{
			     $this->data=array('details'=>$details);
	             $this->middle = 'admin/users/block_handyman';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/user/manage_handymen');
		}
		
	}
	
	public function block_handyman_action()
	{
		$this->auth->is_admin_logged_in();
		
		$post=$this->input->post();
		$this->load->model('customer_model');
		if($this->customer_model->block_handyman($post))
		{
                $this->session->set_flashdata('success','Record has been updated successfully.');
		        redirect('/admin/user/manage_handymen');
		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/user/manage_handymen');
		}
		exit;
	}
	public function edit_handyman_action()
	{
		$this->auth->is_admin_logged_in();
		
		$post=$this->input->post();
		$this->load->model('customer_model');
		$post['other_table'] = 'users';
		if($this->customer_model->update_handyman($post))
		{
                $this->session->set_flashdata('success','Record has been updated successfully.');
		        redirect('/admin/user/manage_handymen');
		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/user/manage_handymen');
		}
		exit;
	}
	
	public function change_status_handyman()
	{
		$this->auth->is_admin_logged_in();
		
		$post=$this->input->post();
		$this->load->model('customer_model');
		if($this->customer_model->change_status_handyman($post['id'],$post['status'],$post['user_type']))
		{
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
	}
	
	function validate_member($str)
	{
		$this->auth->is_admin_logged_in();
		
	   $this->load->model('customer_model');
	   if($this->customer_model->validate_member($str))
	   {
		 return TRUE;
	   }
	   else
	   {
		   $this->form_validation->set_message('validate_member','Given email is already registered !');
		   return false;
	   }
	}
	
function unblock_handyman()
{
		$this->auth->is_admin_logged_in();
		$post=$this->input->post();
		$this->load->model('customer_model');
		if($this->customer_model->unblock_handyman($post['id']))
		{
			 $this->session->set_flashdata('success','Handyman has been unblocked successfully.');
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
}
function unblock_user()
{
		$this->auth->is_admin_logged_in();
		$post=$this->input->post();
		$this->load->model('customer_model');
		if($this->customer_model->unblock_user($post['id']))
		{
			 $this->session->set_flashdata('success','User has been unblocked successfully.');
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
}



	function change_password()
	{	

			
	             $this->middle = 'admin/users/change_password';  
                 $this->layoutAdmin();
	    
		
	}
	
	
    public function change_password_action()
	{
			
         $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|min_length[5]|max_length[20]'); 
         $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[5]|max_length[20]'); 
         $this->form_validation->set_rules('retype_password', 'Retype Password', 'trim|required|min_length[5]|max_length[20]'); 
			
         if ($this->form_validation->run() == FALSE) 
         { 
	             $this->middle = 'admin/users/change_password';  
                 $this->layoutAdmin();
         } 
         else
         { 
			$this->load->model('customer_model');
			$data=$this->input->post();
			if($data['new_password']!=$data['retype_password'])
			{
				$this->session->set_flashdata('error','New password and retype password should be same !');
		        redirect('admin/user/change_password');
			}
			$update['password']=md5($data['new_password']);
			$update['old_password']=md5($data['old_password']);
			$update['id']=$data['id'];
			
			
			if($this->customer_model->change_password_admin($update))
			{
				
				//success
				$this->session->set_flashdata('success','Your password has been changed successfully.');
		        redirect('admin/user/change_password');
			
			}
			else
			{
                //throw error
                $this->session->set_flashdata('error','Please enter valid old password!');
		        redirect('admin/user/change_password');
			}
         } 
         
	}
	
	
}

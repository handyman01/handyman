<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
  	function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('download');
        $this->load->library('PHPReport');
    }
	
	public function manage($download=false)
	{
				
	    $this->auth->is_admin_logged_in();
		$this->load->model('payment_model');
		$param_yearly="Y";
		$param_monthly="M";
		if(!$download)
		{
			$this->data=array('list'=>$this->payment_model->getList(),'pending_year'=>$this->payment_model->getTotal('pending',$param_yearly),'pending_month'=>$this->payment_model->getTotal('pending',$param_monthly),'completed_year'=>$this->payment_model->getTotal('complete',$param_yearly),'completed_month'=>$this->payment_model->getTotal('complete',$param_monthly));
			$this->middle = 'admin/payment/manage';  
            $this->layoutAdmin(); 
		}
		else
		{
		    $data = $this->payment_model->getList();
			 $final_data=array();
			 $count=0;
			 foreach($data as $val)
			 {
				 $final_data[$count]['task_id']=$val['task_id'];
				 $final_data[$count]['user_id']=$val['user_id'];
				 $final_data[$count]['name']=$val['first_name'].' '.$val['last_name'];
				 $final_data[$count]['email']=$val['payer_email'];
				 $final_data[$count]['txn_id']=$val['txn_id'];
				 $final_data[$count]['currency_code']=$val['currency_code'];
				 $final_data[$count]['payment_gross']='$ '.$val['payment_gross'];
				 $final_data[$count]['payment_status']=$val['payment_status'];
				 $final_data[$count]['created']=$val['updated'];
	            
				 $count++;
			 }
			 		
			 $template ='payment.xlsx';
			 $templateDir='assets/';
			 
			 $config = array(
			'template' => $template,
			'templateDir' => $templateDir
			 );

			  //load template
			  $R = new PHPReport($config);

			  $R->load(array(
					  'id' => 'payment',
					  'repeat' => TRUE,
					  'data' => $final_data   	
				  )
			  );
			  $output_file_dir = "assets/";			 
			  $output_file_excel = $output_file_dir  . "Payment.xlsx";
			  $result = $R->render('excel', $output_file_excel);
			  header("Location: ".site_url('/assets/Payment.xlsx'));
		 }
	}
	
	
	public function detail($id)
	{
		$this->auth->is_admin_logged_in();
		$this->load->model('payment_model');
		$this->load->model('notification_model');
		if($details=$this->payment_model->getDetail($id))
		{
			 $this->notification_model->update_notify($id,'payment_id','payments');
			 $this->data=array('details'=>$details);
			 $this->middle = 'admin/payment/detail';  
             $this->layoutAdmin();
   		}
		else
		{
			//throw error
			$this->session->set_flashdata('error','Something went wrong, please try again later!');
			redirect('/admin/payment/manage');
		}
		
	}
	
	
}

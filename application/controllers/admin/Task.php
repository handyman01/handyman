<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
  	function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('download');
        $this->load->library('PHPReport');
         $this->load->library('paypal_lib');
        
		
	}

 	public function recover_task(){
		$post=$this->input->post();
		$this->load->model('task_model');
		$taskdetail=$this->task_model->getTasks($service_type=false,$post['task_id']);
		if($taskdetail[0]['status'] == 'X'){
			$this->task_model->recover_task($taskdetail);
			return true;
		}else return false;
		exit;
	}
	
	public function cancel(){
	
	
	}
	

	public function add()
	{
		//$this->auth->is_admin_logged_in();
	//	$this->middle = 'admin/task/add';  
       // $this->layoutAdmin(); 
	}
	
	public function add_action()
	{
	     $this->auth->is_admin_logged_in();	
         $this->form_validation->set_rules('first_name', 'Firstname', 'trim|required'); 
         $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required'); 
         $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_validate_member'); 
         $this->form_validation->set_rules('password', 'Password', 'trim|required'); 
			
         if ($this->form_validation->run() == FALSE) { 

         		$this->middle = 'admin/users/add_customer';  
                $this->layoutAdmin(); 
         } 
         else
         { 
			$this->load->model('customer_model');
			$data=$this->input->post();
			$data['user_type']=1;
			$data['password']=md5($data['password']);
			
			$data['query'] = $this->customer_model->save_customer($data);
			if($data['query'] != NULL)
			{
				//success
				$this->session->set_flashdata('success','Customer has been added successfully.');
		        redirect('/admin/user/add_customer');
			
			}
			else
			{
                //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again!');
		        redirect('/admin/user/add_customer');
			}
         } 
	}
	public function response($id){
		 $this->auth->is_admin_logged_in();
		 $this->load->model('task_model');
		 $data['task_id']=$id;
		 $res=$this->task_model->getTasksResponses($data);
		 $this->data=array('handy'=>$res,'task_id'=>$id);
		 $this->middle = 'admin/tasks/response';  
         $this->layoutAdmin(); 
	}
	public function response_renovation($id){
		 $this->auth->is_admin_logged_in();
		 $this->load->model('task_model');
		 $data['task_id']=$id;
		 $data['is_admin']=1;
		 $res=$this->task_model->getTasksResponses($data);
		 
		 $this->data=array('handy'=>$res,'task_id'=>$id);
		 $this->middle = 'admin/tasks/response_renovation';  
         $this->layoutAdmin(); 
	}
	public function responseDetail($handyid,$task_id){
		  $this->load->model('customer_model');
		  $this->send_data['details']=$this->customer_model->get_handyman_detail($handyid,$task_id);			
		  $this->send_data['task_id']=$task_id;								
		  $this->data=$this->send_data;
		  $this->middle = 'admin/tasks/responseDetail';  
		  $this->layoutAdmin(); 
	}
	public function response_renovation_Detail($handyid,$task_id){
		  $this->load->model('customer_model');
		  $this->send_data['details']=$this->customer_model->get_handyman_detail($handyid,$task_id);			
		  $this->send_data['task_id']=$task_id;								
		  $this->data=$this->send_data;
		  $this->middle = 'admin/tasks/response_renovation_Detail';  
		  $this->layoutAdmin(); 
	}
	
	
	public function manage($download=false)
	{
				
	    $this->auth->is_admin_logged_in();
		$this->load->model('task_model');
		$param_service_type="home_maintenance";
		$param_yearly="Y";
		$param_monthly="M";
		if(!$download)
		{
			$this->data=array('tasks'=>$this->task_model->getTasks($param_service_type),'submitted_year'=>$this->task_model->getTotalSubmitted($param_service_type,$param_yearly),'completed_year'=>$this->task_model->getTotalCompleted($param_service_type,$param_yearly),'active_year'=>$this->task_model->getTotalActive($param_service_type,$param_yearly),'amount_year'=>$this->task_model->getTotalTaskAmount($param_service_type,$param_yearly),'submitted_month'=>$this->task_model->getTotalSubmitted($param_service_type,$param_monthly),'completed_month'=>$this->task_model->getTotalCompleted($param_service_type,$param_monthly),'active_month'=>$this->task_model->getTotalActive($param_service_type,$param_monthly),'amount_month'=>$this->task_model->getTotalTaskAmount($param_service_type,$param_monthly));
			$this->middle = 'admin/tasks/manage';  
            $this->layoutAdmin(); 
		}
		else
		{
		     $arr_status=$this->config->item('task_status');
			 $data = $this->task_model->getTasks($param_service_type);
			 $final_data=array();
			 $count=0;
			 foreach($data as $val)
			 {
				 $final_data[$count]['id']=$val['id'];
				 $final_data[$count]['user_id']=$val['user_id'];
				 $final_data[$count]['type']=$val['is_residential']==1 ? 'Residential':'Commercial';
				 $final_data[$count]['address']=$val['address'];
				 $final_data[$count]['description']=$val['description'];
				 $final_data[$count]['task_date']=$val['task_date'];
				 $final_data[$count]['task_time']=$val['task_time'];
				 $final_data[$count]['estimated_time']=$val['estimated_time']." Hour(s)";
				 $final_data[$count]['total_price']='$ '.$val['total_price'];
				 $final_data[$count]['total_deposit_amt']='$ '.$val['total_deposit_amt'];
				 $final_data[$count]['customer_name']=$val['first_name']." ".$val['last_name'];
	             $final_data[$count]['lat']=$val['lat'];
	             $final_data[$count]['lon']=$val['lon'];
	             $final_data[$count]['status']=$arr_status[$val['status']];
	             $final_data[$count]['payment_status']=($val['payment_status']==0?"Done":"Not Done");
	             $final_data[$count]['created']=$val['created'];
	             $final_data[$count]['handyman_name']=($val['name']=='' ? 'Not Assigned Yet':$val['name']);
	             $final_data[$count]['handyman_id']=$val['handyman_id'];
	             $final_data[$count]['category_name']=$val['category_name'];
	             $final_data[$count]['task_type']=ucfirst($val['task_type']);
	             $final_data[$count]['cust_wrk_hrs']=$val['cust_wrk_hrs'];
	             $final_data[$count]['cust_wrk_amt']="$".$val['cust_wrk_amt'];
	             $final_data[$count]['handy_wrk_hrs']=$val['handy_wrk_hrs'];
	             $final_data[$count]['handy_wrk_amt']="$".$val['handy_wrk_amt'];
	             $final_data[$count]['store_credit_percent']=$val['store_credit_percent'];
	             $final_data[$count]['store_credit_amount']="$".$val['store_credit_amount'];
	             $final_data[$count]['store_credit_status']=($val['store_credit_status']==0?"Done":"Not Done");
	             if($val['cust_wrk_amt']!=0 && $val['handy_wrk_amt']!=0 && ($val['handy_wrk_amt'] != $val['cust_wrk_amt'] ))
				 {
					if($val['cust_wrk_amt']>$val['handy_wrk_amt']) $final_data[$count]['difference_amt']= $val['cust_wrk_amt']-$val['handy_wrk_amt'];
					else $final_data[$count]['difference_amt']="$".$val['handy_wrk_amt']-$val['cust_wrk_amt'];

				 }
				 else $final_data[$count]['difference_amt']= "$0";
				 
				 if($val['handy_wrk_hrs']!=0 && $val['cust_wrk_hrs']!=0 && ($val['handy_wrk_hrs'] != $val['cust_wrk_hrs'] ))
				 {
					if($val['handy_wrk_hrs']>$val['cust_wrk_hrs']) $final_data[$count]['difference_hours']= $val['handy_wrk_hrs']-$val['cust_wrk_hrs'];
					else $final_data[$count]['difference_hours']=$val['cust_wrk_hrs']-$val['handy_wrk_hrs'];

				 }
				 else $final_data[$count]['difference_hours']= 0;
	             

				 $count++;
			 }
			 		
			 $template ='Task.xlsx';
			 $templateDir='assets/';
			 
			 $config = array(
			'template' => $template,
			'templateDir' => $templateDir
			 );

			  //load template
			  $R = new PHPReport($config);

			  $R->load(array(
					  'id' => 'handy',
					  'repeat' => TRUE,
					  'data' => $final_data   	
				  )
			  );
			  
			  $output_file_dir = "assets/";			 
			  $output_file_excel = $output_file_dir  . "Task_detail.xlsx";
			  $result = $R->render('excel', $output_file_excel);
			  header("Location: ".site_url('/assets/Task_detail.xlsx'));
			  
			
		}
	}
	
	public function manage_filter($type=null,$flag=null,$download=false)
	{
		$post=$this->input->post();
		$this->auth->is_admin_logged_in();
		$this->load->model('task_model');
		$param_service_type="home_maintenance";
		$param_yearly="Y";
		$param_monthly="M";
		
		if(!$download)
		{
			$this->data=array('post_data'=>$post,'tasks'=>$this->task_model->getFilteredTasks($param_service_type,$type,$flag,$post),'submitted_year'=>$this->task_model->getTotalSubmitted($param_service_type,$param_yearly),'completed_year'=>$this->task_model->getTotalCompleted($param_service_type,$param_yearly),'active_year'=>$this->task_model->getTotalActive($param_service_type,$param_yearly),'amount_year'=>$this->task_model->getTotalTaskAmount($param_service_type,$param_yearly),'submitted_month'=>$this->task_model->getTotalSubmitted($param_service_type,$param_monthly),'completed_month'=>$this->task_model->getTotalCompleted($param_service_type,$param_monthly),'active_month'=>$this->task_model->getTotalActive($param_service_type,$param_monthly),'amount_month'=>$this->task_model->getTotalTaskAmount($param_service_type,$param_monthly));
					
		  $this->middle = 'admin/tasks/manage_filter';  
          $this->layoutAdmin(); 
        }
        else
        {
		     $arr_status=array('S'=>'Submitted','P'=>'Processing','X'=>'Cancelled','C'=>'Completed');
			 $data = $this->task_model->getFilteredTasks($param_service_type,$type,$flag);
			 $final_data=array();
			 $count=0;
			 foreach($data as $val)
			 {
				 $final_data[$count]['id']=$val['id'];
				 $final_data[$count]['user_id']=$val['user_id'];
				 $final_data[$count]['type']=$val['is_residential']==1 ? 'Residential':'Commercial';
				 $final_data[$count]['address']=$val['address'];
				 $final_data[$count]['description']=$val['description'];
				 $final_data[$count]['task_date']=$val['task_date'];
				 $final_data[$count]['task_time']=$val['task_time'];
				 $final_data[$count]['estimated_time']=$val['estimated_time']." Hour(s)";
				 $final_data[$count]['total_price']='$ '.$val['total_price'];
				 $final_data[$count]['customer_name']=$val['first_name']." ".$val['last_name'];
	             $final_data[$count]['lat']=$val['lat'];
	             $final_data[$count]['lon']=$val['lon'];
	             $final_data[$count]['status']=$arr_status[$val['status']];
	             $final_data[$count]['payment_status']=$val['payment_status'];
	             $final_data[$count]['created']=$val['created'];
	             $final_data[$count]['handyman_name']=$val['name']=='' ? 'Not Assigned Yet':'';
	             $final_data[$count]['category_name']=$val['category_name'];

				 $count++;
			 }
			 
             //start loop for managing data again
			//https://www.studytutorial.in/how-to-export-data-from-database-to-excel-sheet-xls-using-codeigniter-php-tutorial
			
			 $template ='Task.xlsx';
			 $templateDir='assets/';
			 
			 $config = array(
			'template' => $template,
			'templateDir' => $templateDir
			 );

			  //load template
			  $R = new PHPReport($config);

			  $R->load(array(
					  'id' => 'handy',
					  'repeat' => TRUE,
					  'data' => $final_data   	
				  )
			  );
			  
			  // define output directoy 
			  $output_file_dir = "assets/";
			 

			  $output_file_excel = $output_file_dir  . "Task_detail.xlsx";
			  //download excel sheet with data in /tmp folder
			  $result = $R->render('excel', $output_file_excel);
			  header("Location: ".site_url('/assets/Task_detail.xlsx'));
		}
	}
	

	
	public function delete()
	{
		$this->auth->is_admin_logged_in();
		$post=$this->input->post();
		$this->load->model('customer_model');
		if($this->customer_model->delete_customer($post['id'],$post['user_type']))
		{
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
	}
	
	public function edit($id)
	{
		$this->auth->is_admin_logged_in();
		$this->load->model('task_model');
		$this->load->model('category_model');
		$this->load->model('customer_model');
		if($details=$this->task_model->getTasks($service_type=false,$id))
		{
			 
			  
			    $arr_status= $this->task_model->admin_status_option($details[0]['status']);
			    /*get all handyman */
			    
			    $handyman_list=$this->customer_model->get_handyman();
				$arr_handyman[0]='Not Assigned Yet';			
				if(!empty($handyman_list))
				{
					foreach($handyman_list as $var)
					{
					 $arr_handyman[$var['id']]=$var['name'];
					}
				}
				    
			    
				$cats=$this->category_model->get_categories(-1);	
				$arr_categories=array();			
				if(!empty($cats))
				{
					foreach($cats as $var)
					{
					 $arr_categories[$var['slug']]=$var['name'];
					}
				}
				
				
				$arr_subcategories=array();
				$subcats=$this->category_model->get_categories_bySlug($details[0]['categ_parent_slug']);		
		
						
				if(!empty($subcats))
				{
					foreach($subcats as $var)
					{
					
					 $arr_subcategories[$var['id']]=$var['name'];
					}
				}
				$this->data=array('details'=>$details,'parent_categories'=>$arr_categories,'sub_category'=>$arr_subcategories,'handyman_list'=>$arr_handyman,'status'=>$arr_status);
			     
	             $this->middle = 'admin/tasks/edit';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/task/manage');
		}
		
	}
	
	
	function handyman_task_estimation($id)
	{
		$this->auth->is_admin_logged_in();
		$this->load->model('task_model');
		if($details=$this->task_model->getTasks($service_type=false,$id))
		{
			 
			  
			
				$this->data=array('details'=>$details);
			     
	             $this->middle = 'admin/tasks/handyman_task_estimation';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/task/manage');
		}
		
	}
	
	function handyman_task_estm_action()
	{
		$this->auth->is_admin_logged_in();
		
		$post=$this->input->post();
		$this->load->model('task_model');
		if($this->task_model->handyman_task_estimation($post))
		{
                $this->session->set_flashdata('success','Record has been updated successfully.');
		        redirect('/admin/task/manage');
		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/task/manage');
		}
		exit;
	}
	
	function add_note()
	{
		$this->auth->is_admin_logged_in();
		
		$post=$this->input->post();
		$this->load->model('task_model');
		if($this->task_model->add_note($post))
		{
                $this->session->set_flashdata('success','Record has been updated successfully.');
		        redirect('/admin/task/manage');
		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/task/manage');
		}
		exit;
		
	}
	
	public function detail($id,$complete_notify=false)
	{
		$this->auth->is_admin_logged_in();
		
		
		$this->load->model('task_model');
		$this->load->model('notification_model');
		$this->load->model('category_model');
		$this->load->model('customer_model');
		if($details=$this->task_model->getTasks($service_type=false,$id))
		{
			 if($complete_notify)
			 {
			   //update complete notification when coming through nitifications
			   $this->notification_model->update_notify_complete($id);
		     }
		     
			   $this->notification_model->update_notify($id,'task_id','cancel_tasks');
			   
			    /*get all handyman */
			    
			    $handyman_list=$this->customer_model->get_handyman();
				//$arr_handyman[0]='Not Assigned Yet';			
				$arr_handyman=array();
				if(!empty($handyman_list))
				{
					foreach($handyman_list as $var)
					{
					 $arr_handyman[$var['id']]=$var['name'];
					}
				}
				    
			    
				$cats=$this->category_model->get_categories(-1);	
				$arr_categories=array();			
				if(!empty($cats))
				{
					foreach($cats as $var)
					{
					 $arr_categories[$var['slug']]=$var['name'];
					}
				}
				
				
				$arr_subcategories=array();
				$subcats=$this->category_model->get_categories_bySlug($details[0]['categ_parent_slug']);		
		
						
				if(!empty($subcats))
				{
					foreach($subcats as $var)
					{
					
					 $arr_subcategories[$var['id']]=$var['name'];
					}
				}

				
		
			     $this->data=array('details'=>$details,'parent_categories'=>$arr_categories,'sub_category'=>$arr_subcategories,'handyman_list'=>$arr_handyman,'status'=>$this->config->item('task_status'));
			     
	             $this->middle = 'admin/tasks/detail';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/task/manage');
		}
		
	}
	public function cancel_detail($id)
	{
		$this->auth->is_admin_logged_in();
		
		
		$this->load->model('task_model');
		
		if($details=$this->task_model->cancel_task_message($id))
		{			 			
			     $this->data=array('details'=>$details);
			     
	             $this->middle = 'admin/tasks/cancel_detail';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/task/manage');
		}
		
	}
	
	public function detail_renovation($id)
	{
		$this->auth->is_admin_logged_in();
		$this->load->model('task_model');
		$this->load->model('category_model');
		$this->load->model('customer_model');
		if($details=$this->task_model->getTasks($service_type=false,$id))
		{
			 
			   $arr_status=array('S'=>'Submitted','P'=>'Processing','X'=>'Cancelled','C'=>'Completed');

			    /* get all handyman */
			    
			    $handyman_list=$this->customer_model->get_handyman();
				$arr_handyman[0]='Not Assigned Yet';			
				if(!empty($handyman_list))
				{
					foreach($handyman_list as $var)
					{
					 $arr_handyman[$var['id']]=$var['name'];
					}
				}
				    
			    
				$cats=$this->category_model->get_categories(-1);	
				$arr_categories=array();			
				if(!empty($cats))
				{
					foreach($cats as $var)
					{
					 $arr_categories[$var['slug']]=$var['name'];
					}
				}
				
				
				$arr_subcategories=array();
				$subcats=$this->category_model->get_categories_bySlug($details[0]['categ_parent_slug']);		
		
						
				if(!empty($subcats))
				{
					foreach($subcats as $var)
					{
					
					 $arr_subcategories[$var['id']]=$var['name'];
					}
				}

				
		
			     $this->data=array('details'=>$details,'parent_categories'=>$arr_categories,'sub_category'=>$arr_subcategories,'handyman_list'=>$arr_handyman,'status'=>$arr_status);
			     
	             $this->middle = 'admin/tasks/detail';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/task/manage');
		}
		
	}

	
	public function edit_action()
	{
		$this->auth->is_admin_logged_in();
		$post=$this->input->post();
		
		$this->load->model('task_model');
		$taskdetail=$this->task_model->getTasks($service_type=false,$post['id']);
		if(isset($post['handyman_id']) && $post['handyman_id'] != 0 && $taskdetail[0]['handyman_id']== 0)
		{
			// Assign task to handyman
			$post['status']='P';
			$this->load->model('customer_model');
			$data = $this->customer_model->admin_hire_handyman($post['handyman_id'],$post['id']);
			$post['final_price'] = $data['final_price'];
			$post['accepted_offer'] = $data['accepted_offer'];
			// send mail
			$this->load->model('message_model');
			$msg=$this->message_model->get_template('hire_handyman');
			$this->load->model('customer_model');
			$msg['to_email']=$this->customer_model->email_from_id($post['handyman_id'],'handyman');
			
			$msg['content'] = str_replace('{customer_name}', $taskdetail[0]['first_name'].' '. $taskdetail[0]['last_name'], $msg['content']);
			$msg['content'] = str_replace('{address}', $taskdetail[0]['address_unit'].', '.$taskdetail[0]['address'], $msg['content']);
			$msg['content'] = str_replace('{category_name}', $taskdetail[0]['category_name'], $msg['content']);
			$msg['content'] = str_replace('{total_amount}', $taskdetail[0]['total_price'], $msg['content']);			
			$msg['content'] = str_replace('{task_name}', $taskdetail[0]['title'], $msg['content']);
			$msg['content'] = str_replace('{task_timing}', $taskdetail[0]['task_date']." ".$taskdetail[0]['task_time'], $msg['content']);
			$msg['content'] = str_replace('{task_hours}', $taskdetail[0]['estimated_time'], $msg['content']);
			$msg['content'] = str_replace('{task_description}', $taskdetail[0]['description'], $msg['content']);	
			sendMail($msg);
			
		}
		else if(isset($post['handyman_id']) && $post['handyman_id'] == 0 && $taskdetail[0]['handyman_id']== 0 && ($post['status']=='P' || $post['status']=='C'))
		{
			   $this->session->set_flashdata('error','Please choose handyman to make task completed or in processing state!');
		       redirect('/admin/task/edit/'.$post['id']);
			
		}
		else if($post['status'] == 'X'){
			
			
			
			// update notification
			$this->load->model('notification_model');
			$this->notification_model->update_notify($post['id'],'task_id','cancel_tasks');
			
			
			// cancel task
			$post['user_id_cancelled'] = $taskdetail[0]['user_id'];
			// send mail
			$this->load->model('message_model');
			$msg=$this->message_model->get_template('task_cancelled');
			$this->load->model('customer_model');
			$msg['to_email']=$this->customer_model->email_from_id($taskdetail[0]['user_id'],'users');
			$msg['content'] = str_replace('{task_name}', $taskdetail[0]['title'], $msg['content']);
			$msg['content'] = str_replace('{task_timing}', $taskdetail[0]['task_date']." ".$taskdetail[0]['task_time'], $msg['content']);
			$msg['content'] = str_replace('{task_hours}', $taskdetail[0]['estimated_time'], $msg['content']);
			$msg['content'] = str_replace('{task_description}', $taskdetail[0]['description'], $msg['content']);	
			sendMail($msg);
			if($taskdetail[0]['handyman_id'] != 0){
				$msg['to_email']=$this->customer_model->email_from_id($taskdetail[0]['handyman_id'],'handyman');
				sendMail($msg);
			}
		}else if($post['status'] == 'C'){
			// completed task
			$post['user_id_completed'] = $taskdetail[0]['user_id'];
			$post['handyman_id_completed'] = $taskdetail[0]['handyman_id'];
			// send mail
			$this->load->model('message_model');
			$msg=$this->message_model->get_template('task_completed');
			$this->load->model('customer_model');
			$msg['to_email']=$this->customer_model->email_from_id($taskdetail[0]['user_id'],'users');
			$msg['content'] = str_replace('{task_name}', $taskdetail[0]['title'], $msg['content']);
			$msg['content'] = str_replace('{task_timing}', $taskdetail[0]['task_date']." ".$taskdetail[0]['task_time'], $msg['content']);
			$msg['content'] = str_replace('{task_hours}', $taskdetail[0]['estimated_time'], $msg['content']);
			$msg['content'] = str_replace('{task_description}', $taskdetail[0]['description'], $msg['content']);	
			sendMail($msg);
			$msg['to_email']=$this->customer_model->email_from_id($taskdetail[0]['handyman_id'],'handyman');
			sendMail($msg);
		}
		$this->load->model('task_model');
		if($this->task_model->update_task($post))
		{
				
                $this->session->set_flashdata('success','Record has been updated successfully.');
		        redirect('/admin/task/manage');	
		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/task/manage');
		}
		exit;
	}
	
	public function manage_renovation($download=false)
	{
				
	    $this->auth->is_admin_logged_in();
		$this->load->model('task_model');
		$param_service_type="home_renovation";
		$param_yearly="Y";
		$param_monthly="M";
		if(!$download)
		{
			
			$this->data=array('tasks'=>$this->task_model->getTasks($param_service_type),'submitted_year'=>$this->task_model->getTotalSubmitted($param_service_type,$param_yearly),'completed_year'=>$this->task_model->getTotalCompleted($param_service_type,$param_yearly),'active_year'=>$this->task_model->getTotalActive($param_service_type,$param_yearly),'amount_year'=>$this->task_model->getTotalTaskAmount($param_service_type,$param_yearly),'submitted_month'=>$this->task_model->getTotalSubmitted($param_service_type,$param_monthly),'completed_month'=>$this->task_model->getTotalCompleted($param_service_type,$param_monthly),'active_month'=>$this->task_model->getTotalActive($param_service_type,$param_monthly),'amount_month'=>$this->task_model->getTotalTaskAmount($param_service_type,$param_monthly));
			
		    $this->middle = 'admin/tasks/manage_renovation';  
            $this->layoutAdmin(); 
		}
		else
		{
		     $arr_status=array('S'=>'Submitted','P'=>'Processing','X'=>'Cancelled','C'=>'Completed');
			 $data = $this->task_model->getTasks($param_service_type);
			 $final_data=array();
			 $count=0;
			 foreach($data as $val)
			 {
				 $final_data[$count]['id']=$val['id'];
				 $final_data[$count]['user_id']=$val['user_id'];
				 $final_data[$count]['type']=$val['is_residential']==1 ? 'Residential':'Commercial';
				 $final_data[$count]['address']=$val['address'];
				 $final_data[$count]['description']=$val['description'];
				 $final_data[$count]['task_date']=$val['task_date'];
				 $final_data[$count]['task_time']=$val['task_time'];
				 $final_data[$count]['estimated_time']=$val['estimated_time']." Hour(s)";
				 $final_data[$count]['total_price']='$ '.$val['total_price'];
				 $final_data[$count]['customer_name']=$val['first_name']." ".$val['last_name'];
	             $final_data[$count]['lat']=$val['lat'];
	             $final_data[$count]['lon']=$val['lon'];
	             $final_data[$count]['status']=$arr_status[$val['status']];
	             $final_data[$count]['payment_status']=$val['payment_status'];
	             $final_data[$count]['created']=$val['created'];
	             $final_data[$count]['handyman_name']=$val['name']=='' ? 'Not Assigned Yet':'';
	             $final_data[$count]['category_name']=$val['category_name'];

				 $count++;
			 }
			 		
			 $template ='Task.xlsx';
			 $templateDir='assets/';
			 
			 $config = array(
			'template' => $template,
			'templateDir' => $templateDir
			 );

			  //load template
			  $R = new PHPReport($config);

			  $R->load(array(
					  'id' => 'handy',
					  'repeat' => TRUE,
					  'data' => $final_data   	
				  )
			  );
			  
			  $output_file_dir = "assets/";			 
			  $output_file_excel = $output_file_dir  . "Task_detail.xlsx";
			  $result = $R->render('excel', $output_file_excel);
			  header("Location: ".site_url('/assets/Task_detail.xlsx'));
			  
			
		}
	}
	
    public function filter_renovation($type,$flag,$download=false)
	{
				
	    $this->auth->is_admin_logged_in();
		$this->load->model('task_model');
		$param_service_type="home_renovation";
		$param_yearly="Y";
		$param_monthly="M";
		
		if(!$download)
		{
			$this->data=array('tasks'=>$this->task_model->getFilteredTasks($param_service_type,$type,$flag),'submitted_year'=>$this->task_model->getTotalSubmitted($param_service_type,'Y'),'completed_year'=>$this->task_model->getTotalCompleted($param_service_type,$param_yearly),'active_year'=>$this->task_model->getTotalActive($param_service_type,$param_yearly),'amount_year'=>$this->task_model->getTotalTaskAmount($param_service_type,$param_yearly),'submitted_month'=>$this->task_model->getTotalSubmitted($param_service_type,$param_monthly),'completed_month'=>$this->task_model->getTotalCompleted($param_service_type,$param_monthly),'active_month'=>$this->task_model->getTotalActive($param_service_type,$param_monthly),'amount_month'=>$this->task_model->getTotalTaskAmount($param_service_type,$param_monthly));
					
		  $this->middle = 'admin/tasks/manage_renovation';  
          $this->layoutAdmin(); 
        }
        else
        {
		     $arr_status=array('S'=>'Submitted','P'=>'Processing','X'=>'Cancelled','C'=>'Completed');
			 $data = $this->task_model->getFilteredTasks($param_service_type,$type,$flag);
			 $final_data=array();
			 $count=0;
			 foreach($data as $val)
			 {
				 $final_data[$count]['id']=$val['id'];
				 $final_data[$count]['user_id']=$val['user_id'];
				 $final_data[$count]['type']=$val['is_residential']==1 ? 'Residential':'Commercial';
				 $final_data[$count]['address']=$val['address'];
				 $final_data[$count]['description']=$val['description'];
				 $final_data[$count]['task_date']=$val['task_date'];
				 $final_data[$count]['task_time']=$val['task_time'];
				 $final_data[$count]['estimated_time']=$val['estimated_time']." Hour(s)";
				 $final_data[$count]['total_price']='$ '.$val['total_price'];
				 $final_data[$count]['customer_name']=$val['first_name']." ".$val['last_name'];
	             $final_data[$count]['lat']=$val['lat'];
	             $final_data[$count]['lon']=$val['lon'];
	             $final_data[$count]['status']=$arr_status[$val['status']];
	             $final_data[$count]['payment_status']=$val['payment_status'];
	             $final_data[$count]['created']=$val['created'];
	             $final_data[$count]['handyman_name']=$val['name']=='' ? 'Not Assigned Yet':'';
	             $final_data[$count]['category_name']=$val['category_name'];

				 $count++;
			 }
			 
             //start loop for managing data again
			//https://www.studytutorial.in/how-to-export-data-from-database-to-excel-sheet-xls-using-codeigniter-php-tutorial
			
			 $template ='Task.xlsx';
			 $templateDir='assets/';
			 
			 $config = array(
			'template' => $template,
			'templateDir' => $templateDir
			 );

			  //load template
			  $R = new PHPReport($config);

			  $R->load(array(
					  'id' => 'handy',
					  'repeat' => TRUE,
					  'data' => $final_data   	
				  )
			  );
			  
			  // define output directoy 
			  $output_file_dir = "assets/";
			 

			  $output_file_excel = $output_file_dir  . "Task_detail.xlsx";
			  //download excel sheet with data in /tmp folder
			  $result = $R->render('excel', $output_file_excel);
			  header("Location: ".site_url('/assets/Task_detail.xlsx'));
		}
	}
	
	function download_doc($name)
	{
		$this->load->helper('download');
	    $data = file_get_contents(base_url('/assets/task/'.$name));
        force_download($name, $data);

		exit;
	}
	
	
	function searchByDate($download=false)
	{
		$post=$this->input->post();
	    $this->auth->is_admin_logged_in();
		$this->load->model('task_model');
 
 
        if(!isset($post['curr_page']))
        {
			 redirect('/admin/task/manage');
		}
       else
       {
		   
	   
		/*
		if (strpos($post['curr_page'], 'manage_filter') !== false)
		{
					$param_service_type="home_maintenance";
		$param_yearly="Y";
		$param_monthly="M";
		
			
			$this->data=array('tasks'=>$this->task_model->getFilteredTasks($param_service_type,$type,$flag),'submitted_year'=>$this->task_model->getTotalSubmitted($param_service_type,$param_yearly),'completed_year'=>$this->task_model->getTotalCompleted($param_service_type,$param_yearly),'active_year'=>$this->task_model->getTotalActive($param_service_type,$param_yearly),'amount_year'=>$this->task_model->getTotalTaskAmount($param_service_type,$param_yearly),'submitted_month'=>$this->task_model->getTotalSubmitted($param_service_type,$param_monthly),'completed_month'=>$this->task_model->getTotalCompleted($param_service_type,$param_monthly),'active_month'=>$this->task_model->getTotalActive($param_service_type,$param_monthly),'amount_month'=>$this->task_model->getTotalTaskAmount($param_service_type,$param_monthly));
			
	  $this->middle = 'admin/tasks/manage';  
	  $this->layoutAdmin(); 
			  
		  

            
		}
	   else if (strpos($post['curr_page'], 'download') !== false)
	   {
		   		$param_service_type="home_maintenance";
		$param_yearly="Y";
		$param_monthly="M";
		
		     $arr_status=array('S'=>'Submitted','P'=>'Processing','X'=>'Cancelled','C'=>'Completed');
			 $data = $this->task_model->getTasks($param_service_type);
			 $final_data=array();
			 $count=0;
			 foreach($data as $val)
			 {
				 $final_data[$count]['id']=$val['id'];
				 $final_data[$count]['user_id']=$val['user_id'];
				 $final_data[$count]['type']=$val['is_residential']==1 ? 'Residential':'Commercial';
				 $final_data[$count]['address']=$val['address'];
				 $final_data[$count]['description']=$val['description'];
				 $final_data[$count]['task_date']=$val['task_date'];
				 $final_data[$count]['task_time']=$val['task_time'];
				 $final_data[$count]['estimated_time']=$val['estimated_time']." Hour(s)";
				 $final_data[$count]['total_price']='$ '.$val['total_price'];
				 $final_data[$count]['customer_name']=$val['first_name']." ".$val['last_name'];
	             $final_data[$count]['lat']=$val['lat'];
	             $final_data[$count]['lon']=$val['lon'];
	             $final_data[$count]['status']=$arr_status[$val['status']];
	             $final_data[$count]['payment_status']=$val['payment_status'];
	             $final_data[$count]['created']=$val['created'];
	             $final_data[$count]['handyman_name']=$val['name']=='' ? 'Not Assigned Yet':'';
	             $final_data[$count]['category_name']=$val['category_name'];

				 $count++;
			 }
			 		
			 $template ='Task.xlsx';
			 $templateDir='assets/';
			 
			 $config = array(
			'template' => $template,
			'templateDir' => $templateDir
			 );

			  //load template
			  $R = new PHPReport($config);

			  $R->load(array(
					  'id' => 'handy',
					  'repeat' => TRUE,
					  'data' => $final_data   	
				  )
			  );
			  
			  $output_file_dir = "assets/";			 
			  $output_file_excel = $output_file_dir  . "Task_detail.xlsx";
			  $result = $R->render('excel', $output_file_excel);
			  header("Location: ".site_url('/assets/Task_detail.xlsx'));
	   }
	   else {}
	   */
	   
	   if (strpos($post['curr_page'], 'manage_renovation') !== false)
	   {
		   $param_service_type="home_renovation";
		$param_yearly="Y";
		$param_monthly="M";
		
              $arr_param=array('start_date'=>$post['start_date'],'end_date'=>$post['end_date'],'service_type'=>$param_service_type);
            
       
          	  $this->data=array('type'=>'home_renovation','tasks'=>$this->task_model->getTasksDateFilter($arr_param),'submitted_year'=>$this->task_model->getTotalSubmittedDateFilter($arr_param),'completed_year'=>$this->task_model->getTotalCompletedDateFilter($arr_param),'active_year'=>$this->task_model->getTotalActiveDateFilter($arr_param),'amount_year'=>$this->task_model->getTotalTaskAmountDateFilter($arr_param));
			
				$this->middle = 'admin/tasks/manage_date';  
				$this->layoutAdmin(); 
	   }
		else
		{
					$param_service_type="home_maintenance";
		$param_yearly="Y";
		$param_monthly="M";
		
              $arr_param=array('start_date'=>$post['start_date'],'end_date'=>$post['end_date'],'service_type'=>$param_service_type);
            
       
          	  $this->data=array('type'=>'home_maintenance','tasks'=>$this->task_model->getTasksDateFilter($arr_param),'submitted_year'=>$this->task_model->getTotalSubmittedDateFilter($arr_param),'completed_year'=>$this->task_model->getTotalCompletedDateFilter($arr_param),'active_year'=>$this->task_model->getTotalActiveDateFilter($arr_param),'amount_year'=>$this->task_model->getTotalTaskAmountDateFilter($arr_param));
			
				$this->middle = 'admin/tasks/manage_date';  
				$this->layoutAdmin(); 
		}
	   }

	}
	
	function post_task_status()
	{
		$this->auth->is_admin_logged_in();
		$post=$this->input->post();
		$this->load->model('task_model');
		if($this->task_model->post_task_status($post['id'],$post['status']))
		{
			if($post['status'] == 1){
				// send mail to user
				$this->load->model('message_model');
				$msg=$this->message_model->get_template('renovation_task_posted');
				$this->load->model('customer_model');
				$task=$this->task_model->getTasks('',$post['id']);
				$msg['to_email']=$this->customer_model->email_from_id($task[0]['user_id'],'users');
				$msg['content'] = str_replace('{task_name}', $task[0]['title'], $msg['content']);
				sendMail($msg);
				
			
			}
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
	}
	
	
	function store_credit_user($task_id)
	{
		$this->auth->is_admin_logged_in();		
		$this->load->model('task_model');
		if($details=$this->task_model->getTasks($service_type=false,$task_id))
		{			
			     $this->data=array('details'=>$details);
	             $this->middle = 'admin/tasks/store_credit_user';  
                 $this->layoutAdmin();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/task/manage');
		}
	}
	
	
    function store_credit_user_action() 
    {
		
		$this->form_validation->set_rules('crredit_amt', 'amount', 'trim|required|numeric|callback_validate_amount'); 
		if ($this->form_validation->run() == FALSE) 
		{
			$post=$this->input->post();
			$id=$post['task_id'];
			$this->session->set_flashdata('error','Credit value can not be zero !');
			redirect('/admin/task/detail/'.$id);
		}
		else
		{
				$post=$this->input->post();

		//save credit_percent and crredit_amt for task 
		//save store_credit detail in new table
				$this->load->model('task_model');

				 $this->task_model->update_credit_percent_user($post);

				//Set variables for paypal form
				$returnURL = base_url().'admin/paypal/success_store_credit_user'; //payment success url
				$cancelURL = base_url().'admin/paypal/cancel_store_credit_user'; //payment cancel url
				$notifyURL = base_url().'admin/paypal/ipn_store_credit_user'; //ipn url
				//get particular product data
			   
				
				$logo = base_url().'assets/images/logo.png';
				
				$this->paypal_lib->add_field('return', $returnURL);
				$this->paypal_lib->add_field('cancel_return', $cancelURL);
				$this->paypal_lib->add_field('notify_url', $notifyURL);
				$this->paypal_lib->add_field('item_name', 'Store Credit');
				$this->paypal_lib->add_field('custom', $post['user_id']);
				$this->paypal_lib->add_field('rm','2');
				$this->paypal_lib->add_field('cbt', 'Return to The Store');
					  
				$this->paypal_lib->add_field('item_number',  $post['task_id']);
				$this->paypal_lib->add_field('amount',  $post['crredit_amt']);        
				$this->paypal_lib->image($logo);
				
				$this->paypal_lib->paypal_auto_form();
			   //	$this->paypal_lib->paypal_form();
       
			
		}
		


    }
    
    function validate_amount($str)
	{				
	   
			if ($str*1 ==0)
			{
				$this->form_validation->set_message('validate_amount', 'Amount can not be zero !');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
    
    
    
	 
	}
	
}

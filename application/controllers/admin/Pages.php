<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
  	function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
        $this->load->model('page_model');
    }
	public function add()
	{
		$this->middle = 'admin/page/add';  
		$this->layoutAdmin(); 
	}
	public function add_action()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required'); 
		$this->form_validation->set_rules('description', 'Description', 'trim|required'); 
		if ($this->form_validation->run() == FALSE) { 
				$this->middle = 'admin/page/add';  
                $this->layoutAdmin(); 
		}else{
			$data = $this->input->post();
			$data['slug'] = strtolower(preg_replace("/[\s_]/", "-", $data['name']));
			if($res =$this->page_model->save($data)) $this->session->set_flashdata('success','Record has been added successfully.');
			else $this->session->set_flashdata('error','Something went wrong, please try again!');
		    redirect('/admin/pages/add');

		}
	
	}
	public function edit($id)
	{
		
		if($details=$this->page_model->get_page_detail($id))
		{
			 $this->data=array('details'=>$details);
			 $this->middle = 'admin/page/edit';  
			 $this->layoutAdmin();
   		}
		else
		{
			//throw error
			$this->session->set_flashdata('error','Something went wrong, please try again later!');
			redirect('/admin/pages/manage');
		}
		
	}
	public function edit_action()
	{
		
		
		$post=$this->input->post();
		$post['slug'] = strtolower(preg_replace("/[\s_]/", "-", $post['name']));
		if($this->page_model->update($post))
		{
                $this->session->set_flashdata('success','Record has been updated successfully.');
		        redirect('/admin/pages/manage');		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        redirect('/admin/pages/manage');
		}
		exit;
	}
	public function change_status()
	{
		$post=$this->input->post();
		if($this->page_model->change_status($post['id'],$post['status']))
		{
			echo 'success';
		}
		else
		{
			echo 'error';
		}
		exit;
	}
	public function delete()
	{
		$post=$this->input->post();
		if($this->page_model->delete($post['id']))	echo 'success';
		else echo 'error';
		exit;
	}
	
	public function manage()
	{
		$this->auth->is_admin_logged_in();
		
		$this->data=array('list'=>$this->page_model->getListing());
		$this->middle = 'admin/page/manage';  
		$this->layoutAdmin(); 
	}
	
	
	public function detail($id)
	{
		$this->auth->is_admin_logged_in();
		$this->load->model('payment_model');
		$this->load->model('notification_model');
		if($details=$this->payment_model->getDetail($id))
		{
			 $this->notification_model->update_notify($id,'payment_id','payments');
			 $this->data=array('details'=>$details);
			 $this->middle = 'admin/payment/detail';  
             $this->layoutAdmin();
   		}
		else
		{
			//throw error
			$this->session->set_flashdata('error','Something went wrong, please try again later!');
			redirect('/admin/payment/manage');
		}
		
	}
	
	
}

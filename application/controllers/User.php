<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
    private $send_data;  
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
  	function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('email');
  
		if($this->session->userdata('user'))
		{
			  $user_detail=$this->session->userdata('user');
			  $this->send_data['user_detail']=$user_detail;
		}
	}

	public function do_login()
	{
		
		     $user_detail=$this->session->userdata('user');
		     
		     if($user_detail['user_type']=='user')
		     {
				 
				 $this->load->model('task_model');
				 //$param_service_type="home_maintenance";
			     //$param_service_type1="home_renovation";
			     //$param_service_type1="all";
				 $arr_param=array('user_id'=>$user_detail['id'],'service_type'=>'all');
				 
				 $allTasks=$this->task_model->getUserTasks($arr_param);		

				  $this->data=array('user_detail'=>$user_detail,'tasks'=>$allTasks);		
				  $this->middle = 'user_dashboard';     
				  $this->layoutDashboard();
		     }
		     else if($user_detail['user_type']=='handyman')
		     {
				
				$user_detail=$this->session->userdata('user');
				 $this->load->model('task_model');
				 //$param_service_type="home_maintenance";
			     //$param_service_type1="home_renovation";
			     //$param_service_type1="all";
				 $arr_param=array('user_id'=>$user_detail['id'],'service_type'=>'all');
				 
				 $allTasks=$this->task_model->getAssignedTasks($arr_param);		
	
				  $this->data=array('user_detail'=>$user_detail,'tasks'=>$allTasks);		
				  $this->middle = 'handyman_dashboard';     
				  $this->layoutDashboard();
			 }
			 else
			 {
				   redirect('login');
			 }
		
	}
	public function forget_password_action(){
		$data = $post=$this->input->post();
		// check email exist 
		$this->load->model('customer_model');
		$email_exist = 0;
		$user = $this->customer_model->is_email_exist(array('user_type'=>'user','email'=>$data['email']));
		
		if($user) $email_exist =1;
		else{
			$handyman = $this->customer_model->is_email_exist(array('user_type'=>'handyman','email'=>$data['email']));
			if($handyman) $email_exist =1;
		}
		if($email_exist == 1)
		{
			// send mail and update password
			$password_new = $this->customer_model->update_password($data);
			$this->load->model('message_model');
			$msg=$this->message_model->get_template('forget_password');
			$msg['to_email']=$post['email'];
			$msg['content'] = str_replace('{new_password}', $password_new, $msg['content']);
			sendMail($msg);
			$this->session->set_flashdata('success', 'Please check mail for new password');
		}else{
			$this->session->set_flashdata('error', 'Email does not exist');
			
		}
		redirect('forget_password');
		
		
	}
	public function forget_password($segment=false){
		
		if($this->session->userdata('user'))
		{
			//redirect to corresponding dashboard
             redirect('do_login');
						  			 
		}
		else
		{
	     $segment=$this->uri->segment(2); 	
	     $this->data=array('redirect'=>$segment);
		 $this->middle = 'forget_password';  
         $this->layoutLogin();
	    }	  
	
	
	}
	
	public function login($segment=false)
	{

		if($this->session->userdata('user'))
		{
			//redirect to corresponding dashboard
             redirect('do_login');
						  			 
		}
		else
		{
	     $segment=$this->uri->segment(2); 	
	     if($this->uri->segment(3))
	     {
			 $segment.='/'.$this->uri->segment(3);
		 }
	     $this->data=array('redirect'=>$segment);
		 $this->middle = 'login';  
         $this->layoutLogin();
	    }	  
	}  
	
	public function register()
	{
		if($this->session->userdata('user'))
		{
			//redirect to corresponding dashboard
		        redirect('do_login');
		}
		else
		{
			   $this->middle = 'register';   
               $this->layoutLogin();
		}

	}  
	
	public function register_handyman()
	{
		if($this->session->userdata('user'))
		{
			redirect('do_login');
		}
		else
		{
			  $this->load->model('category_model');
			  $main_services['home_maintenance']=$this->category_model->get_categories(-1,'home_maintenance');
			  $main_services['home_renovation']=$this->category_model->get_categories(-1,'home_renovation');
			  $count=0;
			  foreach($main_services['home_maintenance'] as $val)
			  {
				  $sub_services=$this->category_model->get_categories($val['id'],false);
				  $main_services['home_maintenance'][$count]['sub_services']=$sub_services;
				  $count++;  
			  }
			  $count=0;
			  foreach($main_services['home_renovation'] as $val)
			  {
				  $sub_services=$this->category_model->get_categories($val['id'],false);
				  $main_services['home_renovation'][$count]['sub_services']=$sub_services;
				  $count++;  
			  }

			  $this->data = array('services'=>$main_services);
			  $this->middle = 'register_handyman';
			  $this->layoutDefault();
		}
		

	} 
	
	public function login_fb()
	{
		if($this->session->userdata('admin'))
		{
			$this->session->unset_userdata('admin');
        }
        $post=$this->input->post();
      
        if(isset($post['id']))
		{
			
			
            //create user if not created 
            $this->load->model('customer_model');
            $data['first_name']=$post['first_name'];
            $data['last_name']=$post['last_name'];
            $data['image']=$post['picture']['data']['url'];
            $data['fb_id']=$post['id'];
            $data['email']=$post['email'];
            $data['raw_password']= rand(000000, 999999);
            $data['password']=md5($data['raw_password']);
            $data['user_type']=1;
			if($user_id=$this->customer_model->create_fbuser($data))
			{
			   //send mail to user for new email and password
			   // send Mail
			 
			   if($user_id!=1)
			   {
				  
				  //new user has been created through fb
				$this->load->model('message_model');
				$msg=$this->message_model->get_template('create_user');
				$msg['to_email']=$post['email'];
				$msg['content'] = str_replace('{email}', $post['email'], $msg['content']);
				$msg['content'] = str_replace('{password}', $data['raw_password'], $msg['content']);
				sendMail($msg);
				if($this->auth->login_user($data['email'], $data['raw_password']))
				{
					 $this->customer_model->login_info();
					 echo 'success';
				}	
				else
				{
					 echo 'error';
				}
			   }
			   else
			   {
				  
				 if($this->auth->login_user_fb($post['id']))
				 {
					  $this->customer_model->login_info();
					   echo 'success';
				 }
				 else
				 {
					 echo 'error';
				 }
			   }

			   	
			}
			else
			{
				 echo 'error';
			}

		}
	    else
		{
				 echo 'error';
		}
		
		exit;
	}
	
	public function login_action()
	{
		if($this->session->userdata('admin'))
		{
			$this->session->unset_userdata('admin');
        }
		
		$post=$this->input->post();
		
		if(isset($post['email']) && isset($post['password']))
		{
			
			$email	= $post['email'];
			$password	= $post['password'];

         $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email'); 
         $this->form_validation->set_rules('password', 'Password', 'trim|required'); 
			
         if ($this->form_validation->run() == FALSE) 
         {               
			   $this->middle = 'login';     
               $this->layoutLogin();
			 
		 }
		 else
		 {
			 	    $login	= $this->auth->login_user($email, $password);
					
					if ($login)
					{	
					   $user_detail=$this->session->userdata('user');
					   $this->load->model('customer_model');
					   
					   $this->customer_model->login_info();
					   
					   if(isset($post['redirect']) && $post['redirect']!="")
					   {
						    redirect($post['redirect']); 
					   }
					   else
					   {
						  redirect('do_login');
					   }	

					}
					else
					{
						$this->session->set_flashdata('eemail',$email);
						$this->session->set_flashdata('ppass',$password);
						$this->session->set_flashdata('error', 'Invalid email or password');
						 redirect('login');
					}
		  }

		 }
	    else
		{
				 redirect('login');
		}

	}	 
	

    public function logout()
	{		
		
		$this->load->model('customer_model');
		$this->customer_model->logut_info();
			
		$this->auth->logout_user();		
		//$this->session->set_flashdata('message','You have logged out successfully');
		
		redirect('/');
	}
	
	public function register_user_action()
	{
			
         $this->form_validation->set_rules('first_name', 'Firstname', 'trim|required|alpha'); 
         $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required|alpha'); 
         $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_validate_customer'); 
         $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]'); 
			
         if ($this->form_validation->run() == FALSE) 
         { 
               
               $this->middle = 'register';     
               $this->layoutLogin();
         } 
         else
         { 
			$this->load->model('customer_model');
			$data=$this->input->post();
			$data['user_type']=1;
			$data['password']=md5($data['password']);
			$data['ip_address']=$_SERVER['REMOTE_ADDR'];
			
			$data['query'] = $this->customer_model->save_customer($data);
			if($data['query'] != NULL)
			{
				$this->customer_model->update_isLogin('N','handyman',$this->session->userdata['user']['id']);
				$this->customer_model->update_isLogin('Y','users',$data['query']);	
				
				//success
				$this->session->set_flashdata('success','Congratulations..You have been registered successfully.');
		        redirect('login');
			
			}
			else
			{
                //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again!');
		        redirect('register');
			}
         } 
         
	}
	
	public function register_handyman_action()
	{		
				
         $this->form_validation->set_rules('name', 'Name', 'trim|required|alpha'); 
         $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_validate_handyman'); 
         $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]'); 
         $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric|min_length[6]'); 
         $this->form_validation->set_rules('company', 'Company', 'trim|required'); 
         $this->form_validation->set_rules('address', 'Address', 'trim|required'); 
         $this->form_validation->set_rules('main_services[]', 'main_service', 'callback_validate_services'); 
         $this->form_validation->set_rules('subservices[]', 'subservices', 'callback_validate_subServices'); 
			
         if ($this->form_validation->run() == FALSE) 
         {                
			  $this->load->model('category_model');
			  $main_services['home_maintenance']=$this->category_model->get_categories(-1,'home_maintenance');
			  $main_services['home_renovation']=$this->category_model->get_categories(-1,'home_renovation');
			  $count=0;
			  foreach($main_services['home_maintenance'] as $val)
			  {
				  $sub_services=$this->category_model->get_categories($val['id'],false);
				  $main_services['home_maintenance'][$count]['sub_services']=$sub_services;
				  $count++;  
			  }
			  $count=0;
			  foreach($main_services['home_renovation'] as $val)
			  {
				  $sub_services=$this->category_model->get_categories($val['id'],false);
				  $main_services['home_renovation'][$count]['sub_services']=$sub_services;
				  $count++;  
			  }

				  $this->data = array('services'=>$main_services);
				  $this->middle = 'register_handyman';
				  $this->layoutDefault();
         } 
         else
         {
			 
			$this->load->model('customer_model');
			$data=$this->input->post();
	
			
			$data_insert['password']=md5($data['password']);
			$data_insert['name']=$data['name'];
			$data_insert['email']=$data['email'];
			$data_insert['phone']=$data['phone'];
			$data_insert['company']=$data['company'];
			$data_insert['address']=$data['address'];
			$data_insert['ip_address']=$_SERVER['REMOTE_ADDR'] ;
			
		
			
			$data['query'] = $this->customer_model->save_handyman($data_insert,$data['subservices']);
			if($data['query'] != NULL)
			{
				
				//success
				$this->session->set_flashdata('success','Congratulations..You have been registered successfully.');
		        redirect('login');
			
			}
			else
			{
                //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again!');
		        redirect('register');
			}
			
         } 
         
	}
	
	function change_password()
	{	
		if(!$this->session->userdata('user'))
		{
			//redirect to corresponding dashboard
             redirect('login');
						  			 
		}
		else
		{
			
                  $this->data=$this->send_data;
				  $this->middle = 'change_password';     
				  $this->layoutDashboard();
	    }	
		
	}
	
	
    public function change_password_action()
	{
			
         $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|min_length[5]|max_length[20]'); 
         $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[5]|max_length[20]'); 
         $this->form_validation->set_rules('retype_password', 'Retype Password', 'trim|required|min_length[5]|max_length[20]'); 
			
         if ($this->form_validation->run() == FALSE) 
         { 
                  $this->data=$this->send_data;
				  $this->middle = 'change_password';     
				  $this->layoutDashboard();
         } 
         else
         { 
			$this->load->model('customer_model');
			$data=$this->input->post();
			if($data['new_password']!=$data['retype_password'])
			{
				$this->session->set_flashdata('error','New password and retype password should be same !');
		        redirect('change_password');
			}
			$update['password']=md5($data['new_password']);
			$update['old_password']=md5($data['old_password']);
			
			
			if($this->customer_model->change_password($update))
			{
				
				//success
				$this->session->set_flashdata('success','Your password has been changed successfully.');
		        redirect('change_password');
			
			}
			else
			{
                //throw error
                $this->session->set_flashdata('error','Please enter valid old password!');
		        redirect('change_password');
			}
         } 
         
	}
	
    function edit_profile()
	{	
		if(!$this->session->userdata('user'))
		{
			//redirect to corresponding dashboard
             redirect('login');
						  			 
		}
		else
		{
			    $this->load->model('customer_model');
				if($this->session->userdata('user')['user_type']=='handyman')
				{
						$this->load->model('category_model');
						$main_services['home_maintenance']=$this->category_model->get_categories(-1,'home_maintenance');
						$main_services['home_renovation']=$this->category_model->get_categories(-1,'home_renovation');
						$count=0;
						foreach($main_services['home_maintenance'] as $val)
						{
						$sub_services=$this->category_model->get_categories($val['id'],false);
						$main_services['home_maintenance'][$count]['sub_services']=$sub_services;
						$count++;  
						}
						$count=0;
						foreach($main_services['home_renovation'] as $val)
						{
						$sub_services=$this->category_model->get_categories($val['id'],false);
						$main_services['home_renovation'][$count]['sub_services']=$sub_services;
						$count++;  
						}

						$this->send_data['services'] = $main_services;
			  
			  
					    $this->send_data['details']=$this->customer_model->get_handyman_detail($this->session->userdata('user')['id'])	;				
				}
				else
				{
					 $this->send_data['details']=$this->customer_model->get_customer_detail($this->session->userdata('user')['id'])	;			
				}

                  $this->data=$this->send_data;
				  $this->middle = 'edit_profile';     
				  $this->layoutDashboard();
	    }	
		
	}
	
    public function edit_profile_action()
	{
			
		if($this->session->userdata('user')['user_type']=='handyman')
		{
			 $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
			 $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email'); 
			 $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric|min_length[6]'); 
			 $this->form_validation->set_rules('company', 'Company', 'trim|required'); 
			 $this->form_validation->set_rules('address', 'Address', 'trim|required'); 
			 $this->form_validation->set_rules('subservices[]', 'subservice', 'callback_validate_subServices'); 
		}
		else
		{
         $this->form_validation->set_rules('first_name', 'Firstname', 'trim|required'); 
         $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required'); 
         $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
          
		}
			
         if ($this->form_validation->run() == FALSE) 
         { 
			 	$this->load->model('customer_model');
				if($this->session->userdata('user')['user_type']=='handyman')
				{
						$this->load->model('category_model');
						$main_services['home_maintenance']=$this->category_model->get_categories(-1,'home_maintenance');
						$main_services['home_renovation']=$this->category_model->get_categories(-1,'home_renovation');
						$count=0;
						foreach($main_services['home_maintenance'] as $val)
						{
						$sub_services=$this->category_model->get_categories($val['id'],false);
						$main_services['home_maintenance'][$count]['sub_services']=$sub_services;
						$count++;  
						}
						$count=0;
						foreach($main_services['home_renovation'] as $val)
						{
						$sub_services=$this->category_model->get_categories($val['id'],false);
						$main_services['home_renovation'][$count]['sub_services']=$sub_services;
						$count++;  
						}

						$this->send_data['services'] = $main_services;
						
						
					$this->send_data['details']=$this->customer_model->get_handyman_detail($this->session->userdata('user')['id'])	;				
				}
				else
				{
					$this->send_data['details']=$this->customer_model->get_customer_detail($this->session->userdata('user')['id'])	;
				}
				
                  $this->data=$this->send_data;
				  $this->middle = 'edit_profile';     
				  $this->layoutDashboard();
         } 
         else
         { 
			   $post['image'] ='';
			 
			 	if($_FILES["file"]['name']!='')
				{
					
					
					$new_name = time().$_FILES["file"]['name'];
					$new_name = str_replace(' ', '', $new_name);
					
					$config['file_name'] = $new_name;
					$config['upload_path'] = './assets/users/'; //The path where the image will be save
					$config['allowed_types'] = '*'; //Images extensions accepted
					$config['max_size'] = '2000';
					$this->load->library('upload', $config); //Load the upload CI library
					if ($this->upload->do_upload('file')){
						$post['image'] = $new_name;
						unset($post['file']);
					}
					else
					{
						print_r($this->upload->display_errors());
						//$post['image']='user.jpg';
						die;
					}
			
				}
				else
				{					
					unset($post['file']); 
				}
				
				
			$this->load->model('customer_model');
			$data=$this->input->post();
            $data['image']= $post['image'];
			
			if($this->session->userdata('user')['user_type']=='user')
			{
				$data['other_table'] = 'handyman';
				$response=$this->customer_model->update_customer($data);
			}
			else
			{
				$data['other_table'] = 'users';
				$response=$this->customer_model->update_handyman($data);
			}
			if($response)
			{
				
				//success
				$this->session->set_flashdata('success','Your profile has been changed successfully.');
		        redirect('edit_profile');
			
			}
			else
			{
                //throw error
                $this->session->set_flashdata('error','Something is wrong , please try again later!');
		        redirect('edit_profile');
			}
         } 
         
	}
	

	function switch_user($type=false)
	{
		
		$user_detail=$this->session->userdata('user');
		if($type=='user')
		{
		
			$user_detail=$this->session->userdata('user');
		   	$this->load->model('customer_model');
		   	$arr_param=array('email'=>$user_detail['email'],'user_type'=>'user');
		   	if($res=$this->customer_model->is_email_exist($arr_param))
		   	{
				$this->customer_model->update_isLogin('N','handyman',$this->session->userdata['user']['id']);
				$this->customer_model->update_isLogin('Y','users',$res['id']);	
				
				//logout user and login as handyman
				unset($this->session->userdata['user']['user_type']);
				unset($this->session->userdata['user']['id']);
			    $this->session->userdata['user']['user_type']='user';
			    $this->session->userdata['user']['id']=$res['id'];
			    redirect('do_login');
			}
			else
			{	
				
			   $this->data = array('user_detail'=>$user_detail);

			   $this->middle = 'switchTo_registerUser';   
               $this->layoutDashboard();
				
			}
		
		}
		else if($type=='handyman')
		{
			
		   //check this email in handyman table 
		    $user_detail=$this->session->userdata('user');
		   	$this->load->model('customer_model');
		   	$arr_param=array('email'=>$user_detail['email'],'user_type'=>'handyman');
		   	if($res=$this->customer_model->is_email_exist($arr_param))
		   	{
				$this->customer_model->update_isLogin('Y','handyman',$res['id']);
				$this->customer_model->update_isLogin('N','users',$this->session->userdata['user']['id']);
				//logout user and login as handyman
				unset($this->session->userdata['user']['user_type']);
				unset($this->session->userdata['user']['id']);
			    $this->session->userdata['user']['user_type']='handyman';
			    $this->session->userdata['user']['id']=$res['id'];
			    redirect('do_login');
			}
			else
			{
				
				//email not found register as handyman
				
			  $this->load->model('category_model');
			  $main_services['home_maintenance']=$this->category_model->get_categories(-1,'home_maintenance');
			  $main_services['home_renovation']=$this->category_model->get_categories(-1,'home_renovation');
			  $count=0;
			  foreach($main_services['home_maintenance'] as $val)
			  {
				  $sub_services=$this->category_model->get_categories($val['id'],false);
				  $main_services['home_maintenance'][$count]['sub_services']=$sub_services;
				  $count++;  
			  }
			  $count=0;
			  foreach($main_services['home_renovation'] as $val)
			  {
				  $sub_services=$this->category_model->get_categories($val['id'],false);
				  $main_services['home_renovation'][$count]['sub_services']=$sub_services;
				  $count++;  
			  }

			  $this->data = array('services'=>$main_services,'user_detail'=>$user_detail);
			  
				  $this->middle = 'switchTo_registerHandyman';     
				  $this->layoutDashboard();
				 
				
			}
		}
		else
		{
			 redirect('do_login');
		}

		
	}
	
	function switchTo_registerHandyman_action()	
	{
					
         $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
         $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_validate_handyman'); 
         $this->form_validation->set_rules('password', 'Password', 'trim|required'); 
         $this->form_validation->set_rules('company', 'Company', 'trim|required'); 
         $this->form_validation->set_rules('address', 'Address', 'trim|required'); 
         $this->form_validation->set_rules('main_services[]', 'main_serv', 'callback_validate_services'); 
         $this->form_validation->set_rules('subservices[]', 'subservi', 'callback_validate_subServices'); 
		 if ($this->form_validation->run() == FALSE) 
         {          
			 $user_detail=$this->session->userdata('user');      
			  $this->load->model('category_model');
			  $main_services['home_maintenance']=$this->category_model->get_categories(-1,'home_maintenance');
			  $main_services['home_renovation']=$this->category_model->get_categories(-1,'home_renovation');
			  $count=0;
			  foreach($main_services['home_maintenance'] as $val)
			  {
				  $sub_services=$this->category_model->get_categories($val['id'],false);
				  $main_services['home_maintenance'][$count]['sub_services']=$sub_services;
				  $count++;  
			  }
			  $count=0;
			  foreach($main_services['home_renovation'] as $val)
			  {
				  $sub_services=$this->category_model->get_categories($val['id'],false);
				  $main_services['home_renovation'][$count]['sub_services']=$sub_services;
				  $count++;  
			  }

			  $this->data = array('services'=>$main_services,'user_detail'=>$user_detail);
			  
				  $this->middle = 'switchTo_registerHandyman';     
				  $this->layoutDashboard();
         } 
         else
         {
			 
			$this->load->model('customer_model');
			$data=$this->input->post();
			$data_insert['password']=md5($data['password']);
			$data_insert['name']=$data['name'];
			$data_insert['email']=$data['email'];
			$data_insert['phone']=$data['phone'];
			$data_insert['company']=$data['company'];
			$data_insert['address']=$data['address'];
			$data_insert['ip_address']=$_SERVER['REMOTE_ADDR'] ;
			$data['query'] = $this->customer_model->save_handyman($data_insert,$data['subservices']);
			if($data['query'] != NULL)
			{
				$this->customer_model->update_isLogin('N','users',$this->session->userdata['user']['id']);
				$this->customer_model->update_isLogin('Y','handyman',$data['query']);					 
				
				unset($this->session->userdata['user']['user_type']);
				unset($this->session->userdata['user']['id']);
			    $this->session->userdata['user']['user_type']='handyman';
			    $this->session->userdata['user']['id']=$data['query'];
			    redirect('do_login');

				//success
				//$this->session->set_flashdata('success','Congratilations..You have been registered successfully.');
		        //redirect('switch_user/handyman');
			
			}
			else
			{
				
                //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again!');
		         redirect('do_login');
			}
			
         } 	
	}
	
	public function switchTo_registerUser_action()
	{
			
		 $user_detail=$this->session->userdata('user');
         $this->form_validation->set_rules('first_name', 'Firstname', 'trim|required'); 
         $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required'); 
         $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_validate_customer'); 
         $this->form_validation->set_rules('password', 'Password', 'trim|required'); 
			
         if ($this->form_validation->run() == FALSE) 
         { 
			  $this->data = array('user_detail'=>$user_detail);

			   $this->middle = 'switchTo_registerUser';   
               $this->layoutDashboard();

         } 
         else
         { 
			$this->load->model('customer_model');
			$data=$this->input->post();
			$data['user_type']=1;
			$data['password']=md5($data['password']);
			$data['ip_address']=$_SERVER['REMOTE_ADDR'];
			
			$data['query'] = $this->customer_model->save_customer($data);
			if($data['query'] != NULL)
			{
				unset($this->session->userdata['user']['user_type']);
				unset($this->session->userdata['user']['id']);
			    $this->session->userdata['user']['user_type']='user';
			    $this->session->userdata['user']['id']=$data['query'];
			    redirect('do_login');

			
			}
			else
			{
                //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again!');
		        redirect('register');
			}
         } 
         
	}
	
    function handyman_detail($id=false,$task_id=false)
	{
		
				  $this->load->model('customer_model');
			      $this->send_data['details']=$this->customer_model->get_handyman_detail($id,$task_id);	
			       $this->send_data['task_id']=$task_id;								
                  $this->data=$this->send_data;
				  $this->middle = 'handyman_detail';     
				  $this->layoutDashboard();

	}
	
	
    function hire_handyman($id=false,$task_id=false)
	{		
		$this->load->model('customer_model');
		if($this->customer_model->hire_handyman($id,$task_id))
		{
			// send mail to handyman
			$this->load->model('task_model');
			
			$this->load->model('message_model');
			$msg=$this->message_model->get_template('hire_handyman');
			$this->load->model('task_model');
			$task=$this->task_model->getTasks('',$task_id);
			$msg['to_email']=$this->customer_model->email_from_id($id,'handyman');
			$msg['content'] = str_replace('{customer_name}', $task[0]['first_name'].' '. $task[0]['last_name'], $msg['content']);
			$msg['content'] = str_replace('{address}', $task[0]['address_unit'].', '.$task[0]['address'], $msg['content']);
			$msg['content'] = str_replace('{category_name}', $task[0]['category_name'], $msg['content']);
			$msg['content'] = str_replace('{total_amount}', $task[0]['total_price'], $msg['content']);
			$msg['content'] = str_replace('{task_name}', $task[0]['title'], $msg['content']);
			$msg['content'] = str_replace('{task_timing}', $task[0]['task_date']." ".$task[0]['task_time'], $msg['content']);
			$msg['content'] = str_replace('{task_hours}', $task[0]['estimated_time'], $msg['content']);
			$msg['content'] = str_replace('{task_description}', $task[0]['description'], $msg['content']);	
			sendMail($msg);
			$this->session->set_flashdata('success','You have hired handyman successfully.');
		}
		else $this->session->set_flashdata('error','Something went wrong, please try again!');
		redirect('check_task_response/'.$task_id);
	}
	

	
	function user_profile($type=false,$id=false)
	{
		$user_detail=$this->session->userdata('user');
		$this->data['user_detail']=$user_detail;
		$this->load->model('customer_model');
		
		 if($type=='u')
		 { 
			 if($this->data['details']=$this->customer_model->get_customer_detail($id))
			 {	
				if(isset($this->data['details']['id']))
				{
					$this->layoutCustomerProfile();	
				}
				else
				{
					 $this->middle = 'error_404';
			 	    $this->layoutError(); 
				}			
				
		     }
		     else
		     {
				  $this->middle = 'error_404';
			 	  $this->layoutError(); 
			 }
		 }		 
		 else if($type=='h')
		 {
			 if($this->data['details']=$this->customer_model->get_handyman_detail($id,''))
			 {
				if(isset($this->data['details']['id']))
				{
				   $this->layoutHandymanProfile();
			    }
			    else
			    {
					$this->middle = 'error_404';
			 	    $this->layoutError(); 	
				}	
			 }
			 else
			 {
				  $this->middle = 'error_404';
			 	  $this->layoutError(); 
			 }	
			 		      		
		 }
		 else
		 {
			 	  $this->middle = 'error_404';
			 	  $this->layoutError();				
		 }

		
	}
	
	
	function validate_member($str)
	{				
	   $this->load->model('customer_model');
	   if($this->customer_model->validate_member($str))
	   {
		 return true;
	   }
	   else
	   {
		   $this->form_validation->set_message('validate_member','Given email is already registered with us!');
		   return false;
	   }
	}
	
	function validate_handyman($str)
	{				
	   $this->load->model('customer_model');
	   if($this->customer_model->validate_handyman($str))
	   {
		   	 $pattern = '/ /';
             $result = preg_match($pattern, $str);

			if ($result)
			{
				$this->form_validation->set_message('validate_handyman', 'Given email can not have spaces !');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
	   }
	   else
	   {
		   $this->form_validation->set_message('validate_handyman','Given email is already registered with us!');
		   return false;
	   }
	}
	
	
	function validate_customer($str)
	{				
	   $this->load->model('customer_model');
	   if($this->customer_model->validate_customer($str))
	   {
		     $pattern = '/ /';
             $result = preg_match($pattern, $str);

			if ($result)
			{
				$this->form_validation->set_message('validate_customer', 'Given email can not have spaces !');
				return FALSE;
			}
			else
			{
				return TRUE;
			}
    
       }
	   else
	   {
		   $this->form_validation->set_message('validate_customer','Given email is already registered with us!');
		   return false;
	   }
	}
	
	function validate_services($str)
	{			
		 if (isset($_POST['main_services']) && !empty($_POST['main_services'])) return true;
		$this->form_validation->set_message('validate_services', 'Please check atleast one service.');
		return false;

	}
	
	function validate_subServices($str)
	{			
		 if (isset($_POST['subservices']) && !empty($_POST['subservices'])) return true;
		$this->form_validation->set_message('validate_subServices', 'Please check atleast one child service.');
		return false;

	}	
}

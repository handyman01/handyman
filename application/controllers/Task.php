<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
  	function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
	    $this->load->helper('utility');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('paypal_lib');
		$this->load->model('task_model');
		$this->data['marker_array']=array();
        
        $segment=$this->uri->segment(1);
        if($this->uri->segment(2))
        {
			$segment.='/'.$this->uri->segment(2);
		}
        $this->auth->is_user_logged_in($segment);
	}
	function complete_request($task_id)
	{
		if($res=$this->task_model->complete_request($task_id)) $this->session->set_flashdata('success','Task Complete request has been sent successfully');
		else $this->session->set_flashdata('error','Something went wrong');
		redirect('dashboard');
	
	}
	function complete_task_request()
	{
		$this->load->model('task_model');
		$data = $this->input->post();
		if($data['cust_wrk_hrs']!='' && $data['cust_wrk_amt']!='')
		{
			if($res=$this->task_model->complete_request($data))
			{
				// send Mail
				$this->load->model('message_model');
				$msg=$this->message_model->get_template('cancel_request');
				$task=$this->task_model->getTasks('',$data['task_id']);
				$this->load->model('customer_model');
				$msg['to_email']=$this->customer_model->email_from_id($task[0]['user_id'],'users');
				$msg['content'] = str_replace('{task_name}', $task[0]['title'], $msg['content']);
				sendMail($msg);
				$this->session->set_flashdata('success','Task complete request has been sent successfully');
			   echo 'success';
			}
			else
			{
				$this->session->set_flashdata('error','Something went wrong');
				echo 'error';
			}
		}
		else
		{
				$this->session->set_flashdata('error','Please enter task working hours and amount!');
				echo 'error';
		}
		 exit;
	
	}
	function cancel_task()
	{
		$this->load->model('task_model');
		$data = $this->input->post();
		if($res=$this->task_model->cancel_request($data))
		{
			// send Mail
			$this->load->model('message_model');
			$msg=$this->message_model->get_template('cancel_request');
			$task=$this->task_model->getTasks('',$data['task_id']);
			$this->load->model('customer_model');
			$msg['to_email']=$this->customer_model->email_from_id($task[0]['user_id'],'users');
			$msg['content'] = str_replace('{task_name}', $task[0]['title'], $msg['content']);
			sendMail($msg);
			$this->session->set_flashdata('success','Task cancellation request has been sent successfully');
		   echo 'success';
		}
		else
		{
			$this->session->set_flashdata('error','Something went wrong');
			echo 'error';
		}
		 exit;
		
	}
	function post_task()
	{
		$user_detail=$this->session->userdata('user');
		if($user_detail['user_type']=='handyman')
		{
			$this->session->set_flashdata('success','You need to switch as customer to post any task.');
			redirect('switch_user/user');
		}
		else
		{

		
	   $arr_services = array();
	   $this->load->model('category_model');
			  $main_services['home_maintenance']=$this->category_model->get_categories(-1,'home_maintenance');
			  $main_services['home_renovation']=$this->category_model->get_categories(-1,'home_renovation');
			  $count=0;
			  foreach($main_services['home_maintenance'] as $val)
			  {
				  $sub_services=$this->category_model->get_categories($val['id'],false);
				  $main_services['home_maintenance'][$count]['sub_services']=$sub_services;
				  $count++;  
			  }
			  $count=0;
			  foreach($main_services['home_renovation'] as $val)
			  {
				  $sub_services=$this->category_model->get_categories($val['id'],false);
				  $main_services['home_renovation'][$count]['sub_services']=$sub_services;
				  $count++;  
			  }
	
        $this->data = array('services'=>$main_services,'user_detail'=>$user_detail);        
	   	$this->middle = 'add_task';     
        $this->layoutDefault();
	   }
	   
	}
	
	function add_task_action()
	{
		
		$post=$this->input->post();
		$post['document'] ='';
		$new_name = time().$_FILES["file"]['name'];
		$new_name = str_replace(' ', '', $new_name);
		$config['file_name'] = $new_name;
		$config['upload_path'] = './assets/task/'; //The path where the image will be save
		$config['allowed_types'] = '*'; //Images extensions accepted
		$config['max_size'] = '2000';
		$this->load->library('upload', $config); //Load the upload CI library
		if ($this->upload->do_upload('file')){
			$post['document'] = $new_name;
		}
		
		$this->load->model('task_model');
		$this->load->model('customer_model');
		
		$post['total_price'] = $post['estimated_time'] * $post['hourly_price'];
 		
 		if(isset($post['is_commercial'])) $post['is_commercial'] =1;
 		if(isset($post['is_residential'])) $post['is_residential'] =1;
 		
 		//check if task is free (user has not paid deposit fee ) or guaranteed (ready to pay amount to admin)
 		$task_type='f'; //free
 		$send_to_paypal=0;  //send to paypal flag if amount is due
		$paypal_price=0;
		$update_credit=0;  //check if credit needs to update at the end
		$remaining_credit=0;  //update remaining credit to this val
		$is_whole_paid=0;  //check f whole payment is done via credit
 		if($post['task_type']=='free')
 		{
			//save and post as normal task
			$post['total_deposit_amt']=0;
		}
		else
		{
			// send Mail to admin for guaranteed task
			$this->load->model('message_model');
			$msg=$this->message_model->get_template('guranteed_task_posted');
			$msg['content'] = str_replace('{title}', $post['title'], $msg['content']);
			$msg['content'] = str_replace('{customer_name}', $this->session->userdata('user')['firstname'].' '.$this->session->userdata('user')['lastname'], $msg['content']);
			$msg['content'] = str_replace('{description}', $post['description'], $msg['content']);
			$msg['content'] = str_replace('{total_price}', $post['total_price'], $msg['content']);
			$msg['content'] = str_replace('{total_deposit_amt}', $post['total_deposit_amt'], $msg['content']);
			
			
			
			$arr_mail=array('from_email'=>$this->session->userdata('user')['email'],'subject'=>$msg['subject'],'content'=>$msg['content']);
			sendMailtoadmin($arr_mail);
			
			
			//$post['total_deposit_amt']=$post['hourly_price'];
			$task_type='g';  //guaranteed
			if($post['total_deposit_amt']>0)
			{
			//this task is guaranteed but have some amount 
			//check if user have credit amount
			
			
			
			//$credit_amt=$this->session->userdata('user')['credit'];
			
		$data=array('user_type'=>'user','email'=>$this->session->userdata('user')['email']);
		$res=$this->customer_model->is_email_exist($data);
	    
		    $credit_amt=$res['credit'];
			if($credit_amt>0)
			{
				
			   if(isset($post['is_credit']) )
			   {
				  $post['is_credit'] =1;
				   //use my credit amount
				if($credit_amt >= $post['total_deposit_amt'])
				{
					//credit-payable price
					
					$post['credit_used']=$post['total_deposit_amt'];
					$post['deposited_amt']=$post['total_deposit_amt'];
					//now update credit amount for customer $remaining_credit=$credit_amt-$post['hourly_price'];
					$remaining_credit=$credit_amt-$post['total_deposit_amt'];
                    $is_whole_paid=1;
					$update_credit=1;
					
				}
				else
				{
					$post['credit_used']=$credit_amt;
				    $post['deposited_amt']=$post['total_deposit_amt']-$credit_amt;
				    $send_to_paypal=1;
				    $paypal_price=$post['deposited_amt'];
				    $remaining_credit=0;

					$update_credit=1;
					
					//update customer credit to 0
					//$remaining_credit=0;
					
				}
		    }
		    else
		    {
				//if user not using credit amount or credit amount is zero
				  $post['deposited_amt']=$post['total_deposit_amt'];
				  $send_to_paypal=1;
				  $paypal_price=$post['deposited_amt'];
				
			}
		   }
		   else
		   {
			   //dont use my credit amount   
			   $post['deposited_amt']=$post['total_deposit_amt'];
			    $send_to_paypal=1;
			    $paypal_price=$post['deposited_amt'];
			   
			}
		  }
		  else
		  {
			 
			  //task is guaranteed but have zero price
			  
		  }
			
		}

 	    unset($post['hourly_price']);
		if($res=$this->task_model->save_task($post))
		{
			$date=date_create($post['task_date']);
            $send_date=date_format($date,"D,M d,Y");
            
            $time = date("g:i A", strtotime($post['task_time'].':00'));
			if($res=='redundant')
			{
			   echo json_encode(array('status'=>'redundant','date'=>$send_date,'time'=>$time));
		    }
		    else
		    {
				
				if($update_credit==1)
				{

 				   $send_crd=array('credit'=>$remaining_credit,'task_id'=>$res);
				   if($is_whole_paid==1)
				   {
					   //whole payment is done by credit so mark as paid
					    $send_crd['mark_as_paid']=1;
				   }
				  $this->customer_model->update_user_credit($send_crd);
				}
					
					
				$confirm_url=base_url().'task/buy/'.$res;
				echo json_encode(array('status'=>'success','date'=>$send_date,'time'=>$time,'task_id'=>$res,'confirm_url'=>$confirm_url,'send_paypal'=>$send_to_paypal,'paypal_price'=>$paypal_price,'task_type'=>$task_type));
				$this->session->set_flashdata('success','Your task has been posted successfully.');
			}
		}
		else
		{
			echo json_encode(array('status'=>'error'));
		}
		exit;
	}
	
	 function buy($id){
        //Set variables for paypal form
        $returnURL = base_url().'paypal/success'; //payment success url
        $cancelURL = base_url().'paypal/cancel'; //payment cancel url
        $notifyURL = base_url().'paypal/ipn'; //ipn url
        //get particular product data
        $task = $this->task_model->getTasks($service_type=false,$id);
        
        $logo = base_url().'assets/images/logo.png';
        
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', $task[0]['title']);
        $this->paypal_lib->add_field('custom', $task[0]['user_id']);
        $this->paypal_lib->add_field('rm','2');
        $this->paypal_lib->add_field('cbt', 'Return to The Store');
              
        $this->paypal_lib->add_field('item_number',  $task[0]['id']);
        $this->paypal_lib->add_field('amount',  $task[0]['deposited_amt']);        
        $this->paypal_lib->image($logo);
        
        $this->paypal_lib->paypal_auto_form();
       //	$this->paypal_lib->paypal_form();

    }
    
    function posted_task()
    {
				 $user_detail=$this->session->userdata('user');
				 $this->load->model('task_model');
				 //$param_service_type="home_maintenance";
			     //$param_service_type1="home_renovation";
			     //$param_service_type1="all";
				 $arr_param=array('user_id'=>$user_detail['id'],'service_type'=>'all');
				 
				 $allTasks=$this->task_model->getUserTasks($arr_param);		
					
				  $this->data=array('user_detail'=>$user_detail,'tasks'=>$allTasks);		
				  $this->middle = 'user_dashboard';     
				  $this->layoutDashboard();
	}
    
    function assigned_task()
    {
				 $user_detail=$this->session->userdata('user');
				 $this->load->model('task_model');
				 //$param_service_type="home_maintenance";
			     //$param_service_type1="home_renovation";
			     //$param_service_type1="all";
				 $arr_param=array('user_id'=>$user_detail['id'],'service_type'=>'all');
				 
				 $allTasks=$this->task_model->getAssignedTasks($arr_param);		
	
				  $this->data=array('user_detail'=>$user_detail,'tasks'=>$allTasks);		
				  $this->middle = 'handyman_dashboard';     
				  $this->layoutDashboard();
	}
	
    function applied_task()
    {
				 $user_detail=$this->session->userdata('user');
				 $this->load->model('task_model');
				 //$param_service_type="home_maintenance";
			     //$param_service_type1="home_renovation";
			     //$param_service_type1="all";
				 $arr_param=array('user_id'=>$user_detail['id']);
				 
				  $allTasks=$this->task_model->getTasksResponses($arr_param);		
	

				  $this->data=array('user_detail'=>$user_detail,'tasks'=>$allTasks);		
				  $this->middle = 'applied_task';     
				  $this->layoutDashboard();
	}
	
	function apply_maintenance_task()
	{
		         $user_detail=$this->session->userdata('user');
				 $this->load->model('task_model');
				 $data['task_id']=$_REQUEST['id'];
				 $data['user_id']=$user_detail['id'];
				 if($res=$this->task_model->applyTask($data))
				 {
					 echo 'success';
				 }
				 else
				 {
					 echo 'error';
				 }	
                 exit;
			
	}
	
	function check_task_response($id=false)
	{
		
				 $user_detail=$this->session->userdata('user');
				 $this->load->model('task_model');
				 $data['task_id']=$id;
				 $res=$this->task_model->getTasksResponses($data);
			     $arr_status=array('A'=>'Applied','X'=>'Alloted','S'=>'Alloted');
				 $this->data=array('user_detail'=>$user_detail,'handy'=>$res,'status'=>$arr_status,'task_id'=>$id);
				 		
				 $this->middle = 'check_task_response';     
				 $this->layoutDashboard();
	}
	

	
	function check_task_detail($id=false)
	{
		    if(!$id)
		    {
					$this->middle = 'error_404';
			 	    $this->layoutError(); 
			}
			else
			{
		         $user_detail=$this->session->userdata('user');
				 $this->load->model('task_model');
				 $this->load->model('category_model');
				 $this->load->model('customer_model');
				 $data['task_id']=$id;
				 $res=$this->task_model->getUserTasks($data);
				 if(empty($res))
				 {
					 $this->middle = 'error_404';
			 	     $this->layoutError(); 
				 }
				 else
				 {					 					
							 $arr_subcategories=array();
							 $subcats=$this->category_model->get_categories_bySlug($res[0]['categ_parent_slug']);		
					
									
							if(!empty($subcats))
							{
								foreach($subcats as $var)
								{
								
								 $arr_subcategories[$var['id']]=$var['name'];
								}
							}

							/*get all handyman */
							
							$handyman_list=$this->customer_model->get_handyman();
							$arr_handyman[0]='Not Assigned Yet';			
							if(!empty($handyman_list))
							{
								foreach($handyman_list as $var)
								{
								 $arr_handyman[$var['id']]=$var['name'];
								}
							}
						   $arr_status=array('S'=>'Submitted','P'=>'Processing','CR'=>'Cancel Request','X'=>'Cancelled','C'=>'Completed');

							 $this->data=array('user_detail'=>$user_detail,'details'=>$res,'sub_category'=>$arr_subcategories,'handyman_list'=>$arr_handyman,'status'=>$arr_status);		
							 $this->middle = 'task_detail_dashboard';     
							 $this->layoutDashboard();
				 
				 
				 }
			 }

				  
	}
	
	function make_offer()
	{
		
	
		$data=$this->input->post();
		
		$this->load->model('task_model');
		if($res=$this->task_model->save_offer($data))	     
		{
			// send email
			$this->load->model('message_model');
			$this->load->model('customer_model');
			$msg=$this->message_model->get_template('offer');
			$task=$this->task_model->getTasks('',$data['task_id']);
			$handy_man=$this->customer_model->get_handyman_detail($this->session->userdata['user']['id'],'');
			$msg['to_email']=$this->customer_model->email_from_id($task[0]['user_id'],'users');
			$msg['subject'] = str_replace('{task_name}', $task[0]['title'], $msg['subject']);
			$msg['content'] = str_replace('{handy_man}', $handy_man['name'], $msg['content']);
			$msg['content'] = str_replace('{handyman_email}', $handy_man['email'], $msg['content']);
			$msg['content'] = str_replace('{handyman_phone}', $handy_man['phone'], $msg['content']);
			$msg['content'] = str_replace('{amount}', $data['price'], $msg['content']);
			$msg['content'] = str_replace('{task_name}', $task[0]['title'], $msg['content']);
			$msg['content'] = str_replace('{task_timing}', $task[0]['task_date']." ".$task[0]['task_time'], $msg['content']);
			$msg['content'] = str_replace('{task_hours}', $task[0]['estimated_time'], $msg['content']);
			$msg['content'] = str_replace('{task_description}', $task[0]['description'], $msg['content']);
			$msg['content'] = str_replace('{task_price}', $task[0]['total_price'], $msg['content']);
			sendMail($msg);
			echo 'success'; 
		  
		}
		else
		{
			echo 'error';
		}
		 exit;

	}

	function get_subcateg_bySlug()
	{
	    $this->load->model('category_model');
		$post=$this->input->post();
		if($res=$this->category_model->get_categories_bySlug($post['slug']))
		{
			$response=array('msg'=>'success','response'=>$res);
			echo json_encode($response);
		}
		else
		{
			$response=array('msg'=>'error','response'=>'');
			echo json_encode($response);
		}
		exit;
		
	}
	
	

	public function edit_task($id)
	{
		$this->load->model('task_model');
		$this->load->model('category_model');
		$this->load->model('customer_model');
		if($details=$this->task_model->getTasks($service_type=false,$id))
		{
			   $user_detail=$this->session->userdata('user');
			   $arr_status=array('S'=>'Submitted','P'=>'Processing','CR'=>'Cancel Request','X'=>'Cancelled','C'=>'Completed');

			    /* get all handyman */
			    
			    $handyman_list=$this->customer_model->get_handyman();
				$arr_handyman[0]='Not Assigned Yet';			
				if(!empty($handyman_list))
				{
					foreach($handyman_list as $var)
					{
					 $arr_handyman[$var['id']]=$var['name'];
					}
				}
				    
			    
				$cats=$this->category_model->get_categories(-1);	
				$arr_categories=array();			
				if(!empty($cats))
				{
					foreach($cats as $var)
					{
					 $arr_categories[$var['slug']]=$var['name'];
					}
				}
				
				
				$arr_subcategories=array();
				$subcats=$this->category_model->get_categories_bySlug($details[0]['categ_parent_slug']);		
				
			    
			    $hourly_price=0;
			    			
				if(!empty($subcats))
				{
					foreach($subcats as $var)
					{		
				      if($details[0]['categ_id']==$var['id'])
				      {
						  $hourly_price=$var['hourly_price'];
					  }					
					 $arr_subcategories[$var['id']]=$var['name'];
					}
				}
				

				
				$this->data=array('hourly_price'=>$hourly_price,'user_detail'=>$user_detail,'details'=>$details,'parent_categories'=>$arr_categories,'sub_category'=>$arr_subcategories,'handyman_list'=>$arr_handyman,'status'=>$arr_status);
			     
				  $this->middle = 'edit_task';     
				  $this->layoutDashboard();
   		}
		else
		{
			    //throw error
                $this->session->set_flashdata('error','Something went wrong, please try again later!');
		        //redirect('task/manage');
		}
		
	}
	
	function follow_task()
	{
			
		$data=$this->input->post();
		$this->load->model('task_model');
		if($res=$this->task_model->follow_task($data))	     
		{
			
            
			echo $res;
		  
		}
		else
		{
			echo 'error';
		}
		 exit;
	}
	
    function edit_task_action()
	{
		 $post=$this->input->post();
		 $this->form_validation->set_rules('title', 'Title', 'trim|required'); 
         $this->form_validation->set_rules('address', 'Address', 'trim|required'); 
         $this->form_validation->set_rules('task_date', 'Task Date', 'trim|required'); 
         $this->form_validation->set_rules('task_time', 'Task Time', 'trim|required'); 
         $this->form_validation->set_rules('estimated_time', 'Estimated Hour(s)', 'trim|required'); 
         $this->form_validation->set_rules('description', 'Description', 'trim|required'); 
         $this->form_validation->set_rules('lat', 'Address', 'callback_validate_lataddress'); 
         $this->form_validation->set_rules('lon', 'Address', 'callback_validate_lonaddress'); 
         

	
	
         if ($this->form_validation->run() == FALSE) 
         { 
               $details=$this->task_model->getTasks($service_type=false,$post['id']);
               $user_detail=$this->session->userdata('user');
			    $arr_status=array('S'=>'Submitted','P'=>'Processing','CR'=>'Cancel Request','X'=>'Cancelled','C'=>'Completed');

			    /* get all handyman */
			    $this->load->model('customer_model');
			    $this->load->model('category_model');
			    $handyman_list=$this->customer_model->get_handyman();
				$arr_handyman[0]='Not Assigned Yet';			
				if(!empty($handyman_list))
				{
					foreach($handyman_list as $var)
					{
					 $arr_handyman[$var['id']]=$var['name'];
					}
				}
				    
			    
				$cats=$this->category_model->get_categories(-1);	
				$arr_categories=array();			
				if(!empty($cats))
				{
					foreach($cats as $var)
					{
					 $arr_categories[$var['slug']]=$var['name'];
					}
				}
				
				
				$arr_subcategories=array();
				$subcats=$this->category_model->get_categories_bySlug($details[0]['categ_parent_slug']);		
				
			    
			    $hourly_price=0;
			    			
				if(!empty($subcats))
				{
					foreach($subcats as $var)
					{		
				      if($details[0]['categ_id']==$var['id'])
				      {
						  $hourly_price=$var['hourly_price'];
					  }					
					 $arr_subcategories[$var['id']]=$var['name'];
					}
				}
				

				
				$this->data=array('hourly_price'=>$hourly_price,'user_detail'=>$user_detail,'details'=>$details,'parent_categories'=>$arr_categories,'sub_category'=>$arr_subcategories,'handyman_list'=>$arr_handyman,'status'=>$arr_status);
			     
				  $this->middle = 'edit_task';     
				  $this->layoutDashboard();
				  
            
         }
         else
         {
			 
			    if($_FILES["file"]['name']!='')
				{
					$post['document'] ='';
					
					$new_name = time().$_FILES["file"]['name'];
					$new_name = str_replace(' ', '', $new_name);
					$config['file_name'] = $new_name;
					$config['upload_path'] = './assets/task/'; //The path where the image will be save
					$config['allowed_types'] = '*'; //Images extensions accepted
					$config['max_size'] = '2000';
					$this->load->library('upload', $config); //Load the upload CI library
					if ($this->upload->do_upload('file')){
						$post['document'] = $new_name;
						unset($post['file']);
					}
					else
					{
						$this->session->set_flashdata('error','Invalid uploaded file, please try again!');
					    redirect('posted_task');
					}
				}
				else
				{					
					unset($post['file']); 
				}
				
				
				
				$this->load->model('task_model');
				
				$post['total_price'] = $post['estimated_time'] * $post['hourly_price'];
				unset($post['hourly_price']);
				if(isset($post['is_commercial'])) $post['is_commercial'] =1;
				if(isset($post['is_residential'])) $post['is_residential'] =1;
			    $post['updated']=date('Y-m-d H:i:s');
				if($res=$this->task_model->updateMyTask($post))
				{
					 $this->session->set_flashdata('success','Task has been updated successfully.');
					 redirect('posted_task');
				}
				else
				{
					 $this->session->set_flashdata('error','Task has not been updated, please try again!');
					 redirect('posted_task');			
		     	}
				

		 } 

	}
	
	     function validate_lataddress($str)
	{				
	   
	   if(trim($str)!="")
	   {
		 return true;
	   }
	   else
	   {
		   $this->form_validation->set_message('validate_lataddress','Please enter valid address!');
		   return false;
	   }
	}
         
     function validate_lonaddress($str)
	{				
	   
	   if(trim($str)!="")
	   {
		 return true;
	   }
	   else
	   {
		   $this->form_validation->set_message('validate_lonaddress','Please enter valid address!');
		   return false;
	   }
	}
	
	function following_tasks()
	{
				 $user_detail=$this->session->userdata('user');
				 $this->load->model('task_model');

				 $arr_param=array('handyman_id'=>$user_detail['id']);
				 
				 $allTasks=$this->task_model->getFollowingTasks($arr_param);		

				  $this->data=array('user_detail'=>$user_detail,'tasks'=>$allTasks);		
				  $this->middle = 'handyman_following_tasks';     
				  $this->layoutDashboard();
	}
	
	function ratingToCustomer()
	{
		$data=$this->input->post();
		$this->load->model('task_model');
		if($res=$this->task_model->ratingToCustomer($data))	     
		{
			$this->session->set_flashdata('success','Review has been posted successfully.');					
			redirect($_SERVER['HTTP_REFERER']);
		}
		else
		{
			$this->session->set_flashdata('error','Review has not been posted, please try again!');					
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	function ratingToHandyman()
	{
		$data=$this->input->post();
		$this->load->model('task_model');
		if($res=$this->task_model->ratingToHandyman($data))	     
		{
			$this->session->set_flashdata('success','Review has been posted successfully.');					
			redirect($_SERVER['HTTP_REFERER']);
		}
		else
		{
			$this->session->set_flashdata('error','Review has not been posted, please try again!');					
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	    	
}

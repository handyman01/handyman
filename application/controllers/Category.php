<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
  	function __construct()
	{
		parent::__construct();
	    $this->load->helper('form');
        $this->load->library('form_validation');

	}

	function get_price(){
		
		$this->load->model('category_model');
		$price=$this->category_model->get_hourly_price($this->input->post('id'));
		echo json_encode($price);
		
		
	}
	function home_maintenance()
	{
		
	   $arr_services = array();
	   $this->load->model('category_model');
	   if($result=$this->category_model->check_service('home_maintenance'))
	   {
           $arr_services = $result;  
	   }
	   
        $this->data = array('services'=>$arr_services);        
	   	$this->middle = 'services';     
        $this->layoutLogin();
	   
	}
	
	function home_renovation()
	{
		
	   $arr_services = array();
	   $this->load->model('category_model');
	   if($result=$this->category_model->check_service('home_renovation'))
	   {
           $arr_services = $result;  
	   }
	  
        $this->data = array('services'=>$arr_services);        
	   	$this->middle = 'renovation_services';     
        $this->layoutLogin();
	   
	}
	
	function maintenance_list($categ_id)
	{
	   $arr_tasks = array();
	   $this->load->model('task_model');
	   if($result=$this->task_model->getTasksByCateg('home_maintenance',$categ_id))
	   {
           $arr_tasks = $result;  
	   }
	    $user_detail=$this->session->userdata('user');
	    $applied_tasks=array();
	    if($user_detail && !empty($user_detail))
	    {
			$tasks=$this->task_model->getAppliedTasks($user_detail['id']);
			foreach($tasks as $val)
			{
				
				$applied_tasks[$val['task_id']]=$val['status'];
			}
		}


        $this->load->model('category_model');
        $sub_categories=$this->category_model->get_categories_bySlug('home_maintenance');
      
        $this->data = array('tasks'=>$arr_tasks,'user_detail'=>$user_detail,'applied_tasks'=>$applied_tasks,'sub_categories'=>$sub_categories);        
	   	$this->middle = 'maintenance_list';     
        $this->layoutDefault();
	}
	
	function renovation_list($categ_id)
	{
	   $arr_tasks = array();
	   $this->load->model('task_model');
	   if($result=$this->task_model->getTasksByCateg('home_renovation',$categ_id))
	   {
           $arr_tasks = $result;  
	   }
	    $user_detail=$this->session->userdata('user');
	    $applied_tasks=array();
	    if($user_detail && !empty($user_detail))
	    {
			$tasks=$this->task_model->getAppliedTasks($user_detail['id']);
			foreach($tasks as $val)
			{
				
				$applied_tasks[$val['task_id']]=$val['status'];
			}
		}

       // $total_offers=$this->task_model->getTotalOffers();

        $this->data = array('tasks'=>$arr_tasks,'user_detail'=>$user_detail,'applied_tasks'=>$applied_tasks);        
	   	$this->middle = 'renovation_list';     
        $this->layoutDefault();

	}
	
	
    function detail($id=false)
	{
		         $this->load->model('task_model');
		         $user_detail=$this->session->userdata('user');
		       
				 $data['task_id']=$_REQUEST['id'];
				 $res=$this->task_model->getUserTasks($data);
				 $offer_param['task_id']=$_REQUEST['id'];
				 if(isset($user_detail['id']) && $user_detail['user_type']=='handyman')
				 {
					$offer_param['handyman_id']=$user_detail['id']; 
				 }
				 $offers=$this->task_model->getUserOffers($offer_param);
				 
				 //check user wishlist
				 $following_task=$this->task_model->getFollowingTask($offer_param);
				 $is_following=0;
				 if(!empty($following_task))
				 {
				   $is_following=1;	 
				 }

				 
				$date=date_create($res[0]['created']);
                $posted_date=date_format($date,"D,M d,Y");
                $task_date=date_create($res[0]['task_date']);
                $task_date_time=date_format($task_date,"D,M d,Y").' ('.date("g:i A", strtotime($res[0]['task_time'])).')';    
                                 
				 $send='<div class="pad_right_blk">
                    	<div class="task_top_blk">                        	
                            <div class="task_pay">';
                            if($user_detail['user_type'] && $user_detail['user_type']=='handyman')
                            {
								 $send .='<div class="col-md-7 col-sm-7 pad_0">';
							}
							else
							{
								$send .='<div class="col-md-12 col-sm-12 pad_0">';
							}
							
							if(strlen($res[0]['title'])>70)
							{
								$post_titlee=substr(ucfirst(strtolower($res[0]['title'])),0,70).'...';
							}
							else
							{
								$post_titlee=$res[0]['title'];
							}
                            $send .='<div class="task_pay_left">
                                    	<h1 title="'.$res[0]['title'].'">'.$post_titlee.'</h1>
                                        <span class="detail_profile_img"><img src="'.site_url('assets/users/'.$res[0]['image']).'" alt="user" class="mCS_img_loaded img-circle">Posted by: <a href="'.site_url('user_profile/u/'.$res[0]['user_id']).'">'.$res[0]['first_name'].' '.$res[0]['last_name'].'</a></span>';
                   $send .='<br/><span>Posted On: '.$posted_date.'</span><br/><span>Total Price: $'.$res[0]['total_price'].'</span><br/><span>Service Type: '.$res[0]['category_name'].'</span><br/><span>Task Date and Time:  '.$task_date_time.'</span><br/><span>Estimated Hour(s): '.$res[0]['estimated_time'].'</span><br/><span>Address: '.$res[0]['address'].'</span><br/>';
                        if($res[0]['document']!="")
						{
							
							$ext = array("png", "jpg", "jpeg", "gif", "gif");
							$doc=$res[0]['document'];
							 $file_name=explode(".", $doc);
							
                            if(!in_array(end($file_name), $ext)) 
                            {
    $send .='<a href="'.site_url('/admin/task/download_doc/'.$res[0]['document']).'"></a><p><img src="'.site_url('/assets/images/task_doc.png').'" height="50" width="50"/><br/></p><p style="width: 20%;text-align: center;"><span><i class="fa fa-download"></i></span></p></a>';
					
                            }
                            else
                            {
								$send .='<a href="'.site_url('/admin/task/download_doc/'.$res[0]['document']).'">	<img src="'.site_url('/assets/task/'.$res[0]['document']).'" height="50" width="50"/><p style="width: 20%;text-align: center;"><span><i class="fa fa-download"></i></span></p></a>';
							
							
							}
                         }        
                                        
                               $send .='</div>
                                </div>
                                <div class="col-md-5 col-sm-5 pad_left">
                                	<div class="offer_blk">';
                             $send .='<div class="loading_detail">
                                        <img class="loading_detail_img" style="display:none;" src="'.site_url('assets/images/load-new.gif').'">
                                        </div>';
                              $style='';
	                           $style1='text-align: center';
	                           $response='';
	                           $send.='<input type="hidden" name="task_id" id="task_id" value="'.$data['task_id'].'" >
                                  <input type="hidden" name="user_id" id="user_id" value="'.$_REQUEST['user_id'].'" >';
					  if($user_detail['user_type'] && $user_detail['user_type']=='handyman')
					  {
	                           $form_view='true';
	                           if(!empty($offers))
	                           {
								   $style="visibility:hidden;";
								   $style1="color:#247b58;font-weight:bold;font-size:20px;text-align: center";
								   $response='Offer made for $'.$offers['price'];
							   }
                               $send .='<form action="" class="" id="make_offer" style="'.$style.'">';
                                 

                                $send .='<span class="paid_txt">Paid with Airtasker Pay<img src="'.site_url('assets/images/help.png').'" alt="help" class="mCS_img_loaded"></span>

                                  
                                  
                                  <input type="text" class="offer_price" placeholder="$0" name="offer_price" id="offer_price">
                                    <input type="button"  class="offer_price_button" id="makeoffer_btn"  value="Make an Offer">';
                                        
                                    $send .='</form>'; 
                                }
                               
                                    $send.='<div id="offer_response" style="'.$style1.'">'.$response.'</div></div>';
                            
                           if(($user_detail['user_type'] && $user_detail['user_type']=='handyman'))
                           {
							    
                             $send .= '<div class="dropdown more_option_txt">
                                      <button class="btn dropdown-toggle btn-lg more_option" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        MORE OPTIONS<i class="fa fa-angle-down" aria-hidden="true"></i>
                                      </button>
                                      <input type="hidden" name="follow_value" id="follow_value" value="'.$is_following.'">
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">';
                                     
                                         if($is_following==1)
                                         {
											
											 $send.='<li><a href="javascript:void(0);" class="follow_task"><i class="fa fa-minus-circle" aria-hidden="true"></i>Unfollow</a></li>';
											 
										 }
										 else
										 {
											
											 $send.='<li><a href="javascript:void(0);" class="follow_task"><i class="fa fa-plus-circle" aria-hidden="true"></i>Follow</a></li>';
											 
										 }
                                      
                                        
                                      $send.='<li role="separator" class="divider"></li>
                                       
                                      </ul>
                                    </div>';
                                }    
                               $send .='</div>
                            </div>
                        </div>
                        <div class="description_blk">
                        	<h4>Description</h4>
                            <p>'.$res[0]['description'].'</p>
                             <ul>
                            	<li><a href="javascript:void(0);" style="cursor: default;" id="tot_offer_deail">'.$res[0]['total_offers'].' offer(s)</a></li>
                            </ul>
                        </div>
                      </div>';
                      echo $send;
                      /*
				 if($res=$this->task_model->getUserTasks($data))
				 {
					 echo 'success';
				 }
				 else
				 {
					 echo 'error';
				 }	
				 */
exit;
			
	}
	


	
}

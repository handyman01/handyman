<?php
function sendMail($data)
{
	if($_SERVER['SERVER_NAME'] != '192.168.1.253'){
		$CI =& get_instance();
		$CI->email->from($CI->config->item('admin_email'));
		$CI->email->to($data['to_email']);
		$CI->email->subject($data['subject']);
		$CI->email->message($data['content']);
		$CI->email->set_mailtype("html");
		$CI->email->send();
	}
}

function sendMailtoadmin($data)
{
	if($_SERVER['SERVER_NAME'] != '192.168.1.253')
	{
		$CI =& get_instance();
		$CI->email->from($data['from_email']);
		$CI->email->to($CI->config->item('admin_email'));
		$CI->email->subject($data['subject']);
		$CI->email->message($data['content']);
		$CI->email->set_mailtype("html");
		$CI->email->send();
	}
}


function timeFunc($date){

	$difference = strtotime(date('Y-m-d h:i:s A')) -  strtotime($date);
	if($difference < 3570) $output = round($difference / 60).' minutes ago ';
	elseif ($difference < 86370) $output = round($difference / 3600).' hours ago';
	elseif ($difference < 604770) $output = round($difference / 86400).' days ago';
	elseif ($difference < 31535970) $output = round($difference / 604770).' week ago';
	else $output = round($difference / 31536000).' years ago';
	return $output;

}

function canApplytask($type)
{
	//type can be user or handyman
	$CI =& get_instance();
	$CI->load->model('customer_model');
	$data=array('user_type'=>$type,'email'=>$CI->session->userdata('user')['email']);
	$res=$CI->customer_model->is_email_exist($data);
	if(strtotime(date("Y-m-d H:i:s")) < strtotime($res['block_till']) )
	{
		return 'N';
	}
	else
	{
		return 'Y';
	}
	
}

function get_my_credit()
{
	    $CI =& get_instance();
	    $CI->load->model('customer_model');
		$data=array('user_type'=>'user','email'=>$CI->session->userdata('user')['email']);
		$res=$CI->customer_model->is_email_exist($data);
		return $res['credit'];
	
}

function get_my_assigned_tasks()
{
	    $CI =& get_instance();
	    $CI->load->model('customer_model');
		$res=$CI->customer_model->mytotal_assigned_task($CI->session->userdata('user')['id']);
		return $res['total_task'];
	
}


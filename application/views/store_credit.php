<style>

.profile_right_blk h2
{
    margin-bottom: 0; 
}
.right_form{
	    background: #fff;
    border: 2px solid #a1a1a1;
    border-radius: 5px;	
}

.right_form_button
{
	    padding-bottom: 30px;
}

</style>


<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>

          <div class="profile_right_blk right_form">
                    
                      
				
				  
                <div class="col-md-12 right_form_content">                
                  
                  
                   <div class="col-md-12" >
					 <?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger " style="text-align: center;"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success " style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger" style="text-align: center;"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                       
					 </div>
					 
					 
					 <?php echo form_open(site_url('store_credit_action'),array('class'=>'',"id"=>'store_credit')); ?>                       
					  <div class="form-group">
							  <div class="col-sm-4"><label>Amount to be credit : </label></div>
							  
							  <div class="col-sm-7">  
								  <?php echo form_input(array('name'=>'amount','id'=>'amount', 'class'=>'form-control',"placeholder"=>"Amount",'label' =>false)); ?>
							  </div>
				      </div>
				  
				  
                   
                  <div class="col-lg-12 text-center right_form_button">
                     <a href="javascript:void(0)" id="change_pass" class="sign-up">Send</a>
                  </div>
                  
                   </form>	
                   
                </div>
              
             
           
          </div>
          

			
    <script>
    $('#change_pass').click(function(){
		
		 $("#store_credit").submit();
		
		});
		
	$(document).on('keypress',"#amount",function(e){
	 
	 var vall = String.fromCharCode(e.which);

     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57 )) {
            e.preventDefault();
              // return false;
      }

   });
   
    </script>

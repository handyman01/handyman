  
  <div class="middle-section">
    <div class="container">
      <div class="row">
		
		<?php
	 if ($this->session->flashdata('error')) { ?>
		  <div class="alert alert-danger col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"> <?= $this->session->flashdata('error') ?> </div>
	   <?php } 
	   
	  if ($this->session->flashdata('success')) { ?>
		  <div class="alert alert-success col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1" style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
	   <?php } ?>
		<?php
	  
	   if(validation_errors())
	   {
		   ?>
		   <div class="alert alert-danger col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"><?php echo validation_errors();?></div>
		   <?php
	   }
	  
	  
	   ?>  
  <?php echo form_open(site_url('registerHandyman_action'),array('class'=>'',"id"=>'sign_up_user')); ?>    
                  
        <div class="profile_top_blk">
          <div class="col-md-7 col-xs-offset-2">
            <h2>Create your account to get started</h2>
            
            <ul>
              <li>
                <div class="col-md-3">
                  <label>Name</label>
                </div>
                <div class="col-md-9">
                   <?php echo form_input(array('name'=>'name', 'class'=>'',"placeholder"=>"Name",'label' =>false,'value'=>set_value('name'))); ?>
                </div>
              </li>
              <li>
                <div class="col-md-3">
                  <label>Email</label>
                </div>
                <div class="col-md-9">
                 <?php echo form_input(array('name'=>'email', 'class'=>'',"placeholder"=>"Email",'label' =>false,'value'=>set_value('email'))); ?>
                </div>
              </li>
              <li>
                <div class="col-md-3">
                  <label>Phone</label>
                </div>
                <div class="col-md-9">
                 <?php echo form_input(array('name'=>'phone', 'class'=>'',"placeholder"=>"Phone",'label' =>false,'value'=>set_value('phone'))); ?>
                </div>
              </li>
              <li>
                <div class="col-md-3">
                  <label>Password</label>
                </div>
                <div class="col-md-9">
                  <?php echo form_password(array('name'=>'password', 'class'=>'form-control',"placeholder"=>"Password",'value'=>set_value('password'))); ?>
                </div>
              </li>
              <li>
                <div class="col-md-3">
                  <label>Company</label>
                </div>
                <div class="col-md-9">
                 <?php echo form_input(array('name'=>'company', 'class'=>'',"placeholder"=>"Company",'label' =>false,'value'=>set_value('company'))); ?>
                </div>
              </li>
              <li>
                <div class="col-md-3">
                  <label>Address</label>
                </div>
                <div class="col-md-9">
                 <?php echo form_input(array('name'=>'address', 'class'=>'',"placeholder"=>"Address",'label' =>false,'value'=>set_value('address'))); ?>
                </div>
              </li>
              
              <?php
     
              foreach($services as $service_arr)
              {
				   foreach($service_arr as $service)
				   {
				
              ?>
              <li>
                <div class="col-md-6">&nbsp;</div>
                <div class="col-md-6">
                  <div>
					   <input type="checkbox"  id="checkbox-<?php echo $service['id']?>" class="checkbox-custom main_services" name="main_services[]" value="<?php echo $service['id'];?>"/> 				                     
                    <label for="checkbox-<?php echo $service['id'];?>" class="checkbox-custom-label"><?php echo ucfirst($service['name']);?></label>
                  </div>
                </div>
              </li>
              <?php
		         }
             
		     }
              ?>

              
            </ul>
          
         
          
          </div>
        </div>
        
        
        
        <div class="profile_bottom_blk pad_btm_0" >
          <div class="row">
			  <!-- 
            <div class="col-lg-12">
              <div class="title-sec text-center wow fadeIn animated animated" style="visibility: visible; animation-name: fadeIn;">
                <h5>All Services</h5>
              </div>
            </div>
            -->
            <?php
            
              foreach($services as $service_arr)
              {
				  	foreach($service_arr as $service)
				   {
				  
             ?>
             

            
              
            <div class="al-serv" style="display:none;" id="subservice_<?php echo $service['id'];?>">
            
            <div class="col-lg-12">
              <div class="title-sec text-center wow fadeIn animated animated" style="visibility: visible; animation-name: fadeIn;">
                <b><?php echo ucfirst($service['name']);?></b>
              </div>
            </div>
            
              <ul>
				  
              <?php
              foreach($service['sub_services'] as $sub)
              {
              ?>

                <li class="wow fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><a href="#">
                  <div>
                    <input id="checkbox-<?php echo $sub['id'];?>" class="checkbox-custom" name="subservices[]" type="checkbox" value="<?php echo $sub['id'];?>">
                    <label for="checkbox-<?php echo $sub['id'];?>" class="checkbox-custom-label"><?php echo ucfirst($sub['name']);?></label>
                  </div>
                  </a>
                 </li>
                <?php
			    }
                ?>
              </ul>
            </div>
         
         <?php
	            }
	            }
	      ?>
         

            
            
            
          </div>
        </div>
      
  
  
  
  
            <div class="service-col2 align_right">
              <input value="Submit" type="submit">
            </div>
            <div class="clearfix"></div>
         </form>
      </div>
    </div>
    
    
  </div>
  <script>
   $(".main_services").change(function() {
	   idd=$(this).attr('id');
	   parts_id=idd.split("checkbox-");
    if(this.checked) {
		$("#subservice_"+parts_id[1]).show();
        //Do stuff
    }
    else
    {
		$("#subservice_"+parts_id[1]).hide();
	}
});
 
  
  </script>


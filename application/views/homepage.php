
  <div class="lower-section"> 
    <!--Banner Section-->
    <div class="banner-sec">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="min-conte text-center wow fadeIn">
              <h5>Open the door to better days</h5>
              <p>Providing qualified and friendly services at fair prices</p>
              <div class="banner-butn"> <a class="post-ta" href="<?php echo site_url('post_task');?>">Post Tasks</a>
                <ul>
                  <li><a href="<?php echo site_url('home_maintenance');?>">Home Maintenance</a></li>
                  <!-- <li><a href="javascript:void(0);" id="home_renovation_link">Home Renovation</a></li> -->
                  <li><a href="<?php echo site_url('home_renovation');?>" id="">Home Renovation</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!--How it work-->
    <section class="how-stpes">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="title-sec text-center wow fadeIn">
              <h5>Easy home maintenance & renovation</h5>
              <p>Handyman is a trusted community marketplace for people and <br>
                businesses to outsource tasks, find local services or hire flexible <br>
                staff in minutes - online or via mobile.</p>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="col-sm-3 wow zoomInDown" data-wow-delay=".2s">
            <div class="first-how text-center">
              <center>
                <img src="<?php echo assets_url('images/step-1.png'); ?>" alt="">
              </center>
              <p>Tell us about the task</p>
            </div>
          </div>
          <div class="col-sm-3 wow zoomInDown" data-wow-delay=".4s">
            <div class="first-how text-center">
              <center>
                <img src="<?php echo assets_url('images/step-2.png'); ?>" alt="">
              </center>
              <p>Receive offers from<br>
                trusted workers within minutes</p>
            </div>
          </div>
          <div class="col-sm-3 wow zoomInDown" data-wow-delay=".6s">
            <div class="first-how text-center">
              <center>
                <img src="<?php echo assets_url('images/step-3.png'); ?>" alt="">
              </center>
              <p>Choose the best person<br>
                for the job</p>
            </div>
          </div>
          <div class="col-sm-3 wow zoomInDown" data-wow-delay=".8s">
            <div class="first-how  text-center no-aro">
              <center>
                <img src="<?php echo assets_url('images/step-4.png'); ?>" alt="">
              </center>
              <p>Pay when it’s done</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!--Search-sec-->
    <section class="src-sec">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <div class="search-sec">
              <input type="text" placeholder="What do you need help with?">
              <button type="submit"><img src="<?php echo assets_url('images/search.png'); ?>" alt=""></button>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!--Category-->
    <section class="cate-sec">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-6 no-paddng wow fadeIn" data-wow-delay=".2s"> <a href="#">
            <div class="first-cate category-1">
              <center>
                <img src="<?php echo assets_url('images/cat-1.png'); ?>" alt="">
              </center>
              <b>Moving</b></div>
            </a> </div>
          <div class="col-md-4 col-sm-6 no-paddng wow fadeIn" data-wow-delay=".4s"> <a href="#">
            <div class="first-cate category-2">
              <center>
                <img src="<?php echo assets_url('images/cat-2.png'); ?>" alt="">
              </center>
              <b>House Cleaning</b></div>
            </a> </div>
          <div class="col-md-4 col-sm-6 no-paddng wow fadeIn" data-wow-delay=".6s"> <a href="#">
            <div class="first-cate category-3">
              <center>
                <img src="<?php echo assets_url('images/cat-3.png'); ?>" alt="">
              </center>
              <b>Handyman</b></div>
            </a> </div>
          <div class="col-md-4 col-sm-6 no-paddng wow fadeIn" data-wow-delay=".8s"> <a href="#">
            <div class="first-cate category-4">
              <center>
                <img src="<?php echo assets_url('images/cat-4.png'); ?>" alt="">
              </center>
              <b>Lawn maintenance</b></div>
            </a> </div>
          <div class="col-md-4 col-sm-6 no-paddng wow fadeIn" data-wow-delay="1s"> <a href="#">
            <div class="first-cate category-5">
              <center>
                <img src="<?php echo assets_url('images/cat-5.png'); ?>" alt="">
              </center>
              <b>Furniture assembly</b></div>
            </a> </div>
          <div class="col-md-4 col-sm-6 no-paddng wow fadeIn" data-wow-delay="1.2s"> <a href="#">
            <div class="first-cate category-6">
              <center>
                <img src="<?php echo assets_url('images/cat-6.png'); ?>" alt="">
              </center>
              <b>Appliance repair</b></div>
            </a> </div>
        </div>
      </div>
    </section>
    
    <!--Need Us-->
    <section class="need-help">
      <div class="container">
        <div class="row">
          <h5 class="need-">There When You Need Us</h5>
          <div class="col-sm-4 text-center wow zoomIn" data-wow-delay=".2s">
            <div class="first-help">
              <center>
                <img src="<?php echo assets_url('images/cont-1.png'); ?>" alt="">
              </center>
              <b>Save You Time</b>
              <p>We’ll handle your chores and <br>
                errands so you’re able to accomplish <br>
                more, every day</p>
            </div>
          </div>
          <div class="col-sm-4 text-center wow zoomIn" data-wow-delay=".4s">
            <div class="first-help">
              <center>
                <img src="<?php echo assets_url('images/cont-2.png'); ?>" alt="">
              </center>
              <b>Same-day Help</b>
              <p>Need it done now? We’ll match <br>
                you with a Tasker within minutes <br>
                of your request</p>
            </div>
          </div>
          <div class="col-sm-4 text-center wow zoomIn" data-wow-delay=".6s">
            <div class="first-help">
              <center>
                <img src="<?php echo assets_url('images/cont-3.png'); ?>" alt="">
              </center>
              <b>Trust and Safety</b>
              <p>Our Taskers undergo on extensive <br>
                vetting process and all jobs are <br>
                insured up to $1 million</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!--Become Tasker-->
    <section class="become-tasker">
      <div class="container">
        <div class="row">
          <div class="col-md-5 wow fadeInLeft">
            <div class="sec-becom">
              <h5>Become a Tasker</h5>
              <p>When you work with handyman, you control <Br>
                your schedule, your rates, and the type of <Br>
                work you want to do.</p>
              <p>Find the jobs you love and the work/life <Br>
                balance that fits you.</p>
              <a href="<?php echo site_url('register_handyman');?>">Register Now</a> </div>
          </div>
        </div>
      </div>
    </section>
  </div>
 <script>
	 $('#home_renovation_link').click(function(){
		
    $("html, body").animate({ scrollTop: $('.search-sec').offset().top }, 1000);
		 });
 </script> 
  
  
  
  

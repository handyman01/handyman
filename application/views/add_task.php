<style type="text/css">

#map{
height: 300px;
	width:100%;
	border: 1px solid #fff;
	box-shadow: 0px 0px 0px 8px rgba(234,234,234,0.9);
}

.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}
.dropdown{
padding-bottom:18px;
}
</style>
<?php
//echo '<pre>';
//print_r($services);
//die;
?>
<link href="<?php echo assets_url('css/jquery.datetimepicker.css'); ?>" rel="stylesheet" type="text/css"/>


    <!-- / Services Part -->
    <section class="service-part">

                       
      <div class="tab_part">
        <div class="container">
          <ul>
            <li class="wow fadeIn"><a href="#" id="step_1"><i>01</i> TASK DETAILS</a></li>
            <li class="wow fadeIn"><a href="#" id="step_2" ><i>02</i> GET PRICE</a></li>
            <li class="wow fadeIn"><a href="#" id="step_3" ><i>03</i> COMFIRM &amp; BOOK</a></li>
          </ul>
        </div>
      </div>
      	
      <div class="container">
 		<?php
		  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
			    
		  <?php echo form_open_multipart(site_url('add_task_action'),array('class'=>'',"id"=>'taskForm')); ?>  
		  <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_detail['id'];?>">
		  <input type="hidden" name="categ_id" id="categ_id" value=""/>
		  <input type="hidden" name="categ_parent_slug" id="categ_parent_slug" value=""/>
		  <!-- step 1 -->
        <div class="service_col wow fadeIn" id="step_div_1">
			<div class="alert alert-danger col-sm-12 " style="display:none;"></div>
          <h3>Service </h3>

  
          <div class="row">

            <div class="form-group col-md-6 col-sm-6 col-xs-12">



<div class="dropdown">
      <button class="btn btn-default dropdown-toggle service_btn" type="button" data-toggle="dropdown" data-submenu="" aria-expanded="false">
        Services <span class="caret"></span>
      </button>

      <ul class="dropdown-menu ">


<li class="dropdown-submenu">
  <a class="parent-menu" style="pointer-events: none;" tabindex="0" data-value="<?php echo $services['home_maintenance'][0]['slug'];?>"><?php echo ucfirst($services['home_maintenance'][0]['name']);?></a>

  <ul class="dropdown-menu">
	  <?php
	  foreach($services['home_maintenance'][0]['sub_services'] as $val)
	  {
		  ?>
		   <li><a tabindex="0" data-value="<?php echo $val['id'];?>"><?php echo ucfirst($val['name']);?></a></li>
		 <?php
	  }
	  ?>

  </ul>
</li>

<li class="dropdown-submenu">
  <a tabindex="0" class="parent-menu" style="pointer-events: none;" data-value="<?php echo $services['home_renovation'][0]['slug'];?>"><?php echo ucfirst($services['home_renovation'][0]['name']);?></a>

  <ul class="dropdown-menu">
	  <?php
	  foreach($services['home_renovation'][0]['sub_services'] as $val)
	  {
		  ?>
		   <li><a tabindex="0" data-value="<?php echo $val['id'];?>"><?php echo ucfirst($val['name']);?></a></li>
		 <?php
	  }
	  ?>
  </ul>
</li>

</ul>

    </div>
 

       
       <h3>Task Type </h3>
       
   <select onclick="task_type_action(this.value)" class="custom-select" name="task_type" id="task_type">
				  <option value="free">Free</option>
				  <option value="guaranteed">Guaranteed</option>
				 
 </select>
 
 
<!--
  
              <select onclick="getPrice(this.value)" class="custom-select" name="categ_id" id="categ_id">
				  <option>Home maintenance services*</option>
				  <?php
				  /*
				  if(isset($services[0]['sub_services']) && !empty($services[0]['sub_services']))
				  {
					  foreach($services[0]['sub_services'] as $val)
					  {
				  ?>
                <option  value="<?php echo $val['id'];?>"><?php echo ucfirst($val['name']);?></option>

                <?php
			          }
			}
			*/
			?>
              </select>
              
              -->
              
            </div>
          </div>
        </div>
       
        
        <div class="clear"></div>
        
       
        <div class="service-col2 wow fadeIn">
          <h3 class="serv_heading">Task Location</h3>
          <div class="check_box_part">
            <div>
              <input id="is_residential" class="checkbox-custom" name="is_residential" type="checkbox" checked>
              <label for="is_residential" class="checkbox-custom-label">Residential</label>
            </div>
            <div>
              <input id="is_commercial" class="checkbox-custom" name="is_commercial" type="checkbox">
              <label for="is_commercial" class="checkbox-custom-label">Commercial</label>
            </div>
          </div>
          <div class="row">
			  
			
			<div class="col-md-2 col-sm-2">
              <input type="text" id="address_unit" name="address_unit" placeholder="Unit #">
            </div>
            
            <div class="col-md-8 col-sm-8">
              <input type="text" id="address" name="address" placeholder="Enter address">      
              <p style="padding-bottom: 22px;">Fill your complete address and than press enter to locate your location on map below.</p>       
            </div>
         
            
            
            <div class="clear"></div>
            <div class="col-md-12 col-sm-12">
             <div class="col-md-2">
		     </div>
		     <div class="col-md-8">
				 				<div id="map"></div>
		     </div>
		     <div class="col-md-2">
			 </div>
             <input type="hidden" name="lat" id="latbox" value=""/>
			  <input type="hidden" name="lon" id="lngbox" value=""/>
				

            </div>
            
          
			 <div class="clear"></div>
            <div class="col-md-12 text-center">
              <input type="button" value="Next" id="button_step_1">
            </div>
          </div>
        </div>
    
        <div class="clear"></div>
        
         <!-- step 1 ends here -->
         

   
     <!-- step 2 -->
        <div class="service-col2 wow fadeIn"  id="step_div_2">
			
         
          <div class="row">
			 <h3 class="serv_heading" style="margin-left: 15px;">Task Title</h3>
            <div class="col-md-12 col-sm-12">
              <input type="text" name="title" id="title" placeholder="Task Title" value=""/>
              <p style="padding-bottom:25px;font-size:16px;"><span id="count"></span> characters left</p>
            </div>
			 <h3 class="serv_heading" style="margin-left: 15px;">Tell us about your task</h3>
            <div class="col-md-10 col-sm-10">
              <textarea name="description" id="description" placeholder="Example: We have a few lightbulbs that need replacing. We’ve got thelightbulbs, but we’d need you to 
provide the ladder. "></textarea>
            </div>
            <div class="clear"></div>
             <h3 class="serv_heading" style="margin-left: 15px;">Upload Document/Image( If any)</h3>
              <div class="col-md-10 col-sm-10">
				
			   <input onchange='check_upload()' id="file" type="file" name="file" class="file">
             </div>
            
             <div class="clear"></div>
            <div class="col-md-12 text-center">
              <input type="button" value="Next" id="button_step_2">
            </div>
            <div class="clear"></div>
          </div>
        </div>
    
     <!-- step 2 ends -->
   
   


        <div class="clear"></div>
        
    <!-- step 3 -->
        <div class="service-col2 wow fadeIn"  id="step_div_3">
          <h3 class="serv_heading">Date and time</h3>
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="date-form">
				  
                 <input type="text" id="datetimepicker2" name="task_date" placeholder="Date"/>   
          
                <div class="time_select">
                 	<input type="text" id="datetimepicker1" name="task_time" placeholder="Time"/>
                </div>
                <label class="time-lable">Estimated hour(s)</label>
                <div class="time_select">
                 	<input type="text" id="estimated_time" name="estimated_time" placeholder="Estimated hour(s)"/>
                 	<span style="color: green;font-size: 16px;"></span>
                 	<input type="hidden" name="hourly_price" id="hourly_price" value=""/>
                 	<input type="hidden" name="total_deposit_amt" id="total_deposit_amt" value=""/>
                </div>
                 <div class="time_select1">
					 <span style="color: green;font-size: 16px;">Total amount as deposit: $<span id="hourly_pprice">0</span></span>
				</div>
              </div>
            </div>
            
            <div class="col-md-6 col-sm-6 text-center">
              <div class="QuickAssign">
                <h1>Quick Assign</h1>
                <div class="hr_text"><span>$56</span>/Hr</div>
                <h3>How Quick Assign will work:</h3>
                <p>We will notify qualified Taskers in your area about your 
                  task based on the information you’ve provided.</p>
              </div>
            </div>
            <div class="clear"></div>
            <div class="col-md-12 text-center">
				<!-- if user have some credit show this option-->
				<?php
				if(get_my_credit()>0)
				{
				?>
				<div class="check_box_part is_credit_part">
					<div>
					  <input id="is_credit" class="checkbox-custom" name="is_credit" type="checkbox" checked>
					  <label for="is_credit" class="checkbox-custom-label">Use your account credit ?</label>
					</div>
              </div>
              <?php
		         }  
		       ?>
              
              <input type="button" value="Continue" id="button_step_3">
              <div class="alert alert-success col-sm-12 "   style="display:none;"></div>
             <img class="loading_onsave" style="display:none;"  src=<?php echo assets_url('css/images/loading.gif');?>>
            </div>
            <div class="clear"></div>
          </div>
        </div>
       
     <!-- step 3 ends -->
     
        </form>
 
        
        <div class="clear"></div>
        <div class="service-col2 wow fadeIn credit-form" id="step_div_4">
          <div class="row">
			  
  
            <div class="col-md-12 col-sm-12 col-right">
              <div class="text-row">
                <label>Date &amp; Time</label>
                <p id="task_id_review" style="display:none;"></p>
                <p id="confirm_url" style="display:none;"></p>
                <p id="task_date_review">Mon, July11 (11:30am)</p>
              </div>
              <div class="text-row">
                <label>Task Location</label>
                <p id="task_location_review">Unit 208, 500 Alden, Markham</p>
              </div>
              <div class="text-row">
                <label>Task Title</label>
                <p id="task_title_review"></p>
              </div>
              <div class="text-row">
                <label>Task Description</label>
                <p id="task_desc_review"></p>
              </div>
              
              <div class="text-row">
                <label>Total payable amount</label>
                <p id="task_tottal_amount_review"></p>
              </div>
          
              <div class="text-row">
                <label>Total amount as deposit</label>
                <p id="task_min_amount_review"></p>
              </div>
          
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-center">
				
              <input type="button" value="Conﬁrm and Book"  id="confirm_btn">
              
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="note_text"><span>You are charged only after your task is completed.</span> Tasks have a one-hour minimum. </div>
        <div class="note_text">If you cancel your task within 24 hours of the scheduled start time, you will be charged a one-hour 
          cancellation fee at the Tasker's hourly rate. </div>
      </div>
    </section>
 <script src="<?php echo assets_url('js/jquery.datetimepicker.full.js'); ?>"></script> 
 
  <script>
	  var canApplytask='<?php echo canApplytask('user');?>';
 $('body').bind('cut copy paste', function (e) {
      e.preventDefault();
   });

	var maxchar = 100;
var i = document.getElementById("title");
var c = document.getElementById("count");
c.innerHTML = maxchar;
    
i.addEventListener("keydown",count);

function count(e){
	 var keyCode = e.keyCode;

    var len =  i.value.length;
    if (len >= maxchar){
		 if(keyCode!=8)
         {
             e.preventDefault();
		 }
    } else{
       c.innerHTML = maxchar - len-1;   
    }
}


  
//var _validFileExtensions = [".jpg", ".jpeg", ".gif", ".png",".pdf",".doc"];    
function check_upload() {
	 $(".alert-danger").hide();
	 var image_file = document.getElementById("file");
	 var size = image_file.files[0].size/1024/1024;
	
    //checking file name and extension here
   // var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif|.jpeg|.pdf|.doc|.docx|.txt|.xls|.xlsx|.odt|.ods|.csv)$");
   var extensions = ["jpg", "jpeg", "txt", "png","gif","pdf","doc","docx","txt","xls","xlsx","odt","ods","csv"];
   var extension = image_file.files[0].name.replace(/.*\./, '').toLowerCase();

	if (extensions.indexOf(extension) < 0) { 
				$("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		$(".alert-danger").text("Supported extension are jpg, png, gif, jpeg, pdf, doc, docx, txt, xls, xlsx, odt, ods, csv").show();
        $("#file").val('');
        return false;
	}
	else
	{
		 if(size > 2){
			 $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
			 $(".alert-danger").text('File size exceed the size of 2MB').show();
			 $("#file").val('');
			 return false;
		
		}
	}

}
	$('.time_select1').hide();  
	$('.is_credit_part').hide();
	 function task_type_action(val)
	 {
		 $('.time_select1').hide();
		 if(val=='free')
		 {
			 $('.time_select1').hide();
			 $('.is_credit_part').hide();
		 }
		 else
		 {
			$('.time_select1').show();
			 $('.is_credit_part').show();
		 }
	 } 
	 
	//get price according to service  
	function getPrice(id){
		$("#estimated_time").val('');
		$("#hourly_price").val(0);
		$("#hourly_pprice").text('');
		if(id !=''){
			jQuery.ajax({ 
				url: '<?php echo site_url('get_price'); ?>',
				type: 'POST',
				data:{'id':id},	
				dataType: "json",  	
				success: function(data) {

					$("#hourly_price").val(data.hourly_price);
					$("#total_deposit_amt").val(data.hourly_price*data.deposit_hrs);
					$("#hourly_pprice").text(data.hourly_price*data.deposit_hrs);
					
				}
			});
		}
	}  
	  
   $(document).ready(function(){
	   




  document.getElementById('address').onblur = function () {

    google.maps.event.trigger(this, 'focus');
  
};

	$('#step_div_2,#step_div_3,#step_div_4').hide();   
	});
	
   $("#button_step_1").click(function() {
	   
	   $("#step_div_2").show();
	    $("html, body").animate({ scrollTop: $("#step_div_2").offset().top }, 1000);
	  
	   
   });	
   
   $(document).on('click','#button_step_2',function(){	
	       $("#step_div_3").show();
	       	$("html, body").animate({ scrollTop: $("#step_div_3").offset().top }, 1000);
	   
	   });
   
   $("#button_step_3").click(function() {
	 if(canApplytask=='Y')
	 {
	    
	   if($('#hourly_price').val()==0)
	   {
		   $('#confirm_btn').hide();
	   }
	   $('.loading_onsave').show();
	  //validate all fields
	  if ($("#categ_id").val() == "") 
	  {
		   $('.loading_onsave').hide();
		   $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		  $('.alert-danger').text('Please choose maintenance service !').show();
		  return false;
	  }
	  else if ($("#task_type").val() == "") 
	  {
		   $('.loading_onsave').hide();
		   $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		  $('.alert-danger').text('Please choose task type !').show();
		  return false;
	  }
	  else if(!$("#is_residential").is(':checked') && !$("#is_commercial").is(':checked'))
	  {
		  $('.loading_onsave').hide();
	      $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		  $('.alert-danger').text('Please check atleast one task location !').show();
		  return false;	  
	  }	    
	  else if($("#title").val()=="")
	  {
		  $('.loading_onsave').hide();
	      $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		  $('.alert-danger').text('Please enter task title !').show();
		  return false;	  
	  }
	  else if($("#address").val()=="")
	  {
		  $('.loading_onsave').hide();
	      $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		  $('.alert-danger').text('Please enter address !').show();
		  return false;	  
	  }
	  else if($("#description").val()=="")
	  {
		  $('.loading_onsave').hide();
	      $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		  $('.alert-danger').text('Please tell us about your task !').show();
		  return false;	  
	  }
	  else if($("#datetimepicker2").val()=="")
	  {
		  $('.loading_onsave').hide();
	      $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		  $('.alert-danger').text('Please enter task date !').show();
		  return false;	  
	  }
	  else if($("#datetimepicker1").val()=="")
	  {
		  $('.loading_onsave').hide();
	      $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		  $('.alert-danger').text('Please enter task time !').show();
		  return false;	  
	  }
	  else if($("#estimated_time").val()=="")
	  {
		  $('.loading_onsave').hide();
	      $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		  $('.alert-danger').text('Please enter estimated_time !').show();
		  return false;	  
	  }
	  else if(($("#latbox").val()=="" && $("#lngbox").val()=="") || ($("#latbox").val()=="") || ($("#lngbox").val()==""))
	  {
		     $('.loading_onsave').hide();
		  	 $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		     $('.alert-danger').text('Please enter valid address !').show();
		     return false;	  
	  }
	  else
	  {
		
						/* prepare review section */
						
                        
                        
		  //ajax to save data
		  var form = new FormData($("#taskForm")[0]);
		  jQuery.ajax({ 
				url: '<?php echo site_url('add_task_action'); ?>',
				method: "POST",
				dataType: 'json',
				data: form,
				processData: false,
				contentType: false,
				success: function(data1) {
				//	console.log(data1);
					if(data1.status == 'success'){
						$('.loading_onsave').hide();
						$('.alert-success').text('Your task has been saved !').show();
						if(data1.task_type=='g' && data1.send_paypal!=0 && data1.paypal_price!=0)
						{
						  location.href = data1.confirm_url;	//redirect to paypal for payment guranteed task
					    }
					    else
					    {
							 window.setTimeout(function(){location.href="<?php echo site_url('dashboard'); ?>";},2000)
						  
						}

						//$('#task_date_review').text(data1.date+' ('+data1.time+') ');
						//$('#task_location_review').text($('#address_unit').val()+', '+$('#address').val());
						//$('#task_title_review').text($('#title').val());
						//$('#task_desc_review').text($('#description').val());
						//$('#task_id_review').text(data1.task_id);
						//$('#confirm_url').text();
						//$("#step_div_4").show();
						//$("html, body").animate({ scrollTop: $("#step_div_4").offset().top }, 1000);
						

					}
					else if(data1.status == 'redundant'){
						$('.loading_onsave').hide();
						$('.alert-success').text('Your task has been already saved !').show();
					//	$("#step_div_4").show();
						//$("html, body").animate({ scrollTop: $("#step_div_4").offset().top }, 1000);
						
					}
					else if(data1.status == 'error'){
						$('.loading_onsave').hide();
						$('.alert-error').text('Something went wrong !').show();
						 $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);							
					}
				}
			});
			
			
	  }
	    
	 }
	 else
	 {
		  $('.loading_onsave').hide();
	      $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		  $('.alert-danger').text('You are not allowed to post any task, please contact to administrator !').show();
		  return false;	  
		
	 }
	   
   });

   $("#estimated_time").keyup(function() {
        var vall = $('input:text[name=estimated_time]').val();
		  $(".time_select span").html('<img src="http://p.brsoftech.net/handyman/assets/css/images/loading.gif">');
		  

	   	  var amount =vall * $("#hourly_price").val(); 
	      $(".time_select span").text("Estimated total amount is $"+amount.toFixed(2));
    });
    
  $("#estimated_time").keypress(function (e) {
	 
	 var vall = String.fromCharCode(e.which);

     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            e.preventDefault();
              // return false;
      }

   });

     
   $(".final_step").click(function() {
	   idd=$(this).attr('id');
	   parts_id=idd.split("checkbox-");
    if(this.checked) {
		$("#subservice_"+parts_id[1]).show();
        //Do stuff
    }
    else
    {
		$("#subservice_"+parts_id[1]).hide();
	}
});
 
  
  </script>   
  
<script>
	
	$('#datetimepicker1').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:5
});
$("#datetimepicker2").on("keydown keypress keyup", false);


	var dateToday = new Date();
	$('#datetimepicker2').datetimepicker({
	timepicker:false,
	format:'Y/m/d',
	minDate: dateToday


});
</script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCd_Tujq99uHzmtznB4SqOWvjy6KHmDpzQ&amp;libraries=places"></script>
<script>
	  function initialize() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('address');
        var searchBox = new google.maps.places.SearchBox(input);
       // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
			
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
			 $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		     $('.alert-danger').text('Please enter valid address !').show();
		     //return false;	  
		  
			  //console.log('dsfghdsgf');
            return;
          }
		 
          // Clear out the old markers.
          for (var i = 0, marker; marker = markers[i]; i++) {
			marker.setMap(null);
			}
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
               var pinColor = "FE7569";
    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
        new google.maps.Size(21, 34),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));
   
			  var marker = new google.maps.Marker({
				draggable: true,
				map: map,
				icon: pinImage,
				title: place.name,
				position: place.geometry.location
				
			  });
			  
				document.getElementById("latbox").value = marker.getPosition().lat();
				document.getElementById("lngbox").value = marker.getPosition().lng();
				
				google.maps.event.addListener(map, 'click', function(event) {
					
					marker.setPosition(event.latLng);
					document.getElementById("latbox").value = event.latLng.lat();
					document.getElementById("lngbox").value = event.latLng.lng();
					
					
			   });
				
				google.maps.event.addListener(marker, 'dragend', function (event) {
				document.getElementById("latbox").value = event.latLng.lat();
				document.getElementById("lngbox").value = event.latLng.lng();
			
				
				});
				
			  markers.push(marker);
			
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
  google.maps.event.addDomListener(window, 'load', initialize());
  
  
  $(function(){

    $(".dropdown-menu li a").click(function(){

      $(".btn:first-child").text($(this).text());
      $(".btn:first-child").val($(this).text());
      $("#categ_id").val($(this).data('value'));
      $("#categ_parent_slug").val($(this).parents(".dropdown-submenu").find('a').data('value'));
      
      getPrice($(this).data('value'));

   });

$('#confirm_btn').click(function(){	
	//location.href = $('#confirm_url').text();	
});
 
});


</script>



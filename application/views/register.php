 
    <div class="lower-section">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="res-box">
              <p class="cret-your"> <a href="<?php echo site_url('login');?>">Log In</a> | Create your account to get started</p>
              <div class="row">
				<?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1" style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                       
                      
				 <?php echo form_open(site_url('register_action'),array('class'=>'',"id"=>'sign_up_user')); ?>  
				  
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                  <div class="col-lg-12"> </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>First name</label>
                       <?php echo form_input(array('name'=>'first_name', 'class'=>'form-control',"placeholder"=>"Firstname",'label' =>false,'value'=>set_value('first_name'))); ?>
                       
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Last name</label>
                       <?php echo form_input(array('name'=>'last_name', 'class'=>'form-control',"placeholder"=>"Lastname",'label' =>false,'value'=>set_value('last_name'))); ?>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label>Email Address</label>
                      <?php echo form_input(array('name'=>'email', 'class'=>'form-control',"placeholder"=>"Email",'label' =>false,'value'=>set_value('email'))); ?>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label>Password</label>
                      <?php echo form_password(array('name'=>'password', 'class'=>'form-control',"placeholder"=>"Password",'value'=>set_value('password'))); ?>
                    </div>
                  </div>
                  <div class="col-lg-12 text-center">
                    <p class="terms-con">By signing up, you are agreeing to <a href="<?php echo site_url().'pages/content/terms-and-conditions';?>">Terms and Conditions</a></p>
                     <a href="javascript:void(0)" id="sign_up" class="sign-up">Sign me up</a>
                  </div>
                </div>
              
              </form>
              
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
    $('#sign_up').click(function(){
		
		 $("#sign_up_user").submit();
		
		});
    </script>
 
 

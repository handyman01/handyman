<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>:: Handyman ::</title>
<link href="<?php echo assets_url('css/bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/animate.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/styles.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/responsive.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo assets_url('js/jquery.min.js'); ?>"></script> 
<link href="<?php echo assets_url('css/rating.css'); ?>" rel="stylesheet" type="text/css"/>
<script>
	var rating_imgg='<?php echo assets_url('css/images'); ?>';
</script>
<script src="<?php echo assets_url('js/rating.js'); ?>"></script> 

<style>
.codexworld_rating_widget
{
pointer-events: none;
padding-bottom: 15px;
}
.rating_blkk
{
	    padding-bottom: 40px;
}
.tabbable-line > .nav-tabs > li.active
{
	border:none;
}
	</style>
</head>
<body class="time-body">
	<div class="main-div">

			
 <?php if($header) echo $header ;?>

  <div class="profile_middle_container">
    <div class="container">
      <div class="profile_blk">
        <div class="cover_blk">
          <div class="img_blk"> <img src="<?php echo site_url('/assets/users/'.$details['image']);?>" alt="user">  </div>
        </div>
        <div class="profile_detail_blk">
          <div class="content_blk">
            <div class="col-md-6">
              <div class="info_blk">
                <h2>
				<?php
                if(isset($details['first_name']))
                {
					echo $details['first_name'].' '.$details['last_name'];
				}
				else
				{
					echo $details['name'];
				}
                ?>
                </h2>
                
                <p>Last online <?=timeFunc($details['last_login']); ?></p>
                <?php
                if(isset($details['address']))
                {
                ?>
                <p><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo  $details['address'];?></p>
                <?php
			     }
                ?>
                <p>Member since  <?php echo date('M d,Y',strtotime($details['created']));?></p>
                <!-- <span><i class="fa fa-flag-o" aria-hidden="true"></i><a href="#">Report this Member</a></span> -->
                
                 </div>
            </div>
            <div class="col-md-6">
              <div class="info_blk align_right">
                <div class="tabbable-panel">
                  <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                      <li class="active"><a href="#tab_default_1" data-toggle="tab"></a></li>
                     
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_default_1">
                      	<span class="rating_blk">
							 <input name="rating" value="" id="rating_star2" class="rating_star" type="hidden" postID="2" /> 

							</span>
                        <p><?php echo $details['avg_rating'];?> Star - <?php echo count($details['reviews']);?> Review(s)</p>
                       <!--  <p>66% Completion Rate</p>-->
                        <span><?php echo $details['total_completed'];?> completed task(s)</span>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
   		 <?php
          if($details['about_urself'] && $details['about_urself']!="")
          {
          ?>       
          <div class="content_blk">

          <h3>About</h3>

            	<div class="about_blk">
					<ul>
					<li><?php
                	echo $details['about_urself'];
                	?></li>
					</ul>
                	
                </div>

          </div>
                       <?php
                
			}
			?>   

    	 <?php
          if($details['education']!="" || $details['speciality']!="" || $details['languages']!="" || $details['workings']!="")
          {
          ?>   
 
          <div class="content_blk">
          <h3>Skills</h3>
          <div class="row">
          	<div class="col-md-3">
            	<div class="skills_blk">
                <h4>Education</h4>
                  <ul>
					  <li>
							<?php
							echo ($details['education']!='' ? $details['education']:'-');
							?>
					  </li>
				  </ul>
                	
                </div>
            </div>
            <div class="col-md-3">
            	<div class="skills_blk">
                <h4>Specialities</h4>
                     <ul>
					  <li>
							<?php
							echo ($details['speciality']!='' ? $details['speciality']:'-');
							?>
					  </li>
				  </ul>

                </div>
            </div>
            <div class="col-md-3">
            	<div class="skills_blk">
                <h4>Languages</h4>
                      <ul>
					  <li>
							<?php
							echo ($details['languages']!='' ? $details['languages']:'-');
							?>
					  </li>
				  </ul>
				  
                	
                </div>
            </div>
            <div class="col-md-3">
            	<div class="skills_blk">
                <h4>Work</h4>
                      <ul>
					  <li>
							<?php
							echo ($details['workings']!='' ? $details['workings']:'-');
							?>
					  </li>
				  </ul>
                </div>
            </div>
           </div>
          </div>
          
          
          
       <?php
	  }
	  ?>
          <div class="content_blk">
          	<h3>Reviews</h3>
            <div class="row">
            <div class="col-md-12 rating_blkk">
  
                    
                	<div class="info_blk align_left">
						 <input name="rating" value="" id="rating_star1" class="rating_star" type="hidden" postID="1" /> 
						
                        <p><?php echo $details['avg_rating'];?> Star -  <?php echo count($details['reviews']);?> Review(s)</p>
                       <!--  <p>66% Completion Rate</p> -->
                        <span> <?php echo $details['total_completed'];?> completed task(s)</span>
                    </div>
                    
               
             
                </div>
            </div>
            <div class="row">

              
              
              
				<?php 

				$count=0;
				foreach($details['reviews'] as $val)
				{
					
					
					?>
				<div class="col-md-6">
                	<div class="media">
                      <div class="media-left">
                        <a href="#">
                       					<?php			
						if($val['detail'][0]['document']!="")
						{
							
							$ext = array("png", "jpg", "jpeg", "gif", "gif");
							$imgg=$val['detail'][0]['document'];
							 $file_name=explode(".", $imgg);
							
                            if(in_array(end($file_name), $ext)) 
                            {
								?>
								 <img src="<?php echo site_url('/assets/task/'.$imgg);?>" height='80' width='80' class="" alt="">
								<?php
					
                            }
                            else
                            {
								?>
								  <img src="<?php echo site_url('/assets/images/notask.png');?>" class="img-responsive" alt="">
								<?php
							
							
							}
                         }
                         else
                         {
							 ?>
							   <img src="<?php echo site_url('/assets/images/notask.png');?>" class="img-responsive" alt="">
							 <?php
						 }     
						
                         ?>
                        
                        
                        </a>
                      </div>
                      <div class="media-body" style="color:#758389;">
                      <span class="rating_blk">
						   <input name="rating" value="" id="task_rating_star<?php echo $count;?>" class="rating_star" type="hidden" postID="1" /> </span>
                      <span class="time_txt"><?php echo timeFunc($val['created']) ?></span>
                        <h4 class="media-heading"><?php echo ucfirst($val['detail'][0]['title']);?></h4>
                     <span style="color:#008fb6;"> <?php echo $val['detail'][0]['first_name'];?></span> said <?php echo $val['review'];?>
                      </div>
                    </div>
                </div>
                
	
                
                
					<?php
					$count++;
				}
				?>

               
               

           
           
            </div>
       
       
       
       
          </div>
        </div>
      </div>
    </div>
  </div>
 
	     
		     
		     <?php if($footer) echo $footer ;?>
  </div>
 
 

   
         
         
         
</body>

<script src="<?php echo assets_url('js/bootstrap.min.js'); ?>"></script> 
<script src="<?php echo assets_url('js/wow.min.js'); ?>"></script> 
<script>


	var total_task='<?php echo count($details['reviews']);?>';
	var tasks=<?php echo json_encode($details['reviews']);?>;
 new WOW().init();
   
    $("#rating_star1").codexworld_rating_widget({
        starLength: '5',
        initialValue: '<?php echo $details['avg_rating'];?>',
        callbackFunctionName: false,
        imageDirectory: 'images/',
        inputAttr: 'postID'
   
});
    $("#rating_star2").codexworld_rating_widget({
        starLength: '5',
        initialValue: '<?php echo $details['avg_rating'];?>',
        callbackFunctionName: false,
        imageDirectory: 'images/',
        inputAttr: 'postID'
   
});

 for (var key in tasks) {
	
	   
	 	$("#task_rating_star"+key).codexworld_rating_widget({
        starLength: '5',
        initialValue: tasks[key].rating,
        callbackFunctionName: false,
        imageDirectory: 'images/',
        inputAttr: 'postID'
	});
        
 }



</script>
</body>
</html>

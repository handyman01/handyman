			<!-- header start-->
		  <header>
			<div class="container">
			  <div class="row">
				<div class="col-lg-12">
				  <nav class="navbar navbar-default top-menu">
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
					  <a class="page-logo" href="<?php echo base_url('/'); ?>"><img src="<?php echo assets_url('images/logo.png'); ?>" alt=""></a> </div>
					<div id="navbar" class="navbar-collapse collapse pull-right" aria-expanded="false" style="height: 1px;">
					  <ul class="tom-enu">
				<?php
				if(!$this->session->userdata('user'))
				{
					?>
						<li><a href="<?php echo base_url('/login'); ?>"><em>&nbsp;</em>Login<i>&nbsp;</i></a></li>
						<li><a href="<?php echo base_url('/register'); ?>"><em>&nbsp;</em>Sign up<i>&nbsp;</i></a></li>
						<li><a href="<?php echo base_url('/register_handyman'); ?>"><em>&nbsp;</em>Become a Tasker<i>&nbsp;</i></a></li>
					
	            
				<?php
			    }
				else
				{
					if($this->session->userdata('user')['user_type']=='user') $var= 'u';
					else $var= 'h';
				?>	
				
				  <li> <em>&nbsp;</em>Welcome &nbsp;&nbsp;<a href="<?php echo base_url('/user_profile/'.$var.'/'.$this->session->userdata('user')['id']); ?>"><?php echo ucfirst($this->session->userdata('user')['firstname']);?></a><i>&nbsp;</i> </li>
                  
                  <li class="bold-font"><a href="<?php echo base_url('/dashboard'); ?>"><em>&nbsp;</em>Account<i>&nbsp;</i></a></li>   
                  <?php
                  if($this->session->userdata('user')['user_type']=='user')
                  {
					  ?>
				                  <li class="bold-font"><a href="<?php echo base_url('/switch_user/handyman'); ?>"><em>&nbsp;</em>Switch To Handyman<i>&nbsp;</i></a></li>
                  	  <?php
				  }
				  else
				  {
					  ?>
							    <li class="bold-font"><a href="<?php echo base_url('/switch_user/user'); ?>"><em>&nbsp;</em>Switch To Customer<i>&nbsp;</i></a></li>		 
						<?php	                   
				  }
                  ?>

                  <!--<li class="bold-font"><a href="<?php echo base_url('/dashboard'); ?>"><em>&nbsp;</em>Dashboard<i>&nbsp;</i></a></li>-->
                  <li class="bold-font"> <em>&nbsp;</em>Credit :$ <?php 
                  $my_credit=get_my_credit();
                  echo ($my_credit==0 ? 0:$my_credit);?><i>&nbsp;</i> </li>
                  <li class="bold-font"><a href="<?php echo base_url('/logout'); ?>"><em>&nbsp;</em>Logout<i>&nbsp;</i></a></li>
              
              <?php
			  }
			  ?>
                  <!--  if login header-->	
					  </ul>
					</div>
				  </nav>
				</div>
			  </div>
			</div>
		  </header>
		  <!-- header ends-->

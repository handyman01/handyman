<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>:: Handyman ::</title>
<link href="<?php echo assets_url('css/bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/animate.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/styles.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/responsive.css'); ?>" rel="stylesheet" type="text/css"/>

</head>
<body class="time-body">
	<div class="main-div">
		 <?php if($header) echo $header ;?>
		 <?php if($middle) echo $middle ;?>
		 <?php if($footer) echo $footer ;?>
  </div>
 
</body>
<!--Time line End--> 


</body>
</html>

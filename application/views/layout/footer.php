  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="footer-conta">
            <h5 class="fot-tit">Contact Us</h5>
            <p>We would love to here from you </p>
            <span>
			<?php 
			foreach($footer['links'] as $foo){
				echo '<a href="'.site_url('/pages/content/'.$foo['slug']).'">'.$foo['name'].'</a><br/>';
			
			}
			?>
			</span> </div>
        </div>
        <div class="col-md-4">
          <ul class="con-det">
            <li><i><img src="<?php echo assets_url('images/locat.png'); ?>" alt=""></i> 23 Crescent Road Mount Pleasant, 3452</li>
            <li><i><img src="<?php echo assets_url('images/locat-1.png'); ?>" alt=""></i> email@gmail.com</li>
            <li><i><img src="<?php echo assets_url('images/locat-2.png'); ?>" alt=""></i> (021) 6543232</li>
          </ul>
        </div>
        <div class="col-md-4">
          <div class="folwo">
            <h5 class="fot-tit">Follow Us</h5>
            <p>Get latest news and proposals</p>
            <span> <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </span> </div>
        </div>
      </div>
    </div>
  </footer>
<!--http://stackoverflow.com/questions/2660201/what-parameters-should-i-use-in-a-google-maps-url-to-go-to-a-lat-lon 

https://gearside.com/easily-link-to-locations-and-directions-using-the-new-google-maps/
https://www.google.com/maps/dir/43.2345,-76.2345/43.12345,-76.12345

http://maps.google.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=14&size=512x512&maptype=roadmap
&markers=color:blue|label:S|40.702147,-74.015794&markers=color:green|label:G|40.711614,-74.012318
&markers=color:red|color:red|label:C|40.718217,-73.998284&sensor=false&key=MAPS_API_KEY

http://maps.google.com/maps/api/staticmap?zoom=14&size=512x512&maptype=roadmap&markers=color:red|color:red|label:C|-33.964318,151.15464999999995&markers=color:red|color:red|label:C|28.6911089,77.11071689999994&markers=color:red|color:red|label:C|-33.8791134,151.20921709999993
-->

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>:: Handyman ::</title>
<link href="<?php echo assets_url('css/bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/slick.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/animate.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/styles.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/responsive.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo assets_url('js/jquery.min.js'); ?>"></script> 
<script>
	var rating_imgg='<?php echo assets_url('css/images'); ?>';
	</script>
</head>
<body class="time-body">
	<div class="main-div">
		<div class="register-bg">
			 <?php if($header) echo $header ;?>
			 <?php //if($left) echo $left ;?>
			 <?php if($middle) echo $middle ;?>
		 </div>
		 <?php if($footer) echo $footer ;?>
  </div>
 
</body>
<!--Time line End--> 
<script src="<?php echo assets_url('js/slick.js'); ?>"></script> 
<script src="<?php echo assets_url('js/bootstrap.min.js'); ?>"></script> 
<script src="<?php echo assets_url('js/wow.min.js'); ?>"></script> 
<script>
 new WOW().init();
   
$('.responsive').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll:1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll:1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

</script>
</body>
</html>

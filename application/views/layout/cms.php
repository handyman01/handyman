<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>:: Handyman ::</title>
<link href="<?php echo assets_url('css/bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/animate.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/styles.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/responsive.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo assets_url('js/jquery.min.js'); ?>"></script> 
<link href="<?php echo assets_url('css/rating.css'); ?>" rel="stylesheet" type="text/css"/>
<script>
	var rating_imgg='<?php echo assets_url('css/images'); ?>';
</script>
<script src="<?php echo assets_url('js/rating.js'); ?>"></script> 

<style>
.padding15 {
    padding: 15px !important;
}
.termswrap {
    margin-bottom: 30px;
}
.termswrap h2 {
    color: #6f6d6d !important ;
    line-height: 24px !important;
    padding-bottom: 15px !important;
    font-weight: 600 !important;
    font-size:18px;
}
.termswrap p{
    color: #6f6d6d !important ;
    line-height: 24px !important;
    padding-bottom: 15px !important;
    font-weight: 400 !important;
    font-size:14px;
}
.termswrap ol {
    margin-left: 15px;
    margin-bottom: 15px;
}
.box-shadow {
    background: #fff;
    -webkit-box-shadow: 1px 1px 4px rgba(0,0,0,.1);
    -moz-box-shadow: 1px 1px 4px rgba(0,0,0,.1);
    box-shadow: 1px 1px 4px rgba(0,0,0,.1);
        border-radius: 3px;
}

	</style>
</head>
<body class="time-body">
	<div class="main-div">

			
 <?php if($header) echo $header ;?>

  <div class="profile_middle_container">
    <div class="container">
    
		  <section class="clearfix padding15 box-shadow border-radius termswrap">
      <?php 
     echo  $content['description'];
      ?>
      </section>
      
 
	     
		     
		     <?php if($footer) echo $footer ;?>
  </div>
 
 

   
         
         
         
</body>


</body>
</html>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>:: Handyman ::</title>
<link href="<?php echo assets_url('css/bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/animate.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/styles.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/responsive.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo assets_url('js/jquery.min.js'); ?>"></script> 

</head>
<body class="time-body">
	<div class="main-div">

			
 <?php if($header) echo $header ;?>
  <?php

  ?>
  <div class="profile_middle_container">
    <div class="container">
      <div class="profile_blk">
        <div class="cover_blk">
          <div class="img_blk"> <img src="<?php echo site_url('/assets/users/'.$details['image']);?>" alt="user">  </div>
        </div>
        <div class="profile_detail_blk">
          <div class="content_blk">
            <div class="col-md-6">
              <div class="info_blk">
                <h2>
				<?php
                if(isset($details['first_name']))
                {
					echo $details['first_name'].' '.$details['last_name'];
				}
				else
				{
					echo $details['name'];
				}
                ?>
                </h2>
                
                <p>Last online 8 hours ago</p>
                <?php
                if(isset($details['address']))
                {
                ?>
                <p><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo  $details['address'];?></p>
                <?php
			     }
                ?>
                <p>Member since  <?php echo date('M d,Y',strtotime($details['created']));?></p>
                <!-- <span><i class="fa fa-flag-o" aria-hidden="true"></i><a href="#">Report this Member</a></span> -->
                
                 </div>
            </div>
            <div class="col-md-6">
              <div class="info_blk align_right">
                <div class="tabbable-panel">
                  <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                      <li class="active"><a href="#tab_default_1" data-toggle="tab">As a Worker</a></li>
                      <li><a href="#tab_default_2" data-toggle="tab">As a Poster</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_default_1">
                      	<span class="rating_blk"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
                        <p>5 Star - 10 Reviews</p>
                        <p>66% Completion Rate</p>
                        <span>5 completed tasks</span>
                      </div>
                      <div class="tab-pane" id="tab_default_2">
                        <p>Lorem ipsum dolor sit amet</p>
                        <p>Lorem ipsum</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
   		 <?php
          if(isset($details['about_urself']))
          {
          ?>       
          <div class="content_blk">

          <h3>About</h3>

            	<div class="about_blk">
                	<ul>
                    	<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    </ul>
                </div>

          </div>
                       <?php
                
			}
			?>   

 
 
          <div class="content_blk">
          <h3>Skills</h3>
          <div class="row">
          	<div class="col-md-3">
            	<div class="skills_blk">
                <h4>Education</h4>
                	<ul>
                    	<li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="skills_blk">
                <h4>Specialities</h4>
                	<ul>
                    	<li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="skills_blk">
                <h4>Languages</h4>
                	<ul>
                    	<li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
            	<div class="skills_blk">
                <h4>Work</h4>
                	<ul>
                    	<li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                    </ul>
                </div>
            </div>
           </div>
          </div>
          <div class="content_blk">
          	<h3>Reviews</h3>
            <div class="row">
            <div class="col-md-12">
            	<div class="col-md-6">
                	<div class="reviews_blk">
                    	<ul>
                        	<li><select name=""><option>Most recent</option><option>Most Favourable</option><option>Most Critical</option></select></li>
                            <li><div class="radio">
                                <input name="radio1" id="radio1" value="option1" checked="" type="radio">
                                <label for="radio1">
                                    As a Worker
                                </label>
                            </div>
                            <div class="radio">
                                <input name="radio1" id="radio2" value="option2" type="radio">
                                <label for="radio2">
                                    As a Poster
                                </label>
                            </div></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                <div class="star_blk">
                    	<ul>
                        	<li><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><label>8</label></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><label>0</label></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><label>0</label></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><label>0</label></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i><label>0</label></li>
                        </ul>
                    </div>
                	<div class="info_blk align_right">
                    	<span class="rating_blk"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
                        <p>5 Star - 10 Reviews</p>
                        <p>66% Completion Rate</p>
                        <span>5 completed tasks</span>
                    </div>
                    
                </div>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-6">
                	<div class="media">
                      <div class="media-left">
                        <a href="#">
                        <img src="http://placehold.it/80x80" alt="user-img"> </a>
                      </div>
                      <div class="media-body">
                      <span class="rating_blk"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
                      <span class="time_txt">13 hours ago</span>
                        <h4 class="media-heading">Media heading</h4>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                      </div>
                    </div>
                </div>
                <div class="col-md-6">
                	<div class="media">
                      <div class="media-left">
                        <a href="#">
                        <img src="http://placehold.it/80x80" alt="user-img"> </a>
                      </div>
                      <div class="media-body">
                      <span class="rating_blk"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
                      <span class="time_txt">13 hours ago</span>
                        <h4 class="media-heading">Media heading</h4>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 
	     
		     
		     <?php if($footer) echo $footer ;?>
  </div>
 
</body>

<script src="<?php echo assets_url('js/bootstrap.min.js'); ?>"></script> 
<script src="<?php echo assets_url('js/wow.min.js'); ?>"></script> 
<script>
 new WOW().init();
</script>
</body>
</html>

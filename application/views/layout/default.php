<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>:: Handyman ::</title>
<link href="<?php echo assets_url('css/jquery.mCustomScrollbar.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/animate.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/slick.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/styles.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/responsive.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo assets_url('js/jquery.min.js'); ?>"></script> 
<script>
	var rating_imgg='<?php echo assets_url('css/images'); ?>';
	</script>

</head>
<body class="time-body">
	<div class="main-div">
		 <?php if($header) echo $header ;?>
		 <?php //if($left) echo $left ;?>
		 <?php if($middle) echo $middle ;?>
		 <?php if($footer) echo $footer ;?>
  </div>
 
</body>
<!--Time line End--> 

<script src="<?php echo assets_url('js/bootstrap.min.js'); ?>"></script> 
 <script src="<?php echo assets_url('js/jquery.mCustomScrollbar.concat.min.js');?>">
    </script>
    
<script src="<?php echo assets_url('js/wow.min.js'); ?>"></script> 

<script src="<?php echo assets_url('js/slick.js'); ?>"></script> 
<script src="<?php echo assets_url('js/custom.js'); ?>"></script> 
<script>
 new WOW().init();
 
 (function($){
			$(window).on("load",function(){
				
				$("#content").mCustomScrollbar({
					theme:"minimal"
				});
				
			});
		})(jQuery);
		
</script>

</body>
</html>

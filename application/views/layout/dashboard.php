<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>:: Handyman ::</title>
<link href="<?php echo assets_url('css/bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/animate.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/styles.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/responsive.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo assets_url('js/jquery.min.js'); ?>"></script> 
<script>
	var rating_imgg='<?php echo assets_url('css/images'); ?>';
	</script>
</head>
<body class="time-body">
	<div class="main-div">

			
			 <?php if($header) echo $header ;?>
			 
	  <div class="middle-section">
		<div class="container">
		  <div class="row">
			<div class="profile_top_blk">
			  <ol class="breadcrumb">
				<li><a href="<?php echo base_url('/'); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
				<!-- <li><a href="#">My Account</a></li> -->
				<?php
				 if($this->uri->segment(1)!="do_login")
				 {
				   if($this->uri->segment(1)=='check_task_detail')
				   {
					   ?>
					   <li class="active">Check Task Details</li>
					   <?php
				   }
				   else
				   {
				   
				?>
				<li class="active"><?php echo ucfirst(str_replace("_"," ",$this->uri->segment(1)));?></li>
				<?php
			        }
			     }
			    ?>
			  </ol>
			</div>
			
			<div class="profile_bottom_blk">
				
				
			 <?php if($leftbar) echo $leftbar ;?>
			 <?php if($middle) echo $middle ;?>
		 
		    </div>
		  </div>
		</div>

	  </div>
		 
		     
		     
		     <?php if($footer) echo $footer ;?>
  </div>
 
</body>

<script src="<?php echo assets_url('js/bootstrap.min.js'); ?>"></script> 
<script src="<?php echo assets_url('js/wow.min.js'); ?>"></script> 
<script>
 new WOW().init();
</script>
</body>
</html>

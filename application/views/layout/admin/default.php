<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Handyman</title>

    <!-- Bootstrap -->
    <link href="<?php echo assets_adminUrl('bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo assets_adminUrl('font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- iCheck -->
     <link href="<?php echo assets_adminUrl('iCheck/skins/flat/green.css'); ?>" rel="stylesheet">    
    <!-- Custom Theme Style -->
    <link href="<?php echo assets_adminUrl('css/custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo assets_adminUrl('css/pnotify.css'); ?>" rel="stylesheet">
    <script src="<?php echo assets_adminUrl('jquery/dist/jquery.min.js'); ?>"></script>
	<script src="<?php echo assets_adminUrl('js/moment.min.js'); ?>"></script> 
<script src="<?php echo assets_adminUrl('js/daterangepicker.js'); ?>"></script> 
	<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<style>
	body,html,.container {
  height: 100% !important;
}
	</style>
  </head>
<body class="nav-md" >
<div class="container body">
  <div class="main_container">
	     <?php if($header) echo $header ;?>
		 <?php if($left) echo $left ;?>
		 <?php if($middle) echo $middle ;?>
		 <?php if($footer) echo $footer ;?>
  </div>
 </div>
</body>
</html>

	   <div class="col-md-3 left_col">		
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo site_url('/admin/user/dashboard');  ?>" class="site_title"><i class="fa fa-paw"></i> <span>Handyman</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
               <a href="<?php echo site_url('/admin/user/dashboard');  ?>"> <img src="<?php echo assets_adminUrl('images/img.jpg'); ?>" alt="..." class="img-circle profile_img"></a>
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo ucfirst($this->session->userdata()['admin']['firstname']);?></h2>
              </div>
            </div>
            
            
            
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
				  <li><a><i class="fa fa-users"></i> Customers <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('/admin/user/add_customer');  ?>">Add</a>
                      </li>
                      
                      <li><a href="<?php echo site_url('/admin/user/manage_customers');  ?>">Manage</a>
                      </li>
                    </ul>
                  </li>
                  
				  <li><a><i class="fa fa-users"></i> Handymen <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('/admin/user/add_handyman');  ?>">Add</a>
                      </li>
                      
                      <li><a href="<?php echo site_url('/admin/user/manage_handymen');  ?>">Manage</a>
                      </li>
                    </ul>
                  </li>
                  
				  <li><a><i class="fa fa-table"></i> Business Categories <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('/admin/category/add');  ?>">Add</a>
                      </li>
                      
                      <li><a href="<?php echo site_url('/admin/category/manage');  ?>">Manage</a>
                      </li>
                    </ul>
                  </li>
                  
				  <li><a><i class="fa fa-clone"></i>Maintenance Tasks <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <!-- <li><a href="<?php echo site_url('/admin/task/add');  ?>">Add</a>
                      </li>
                      -->
                      <li><a href="<?php echo site_url('/admin/task/manage');  ?>">Manage</a>
                      </li>
                    </ul>
                  </li>
                  
				  <li><a><i class="fa fa-clone"></i>Renovation Tasks <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <!--<li><a href="<?php echo site_url('/admin/task/add_renovation');  ?>">Add</a>
                      </li> -->
                      
                      <li><a href="<?php echo site_url('/admin/task/manage_renovation');  ?>">Manage</a>
                      </li>
                    </ul>
                  </li>
				
				 <li><a><i class="fa fa-money"></i>Transaction History<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('/admin/payment/manage');  ?>">Customer</a>
                      </li>
                      
                    <!--  <li><a href="<?php echo site_url('/admin/payment/handyman');  ?>">Handyman</a>
                      </li> -->
                    </ul>
                  </li>
                  
                  
                  <li><a><i class="fa fa-cog"></i>Setting<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('/admin/pages/manage');  ?>">CMS Pages</a>
                      </li>
             
                    </ul>
                  </li> 
                  
				
                
                </ul>
              </div>
              
              <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">
					

              </div>

            </div>
            <!-- /sidebar menu -->

            
          </div>
        </div>


        <!-- top navigation -->
        <div class="top_nav">

          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle" style="width: 175px;">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                <?php
                $reff_url='';
                if(isset($_SERVER['HTTP_REFERER'])) {
  //do what you need to do here if it's set    
  $reff_url=$_SERVER["HTTP_REFERER"];
   }


                ?>
                	<button type="button" class="btn btn-info" onclick="location.href='<?php echo $reff_url;?>'"><i class="fa fa-reply"></i> Go Back</button>

              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo assets_adminUrl('images/img.jpg'); ?>" alt=""><?php echo ucfirst($this->session->userdata()['admin']['firstname']);?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                  
                    <li><a href="<?php echo site_url('admin/user/logout');  ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    <li><a href="<?php echo site_url('admin/user/change_password');  ?>"><i class="fa fa-gear pull-right"></i> Change Password</a>
                    </li>
                  </ul>
                </li>

					

                <li role="presentation" class="dropdown">
					
                  <a href="javascript:void(0);" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green"></span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu" style="max-height: 331px;overflow-y: auto;">
                 
                  </ul>
                </li>
               
             
              </ul>
            </nav>
          </div>

        </div>
        <!-- /top navigation -->
</div>

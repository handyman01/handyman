        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
         <!-- jQuery -->
         
    <!-- Bootstrap -->
             

    
    <script src="<?php echo assets_adminUrl('bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo assets_adminUrl('js/bootbox.min.js'); ?>"></script>
    <script src="<?php echo assets_adminUrl('js/pnotify.js'); ?>"></script>
    <script src="<?php echo assets_adminUrl('js/custom.js'); ?>"></script>
 
 <script>
$(document).ready(function() {
    $('#datatable').DataTable();
} );


function notifications()
{
	$.ajax({  
		type: "POST",
		url: '<?php echo site_url('/admin/user/notifications'); ?>',
		dataType: 'json',
		global: false,
		success: function(data) {
			if(data.count > 0)
			{
				
				$(".badge").text(data.count);
				$("#menu1").html(data.notification);
				setTimeout(function(){notifications();}, 10000);
				
			}else{
			$("#menu1").html('<li><div class="text-center"><a><strong>No Alerts</strong><i class="fa fa-angle-right"></i></a></div></li>');
			setTimeout(function(){notifications();}, 10000);
			}
		}
	});
	
		
   }
 $(function(){
	notifications();
	
});  


</script>
<script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>	

<script>

<!---- tinymce editor  ----->

tinymce.init({
  selector: 'textarea.editor',
    menubar: false,
	
  height: 100,
  width:474,
  theme: 'modern',
  
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  
  toolbar1: ' Source code |  undo redo |styleselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor |bullist numlist outdent indent|fontselect',
  content_css: [
    'http://fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
    'http://www.tinymce.com/css/codepen.min.css'
  ]
 });
 
 <!---- /tinymce editor  ----->
</script>

  

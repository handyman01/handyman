<?php
//echo "<pre>";
//print_r($services);

?>
<div class="lower-section"> 
    <!--Banner Section-->
    <div class="banner-sec servnce-img">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h5 class="heai-title wow fadeIn">It’s never been easier to book, schedule and pay for<br>
              home services from trusted service professionals</h5>
            <div class="row">
              <div class="col-sm-4">
                <div class="task-uw wow zoomIn" data-wow-delay=".2s"> <em>1</em> <b>Pick a Task</b>
                  <p>Choose from a list of popular<br>
                    chores and errands</p>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="task-uw wow zoomIn" data-wow-delay=".4s"> <em>2</em> <b>Get Matched</b>
                  <p>We'll connect you with a<br>
                    skilled Tasker within minutes <br>
                    of your request </p>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="task-uw wow zoomIn" data-wow-delay=".6s"> <em>3</em> <b>Get it Done</b>
                  <p>Your Tasker arrives, completes<br>
                    the job and bills directly 
                    in the app </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!--How we can help-->
    <section class="how-stpes">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="title-sec text-center wow fadeIn animated" style="visibility: visible; animation-name: fadeIn;">
              <h5>How can we help</h5>
              <p>popular home maintance service</p>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="slider responsive">
          <?php
          if(!empty($services['popularServices']))
          {
          foreach($services['popularServices'] as $service)
          {
          ?>
          
          <div >
            <div class="first-how how-help text-center"> <a href="<?php echo site_url('maintenance_list/'.$service['id']);?>"> 
              <center>
               <img src="<?php echo assets_url('/uploads/categories/'.$service['icon_big']);?>" alt="">
              
              </center>
              <b><?php echo $service['name'];?> </b></a> </div>
          </div>
          <?php
          
	      }

	     }
          ?>
   </div>
        </div>
      </div>
    </section>
    <section class="how-stpes serv-s">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="title-sec text-center wow fadeIn animated" style="visibility: visible; animation-name: fadeIn;">
              <h5>All Services</h5>
            </div>
          </div>
          <div class="al-serv">
            <ul>
	     <?php
	      if(!empty($services['allServices']))
          {
			  foreach($services['allServices'] as $service)
			  {
			  ?>
				  <li class="wow fadeInUp" data-wow-delay=".1s"><a href="<?php echo site_url('maintenance_list/'.$service['id']);?>"><?php echo ucfirst($service['name']);?></a></li>
			  <?php
			  }
	       }
          ?>
            </ul>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </section>
  </div>


<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<?php
$arr_task_status=array('A'=>'Applied','X'=>'Expired','S'=>'Assigned');

?>
<style>
	.task_action{
		color: #247b58;
		font-weight:bold;
		font-size:20px !important;
		pointer-events:none;
	}
.offercount
{
	display: inline !important;
    font-size: 16px !important;
    margin-bottom: 10px !important;
    margin-top: 30px !important;
    text-align: right !important;
    padding-right: 0 !important;
}
	.locate_task{
		vertical-align: baseline;
	}
	.loading_leftsidebar img,.loading_detail img
	{
		width: 100%;
	}
	.locate_tasks_btn {
    background-color: rgb(70, 70, 70);
    border-radius: 5px;
    color: rgb(255, 255, 255) !important;
    display: inline-block;
    font-size: 17px;
    height: 50px;
    line-height: 50px;
    padding: 0 30px;
    text-transform: uppercase;
     margin-bottom: 12px; 
}

.gm-style-iw {
	width: 350px !important;
	top: 15px !important;
	left: 0px !important;
	background-color: #fff;
	box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
	border: 1px solid rgba(72, 181, 233, 0.6);
	border-radius: 2px 2px 10px 10px;
}
#iw-container {
	margin-bottom: 10px;
}
#iw-container .iw-title {
	font-family: 'Open Sans Condensed', sans-serif;
	font-size: 22px;
	font-weight: 400;
	padding: 10px;
	background-color: #48b5e9;
	color: white;
	margin: 0;
	border-radius: 2px 2px 0 0;
	line-height: 1;
}
#iw-container .iw-content {
	font-size: 13px;
	line-height: 18px;
	font-weight: 400;
	margin-right: 1px;
	padding: 15px 5px 20px 15px;
	max-height: 140px;
	overflow-y: auto;
	overflow-x: hidden;
}
.iw-content img {
	float: right;
	margin: 0 5px 5px 10px;	
}
.iw-subTitle {
	font-size: 16px;
	font-weight: 700;
	padding: 5px 0;
}
.iw-bottom-gradient {
	position: absolute;
	width: 326px;
	height: 25px;
	bottom: 10px;
	right: 18px;
	background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
}
</style>
<div class="middle-section">
    <div class="middle_top_blk">
    	<div class="container">
            	<ul>
                	<li>
                    	<label>Area:</label>
                        <select name=""><option>North York</option></select>
                    </li>
                    <li>
                    	<label>Square:</label>
                        <select name=""><option>800 sqf</option></select>
                    </li>
                    <li>
                    	<label>Style:</label>
                        <select name=""><option>Modern</option></select>
                    </li>
                </ul>
        </div>
    </div>
    <div class="middle_bottom_blk">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-5">
                	<div class="task_blk">
                    	<div class="search_blk">
                        	<input name="" type="text">
                        </div>
                        <div class="task_list mCustomScrollbar">
							
						  <div class="table-responsive" >
				
										
                      <table class="table table-striped jambo_table bulk_action" id = "datatable">
                        <thead>
                          <tr class="headings">
                                <th></th>
                          </tr>
                        </thead>
                        <tbody>
					<?php
					//echo '<pre>';
					//print_r($tasks);
					//die;
					if(!empty($tasks))
					{
						?>
						
						<?php
												
								$c=0;
								$count=1;
								$marker_array = array();
								foreach($tasks as $val)
								{
									$var_document='';
									 if($val['document']!="")
									{
									
									$ext = array("png", "jpg", "jpeg", "gif", "gif");
									$doc=$val['document'];
									$file_name=explode(".", $doc);
									
									if(!in_array(end($file_name), $ext)) 
									{
		   
		   
		   $var_document='<a href="'.site_url('/admin/task/download_doc/'.$val['document']).'">
		<p><img src="'.site_url('/assets/images/task_doc.png').'" height="115" width="86"/><br/></p>
		<p style="width: 20%;text-align: center;"><span></span></p></a>';
		   

									}
									else
									{
										$var_document='<a href="'.site_url('/admin/task/download_doc/'.$val['document']).'"><img src="'.site_url('/assets/task/'.$val['document']).'" height="115" width="86"/><p style="width: 20%;text-align: center;"><span></span></p></a>';
										
									}
								 }
                         
                         
									$marker_array[$c] = array('lat'=>$val['lat'],'lng'=>$val['lon'],'category'=>ucfirst($val['category_name']),'total_price'=>$val['total_price'],'created_on'=>$val['created'],'description'=>$val['description'],'address'=>$val['address'],'type'=>($val['is_residential']==1 ? 'Residential':'Commercial'),'email'=>$val['customer_email'],'document'=>$var_document,'title'=>ucfirst($val['title']),'customer_name'=>$val['first_name'].' '.$val['last_name'],'customer_link'=>site_url('user_profile/u/'.$val['user_id']));
									
									
									
									?>
								<tr>
							       
							         <td>
								
						
                                	<div class="col-md-3 col-xs-3 pad_0">
							<?php			
						if($val['document']!="")
						{
							
							$ext = array("png", "jpg", "jpeg", "gif", "gif");
							$imgg=$val['document'];
							 $file_name=explode(".", $imgg);
							
                            if(in_array(end($file_name), $ext)) 
                            {
								?>
								 <img src="<?php echo site_url('/assets/task/'.$imgg);?>" class="img-responsive" alt="">
								<?php
					
                            }
                            else
                            {
								?>
								  <img src="<?php echo site_url('/assets/images/notask.png');?>" class="img-responsive" alt="">
								<?php
							
							
							}
                         }
                         else
                         {
							 ?>
							   <img src="<?php echo site_url('/assets/images/notask.png');?>" class="img-responsive" alt="">
							 <?php
						 }     
                         ?>
                       </div>
                                    <div class="col-md-7 col-xs-7">
                                    	<h2><?php echo $val['title'];?></h2>
                                        <h3>Posted On : <?php $date=date_create($val['created']);
                                            echo date_format($date,"D,M d,Y");?></h3>
                                        <h3>Posted By: <?php echo $val['first_name'].' '.$val['last_name'];?></h3>
                                        <h3>Service Type: <?php echo $val['category_name'];?></h3>
                                       <a id="detail_<?php echo $val['user_id'];?>###<?php echo $val['id'];?>" style="color: #008fb6;font-weight: bold;" class="task_detail" href="javascript:void(0)">Check Details</a>
                                        <p><span class="offercount" id="offercount_<?php echo $val['id'];?>"><?php echo $val['total_offers'];?></span> offer(s)</p>
                                    </div>
                                    <div class="col-md-2 col-xs-2 pad_0">
                                    	<span>$<?php echo number_format($val['total_price'],2);?></span>
                                        
                                        
                                        <?php
                                        
                                       if(isset($user_detail) && !empty($user_detail) && $user_detail['user_type']!='user')
                                       {
										   
									   
                                         if(isset($applied_tasks) && array_key_exists($val['id'], $applied_tasks)) 
                                         {
										
											echo "<span class='task_action'>".$arr_task_status[$applied_tasks[$val['id']]]."</span>";
										 }
										 else
										 {
											
											?>
											<a class="earn_btn apply_task" id="<?php echo $val['id'];?>" href="javascript:void(0)">Apply</a>
											<?php
										 }
										 
									 }
                                        ?>
                                       <div class="loading_leftsidebar">
                                        <img style="display:none;" id="load_<?php echo $val['id'];?>" src="<?php echo site_url('assets/images/load-new.gif');?>">
                                        </div>
                                    </div>
                              
                                
									<?php
									$count++;
									$c++;
									?>
								
								   </td>
								</tr>
									<?php
								}
								?>
								
								<?php
								 $locations=json_encode($marker_array,JSON_NUMERIC_CHECK); 
					}
			       else
			       {
					   ?>
					<tr ><td colspan='7'>No tasks found ! </td></tr>   
					   <?php
				   }
                 ?>

                         </tbody>
                      </table>
                     
           
           
 
            </div>
            
            
            
	
                               
                        </div>
                    </div>
                </div>
              
                <div class="col-md-7" >
					
					<a href="javascript:void(0)" style="display:none;" class="pull-right" id="switch_to_map">	<button type="button" class="btn btn-success">Switch to map</button></a>	
					
					<div class="loading_rightsidebar">
					   <img style="display:none;" id="loading_right" src="<?php echo site_url('assets/images/load-new.gif');?>">
					</div>
                	<div class="map_blk">
                    	<div id="map" style="height: 470px;width: 100%;position: absolute;"></div> 
                    </div>
                </div>
           
                           
                <div class="col-md-7 task_detail_right" style="display:none;">
                	<div class="task_detail_blk mCustomScrollbar">
                      <div class="pad_right_blk">
                    	<div class="task_top_blk">
                        	<div class="post_task">
                            	<p>Aj m got help with his task. You can use Airtasker to get similor task done.</p>
                                <a href="#">Post a similor task</a>
                            </div>
                            <div class="task_pay">
                            	<div class="col-md-7 col-sm-7 pad_0">
                                	<div class="task_pay_left">
                                    	<h1>Clean up mp3 voicemail so voice can be heard</h1>
                                        <span><img src="images/user-img.png" alt="user">Posted by <a href="#">Aj m.</a> 2 hours ago</span>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 pad_left">
                                	<div class="offer_blk">
                                    	<span class="paid_txt">Paid with Airtasker Pay<img src="images/help.png" alt="help"></span>
                                        <span class="price">$5</span>
                                        <span class="offer_txt">Make an Offer</span>
                                    </div>
                                    <div class="more_option"><a href="#">MORE OPTIONS<i class="fa fa-angle-down" aria-hidden="true"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="description_blk">
                        	<h4>Description</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipising elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <ul>
                            	<li><a href="#">ONLINE</a></li>
                                <li><a href="#">DUE TOMORROW</a></li>
                                <li><a href="#">PAID WITH AIRTASKER PAY</a></li>
                            </ul>
                        </div>
                      </div>
                    </div>
                </div>
            
            
           
            </div>
        </div>
    </div>
  </div>
  <script>
      function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
           center: new google.maps.LatLng(52.2, 5)
          //center: {lat:  <?php echo $marker_array[0]['lat'] ?>, lng:  <?php echo $marker_array[0]['lng'] ?>}
        });

 var infowindow = new google.maps.InfoWindow();
        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
       
       /* var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            label: labels[i % labels.length]
          });
          
          
        });
*/
var stack = [];
var markers=[];
var contents = [];
var infowindows = [];

   for (var i = 0; i < locations.length; i++)
   {
                    // obtain the attribues of each marker
                    //console.log(locations);

                    
                      // InfoWindow content
                     var content = '<div id="iw-container">' +
                    '<div class="iw-title">'+locations[i].title+'</div>' +
                    '<div class="iw-content">' +
                      '<div class="iw-subTitle">Category</div><p>'+locations[i].category+'</p><div class="iw-subTitle">Description</div>' +
                      locations[i].document+
                      '<p>'+locations[i].description+'</p>' +
                      '<div class="iw-subTitle">Type</div><p>'+locations[i].type+'</p><div class="iw-subTitle">Address</div><p>'+locations[i].address+'</p><div class="iw-subTitle"></div>' +
                      '<p>Name: <a target="_blank" href="'+locations[i].customer_link+'" style="color:blue;">'+locations[i].customer_name+'</a><br/>Email:  '+locations[i].email+'<br><a href="#" style="color:blue;"> See Detail</a></p>'+
                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                  '</div>';
                  
                    // A new Info Window is created and set content
                    /*
  var infowindow = new google.maps.InfoWindow({
    content: content,

    // Assign a maximum value for the width of the infowindow allows
    // greater control over the various content elements
    maxWidth: 350
  }); 
  */
  
                      var lat = parseFloat(locations[i].lat);
                    var lng = parseFloat(locations[i].lng);
                   markers[i] = new google.maps.Marker({
                        position : new google.maps.LatLng(lat, lng),
                        map: map,
                        title:"This is a marker"
                    });    
        
                    stack.push(markers[i]);
                     markers[i].index = i;
                      contents[i] = content;
                    
                    infowindows[i] = new google.maps.InfoWindow({
        content: contents[i],
        maxWidth: 350
    });

    google.maps.event.addListener(markers[i], 'click', function() {
        console.log(this.index); // this will give correct index
        console.log(i); //this will always give 10 for you
        infowindows[this.index].open(map,markers[this.index]);
        map.panTo(markers[this.index].getPosition());
    });  
                }


        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, stack,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

      }
      
locations=<?php echo $locations;?>;

var is_login='no';
login_url='<?php echo site_url("login");?>';
loading_url='<?php echo site_url("assets/images/load-new.gif");?>';
var user_id='';
<?php
if($user_detail && !empty($user_detail))
{
	?>
	user_id='<?php echo $user_detail['id'];?>';
	is_login='yes';
	<?php
}

?>
$(document).ready(function() {
	

	$(document).on('click','.follow_task',function(){
		
		if(is_login=='no')
		{
           window.location.href = login_url;
		}
		else
		{
			    task_id=$('#task_id').val();
			   // handyman_id=$('#user_id').val();
			    //follow_value=$('#follow_value').val();
			    
			    jQuery.ajax({ 
							url: '<?php echo site_url('follow_task'); ?>',
							
							data: {'task_id':id,'handyman_id':user_id},
							type: 'POST',
							success: function(data) {
								
								
							
								  if(data.trim()=='followed')
								  {
									 
									 //$(document).find('.follow_task').find('i').removeClass('fa-plus-circle').addClass('fa-minus-circle');		
									  $(document).find('.follow_task').html('<i class="fa fa-minus-circle" aria-hidden="true"></i>Unfollow');
								  }
								  else
								  {
									   
									     //$(document).find('.follow_task').find('i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
									     //$(document).find('.follow_task').text('Follow');
									      $(document).find('.follow_task').html('<i class="fa fa-plus-circle" aria-hidden="true"></i>Follow');
								  }
								  
									

								
							},
							complete: function(){
									//$('#'+id).show();
									// $(".loading_detail_img").hide();
							}
						});
						
						
						
		}
		return false;
	});
	
		
	$(document).on('click','#makeoffer_btn',function(){
	    $("#make_offer").hide();
		if(is_login=='no')
		{
           window.location.href = login_url;
		}
		else
		{
			
				
			    task_id=$('#task_id').val();
			    task_user_id=$('#user_id').val();
			    offer_price=$('#offer_price').val();
			    if(offer_price.trim()=='')
			    {
					$("#make_offer").show();
					$('#offer_response').css({"color": "red", "font-weight": "bold","font-size":"13px","pointer-events":"none"}).text('Please enter offer amount!');
					
				}
				else
				{									
							$(".loading_detail_img").show();
							jQuery.ajax({ 
							url: '<?php echo site_url('make_offer'); ?>',
							
							data: {'task_id':id,'handyman_id':user_id,'price':offer_price,'user_id':task_user_id},
							type: 'POST',
							success: function(data) {
								
								if(data == 'success'){
							
								  $('#offer_response').css({"color": "#247b58", "font-weight": "bold","font-size":"20px","pointer-events":"none"}).text('Offer made for $'+offer_price);
								  
								  $('#'+task_id).removeClass('earn_btn').css({"color": "#247b58", "font-weight": "bold","font-size":"20px","pointer-events":"none"}).text('Applied');
								  $('#offercount_'+task_id).text(parseInt($('#offercount_'+task_id).text())+1);
								  
								  //$('#'+id).show();
				
								}
								if(data == 'error'){
									
									 //$('#'+id).show();
									
								}
							},
							complete: function(){
									//$('#'+id).show();
									 $(".loading_detail_img").hide();
							}
						});
		      }
			
		}
	   	
		
    });	
		
    $('#datatable').DataTable();
    $('#switch_to_map').click(function(){
		$('.task_detail_right').hide();
		$('.map_blk').show();
		});
		
		
		
    $('.task_detail').click(function(){  
		
		$('.map_blk').hide();
		$('.task_detail_right').hide();
		$('#loading_right').show();
		var detail_url='<?php echo site_url('task_detail');?>';
		 
	     ids=$(this).attr("id");
	     id_parts=ids.split('detail_');
	     id_parts_arr=id_parts[1].split('###');
	     task_user_id=id_parts_arr[0];
	     id=id_parts_arr[1];
		 jQuery.ajax({ 
				url: detail_url,
				
				data: {'id':id,'user_id':task_user_id},
				type: 'POST',
				success: function(data) {
					
					if(data != 'success'){
				
				          $('.map_blk').hide();
				          $('.pad_right_blk').html(data);
				          $('.task_detail_right').show();
				          $('#loading_right').hide();
                          $('#switch_to_map').show();
					}
					if(data == 'error'){
						$('.map_blk').show();
						$('.task_detail_right').hide();
						$('#loading_right').hide();
                         //$('#'+id).show();
						
					}
				},
                complete: function(){
                        //$('#'+id).show();
                        // $("#load_"+id).hide();
                }
			});
		
	});
		
	$(document).on('keypress',"#offer_price",function(e){
	 
	 var vall = String.fromCharCode(e.which);

     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57 )) {
            e.preventDefault();
              // return false;
      }

   });
	  $('.apply_task').click(function(){
		
		if(is_login=='no')
		{
           window.location.href = login_url;
		}
		else
		{
			    $(this).hide();
			    id=$(this).attr("id");
			    $("#load_"+id).show();
				jQuery.ajax({ 
				url: '<?php echo site_url('apply_task'); ?>',
				
				data: {'id':id},
				type: 'POST',
				success: function(data) {
					
					if(data == 'success'){
				
				      $('#'+id).removeClass('earn_btn').css({"color": "#247b58", "font-weight": "bold","font-size":"20px","pointer-events":"none"}).text('Applied');
	                  $('#'+id).show();
    
					}
					if(data == 'error'){
						
                         $('#'+id).show();
						
					}
				},
                complete: function(){
                        //$('#'+id).show();
                         $("#load_"+id).hide();
                }
			});
			
		}

				
	});
	
} );



  </script>
  <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>

    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCd_Tujq99uHzmtznB4SqOWvjy6KHmDpzQ&callback=initMap">
</script>
 

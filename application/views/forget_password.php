    
    <div class="lower-section">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="res-box">
              <div class="login-part">
                <ul>
                  <li><a href="<?php echo site_url('login');?>">Log In</a></li>
                  <li><a href="<?php echo site_url('register');?>">Not Registered ?</a></li>
                </ul>
                <div class="row">
					<?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1" style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"><?php echo validation_errors();?></div>
						   <?php
					   }
                       ?>  
                       
                <?php echo form_open(site_url('forget_password_action'),array('class'=>'',"id"=>'forget_password')); ?>  
                 <input type="hidden" name="redirect" value="<?php echo (isset($redirect) ? $redirect :'');?>">
                       
                  <div class="col-md-10 col-sm-10">
                    <div class="form-group">
                        <?php echo form_input(array('name'=>'email','type'=>'email', 'class'=>'form-control',"placeholder"=>"Email Id",'required'=>true)); ?>
                      <i><img src="<?php echo assets_url('images/user-icon.png'); ?>"></i> </div>
                       
                  </div>
                  <div class="col-md-2 col-sm-2 col-lg-2 text-center">
                    <input type="submit" value="Send Mail">
                  </div>
                  
                  </form>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
 
 
 

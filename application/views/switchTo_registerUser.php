<style>

.profile_right_blk h2
{
    margin-bottom: 0; 
}
.right_form{
	    background: #fff;
    border: 2px solid #a1a1a1;
    border-radius: 5px;	
}

.right_form_button
{
	    padding-bottom: 30px;
}

</style>


<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>

          <div class="profile_right_blk right_form">
                    
                      
				
				  
                <div class="col-md-12 right_form_content">                
                  
                  
                   <div class="col-md-12" >
					 <?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger " style="text-align: center;"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success " style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger" style="text-align: center;"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                       
					 </div>
					 
					 
					 <?php echo form_open(site_url('switchTo_registerUser_action'),array('class'=>'',"id"=>'change_password')); ?>  
					                      
					  <div class="form-group">
							  <div class="col-sm-4"><label>First name: </label></div>
							  
							  <div class="col-sm-7">  
								  <?php echo form_input(array('name'=>'first_name', 'class'=>'form-control',"placeholder"=>"Firstname",'label' =>false,'value'=>$user_detail['firstname'])); ?>
							  </div>
				      </div>
					  <div class="form-group">
							  <div class="col-sm-4"><label>Last name: </label></div>
							  
							  <div class="col-sm-7">  
								  <?php echo form_input(array('name'=>'last_name', 'class'=>'form-control',"placeholder"=>"Lastname",'label' =>false,'value'=>$user_detail['lastname'])); ?>
							  </div>
				      </div>
				      
					  <div class="form-group">
							  <div class="col-sm-4"><label>Email: </label></div>
							  
							  <div class="col-sm-7">  
                                    <?php echo form_input(array('name'=>'email', 'class'=>'form-control',"placeholder"=>"Email",'label' =>false,'readonly'=>'readonly','value'=>$user_detail['email'])); ?>
							  </div>
				      </div>
				      

				      
					  <div class="form-group">
							  <div class="col-sm-4"><label>Password: </label></div>
							  
							  <div class="col-sm-7">  
                                  <?php echo form_password(array('name'=>'password', 'class'=>'form-control',"placeholder"=>"Password")); ?>
							  </div>
				      </div>
				  
					 

  
          
                  <div class="col-lg-12 text-center right_form_button">
                     <a href="javascript:void(0)" id="change_pass" class="sign-up">Save</a>
                  </div>
                  
                   </form>	
                   
                </div>
              
             
           
          </div>
          

			
  <script>

    
    $('#change_pass').click(function(){
		
		 $("#change_password").submit();
		
		});
    </script>
  

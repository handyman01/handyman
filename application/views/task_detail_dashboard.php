<style>

.profile_right_blk h2
{
    margin-bottom: 0; 
}
.right_form{
	    background: #fff;
    border: 2px solid #a1a1a1;
    border-radius: 5px;	
}

.right_form_button
{
	    padding-bottom: 30px;
}
.profile_title_dashboard h4 {
font-size: 20px;
padding-bottom: 15px;
}
.detail_col
{
	color:#31708f;
	font-size: 18px;
	font-weight:300;
}
</style>

<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>
                      	 <div class="" >
<?php
if($user_detail['user_type']=='user')
{
	?>
	   <a href="<?php echo site_url('posted_task');?>" style="margin-bottom: 12px;"  class="pull-right">	<button type="button" class="btn btn-success">Back</button></a>  
	<?php
}
else
{
	?>
	   <a href="<?php echo site_url('assigned_task');?>" style="margin-bottom: 12px;"  class="pull-right">	<button type="button" class="btn btn-success">Back</button></a>  
	
	<?php
}
?>

   </div>
          <div class="profile_right_blk right_form">
                  		              
               

				
				  
                <div class="col-md-12 right_form_content">  
			
                  
                   <div class="col-md-12" >
					 <?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger " style="text-align: center;"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success " style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger" style="text-align: center;"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                       
					 </div>
					 
					 
					 <?php 
					 
					 if($this->session->userdata('user')['user_type']=='user')
					 {
					   ?>                       
       
                    <div class="col-md-12 col-sm-12 col-xs-12" >

                      <div class="profile_title_dashboard">
                        <div class="col-md-12" style="padding-bottom: 25px;">
                          <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                              Task Date :  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> <?php 
                              $date=date_create($details[0]['task_date']);
                              echo date_format($date,"D,M d,Y").' ('.date("g:i A", strtotime($details[0]['task_time'])).')';
                              ?>
                          </div>
                        </div>
                        
                        <div class="col-md-12">
							
							
							 <div class="col-md-6">
								<h4>Task Title<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo ucfirst($details[0]['title']);?><h4>
						     </div>
						     
						             <div class="clear"></div>
						             
							 <div class="col-md-6">
								<h4>Task Type<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col" >
								<h4> <?php echo ucfirst($details[0]['task_type']);?><h4>
						     </div>
						     
						             <div class="clear"></div>

							 <div class="col-md-6">
								<h4> User Name<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $details[0]['first_name']." ".$details[0]['last_name'];?><h4>
						     </div>
						             <div class="clear"></div>

						     
							 <div class="col-md-6">
								<h4> Category<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $sub_category[$details[0]['categ_id']];?><h4>
						     </div>
                                 <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Handyman<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> 
									
									 <?php 
								if($details[0]['handyman_id']!=0)
								{
								  echo $handyman_list[$details[0]['handyman_id']];
								 }
								 else
								 {
									 echo '-';
								 }
								  ?>
								  
									</h4>
						     </div>
                                 <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Address<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col" >
								<h4> <?php 
								if($details[0]['address']!="")
								{
								  echo $details[0]['address'];
								 }
								 else
								 {
									 echo '-';
									 }?>
							    <h4>
						     </div>
						             <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Description<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $details[0]['description'];?><h4>
						     </div>
						             <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Status<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $status[$details[0]['status']];?><h4>
						     </div>
						             <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Created On<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php $date=date_create($details[0]['created']);
                              echo date_format($date,"D,M d,Y");?><h4>
						     </div>
                             <div class="clear"></div>

                     		 <div class="col-md-6">
								<h4> Total Task Amount<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4>$ <?php echo $details[0]['total_price'];?><h4>
						     </div>
						             <div class="clear"></div>
						             
                     		 <div class="col-md-6">
								<h4>Total deposit amount<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4>
								<?php
								if($details[0]['total_deposit_amt'] > 0)
								{
									echo '$'.$details[0]['total_deposit_amt'];
								}
								else
								{
									echo '-';
								}
								?>	
							  		
								<h4>
						     </div>
						         <div class="clear"></div>
						         
                     		 <div class="col-md-6">
								<h4>Credit Used<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4>
								<?php
								if($details[0]['credit_used'] > 0)
								{
									echo '$'.$details[0]['credit_used'];
								}
								else
								{
									echo '-';
								}
								?>	
							  		
								<h4>
						     </div>
						         <div class="clear"></div>

                     		 <div class="col-md-6">
								<h4> Deposit Status <h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php 
								
							if($details[0]['task_type']=='guaranteed' && $details[0]['payment_status']==0 && $details[0]['total_deposit_amt']>0)
							{
										$confirm_url=base_url().'task/buy/'.$details[0]['id'];
								?>
								 <a href="<?php echo $confirm_url;?>" style="margin-bottom: 12px;"  class="">	<button type="button" class="btn btn-success  btn-xs">Pay Now</button></a> 
								<?php
							}
							else if($details[0]['payment_status']==1)
							{
							   echo 'Done';	
							}
							else
							{
								echo '-';
							}
								
							?>
										<h4>
						     </div>
						             <div class="clear"></div>

						    
						    <div class="col-md-6">
								<h4>Customer confirmed working hour(s)<h4>
						     </div>  
						      
						   <div class="col-md-6 detail_col">
								<h4>
								<?php
								if($details[0]['cust_wrk_hrs'] > 0)
								{
									echo $details[0]['cust_wrk_hrs']." hour(s)";
								}
								else
								{
									echo '-';
								}
								?>	
							  		
								<h4>
						    </div>
						    <div class="clear"></div>
						    
						    <div class="col-md-6">
								<h4>Customer confirmed amount<h4>
						     </div>  
						      
						   <div class="col-md-6 detail_col">
								<h4>
								<?php
								if($details[0]['cust_wrk_amt'] > 0)
								{
									echo '$'.$details[0]['cust_wrk_amt'];
								}
								else
								{
									echo '-';
								}
								?>	
							  		
								<h4>
						    </div>
						    <div class="clear"></div>
						    
						    <div class="col-md-6">
								<h4>Handyman confirmed working hour(s)<h4>
						     </div>  
						      
						   <div class="col-md-6 detail_col">
								<h4>
								<?php
								if($details[0]['handy_wrk_hrs'] > 0)
								{
									echo $details[0]['handy_wrk_hrs']." hour(s)";
								}
								else
								{
									echo '-';
								}
								?>	
							  		
								<h4>
						    </div>
						    <div class="clear"></div>
						    
						    <div class="col-md-6">
								<h4>Handyman confirmed amount<h4>
						     </div>  
						      
						   <div class="col-md-6 detail_col">
								<h4>
								<?php
								if($details[0]['handy_wrk_amt'] > 0)
								{
									echo '$'.$details[0]['handy_wrk_amt'];
								}
								else
								{
									echo '-';
								}
								?>	
							  		
								<h4>
						    </div>
						    <div class="clear"></div>
						         
						            
                         
                        </div>
                        
            
                      </div>

                    </div>
                  
                  
                   <?php
			        }
			        else
			        {
						?>
                   <div class="col-md-12 col-sm-12 col-xs-12">

                      <div class="profile_title_dashboard">
                        <div class="col-md-12">
                          <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                              Task Date :  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> <?php 
                              $date=date_create($details[0]['task_date']);
                              echo date_format($date,"D,M d,Y").' ('.date("g:i A", strtotime($details[0]['task_time'])).')';
                              ?>
                          </div>
                        </div>
                        
                        <div class="col-md-12">
							
							 <div class="col-md-6">
								<h4>Task Title<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo ucfirst($details[0]['title']);?><h4>
						     </div>
						     
						             <div class="clear"></div>

							 <div class="col-md-6">
								<h4> User Name<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <a target="_blank" style="color:blue;text-decoration:underline;" href="<?php echo site_url('user_profile/u/'.$details[0]['user_id']);?>"><?php echo $details[0]['first_name']." ".$details[0]['last_name'];?></a><h4>
						     </div>
						     
						             <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Category<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $sub_category[$details[0]['categ_id']];?><h4>
						     </div>
                                 <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Handyman<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php 
								if($details[0]['handyman_id']!=0)
								{
								  echo $handyman_list[$details[0]['handyman_id']];
								 }
								 else
								 {
									 echo '-';
								 }
								  ?>
								  <h4>
						     </div>
                                 <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Address<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $details[0]['address'];?><h4>
						     </div>
						             <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Description<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $details[0]['description'];?><h4>
						     </div>
						             <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Status<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $status[$details[0]['status']];?><h4>
						     </div>
						             <div class="clear"></div>

							 <div class="col-md-6">
								<h4> Created On<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php $date=date_create($details[0]['created']);
                              echo date_format($date,"D,M d,Y");?><h4>
						     </div>
                             <div class="clear"></div>

                     		 <div class="col-md-6">
								<h4> Total Price<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4>$ <?php echo $details[0]['total_price'];?><h4>
						     </div>
						             <div class="clear"></div>

						    
						    <div class="col-md-6">
								<h4>Customer confirmed working hour(s)<h4>
						     </div>  
						      
						   <div class="col-md-6 detail_col">
								<h4>
								<?php
								if($details[0]['cust_wrk_hrs'] > 0)
								{
									echo $details[0]['cust_wrk_hrs']." hour(s)";
								}
								else
								{
									echo '-';
								}
								?>	
							  		
								<h4>
						    </div>
						    <div class="clear"></div>
						    
						    <div class="col-md-6">
								<h4>Customer confirmed amount<h4>
						     </div>  
						      
						   <div class="col-md-6 detail_col">
								<h4>
								<?php
								if($details[0]['cust_wrk_amt'] > 0)
								{
									echo '$'.$details[0]['cust_wrk_amt'];
								}
								else
								{
									echo '-';
								}
								?>	
							  		
								<h4>
						    </div>
						    <div class="clear"></div>
						    
						    <div class="col-md-6">
								<h4>Handyman confirmed working hour(s)<h4>
						     </div>  
						      
						   <div class="col-md-6 detail_col">
								<h4>
								<?php
								if($details[0]['handy_wrk_hrs'] > 0)
								{
									echo $details[0]['handy_wrk_hrs']." hour(s)";
								}
								else
								{
									echo '-';
								}
								?>	
							  		
								<h4>
						    </div>
						    <div class="clear"></div>
						    
						    <div class="col-md-6">
								<h4>Handyman confirmed amount<h4>
						     </div>  
						      
						   <div class="col-md-6 detail_col">
								<h4>
								<?php
								if($details[0]['handy_wrk_amt'] > 0)
								{
									echo '$'.$details[0]['handy_wrk_amt'];
								}
								else
								{
									echo '-';
								}
								?>	
							  		
								<h4>
						    </div>
						    <div class="clear"></div>

                         
                        </div>
                        
            
                      </div>

                    </div>
                   <?php
					}
			        ?>
                </div>
              
             
           
          </div>
          

			
    <script>
    $('#change_pass').click(function(){
		
		 $("#change_password").submit();
		
		});
    </script>

<style>

.profile_right_blk h2
{
    margin-bottom: 0; 
}
.right_form{
	    background: #fff;
    border: 2px solid #a1a1a1;
    border-radius: 5px;	
}

.right_form_button
{
	    padding-bottom: 30px;
}

</style>

<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>

          <div class="profile_right_blk right_form">
                    
                      
				
				  
                <div class="col-md-12 right_form_content">                
                  
                  
                   <div class="col-md-12" >
					 <?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger " style="text-align: center;"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success " style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger" style="text-align: center;"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                       
					 </div>
					 
					 
					 <?php 
					 
					 if($this->session->userdata('user')['user_type']=='user')
					 {
					   echo form_open_multipart(site_url('edit_profile_action'),array('class'=>'',"id"=>'change_password')); ?>                       
					  <div class="form-group">
							  <div class="col-sm-4"><label>First Name: </label></div>
							  
							  <div class="col-sm-7">  
								  <?php echo form_input(array('name'=>'first_name', 'class'=>'form-control',"placeholder"=>"First Name",'label' =>false,'value'=>$this->session->userdata('user')['firstname'])); ?>
							  </div>
				      </div>
				  
				   <div class="form-group">
					  <div class="col-sm-4"><label>Last Name: </label></div>
					  
					  <div class="col-sm-7">  
						  <?php echo form_input(array('name'=>'last_name', 'class'=>'form-control',"placeholder"=>"Last Name",'label' =>false,'value'=>$this->session->userdata('user')['lastname'])); ?>
					  </div>
				  </div>
				  
				   <div class="form-group">
					  <div class="col-sm-4"><label>Profile Picture: </label></div>
					  
					  <div class="col-sm-7">  
						 <?php		  
					    if($details['image']!="")
						{
							
							$ext = array("png", "jpg", "jpeg", "gif", "gif");
							$doc=$details['image'];
							 $file_name=explode(".", $doc);
							
                            if(in_array(end($file_name), $ext)) 
                            {
   
						?>
<a href="<?php echo site_url('/admin/task/download_doc/'.$details['image']);?>">	<img src="<?php echo site_url('/assets/users/'.$details['image']);?>" height="50" width="50"/><p style="width: 20%;text-align: center;"><span></span></p></a>
<?php
                            }
                            
                         }
                         ?>
				     <input onchange='check_upload()' id="file" type="file" name="file" class="file">
					  </div>
				  </div>
				  
				   <div class="form-group">
					  <div class="col-sm-4"><label>Email: </label></div>
					  
					  <div class="col-sm-7">  
						  <?php echo form_input(array('name'=>'email', 'class'=>'form-control',"placeholder"=>"Email",'label' =>false,'value'=>$this->session->userdata('user')['email'])); ?>
						  <div style="margin-top: 10px;" class="alert alert-warning" role="alert">
								<strong>Notice! </strong>Updated email will be applicable for both your account(s) .
						  </div>
					  </div>
                   </div>
                   
                   	<div class="form-group">
							  <div class="col-sm-4"><label>About yourself: </label></div>
							  
							  <div class="col-sm-7">  
								  <textarea class="form-control" name="about_urself"><?php echo $details['about_urself'];?></textarea>
								  
							  </div>
				      </div>
				      
                   	<div class="form-group">
							  <div class="col-sm-4"><label>Education: </label></div>
							  
							  <div class="col-sm-7">  
								  <textarea class="form-control" name="education"><?php echo $details['education'];?></textarea>
								  
							  </div>
				      </div>
                   	<div class="form-group">
							  <div class="col-sm-4"><label>Speciality: </label></div>
							  
							  <div class="col-sm-7">  
								  <textarea class="form-control" name="speciality"><?php echo $details['speciality'];?></textarea>
								  
							  </div>
				      </div>
				  
                   	<div class="form-group">
							  <div class="col-sm-4"><label>Languages: </label></div>
							  
							  <div class="col-sm-7">  
								  <textarea class="form-control" name="languages"><?php echo $details['languages'];?></textarea>
								  
							  </div>
				      </div>
				      
                   	<div class="form-group">
							  <div class="col-sm-4"><label>Working Experience: </label></div>
							  
							  <div class="col-sm-7">  
								  <textarea class="form-control" name="workings"><?php echo $details['workings'];?></textarea>
								  
							  </div>
				      </div>
				  
				  
				  
                  <div class="col-lg-12 text-center right_form_button">
                     <a href="javascript:void(0)" id="change_pass" class="sign-up">Update</a>
                  </div>
                  
                   </form>	
                   <?php
			        }
			        else
			        {
						echo form_open_multipart(site_url('edit_profile_action'),array('class'=>'',"id"=>'change_password')); ?>                       

						                      
					  <div class="form-group">
							  <div class="col-sm-4"><label>Name: </label></div>
							  
							  <div class="col-sm-7">  
								  <?php echo form_input(array('name'=>'name', 'class'=>'form-control',"placeholder"=>"First Name",'label' =>false,'value'=>$details['name'])); ?>
							  </div>
				      </div>
				  
	
				  
				   <div class="form-group">
					  <div class="col-sm-4"><label>Email: </label></div>
					  
					  <div class="col-sm-7">  
						  <?php echo form_input(array('name'=>'email', 'class'=>'form-control',"placeholder"=>"Email",'label' =>false,'value'=>$details['email'])); ?>
						   <div style="margin-top: 10px;" class="alert alert-warning" role="alert">
								<strong>Notice! </strong>Updated email will be applicable for both your account(s) .
						  </div>
					  </div>
                   </div>
                   
                <div class="form-group">
					  <div class="col-sm-4"><label>Phone: </label></div>
					  
					  <div class="col-sm-7">  
						  <?php echo form_input(array('name'=>'phone', 'class'=>'form-control',"placeholder"=>"Phone",'label' =>false,'value'=>$details['phone'])); ?>
					  </div>
				  </div>
				  
                <div class="form-group">
					  <div class="col-sm-4"><label>Company: </label></div>
					  
					  <div class="col-sm-7">  
						  <?php echo form_input(array('name'=>'company', 'class'=>'form-control',"placeholder"=>"Company",'label' =>false,'value'=>$details['company'])); ?>
					  </div>
				  </div>
				  
                <div class="form-group">
					  <div class="col-sm-4"><label>Address: </label></div>
					  
					  <div class="col-sm-7">  
						  <?php echo form_input(array('name'=>'address', 'class'=>'form-control',"placeholder"=>"Address",'label' =>false,'value'=>$details['address'])); ?>
					  </div>
				  </div>
			
			                   	<div class="form-group">
							  <div class="col-sm-4"><label>About yourself: </label></div>
							  
							  <div class="col-sm-7">  
								  <textarea class="form-control" name="about_urself"><?php echo $details['about_urself'];?></textarea>
								  
							  </div>
				      </div>
				      
                   	<div class="form-group">
							  <div class="col-sm-4"><label>Education: </label></div>
							  
							  <div class="col-sm-7">  
								  <textarea class="form-control" name="education"><?php echo $details['education'];?></textarea>
								  
							  </div>
				      </div>
                   	<div class="form-group">
							  <div class="col-sm-4"><label>Speciality: </label></div>
							  
							  <div class="col-sm-7">  
								  <textarea class="form-control" name="speciality"><?php echo $details['speciality'];?></textarea>
								  
							  </div>
				      </div>
				  
                   	<div class="form-group">
							  <div class="col-sm-4"><label>Languages: </label></div>
							  
							  <div class="col-sm-7">  
								  <textarea class="form-control" name="languages"><?php echo $details['languages'];?></textarea>
								  
							  </div>
				      </div>
				      
                   	<div class="form-group">
							  <div class="col-sm-4"><label>Working Experience: </label></div>
							  
							  <div class="col-sm-7">  
								  <textarea class="form-control" name="workings"><?php echo $details['workings'];?></textarea>
								  
							  </div>
				      </div>
				      	  
			  <div class="form-group">
					  <div class="col-sm-4"><label>Profile Picture: </label></div>
					  
					  <div class="col-sm-7">  
						 <?php		  
					    if($details['image']!="")
						{
							
							$ext = array("png", "jpg", "jpeg", "gif", "gif");
							$doc=$details['image'];
							 $file_name=explode(".", $doc);
							
                            if(in_array(end($file_name), $ext)) 
                            {
   
						?>
<a href="<?php echo site_url('/admin/task/download_doc/'.$details['image']);?>">	<img src="<?php echo site_url('/assets/users/'.$details['image']);?>" height="50" width="50"/><p style="width: 20%;text-align: center;"><span></span></p></a>
<?php
                            }
                            
                         }
                         ?>
				     <input onchange='check_upload()' id="file" type="file" name="file" class="file">
					  </div>
				  </div>	  
				            
                      
        <div class="profile_bottom_blk pad_btm_0" >
          <div class="row">
			  <!-- 
            <div class="col-lg-12">
              <div class="title-sec text-center wow fadeIn animated animated" style="visibility: visible; animation-name: fadeIn;">
                <h5>All Services</h5>
              </div>
            </div>
            -->
            <?php
           // echo '<pre>';
           // print_r($details);
              foreach($services as $service_arr)
              {
				  	foreach($service_arr as $service)
				   {
				  
             ?>
             

            
              
            <div class="al-serv" id="subservice_<?php echo $service['id'];?>">
            
            <div class="col-lg-12">
              <div class="title-sec text-center wow fadeIn animated animated" style="visibility: visible; animation-name: fadeIn;">
                <b><?php echo ucfirst($service['name']);?></b>
              </div>
            </div>
            
              <ul>
				  
              <?php
              foreach($service['sub_services'] as $sub)
              {
              ?>

                <li class="wow fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><a href="#">
                  <div>
					  <?php
					  $selected='';
					  foreach($details['work_area'] as $k=>$val)
					  {
						  if($val['categ_id']==$sub['id'])
						  {
							  $selected='checked';
						  }
					  }
					  
					  ?>
                    <input id="checkbox-<?php echo $sub['id'];?>" <?php echo $selected;?> class="checkbox-custom" name="subservices[]" type="checkbox" value="<?php echo $sub['id'];?>">
                    <label for="checkbox-<?php echo $sub['id'];?>" class="checkbox-custom-label"><?php echo ucfirst($sub['name']);?></label>
                  </div>
                  </a>
                 </li>
                <?php
			    }
                ?>
              </ul>
            </div>
         
         <?php
	            }
	            }
	      ?>
         

            
            
            
          </div>
        </div>
      
 
 
      
                  <div class="col-lg-12 text-center right_form_button">
                     <a href="javascript:void(0)" id="change_pass" class="sign-up">Update</a>
                  </div>
                  
                   </form>	
                   <?php
					}
			        ?>
                </div>
              
             
           
          </div>
          

			
    <script>
		function check_upload() {
	 $(".alert-danger").hide();
	 var image_file = document.getElementById("file");
	 var size = image_file.files[0].size/1024/1024;
	
    //checking file name and extension here
   // var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif|.jpeg|.pdf|.doc|.docx|.txt|.xls|.xlsx|.odt|.ods|.csv)$");
   var extensions = ["jpg", "jpeg", "png","gif"];
   var extension = image_file.files[0].name.replace(/.*\./, '').toLowerCase();

	if (extensions.indexOf(extension) < 0) { 
				$("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		$(".alert-danger").text("Supported extension are jpg, png, gif, jpeg").show();
        $("#file").val('');
        return false;
	}
	else
	{
		 if(size > 2){
			 $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
			 $(".alert-danger").text('File size exceed the size of 2MB').show();
			 $("#file").val('');
			 return false;
		
		}
	}

}

    $('#change_pass').click(function(){
		
		 $("#change_password").submit();
		
		});
    </script>

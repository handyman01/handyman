<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<link href="<?php echo assets_url('css/rating.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo assets_url('js/rating.js'); ?>"></script> 


<style>
	.locate_task{
		vertical-align: baseline;
	}
	
	.locate_tasks_btn {
    background-color: rgb(70, 70, 70);
    border-radius: 5px;
    color: rgb(255, 255, 255) !important;
    display: inline-block;
    font-size: 17px;
    height: 50px;
    line-height: 50px;
    padding: 0 30px;
    text-transform: uppercase;
     margin-bottom: 12px; 
}
.profile_right_blk h2
{
    margin-bottom: 0; 
}
.gm-style-iw {
	width: 350px !important;
	top: 15px !important;
	left: 0px !important;
	background-color: #fff;
	box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
	border: 1px solid rgba(72, 181, 233, 0.6);
	border-radius: 2px 2px 10px 10px;
}
#iw-container {
	margin-bottom: 10px;
}
#iw-container .iw-title {
	font-family: 'Open Sans Condensed', sans-serif;
	font-size: 22px;
	font-weight: 400;
	padding: 10px;
	background-color: #48b5e9;
	color: white;
	margin: 0;
	border-radius: 2px 2px 0 0;
	line-height: 1;
}
#iw-container .iw-content {
	font-size: 13px;
	line-height: 18px;
	font-weight: 400;
	margin-right: 1px;
	padding: 15px 5px 20px 15px;
	max-height: 140px;
	overflow-y: auto;
	overflow-x: hidden;
}
.iw-content img {
	float: right;
	margin: 0 5px 5px 10px;	
}
.iw-subTitle {
	font-size: 16px;
	font-weight: 700;
	padding: 5px 0;
}
.iw-bottom-gradient {
	position: absolute;
	width: 326px;
	height: 25px;
	bottom: 10px;
	right: 18px;
	background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
}
</style>


<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>

          <div class="profile_right_blk">
			  <div class="col-md-12" style="margin-bottom: 12px;">
				  
				  <?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger " style="text-align: center;"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success " style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                       
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger" style="text-align: center;"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                       
                       
				<h2>Assigned Tasks</h2>
				
				<div>
		
				<?php
					if(!empty($tasks))
					{ ?>
					
					<a href="#myMapModal"  data-toggle="modal" class="pull-right" style="padding-right: 10px;">
						<button type="button" class="btn btn-info ">Locate My Tasks</button>
					</a>
						
				   
				   <?php } ?>
				</div>
             </div>
             
                          
             <div id="rating" class="modal fade" role="dialog">
				 
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Task Rating</h4>
					  </div>
					  <div class="modal-body">
		               
		               <?php echo form_open_multipart(site_url('ratingToCustomer'),array('class'=>'',"id"=>'rating_form')); ?> 			  
		               
						  <div class="form-group">
							 
							  <input name="rating" value="0" id="rating_star" type="hidden" postID="1" />
							  <input name="task_id" value="0" id="rating_task_id" type="hidden" />
							  <input name="user_id" value="0" id="rating_user_id" type="hidden"  />
							  <input name="user_type" value="handyman" id="" type="hidden"  />
							  <input name="review_by" value="<?php echo $user_detail['id'];?>" id="" type="hidden"  />


							<label for="Message">Your reviews<span class="redcolor">*</span></label>
							<textarea name="review" id="review" cols="" rows="" class="form-control messages" required></textarea>
						  </div>
						  
					   
					    <div class="form-group">
							<button type="button" class="btn btn-default" id="rating_submit" >Submit</button>
					   </div>
					   </form>
					   </div>
					   
					  <!-- <div class="modal-footer">
						<button type="submit" class="btn btn-default" >Submit</button>
					  </div>-->   
						
					
					</div>

				  </div>
				
				
				
				</div>



            <div class="table-responsive" style=" padding: 12px;">
				
										
                  <table class="table table-striped jambo_table bulk_action" id = "datatable">
                        <thead>
                          <tr class="headings">
                    <th>Task No.</th>
                    <th>Submitted On</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>View Details</th>
                          
                          </tr>
                        </thead>

                        <tbody>
					<?php
					if(!empty($tasks))
					{
						//echo '<pre>';
						//print_r($tasks);
						//die;
							
						$count=1;
						
						$marker_array = array();
						foreach($tasks as $k=>$task)
						{
						//	print_r($task);die;
								$var_document='';
						 if($task['document']!="")
						{
							
							$ext = array("png", "jpg", "jpeg", "gif", "gif");
							$doc=$task['document'];
							$file_name=explode(".", $doc);
							
                            if(!in_array(end($file_name), $ext)) 
                            {
   
   
   $var_document='<a href="'.site_url('/admin/task/download_doc/'.$task['document']).'">
<p><img src="'.site_url('/assets/images/task_doc.png').'" height="115" width="86"/><br/></p>
<p style="width: 20%;text-align: center;"><span></span></p></a>';
   

                            }
                            else
                            {
								$var_document='<a href="'.site_url('/admin/task/download_doc/'.$task['document']).'"><img src="'.site_url('/assets/task/'.$task['document']).'" height="115" width="86"/><p style="width: 20%;text-align: center;"><span></span></p></a>';
								
							}
                         }
                         
                         
                         
							$marker_array[$k] = array('lat'=>$task['lat'],'lng'=>$task['lon'],'category'=>ucfirst($task['category_name']),'total_price'=>$task['total_price'],'created_on'=>$task['created'],'description'=>$task['description'],'address'=>$task['address'],'type'=>($task['is_residential']==1 ? 'Residential':'Commercial'),'email'=>$task['customer_email'],'document'=>$var_document,'title'=>ucfirst($task['title']));
						
						?>
						  <tr>
							<td><?php echo $task['id'];?></td>
							<td><?php echo $task['created'];?></td>
							<td><?php echo ucfirst($task['category_name']);?></td>
							<td>$<?php echo $task['total_price'];?></td>
							<td>
								<a href="javascript:void(0)" >
									<?php
									if($task['status']=='CR')
									{
										?>
									<button type="button" class="btn <?php echo $this->config->item('task_status_color')[$task['status']];?> btn-xs">User initiated cancel request</button>	
										<?php
									}
									else
									{
									?>
										<button type="button" class="btn <?php echo $this->config->item('task_status_color')[$task['status']];?> btn-xs"><?php echo $this->config->item('task_status')[$task['status']];?></button>
									<?php
								     }
								   ?>
										
								</a>
							</td>
							<td>
						<a class="googleMapPopUp" rel="nofollow" href="https://maps.google.com/maps?q=<?php echo $task['address'];?>" target="_blank">
									<img class="locate_task" src="<?php echo assets_url('images/marker.png');?>" height="30" width="30" />
									</a>
								<a href="<?php echo site_url('check_task_detail/'.$task['id']);?>"><img src="<?php echo assets_url('/images/view-img.png');?>" alt=""></a>
								<?php
								if($task['status']=='C'  && $task['is_rated']==0)
								{
								   echo ' <a data-toggle="modal" data-target="#rating" data-user="'.$task['user_id'].'"  data-id="'.$task['id'].'" class="btn btn-warning btn-xs rating_button"><i class="fa fa-star"></i> Give Rating </a>	';
								}
								?>
							</td>
						  </tr>
					 <?php
						 $count++;
					   }
					  $locations=json_encode($marker_array,JSON_NUMERIC_CHECK); 
				
					//$array_final = preg_replace('/"([a-zA-Z]+[a-zA-Z0-9_]*)":/','$1:',$locations);

			       
			       }
			       else
			       {
					   ?>
					<tr ><td colspan='7'>No tasks found ! </td></tr>   
					   <?php
				   }
                 ?>

                         </tbody>
                      </table>
                     
           
           
 
            </div>
          </div>
          



		<div class="modal fade" id="myMapModal" >
			<div class="modal-dialog" style="width:90%">
				<div class="modal-content">
					
					<div class="modal-body" style="height: 500px;">
						<div class="container">
							<div class="row">
										<div id="map" style="height: 470px;width: 100%;position: absolute;"></div> 


								<div id="map-canvas" class=""></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.colorbox.js"></script>   
<script>
	
	
	$('.googleMapPopUp').each(function() {
    var thisPopup = $(this);
    thisPopup.colorbox({
        iframe: true,
        innerWidth: 400,
        innerHeight: 300,
        opacity: 0.7,
        href: thisPopup.attr('href') + '&ie=UTF8&t=h&output=embed'
    });
});

        function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
           zoom: 2,
           center: new google.maps.LatLng(52.2, 5)
          //center: {lat:  <?php echo $marker_array[0]['lat'] ?>, lng:  <?php echo $marker_array[0]['lng'] ?>}
        });

 var infowindow = new google.maps.InfoWindow();
        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
       
       /* var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            label: labels[i % labels.length]
          });
          
          
        });
*/
var stack = [];
var markers=[];
var contents = [];
var infowindows = [];

   for (var i = 0; i < locations.length; i++)
   {
                    // obtain the attribues of each marker
                    //console.log(locations);

                    
                      // InfoWindow content
                     var content = '<div id="iw-container">' +
                    '<div class="iw-title">'+locations[i].title+'</div>' +
                    '<div class="iw-content">' +
                      '<div class="iw-subTitle">Category</div><p>'+locations[i].category+'</p><div class="iw-subTitle">Description</div>' +
                      locations[i].document+
                      '<p>'+locations[i].description+'</p>' +
                      '<div class="iw-subTitle">Type</div><p>'+locations[i].type+'</p><div class="iw-subTitle">Address</div><p>'+locations[i].address+'</p><div class="iw-subTitle"></div>' +
                      '<p>'+
                      'Email: <br>'+locations[i].email+'<br></p>'+
                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                  '</div>';
                  
                    // A new Info Window is created and set content
                    /*
  var infowindow = new google.maps.InfoWindow({
    content: content,

    // Assign a maximum value for the width of the infowindow allows
    // greater control over the various content elements
    maxWidth: 350
  }); 
  */
  
                      var lat = parseFloat(locations[i].lat);
                    var lng = parseFloat(locations[i].lng);
                   markers[i] = new google.maps.Marker({
                        position : new google.maps.LatLng(lat, lng),
                        map: map,
                        title:"This is a marker"
                    });    
        
                    stack.push(markers[i]);
                     markers[i].index = i;
                      contents[i] = content;
                    
                    infowindows[i] = new google.maps.InfoWindow({
        content: contents[i],
        maxWidth: 350
    });

    google.maps.event.addListener(markers[i], 'click', function() {
        console.log(this.index); // this will give correct index
        console.log(i); //this will always give 10 for you
        infowindows[this.index].open(map,markers[this.index]);
        map.panTo(markers[this.index].getPosition());
    });  
                }


        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, stack,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

      }
  locations=<?php echo $locations;?>;


		$('#myMapModal').on('show.bs.modal', function() {  
		  resizeMap();
		});
	


    </script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCd_Tujq99uHzmtznB4SqOWvjy6KHmDpzQ&callback=initMap">
</script>
<script>
		google.maps.event.addDomListener(window, 'load', initMap);
	google.maps.event.addDomListener(window, "resize", resizingMap());
	function resizeMap() {
	if(typeof map =="undefined") return;
	setTimeout( function(){resizingMap();} , 400);
	}
	function resizingMap() {
	if(typeof map =="undefined") return;
	google.maps.event.trigger(map, "resize");
	}
	
	
</script>
 <script>
	 


$(document).ready(function() {
	var handyman_id='<?php echo $user_detail["id"];?>';
	
    $('#datatable').DataTable();
     $(document).on('click',".rating_button",function(){
  
    $('#rating_task_id').val($(this).data("id"));
    $('#rating_user_id').val($(this).data("user"));
    });
        $(document).on('click',"#rating_submit",function(){
 
		$('#rating_form').submit();
		//location.reload();
	});
    /*
    $('#rating_submit').click(function(){
		
		task_id=$('#rating_task_id').val();
		user_id=$('#rating_user_id').val();
		rating=$('#rating_star').val();
		 $.ajax({
			type: 'POST',
			url: '<?php echo site_url('ratingToCustomer'); ?>',
			data: {'rating':rating,'user_id':user_id,'task_id':task_id,'user_type':'handyman','review_by':handyman_id,'review':feedbackform},
			success : function(data) {
				if (data.status == 'ok') {
					alert('You have rated '+val+' to CodexWorld');
					$('#avgrat').text(data.average_rating);
					$('#totalrat').text(data.rating_number);
				}else{
					alert('Some problem occured, please try again.');
				}
			}
		});
    
    });
    */

   
    $("#rating_star").codexworld_rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: false,
        imageDirectory: 'images/',
        inputAttr: 'postID'
   
});



} );
</script>

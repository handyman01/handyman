<div class="right_col" role="main">
	<?php
	
	?>
          <div class="">

            <div class="page-title">
			  <div class="col-md-12">
				                     <?php if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
			  </div>
              <div class="title_left">
                <h3>
                      Business Category
                      <small>
                         Listing
                      </small>
                  </h3>

              </div>

              <div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                    <div class="input text"><input type="text" id="search" placeholder="Search for..." class="form-control" name="key"></div>
					<span class="input-group-btn"><button class="btn btn-default" type="button" onclick = "search_result();">Go!</button></span>
                  </div>
                </div>
               
				
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="table-responsive">
						
						
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                             <th class="column-title">S.No. </th>
                            <th class="column-title">Name </th>
                            <th class="column-title">Image </th>
                            <th class="column-title">Icon </th>
                        <?php
                        if($this->uri->segment(4) !==null)
                        {
                        ?>
							<th class="column-title">Status </th>
							<?php
						}
						?>
							
							<th class="column-title">Is Popular </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php 
							$count = 1;
							
							foreach($categories as $data){
								
								 ?>
							<tr id ="user_row_<?= $data['id']; ?>">
								<td><?= $count?>.</td>
	
								<td class=" "><?= $data['name']; ?> </td>
								<td class=" "><img src="<?php echo site_url('/assets/uploads/categories/'.$data['image']);?>" height="50" width="50"/> </td>								<td class=" "><img src="<?php echo site_url('/assets/uploads/categories/'.$data['icon_big']);?>" height="50" width="50"/> </td>
								
							<?php 
							if($data['parent_id']!=-1)
							{
							?>
							
								<td class=" ">
									<input type="hidden" id="user_status_<?= $data['id'] ?>" value ="<?= $data['status']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_user_status(<?php echo $data['id'] ?>)">
									<?php  if($data['status'] == 1){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?></a>
								</td>
							<?php
						     }
						   ?>
								<td class=" ">
									<input type="hidden" id="pop_status_<?= $data['id'] ?>" value ="<?= $data['is_popular']; ?>" />
									<a href="javascript:void(0)" id="pop_id_<?= $data['id']; ?>" onclick="change_popularity(<?php echo $data['id'] ?>)">
									<?php  if($data['is_popular'] == 1){
										echo '<button type="button" class="btn btn-success btn-xs">Make Unpopular</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Make Popular</button>';
									} ?></a>
								</td>
								<td class=" last">
							<a href="<?php echo site_url('/admin/category/edit/'.$data['id']);?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
							
							<a href="<?php echo site_url('/admin/category/manage/'.$data['id']);?>" class="btn btn-info btn-xs"><i class="fa fa-qrcode"></i> Sub Categories </a>
							<?php 
							if($data['parent_id']!=-1)
							{
							?>
									<a href="#" onclick="delete_user(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
							<?php
						}
							?>	
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($categories) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                     
                     
                     
                    </div>
               
               
               
                  </div>
                </div>
              </div>
            </div>
  
  
          </div>
        </div>
<script>
	
function delete_user(id){
	bootbox.confirm("Are you sure?", function(result) {
	
		if(result == true){
			jQuery.ajax({ 
				//url: 'delete',
				url: '<?php echo site_url('/admin/category/delete'); ?>',
				data: {'id':id},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#user_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record has been deleted successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},
				error: function (request) {
					new PNotify({
							  title: 'Error',
							  text: 'This record is being referenced in other place. You cannot delete it.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
					
				},
			});
		}
	});
	
}

function change_user_status(id){
	var status = $("#user_status_"+id).val();
	if(status == 1){
		var ques= "Do you want change the status to DEACTIVE";	
		var status = 0;
		var change = '<button type="button" class="btn btn-danger btn-xs">Deactive</button>'
	}else{
		var ques= "Do you want change the status to ACTIVE";
		var status = 1;
		var change = '<button type="button" class="btn btn-success btn-xs">Active</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo site_url('/admin/category/change_status'); ?>',
				
				data: {'id':id,'status':status},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#status_id_"+id).html(change);
						jQuery("#user_status_"+id).val(status);
						new PNotify({
							  title: 'Success',
							  text: 'Status changed successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You do not have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
		}
	});

	
}
function change_popularity(id){
	var status = $("#pop_status_"+id).val();
	if(status == 1){
		var ques= "Do you want change the status to DEACTIVE";	
		var status = 0;
		var change = '<button type="button" class="btn btn-danger btn-xs">Make Popular</button>'
	}else{
		var ques= "Do you want change the status to ACTIVE";
		var status = 1;
		var change = '<button type="button" class="btn btn-success btn-xs">Make Unpopular</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo site_url('/admin/category/change_popularity'); ?>',
				
				data: {'id':id,'status':status},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#pop_id_"+id).html(change);
						jQuery("#pop_status_"+id).val(status);
						new PNotify({
							  title: 'Success',
							  text: 'Popularity changed successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You do not have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
		}
	});

	
}

</script>

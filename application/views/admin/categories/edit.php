<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>

<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				 <?php echo form_open(site_url('/admin/category/edit_action'),array("enctype"=>"multipart/form-data",'class'=>'form-horizontal form-label-left')); 
				  
					  if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>

                      <span class="section">Edit Category</span>
					  

                      <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                      <input type="hidden" name="id" value="<?php echo $details['id'];?>">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					     <?php echo form_input(array('name'=>'name', 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Name",'label' =>false,'required'=>'required',"value"=>$details['name'])); ?>
                         
                        </div>
                      </div>
                      
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hourly_price">Hourly Price <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					     <?php echo form_input(array('name'=>'hourly_price','id'=>'hourly_price',"value"=>$details['hourly_price'], 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Hourly Price",'label' =>false,'required'=>'required')); ?>
                         
                        </div>
                      </div>
                      
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hourly_price">Deposit Hours <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					     <?php echo form_input(array('name'=>'deposit_hrs','id'=>'deposit_hrs',"value"=>$details['deposit_hrs'], 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Deposit Hours",'label' =>false,'required'=>'required')); ?>
                         
                        </div>
                      </div>
                      
                      
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Overtime Charge <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					     <?php echo form_input(array('name'=>'overtime_charge',"value"=>$details['overtime_charge'], 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"overtime_charge",'label' =>false,'required'=>'required')); ?>
                         
                        </div>
                      </div>
                      
                      <?php
                      if($details['parent_id']!=-1)
                      {
                      ?>
                                            
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Parent Category <span class="required">*</span>
                        </label>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12">
						  <?php echo form_dropdown('parent_id', $categories,$details['parent_id'] , 'class="form-control col-md-7 col-xs-12" id="my_id" required="required"'); ?>
                        <!--  <span>Leave this field blank if creating parent category</span> -->
                         
                        </div>
                      </div>
                                         
                   <?php
				     }
				     ?>
                   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Image <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         	<input multiple="multiple" name="picture" size="20" type="file"/>
<img src="<?php echo site_url('/assets/uploads/categories/'.$details['image']);?>" height="100" width="100"/> 
                        </div>
                      </div>
                      
                   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Icon <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         	<input multiple="multiple" name="icon_big" size="20" type="file"/>
<img src="<?php echo site_url('/assets/uploads/categories/'.$details['icon_big']);?>" height="100" width="100"/> 
                        </div>
                      </div>
                      
                                      
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							 <button type="reset" class="btn btn-primary">Reset</button>
							  <button type="submit" class="btn btn-success">Submit</button>
						
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <script>
				$(document).on('keypress',"#hourly_price",function(e){
	 
	 var vall = String.fromCharCode(e.which);

     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57 )) {
            e.preventDefault();
              // return false;
      }

   });
				$(document).on('keypress',"#deposit_hrs",function(e){
	 
	 var vall = String.fromCharCode(e.which);

     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0  && (e.which < 48 || e.which > 57 )) {
            e.preventDefault();
              // return false;
      }

   });
   
			</script>

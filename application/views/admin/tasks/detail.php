<script>
			var show_alert1=0;
			var show_alert2=0;
			var difference_hrs=0;
			var difference_msg1='';
			var difference_msg2='';
			var difference_amt=0;
<?php
$show_diff1=0;
$show_diff2=0;
$difference_hrs=0;
$difference_amt=0;
if($details[0]['cust_wrk_hrs']!=0 && $details[0]['handy_wrk_hrs']!=0)
{
	if($details[0]['cust_wrk_hrs']>$details[0]['handy_wrk_hrs'])
	{
		$show_diff1=1;
		$difference_hrs=$details[0]['cust_wrk_hrs']-$details[0]['handy_wrk_hrs'];
	?>

	 show_alert1=1;   
	 difference_hrs=<?php echo $details[0]['cust_wrk_hrs']-$details[0]['handy_wrk_hrs'] ?>;
	 difference_msg1="Customer's confirmed hour(s) are different from handyman's working hour(s) .<br/> Total Difference : "+difference_hrs+" hr(s)";
	<?php
	}
	else if($details[0]['handy_wrk_hrs']>$details[0]['cust_wrk_hrs'])
	{
		$show_diff1=1;
		$difference_hrs=$details[0]['handy_wrk_hrs']-$details[0]['cust_wrk_hrs'];
	?>
	 show_alert1=1;   
	 difference_hrs=<?php echo $details[0]['handy_wrk_hrs']-$details[0]['cust_wrk_hrs'] ?>;
	 difference_msg1="Customer's confirmed hour(s) are different from handyman's working hour(s) .<br/> Total Difference : "+difference_hrs+" hr(s)";

	<?php
	}
}
?>
<?php


if($details[0]['cust_wrk_amt']!=0 && $details[0]['handy_wrk_amt']!=0)
{
if($details[0]['cust_wrk_amt']>$details[0]['handy_wrk_amt'])
{
	$show_diff2=1;
	$difference_amt=$details[0]['cust_wrk_amt']-$details[0]['handy_wrk_amt'];
?>
 show_alert2=1;   
 difference_amt=<?php echo $details[0]['cust_wrk_amt']-$details[0]['handy_wrk_amt'] ?>;
  difference_msg2="Customer's confirmed amount is different from handyman's confirmed amount. <br/> Total Difference : $ "+difference_amt;

<?php
}
else if($details[0]['handy_wrk_amt']>$details[0]['cust_wrk_amt'])
{
	$show_diff2=1;
	$difference_amt=$details[0]['handy_wrk_amt']-$details[0]['cust_wrk_amt'];
?>
  show_alert2=1;   
  difference_amt=<?php echo $details[0]['handy_wrk_amt']-$details[0]['cust_wrk_amt'] ?>;
  difference_msg2="Customer's confirmed amount is different from handyman's confirmed amount. <br/> Total Difference : $ "+difference_amt;



<?php
}
}
?>
</script>
        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
				   <div class="col-md-12">
				                     <?php if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
			  </div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Task Details:</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
						<?php
						
						if($details[0]['document']!="")
						{
							
							$ext = array("png", "jpg", "jpeg", "gif", "gif");
							$doc=$details[0]['document'];
							 $file_name=explode(".", $doc);
							
                            if(!in_array(end($file_name), $ext)) 
                            {
   
						?>
						<a href="<?php echo site_url('/admin/task/download_doc/'.$details[0]['document']);?>">
<p><img src="<?php echo site_url('/assets/images/task_doc.png');?>" height="50" width="50"/><br/></p>
<p style="width: 20%;text-align: center;"><span><i class="fa fa-download"></i></span></p></a>
<?php
                            }
                            else
                            {
								?>
							<a href="<?php echo site_url('/admin/task/download_doc/'.$details[0]['document']);?>">	<img src="<?php echo site_url('/assets/task/'.$details[0]['document']);?>" height="50" width="50"/><p style="width: 20%;text-align: center;"><span><i class="fa fa-download"></i></span></p></a>
								<?php
							}
                         }
                         
?>
                    </div>
        
        
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
							
							 <div class="col-md-6">
								<h4>Task Title<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo ucfirst($details[0]['title']);?><h4>
						     </div>
						     
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> User Name<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo '<a target="_blank" style="color:blue;text-decoration:underline;" href="'.site_url('user_profile/u/'.$details[0]['user_id']).'">'.$details[0]['first_name']." ".$details[0]['last_name'].'</a>'; ?><h4>
						       
						        
						 <?php
									if($details[0]['block_till']!='' && strtotime(date('Y-m-d H:i:s')) < strtotime($details[0]['block_till']))
									{
										?>
								<a href="javascript:void(0);" title="<?= 'Block Till '.$details[0]['block_till']; ?>" class="btn btn-danger btn-xs unblock" id="unblock$$<?= $details[0]['user_id']; ?>"><i class="fa fa-ban"></i> Unblock </a>		
										<?php
										
									}
									else
									{
										?>
										<a href="<?php echo site_url('/admin/user/block_user/'.$details[0]['user_id']);?>" class="btn btn-info btn-xs"><i class="fa fa-ban"></i> Block </a>		
										<?php
									}
									?>
									
									
									
						         
						         <?php
						         if($details[0]['store_credit_status']==0)
						         {
						         ?>
						         								
									<a href = "#" data-toggle="modal" data-target="#myModal<?= $details[0]['id']; ?>" class = "btn btn-success btn-xs" > <i class="fa fa-paypal"></i> Store Credit </a>
						
                                    <?php
								}
								else
								{
								?>
								
								<a href="javascript:void(0);" class="btn btn-info btn-xs"> <?php echo $details[0]['store_credit_percent']; ?> % credit stored</a>
								<?php	
								}
                                    ?>
								  <div class="modal fade" id="myModal<?= $details[0]['id']; ?>" role="dialog">
									<div class="modal-dialog modal-sm">
									  <div class="modal-content">
										<div class="modal-header">
										  <button type="button" class="close" data-dismiss="modal">&times;</button>
										  <h4 class="modal-title" style="font-weight:bold;">Store Credit:</h4>
										</div>
										<?php echo form_open(site_url('/admin/task/store_credit_user_action'),array('class'=>'form-horizontal form-label-left')); ?>
												<div class="modal-body">
													
					<div class="item form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">Total Amount: 
                        </label>
                     
                        <label class="control-label col-md-6 col-sm-6 col-xs-12 " for="name"><span class="required pull-left">$ <span id="total_price"><?php echo $details[0]['total_price'];?></span></span>
                        </label>
                        
                      </div>
                      
					<div class="item form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">Send Credit: 
                        </label>
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="name">
                        <input type="text" name="credit_percent" id="credit_percent" value="" placeholder="%" class="form-control col-md-6 col-xs-12"> 
                        <div id="final_credit_amount">$0</div>
                        </label>
                      </div>
                      
                     <input type="hidden" name="crredit_amt" id="crredit_amt" value="" >	
                      
					 <input type="hidden" name="task_id" id="" value="<?= $details[0]['id']; ?>">	
					 <input type="hidden" name="user_id" id="" value="<?= $details[0]['user_id']; ?>">	
																					
													
													
													
												  
												</div>
												<div class="modal-footer">
												  <button type="submit" class="btn btn-success btn-xs" >Send</button>
												   <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Close</button>
												</div>
										</form>
										
									  </div>
									</div>
								
								
								  </div>
								  
								  
						     </div>
						     
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Category<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $sub_category[$details[0]['categ_id']];?><h4>
						     </div>
                          <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Handyman<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php
								
								 if($details[0]['handyman_id'] !=0 ){echo '<a target="_blank" style="color:blue;text-decoration:underline;" href="'.site_url('user_profile/h/'.$details[0]['handyman_id']).'">'.$handyman_list[$details[0]['handyman_id']].'</a>'; 
									?>
									</h4>
									
									<!-- <a href="<?php echo site_url('/admin/user/block_handyman/'.$details[0]['handyman_id']);?>" class="btn btn-info btn-xs"><i class="fa fa-ban"></i> Block Handyman </a> -->
									
									<?php
									
									}else{ echo "No handyman"; }?>
								
									
						     </div>
                          <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Address<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $details[0]['address'];?><h4>
										 <a class="googleMapPopUp" rel="nofollow" href="https://maps.google.com/maps?q=<?php echo $details[0]['address'];?>" target="_blank">
									<img class="locate_task" src="<?php echo assets_url('images/marker.png');?>" height="30" width="30" />
									</a>
									
                         <a class="googleMapPopUp" rel="nofollow" href="https://maps.google.com/maps?q=<?php echo $details[0]['address'];?>" target="_blank">
									View on Map
									</a>
									
						     </div>
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Description<h4>
						     </div>
						     
							 <div class="col-md-6">
								
								
								<h4><?php echo  substr($details[0]['description'],0,70); ?> ... <a href = "#" data-toggle="modal" data-target="#myModalll" class = "green" >Continue</a></h4>
								
							
						
								  <div class="modal fade" id="myModalll" role="dialog">
									<div class="modal-dialog modal-sm">
									  <div class="modal-content">
										<div class="modal-header">
										  <button type="button" class="close" data-dismiss="modal">&times;</button>
										  <h4 class="modal-title">Description</h4>
										</div>
										<div class="modal-body">
										  <p><?= $details[0]['description']; ?></p>
										</div>
										<div class="modal-footer">
										  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									  </div>
									</div>
								  </div>
								  
								  
								  
						     </div>
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Status<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $status[$details[0]['status']];?><h4>
						     </div>
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Created On<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php $date=date_create($details[0]['created']);
                              echo date_format($date,"D,M d,Y");?><h4>
						     </div>
                            <div class="clearfix"></div>
                     		 <div class="col-md-6">
								<h4> Total Task Amount<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4>$ <?php echo $details[0]['total_price'];?><h4>
						     </div>
						      <div class="clearfix"></div>
						      
						      
                     		 <div class="col-md-6">
								<h4> Total deposit amount<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4><?php
								if($details[0]['total_deposit_amt'] > 0)
								{
									echo '$ '.$details[0]['total_deposit_amt'];
								}
								else
								{
									echo '-';
								}
								?>	<h4>
						     </div>
						      <div class="clearfix"></div>
						      
						      
                     		 <div class="col-md-6">
								<h4> Credit Used<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4><?php
								if($details[0]['credit_used'] > 0)
								{
									echo '$ '.$details[0]['credit_used'];
								}
								else
								{
									echo '-';
								}
								?>		<h4>
						     </div>
						      <div class="clearfix"></div>
                     		 <div class="col-md-6">
								<h4>Customer Deposit Status <h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php	
								
								if($details[0]['payment_status']==1)
								{ echo "Done";
									}
									else
									{
										echo 'Not Done';
										
										} ?><h4>
						     </div>
						     
						     <?php
						     if($details[0]['cust_wrk_hrs']!=0 && $details[0]['cust_wrk_amt']!=0)
						     {
						     ?>    
						     <div class="col-md-6">
								<h4>Customer confirmed Hour(s) <h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4><?php echo $details[0]['cust_wrk_hrs'];?><h4>
						     </div>
						      <div class="clearfix"></div>
						      
						     <div class="col-md-6">
								<h4>Customer confirmed Amount<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4>$ <?php echo $details[0]['cust_wrk_amt'];?><h4>
						     </div>
						      <div class="clearfix"></div>
						      
						      <?php
						      }
						      ?>
						      
						     <?php
						     if($details[0]['handy_wrk_hrs']!=0 && $details[0]['handy_wrk_amt']!=0)
						     {
						     ?>    
						     <div class="col-md-6">
								<h4>Handyman confirmed Hour(s) <h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4><?php echo $details[0]['handy_wrk_hrs'];?><h4>
						     </div>
						      <div class="clearfix"></div>
						      
						     <div class="col-md-6">
								<h4>Handyman confirmed Amount<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4>$ <?php echo $details[0]['handy_wrk_amt'];?><h4>
						     </div>
						      <div class="clearfix"></div>
						      
						      <?php
						      }
						      ?>
						      
						      <?php
						      if($show_diff1==1)
						      {
								  ?>
							 <div class="col-md-6">
								<h4>Hour(s) Difference <h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4><?php echo $difference_hrs;?><h4>
						     </div>
						      <div class="clearfix"></div>
						      
								  <?php
							  }
						      ?>
						      <?php
						      if($show_diff2==1)
						      {
								  ?>
							 <div class="col-md-6">
								<h4>Amount Difference <h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4>$ <?php echo $difference_amt;?><h4>
						     </div>
						      <div class="clearfix"></div>
						      
								  <?php
							  }
						      ?>
                         
                        </div>
                        
                        <div class="col-md-6">
                          <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                              Task Date :  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> <?php 
                              $date=date_create($details[0]['task_date']);
                              echo date_format($date,"D,M d,Y").' ('.date("g:i A", strtotime($details[0]['task_time'])).')';
                              ?>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
 
<script>
	$('.unblock').click(function(){
                 var id=$(this).attr('id');
                 var id_parts=id.split("$$");
				jQuery.ajax({ 
				url: '<?php echo site_url('/admin/user/unblock_user'); ?>',
				
				data: {'id':id_parts[1]},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
					location.reload();
						
					}
					if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You do not have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
			
	});	
      $(document).ready(function() {
		 if(show_alert1==1)
		 { 
        new PNotify({
          title: "PNotify",
          type: "info",
          text: difference_msg1,
          nonblock: {
              nonblock: true
          },
          addclass: 'dark',
          styling: 'bootstrap3',
          hide: false,
          before_close: function(PNotify) {
            PNotify.update({
              title: PNotify.options.title + " - Enjoy your Stay",
              before_close: null
            });

            PNotify.queueRemove();

            return false;
          }
        });
	  }
		 if(show_alert2==1)
		 { 
        new PNotify({
          title: "PNotify",
          type: "info",
          text: difference_msg2,
          nonblock: {
              nonblock: true
          },
          addclass: 'dark',
          styling: 'bootstrap3',
          hide: false,
          before_close: function(PNotify) {
            PNotify.update({
              title: PNotify.options.title + " - Enjoy your Stay",
              before_close: null
            });

            PNotify.queueRemove();

            return false;
          }
        });
	  }

      });
    </script>
    
      <script>

   $(document).on('keyup',"#credit_percent",function(e){
    
        var vall = $('input:text[name=credit_percent]').val();
		

		  
          per_amt=(parseFloat($("#total_price").text())*vall)/100;

	   	
	      $("#final_credit_amount").text('$ '+per_amt.toFixed(2));
	      $("#crredit_amt").val(per_amt.toFixed(2));
    });
    
      $(document).on('keypress',"#credit_percent",function(e){
	 
	 var vall = String.fromCharCode(e.which);

     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            e.preventDefault();
              // return false;
      }

   });
   
   
			</script>
			
    

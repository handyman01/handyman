
 <link href="<?php echo assets_url('css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css"/> 
<script src="<?php echo assets_url('js/bootstrap-datetimepicker.min.js'); ?>"></script> 

<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>

<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				 <?php echo form_open(site_url('/admin/user/block_user_action'),array('class'=>'form-horizontal form-label-left')); 
				  
					  if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>

                      <span class="section">Store Credit</span>
					  

                      <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                      <input type="hidden" name="id" value="<?php echo $details[0]['id'];?>">
                      
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Task Title <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							
					     <?php echo form_input(array('name'=>'title', 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Title",'label' =>false,'required'=>'required',"value"=>$details[0]['title'])); ?>
                         
                        </div>
                      </div>
   
   
                      
   
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							 
							  <button type="submit" class="btn btn-success">Submit</button>
						
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

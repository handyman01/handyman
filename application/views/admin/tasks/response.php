<div class="right_col" role="main">
	<?php
	//echo '<pre>';
	//print_r($tasks);
	//die;
	?>
     <div class="title_left">
                <h3>
                      Task Response
                      <small>
                         Listing
                      </small>
                  </h3>
                  
                  <?php
                  $download_link=site_url();
                  if($this->uri->segment(1) !==null)
                  {
					  $download_link.=$this->uri->segment(1);
				  }
                  if($this->uri->segment(2) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(2);
				  }
                  if($this->uri->segment(3) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(3);
				  }
                  if($this->uri->segment(4) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(4);
				  }
                  if($this->uri->segment(5) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(5);
				  }
                  if($this->uri->segment(6) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(6);
				  }
				  $curr_page=$download_link;
				  $download_link.='/download';
                  ?>
         
      </div>
                <div class="">

             <div class="title_right">
				<!--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                    <div class="input text"><input type="text" id="search" placeholder="Search for..." class="form-control" name="key"></div>
					<span class="input-group-btn"><button class="btn btn-default" type="button" onclick = "search_result();">Go!</button></span>
                  </div>
                </div>
               --->
				
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="">
						
						
                      <table class="table table-striped jambo_table bulk_action" id = "datatable">
                        <thead>
                          <tr class="headings">
							<th class="column-title">S.No.</th>
							<th class="column-title">Handyman ID</th>
							<th class="column-title">Handyman Name</th>
							<th class="column-title">Applied On</th>
							<th class="column-title">Category</th>
							<th class="column-title">View Details</th>
						  </tr>
                        </thead>

                        <tbody>
							<?php 
							$count = 1;
							foreach($handy as $detail){
							?>
							<tr >
								<td><?= $count?>.</td>
								<td class=" "><?= $detail['handy_id'];?> </td>
								<td class=" "><?= $detail['name'];?> </td>
								<td class=" "><?= $detail['applied_tasks_created'];?> </td>
								<td class=" "><?= ucfirst($detail['category_name']);?></td>
								<td>
									
									<a href="<?php echo site_url('/admin/task/responseDetail/'.$detail['handy_id'].'/'.$task_id);?>" class="btn btn-warning btn-xs"><i class="fa fa-eye"></i> Details </a>		
									
									<?php
									
								    if($detail['assigned_handyman']==$detail['handy_id'])
								    {
										?>
										
									  <a href="javascript:void(0);" class="btn btn-success btn-xs"> Approved</a>
										<?php
										
									}
								    
								    
									?>	
									
								
							</td>
							</tr>
							<?php  $count++;
							} ?> 
							
							<?php  if(count($handy) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                     
                     
                     
                    </div>
               
               
               
                  </div>
                </div>
              </div>
            </div>
  
  
          </div>
        </div>
<script>
	
function delete_user(id){
	bootbox.confirm("Are you sure?", function(result) {
	
		if(result == true){
			jQuery.ajax({ 
				//url: 'delete',
				url: '<?php echo site_url('/admin/user/delete_customer'); ?>',
				data: {'id':id,'user_type':1},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#user_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record has been deleted successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},
				error: function (request) {
					new PNotify({
							  title: 'Error',
							  text: 'This record is being referenced in other place. You cannot delete it.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
					
				},
			});
		}
	});
	
}
function change_user_status(id){
	var status = $("#user_status_"+id).val();
	if(status == 1){
		var ques= "Do you want change the status to DEACTIVE";	
		var status = 0;
		var change = '<button type="button" class="btn btn-danger btn-xs">Deactive</button>'
	}else{
		var ques= "Do you want change the status to ACTIVE";
		var status = 1;
		var change = '<button type="button" class="btn btn-success btn-xs">Active</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo site_url('/admin/user/change_status_customer'); ?>',
				
				data: {'id':id,'status':status,'user_type':1},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#status_id_"+id).html(change);
						jQuery("#user_status_"+id).val(status);
						new PNotify({
							  title: 'Success',
							  text: 'Status changed successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You do not have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
		}
	});

	
}
</script>
    <!-- datepicker -->
    <script type="text/javascript">
      $(document).ready(function() {

        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2016',
          maxDate: '12/31/2020',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Apply',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
          
        };
        
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        
        $('#start_date').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
        $('#end_date').val(moment().format('YYYY-MM-DD'));
        
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		  $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
		  $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
		  $('#start_date').val('');
		  $('#end_date').val('');
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>

   <?php
  // echo '<pre>';
   //print_r($details);
   //die;
   ?>
        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Task Details:</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">


                    </div>
        
        
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
							
							 <div class="col-md-6">
								<h4>Task Title<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo ucfirst($details[0]['title']);?><h4>
						     </div>
						      <div class="clearfix"></div>
						     
							 <div class="col-md-6">
								<h4> User Name<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $details[0]['first_name']." ".$details[0]['last_name'];?><h4>
						     </div>
						     
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Category<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $sub_category[$details[0]['categ_id']];?><h4>
						     </div>
                          <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Handyman<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $handyman_list[$details[0]['handyman_id']];?><h4>
						     </div>
                          <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Address<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $details[0]['address'];?><h4>
						     </div>
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Description<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $details[0]['description'];?><h4>
						     </div>
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Status<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $status[$details[0]['status']];?><h4>
						     </div>
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Created On<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php $date=date_create($details[0]['created']);
                              echo date_format($date,"D,M d,Y");?><h4>
						     </div>
                      <div class="clearfix"></div>
                     		 <div class="col-md-6">
								<h4> Total Price<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4>$ <?php echo $details[0]['total_price'];?><h4>
						     </div>
						      <div class="clearfix"></div>
                     		 <div class="col-md-6">
								<h4> Payment <h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php if($details[0]['payment_status']==1)
								{ echo "Done";
									}
									else
									{
										echo 'Not Done';
										} ?><h4>
						     </div>
						     
						         
                         
                        </div>
                        
                        <div class="col-md-6">
                          <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                              Task Date :  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> <?php 
                              $date=date_create($details[0]['task_date']);
                              echo date_format($date,"D,M d,Y").' ('.date("g:i A", strtotime($details[0]['task_time'])).')';
                              ?>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

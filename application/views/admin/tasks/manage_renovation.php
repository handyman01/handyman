<div class="right_col" role="main">
	<?php
	//echo '<pre>';
	//print_r($tasks);
	//die;
	?>
     <div class="title_left">
                <h3>
                      Tasks
                      <small>
                         Listing
                      </small>
                  </h3>
                  
                  <?php
                  $download_link=site_url();
                  if($this->uri->segment(1) !==null)
                  {
					  $download_link.=$this->uri->segment(1);
				  }
                  if($this->uri->segment(2) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(2);
				  }
                  if($this->uri->segment(3) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(3);
				  }
                  if($this->uri->segment(4) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(4);
				  }
                  if($this->uri->segment(5) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(5);
				  }
                  if($this->uri->segment(6) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(6);
				  }
				  				  $curr_page=$download_link;

				  $download_link.='/download';
                  ?>
           <a href="<?php echo $download_link;?>">
             <span class="count_top pull-right" style="font-weight: bold;font-size: 15px;"><i class="fa fa-download"></i> Download Excel Data</span>      
            </a>
      </div>
                <div class="">

            <div class="page-title">
			  <div class="col-md-12">
				                     <?php if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
			  </div>


              
	          <!-- top tiles -->
          <div class="row tile_count" >
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/task/filter_renovation/submitted/M');?>">
              <span class="count_top"><i class="fa fa-user"></i> Total Submitted</span>
              <div class="count"><?php echo $submitted_month;?></div>
                </a>
                <span class="count_bottom">This Month</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/task/filter_renovation/completed/M');?>">
              <span class="count_top"><i class="fa fa-user"></i> Total Completed</span>
              <div class="count"><?php echo $completed_month;?></div>
              </a>
               <span class="count_bottom">This Month</span>
            </div>
            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/task/filter_renovation/active/M');?>">
              <span class="count_top"><i class="fa fa-user"></i> Total Active</span>
              <div class="count"><?php echo $active_month;?></div>
              </a>
               <span class="count_bottom">This Month</span>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			 
              <span class="count_top"><i class="fa fa-user"></i> Total Amount</span>
              <div class="count">$ <?php echo number_format($amount_month['total_price'],2);?></div>
               <span class="count_bottom">This Month</span>
         
            </div>
            
            <!-- this month -->
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/task/filter_renovation/submitted/Y');?>">
              <span class="count_top"><i class="fa fa-user"></i> Total Submitted</span>
              <div class="count"><?php echo $submitted_year;?></div>
                </a>
                <span class="count_bottom">This Year</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/task/filter_renovation/completed/Y');?>">
              <span class="count_top"><i class="fa fa-user"></i> Total Completed</span>
              <div class="count"><?php echo $completed_year;?></div>
              </a>
               <span class="count_bottom">This Year</span>
            </div>
            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/task/filter_renovation/active/Y');?>">
              <span class="count_top"><i class="fa fa-user"></i> Total Active</span>
              <div class="count"><?php echo $active_year;?></div>
              </a>
               <span class="count_bottom">This Year</span>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			 
              <span class="count_top"><i class="fa fa-user"></i> Total Amount</span>
              <div class="count">$ <?php echo number_format($amount_year['total_price'],2);?></div>
               <span class="count_bottom">This Year</span>
         
            </div>

          </div>
          <!-- /top tiles -->
          
            <div class="col-md-12 col-sm-9 col-xs-12 ">
                      <?php echo form_open(site_url('/admin/task/searchByDate'),array('class'=>'form-horizontal form-label-left')); 
                      ?>
							<input type="hidden" name="curr_page" id="curr_page" value="<?php echo $curr_page;?>">
							<input type="hidden" name="start_date" id="start_date" value="">
							<input type="hidden" name="end_date" id="end_date" value="">
							<div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
							Search By Date: <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
							<span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                      
                          </div>
                         
                                 <button id="search_bydate" type="submit" style="margin-top:7px;" class="btn btn-success btn-xs pull-right">Search</button>
                        </form>
                      
                 </div>

              <div class="title_right">
				<!--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                    <div class="input text"><input type="text" id="search" placeholder="Search for..." class="form-control" name="key"></div>
					<span class="input-group-btn"><button class="btn btn-default" type="button" onclick = "search_result();">Go!</button></span>
                  </div>
                </div>
               --->
				
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="">
						
						
                      <table class="table table-striped jambo_table bulk_action" id = "datatable">
                        <thead>
                          <tr class="headings">
                             <th class="column-title">S.No.</th>
                             <th class="column-title">Task No.</th>
                             <th class="column-title">Submitted On</th>
                             <th class="column-title">Customer No.</th>
                            <th class="column-title">Customer</th>
                            <th class="column-title">Handyman No.</th>
                            <th class="column-title">Handyman</th>
                            <th class="column-title">Service</th>
                            <th class="column-title">Price </th>
							<th class="column-title">Payment By Customer</th>
							<th class="column-title">Post to Website </th>
							<th class="column-title">Status </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php 
							$count = 1;

							foreach($tasks as $data){
							?>
							<tr id ="user_row_<?= $data['id']; ?>">
								<td><?= $count?>.</td>
	
								<td class=" "><?= $data['id']; ?> </td>
								<td class=" "><?= $data['created']; ?> </td>
								<td class=" "><?= $data['user_id']; ?> </td>
								<td class=" "><?= $data['first_name'].' '.$data['last_name']; ?> </td>
								<td class=" "><?= ($data['handyman_id'] ==0 ? 'Not Assigned':$data['handyman_id']); ?> </td>
								<td class=" "><?= $data['name']; ?> </td>
								<td class=" "><?= $data['category_name']; ?> </td>
								<td class=" "><?= $data['total_price']; ?> </td>
								<td class=" "><?= ($data['payment_status']==0 ? 'Not Done' :'Done'); ?> </td>
								
								
						   <td class=" ">
									<input type="hidden" id="user_status_<?= $data['id'] ?>" value ="<?= $data['is_posted']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_user_status(<?php echo $data['id'] ?>)">
									<?php  if($data['is_posted'] == 1){
										echo '<button type="button" class="btn btn-danger btn-xs">Unpost Task</button>'; 
									}else{
										echo '<button type="button" class="btn btn-success btn-xs">Post Task</button>';
									} ?></a>
							</td>
								
								

								
								
								<td class=" ">
									<a href="javascript:void(0)" >
									
										<button type="button" class="btn <?php echo $this->config->item('task_status_color')[$data['status']];?> btn-xs"><?php echo $this->config->item('task_status')[$data['status']];?></button>
									</a>
								</td>
								<td class=" last">
									<a href="<?php echo site_url('/admin/task/detail/'.$data['id']);?>" class="btn btn-warning btn-xs"><i class="fa fa-search"></i> Details </a>	
									<?php if($data['status'] != 'C'){?>	
									<a href="<?php echo site_url('/admin/task/edit/'.$data['id']);?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
									<?php } ?>
									<!--<a href="#" onclick="delete_user(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>-->
									<a href="<?php echo site_url('/admin/task/response_renovation/'.$data['id']);?>" class="btn btn-success btn-xs"><i class="fa fa-reply"></i> <?=$data['response_count']; ?> Response </a>
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($tasks) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                     
                     
                     
                    </div>
               
               
               
                  </div>
                </div>
              </div>
            </div>
  
  
          </div>
        </div>
<script>
	
function delete_user(id){
	bootbox.confirm("Are you sure?", function(result) {
	
		if(result == true){
			jQuery.ajax({ 
				//url: 'delete',
				url: '<?php echo site_url('/admin/user/delete_customer'); ?>',
				data: {'id':id,'user_type':1},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#user_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record has been deleted successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},
				error: function (request) {
					new PNotify({
							  title: 'Error',
							  text: 'This record is being referenced in other place. You cannot delete it.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
					
				},
			});
		}
	});
	
}
function change_user_status(id){
	var status = $("#user_status_"+id).val();
	if(status == 1){
		var ques= "Do you want to unpost the task on website";	
		var status = 0;
		var change = '<button type="button" class="btn btn-success btn-xs">Post Task</button>';
	}else{
		var ques= "Do you want to post task on website";
        var status = 1;
		var change = '<button type="button" class="btn btn-danger btn-xs">Unpost Task</button>'
	}
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo site_url('/admin/task/post_task_status'); ?>',
				
				data: {'id':id,'status':status},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#status_id_"+id).html(change);
						jQuery("#user_status_"+id).val(status);
						new PNotify({
							  title: 'Success',
							  text: 'Status changed successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You do not have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
		}
	});

	
}
function change_taskpost_status(id){
	var status = $("#user_status_"+id).val();
	if(status == 1){
		var ques= "Do you want change the status to DEACTIVE";	
		var status = 0;
		var change = '<button type="button" class="btn btn-danger btn-xs">Deactive</button>'
	}else{
		var ques= "Do you want change the status to ACTIVE";
		var status = 1;
		var change = '<button type="button" class="btn btn-success btn-xs">Active</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo site_url('/admin/user/change_status_customer'); ?>',
				
				data: {'id':id,'status':status,'user_type':1},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#status_id_"+id).html(change);
						jQuery("#user_status_"+id).val(status);
						new PNotify({
							  title: 'Success',
							  text: 'Status changed successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You do not have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
		}
	});

	
}
</script>
    <!-- datepicker -->
    <script type="text/javascript">
      $(document).ready(function() {

        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2016',
          maxDate: '12/31/2020',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Apply',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
          
        };
        
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        
        $('#start_date').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
        $('#end_date').val(moment().format('YYYY-MM-DD'));
        
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		  $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
		  $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
		  $('#start_date').val('');
		  $('#end_date').val('');
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>

<?php 
if($details[0]['handyman_id'] != 0) $is_disabled = 'disabled';
else $is_disabled = '';
?>
<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>

<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				 <?php echo form_open(site_url('/admin/task/edit_action'),array('class'=>'form-horizontal form-label-left')); 
				  
					  if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>

                      <span class="section">Edit Task</span>
					  

                      <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                      <input type="hidden" name="id" value="<?php echo $details[0]['id'];?>">
                      
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							
					     <?php echo form_input(array('name'=>'title', 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Title",'label' =>false,'required'=>'required',"value"=>$details[0]['title'])); ?>
                         
                        </div>
                      </div>
   
   
                      
                   
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_name">User Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							
					     <?php echo form_input(array('name'=>'first_name', 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"User Name",'label' =>false,'required'=>'required','readonly'=>'readonly',"value"=>$details[0]['first_name']." ".$details[0]['last_name'])); ?>
                         
                        </div>
                      </div>


    
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Parent Category <span class="required">*</span>
                        </label>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12">
						  <?php echo form_dropdown('categ_parent_slug', $parent_categories,$details[0]['categ_parent_slug'], 'class="form-control col-md-7 col-xs-12" id="parent_id" required="required" onchange="populate_subcategory(this.value)"'); ?>
                        <!--  <span>Leave this field blank if creating parent category</span> -->
                         
                        </div>
                      </div>
                      
                       
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Sub Category <span class="required">*</span>
                        </label>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12">
						  <?php echo form_dropdown('categ_id', $sub_category,$details[0]['categ_id'], 'class="form-control col-md-7 col-xs-12" id="subcategory_id" required="required" '); ?>
                        <!--  <span>Leave this field blank if creating parent category</span> -->
                         
                        </div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Handyman <span class="required">*</span>
                        </label>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12">
						  <?php echo form_dropdown('handyman_id',$handyman_list,$details[0]['handyman_id'], 'class="form-control col-md-7 col-xs-12" id=""  '.$is_disabled); ?>
                        <!--  <span>Leave this field blank if creating parent category</span> -->
                         
                        </div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Address <span class="required">*</span>
                        </label>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12">
						 <textarea id="" name="address" rows="3" cols="20" readonly="readonly"><?php echo $details[0]['address'];?></textarea>
						 <a class="googleMapPopUp" rel="nofollow" href="https://maps.google.com/maps?q=<?php echo $details[0]['address'];?>" target="_blank">
									<img class="locate_task" src="<?php echo assets_url('images/marker.png');?>" height="30" width="30" />
									</a>
									
                         <a class="googleMapPopUp" rel="nofollow" href="https://maps.google.com/maps?q=<?php echo $details[0]['address'];?>" target="_blank">
									View on Map
									</a>
                        </div>
                      </div>
                      
                       
                      
                      
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description <span class="required">*</span>
                        </label>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12">
						 <textarea id="" name="description" rows="3" cols="20"><?php echo $details[0]['description'];?></textarea>
                         
                        </div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Current Status </label>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12">
						<button type="button" class="btn <?php echo $this->config->item('task_status_color')[$details[0]['status']];?> btn-xs"><?php echo $this->config->item('task_status')[$details[0]['status']];?></button>
                        </div>
                      </div>
                      
                      
                      
						<?php if(!empty($status)){ ?>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Change Status To <span class="required">*</span>
                        </label>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12">
						  <?php echo form_dropdown('status', $status,$details[0]['status'], 'class="form-control col-md-7 col-xs-12" id=""  '); ?>
                        <!--  <span>Leave this field blank if creating parent category</span> -->
                         
                        </div>
                      </div>
                      <?php }else if($details[0]['status'] == 'X'){ ?>
						  <div class="item form-group">
							
						 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
							<div class="col-md-6 col-sm-6 col-xs-12 col-offset-3">
							<a href="javascript:void(0)" onclick="recover_task(<?= $details[0]['id']; ?>)" class="btn btn-success"> Recover Task </a>
							  </div>
							</div>
						<?php  } ?>
                      
                                       
                                   
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							 <button type="reset" class="btn btn-primary">Reset</button>
							  <button type="submit" class="btn btn-success">Submit</button>
						
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<script>
	
	function recover_task(id){
		bootbox.confirm("Are you sure?", function(result) {
		if(result == true)
		{
			jQuery.ajax({ 
				url: '<?php echo site_url('/admin/task/recover_task'); ?>',
				data: {'task_id':id},
				type: 'POST',
				success: function(data) {
					 location.reload();
					
				}
			});
		}
		});
	}
	
	function populate_subcategory(slug){
		
				jQuery.ajax({ 
				url: '<?php echo site_url('/admin/category/get_subcateg_bySlug'); ?>',
				
				data: {'slug':slug},
				type: 'POST',
				success: function(data) {
					var obj = jQuery.parseJSON ( data );
					console.log(obj);
					
					console.log(obj.msg);
					if(obj.msg=='success'){
						
						var select = $('#subcategory_id');

						select.empty();


						for (var j = 0; j < obj.response.length; j++){
						console.log(obj.response[j].name);
						$("#subcategory_id").append("<option value='" +obj.response[j].id+ "'>" +obj.response[j].name+ "     </option>");
						}
						
						
					}
			
				}
			});
		}
</script>

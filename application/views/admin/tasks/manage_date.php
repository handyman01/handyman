<div class="right_col" role="main">
	<?php
	if($type=='home_renovation') $function = 'response_renovation';
	else   $function = 'renovation';
	?>
     <div class="title_left">
                <h3>
                      Tasks
                      <small>
                         Listing
                      </small>
                  </h3>
                  
                  <?php
                  $download_link=site_url();
                  if($this->uri->segment(1) !==null)
                  {
					  $download_link.=$this->uri->segment(1);
				  }
                  if($this->uri->segment(2) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(2);
				  }
                  if($this->uri->segment(3) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(3);
				  }
                  if($this->uri->segment(4) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(4);
				  }
                  if($this->uri->segment(5) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(5);
				  }
                  if($this->uri->segment(6) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(6);
				  }
				  $curr_page=$download_link;
				  $download_link.='/download';
                  ?>
          
      </div>
                <div class="">

            <div class="page-title">
			  <div class="col-md-12">
				                     <?php if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
			  </div>


              
	          <!-- top tiles -->
          <div class="row tile_count" >
 
            
            <!-- this month -->
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				
              <span class="count_top"><i class="fa fa-user"></i> Total Submitted</span>
              <div class="count"><?php echo $submitted_year;?></div>
                
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				
              <span class="count_top"><i class="fa fa-user"></i> Total Completed</span>
              <div class="count"><?php echo $completed_year;?></div>
             
            </div>
            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				
              <span class="count_top"><i class="fa fa-user"></i> Total Active</span>
              <div class="count"><?php echo $active_year;?></div>
             
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			 
              <span class="count_top"><i class="fa fa-user"></i> Total Amount</span>
              <div class="count">$ <?php echo number_format($amount_year['total_price'],2);?></div>
         
            </div>

          </div>
          <!-- /top tiles -->
          
               

              <div class="title_right">
				<!--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                    <div class="input text"><input type="text" id="search" placeholder="Search for..." class="form-control" name="key"></div>
					<span class="input-group-btn"><button class="btn btn-default" type="button" onclick = "search_result();">Go!</button></span>
                  </div>
                </div>
               --->
				
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="">
						
						
                      <table class="table table-striped jambo_table bulk_action" id = "datatable">
                        <thead>
                          <tr class="headings">
                             <th class="column-title">S.No.</th>
                             <th class="column-title">Task No.</th>
                             <th class="column-title">Submitted On</th>
                             <th class="column-title">Customer No.</th>
                            <th class="column-title">Customer</th>
                            <th class="column-title">Handyman No.</th>
                            <th class="column-title">Handyman</th>
                            <th class="column-title">Service</th>
                            <th class="column-title">Price </th>
							<th class="column-title">Payment </th>
							<th class="column-title">Status </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php 
							$count = 1;
							foreach($tasks as $data){
								
								 ?>
							<tr id ="user_row_<?= $data['id']; ?>">
								<td><?= $count?>.</td>
	
								<td class=" "><?= $data['id']; ?> </td>
								<td class=" "><?= $data['created']; ?> </td>
								<td class=" "><?= $data['user_id']; ?> </td>
								<td class=" "><?= $data['first_name'].' '.$data['last_name']; ?> </td>
								<td class=" "><?= ($data['handyman_id'] ==0 ? 'Not Assigned':$data['handyman_id']); ?> </td>
								<td class=" "><?= $data['name']; ?> </td>
								<td class=" "><?= $data['category_name']; ?> </td>
								<td class=" "><?= $data['total_price']; ?> </td>
								<td class=" "><?= ($data['payment_status']==0 ? 'Not Done' :'Done'); ?> </td>
								
								
								
								<td class=" ">
									<input type="hidden" id="user_status_<?= $data['id'] ?>" value ="<?= $data['status']; ?>" />
									<a href="javascript:void(0)" >
									
										<button type="button" class="btn <?php echo $this->config->item('task_status_color')[$data['status']];?> btn-xs"><?php echo $this->config->item('task_status')[$data['status']];?></button>
									</a>
								</td>
								<td class=" last">
									<a href="<?php echo site_url('/admin/task/detail/'.$data['id']);?>" class="btn btn-warning btn-xs"><i class="fa fa-eye"></i> Details </a>
									<?php if($data['status'] != 'C'){?>			
									<a href="<?php echo site_url('/admin/task/edit/'.$data['id']);?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
									<?php } ?>
									<!--<a href="#" onclick="delete_user(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>-->
									<a href="<?php echo site_url('/admin/task/'.$function.'/'.$data['id']);?>" class="btn btn-success btn-xs"><i class="fa fa-reply"></i> <?=$data['response_count']; ?> Response </a>
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($tasks) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                     
                     
                     
                    </div>
               
               
               
                  </div>
                </div>
              </div>
            </div>
  
  
          </div>
        </div>
<script>
	
function delete_user(id){
	bootbox.confirm("Are you sure?", function(result) {
	
		if(result == true){
			jQuery.ajax({ 
				//url: 'delete',
				url: '<?php echo site_url('/admin/user/delete_customer'); ?>',
				data: {'id':id,'user_type':1},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#user_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record has been deleted successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},
				error: function (request) {
					new PNotify({
							  title: 'Error',
							  text: 'This record is being referenced in other place. You cannot delete it.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
					
				},
			});
		}
	});
	
}
function change_user_status(id){
	var status = $("#user_status_"+id).val();
	if(status == 1){
		var ques= "Do you want change the status to DEACTIVE";	
		var status = 0;
		var change = '<button type="button" class="btn btn-danger btn-xs">Deactive</button>'
	}else{
		var ques= "Do you want change the status to ACTIVE";
		var status = 1;
		var change = '<button type="button" class="btn btn-success btn-xs">Active</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo site_url('/admin/user/change_status_customer'); ?>',
				
				data: {'id':id,'status':status,'user_type':1},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#status_id_"+id).html(change);
						jQuery("#user_status_"+id).val(status);
						new PNotify({
							  title: 'Success',
							  text: 'Status changed successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You do not have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
		}
	});

	
}
</script>
    <!-- datepicker -->
    

   <?php
  // echo '<pre>';
   //print_r($details);
   //die;
   ?>
        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Task Response Details:</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
  <?php 
						  if(empty($details))
						  {
							 ?>
							 <div class="col-md-12">
								<div class="alert alert-warning" role="alert">
								<strong>Sorry ! </strong>No associated data found!
								</div>
							</div>
							 <?php 
						  }
						  else
						  {
						  
							  if(!empty($details['offers']))
							 {
							 ?>
									<div class="alert alert-success col-md-12" role="alert">
									<strong>Offer!</strong> Tasker has offered you $<?php echo $details['offers']['price'];?>.
									</div>
								
							<?php
							}
							?>	
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
						 
						  
						  
                        <div class="col-md-6">
							 
						  
							 <div class="col-md-6">
								<h4>Handyman Name<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4><?php echo ucfirst($details['name']);?><h4>
						     </div>
						     
						     <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Email<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4>  <?php echo $details['email'];?><h4>
						     </div>
						      <div class="clearfix"></div>
						     
							 <div class="col-md-6">
								<h4> Phone<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4>  <?php echo $details['phone'];?><h4>
						     </div>
                          <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Company<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $details['company'];?><h4>
						     </div>
                          <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Address<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $details['address'];?><h4>
						     </div>
						     <?php } ?>
						 </div>
                        
                     </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

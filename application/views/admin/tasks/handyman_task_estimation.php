
<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>

<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				 <?php echo form_open(site_url('/admin/task/handyman_task_estm_action'),array('class'=>'form-horizontal form-label-left')); 
				  
					  if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>

                      <span class="section">Handyman Task Estimation</span>
					  

                      <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                      <input type="hidden" name="id" value="<?php echo $details[0]['id'];?>">

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Handyman Working Hours <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					     <?php echo form_input(array('name'=>'handy_wrk_hrs','id'=>'handy_wrk_hrs','class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Handyman Working Hours",'label' =>false,'required'=>'required',"value"=>$details[0]['handy_wrk_hrs'])); ?>
                         
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Handyman Working Fees <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					     <?php echo form_input(array('name'=>'handy_wrk_amt','id'=>'handy_wrk_amt','class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Handyman Working Fees",'label' =>false,'required'=>'required',"value"=>$details[0]['handy_wrk_amt'])); ?>
                         
                        </div>
                      </div>
                      
                      
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							 
							  <button type="submit" class="btn btn-success">Submit</button>
						
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <script>
				$(document).on('keypress',"#handy_wrk_hrs,#handy_wrk_amt",function(e){
	 
	 var vall = String.fromCharCode(e.which);

     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57 )) {
            e.preventDefault();
              // return false;
      }

   });
   
			</script>

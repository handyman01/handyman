   <?php
  
   ?>
        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Customer Message </h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                    </div>
        
        
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
							
							 <div class="col-md-6">
								<h4>Message:<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $details['cancel_reason'];?><h4>
								
																		
						     </div>
						     
						      <div class="clearfix"></div>
							
							 <div class="col-md-6">
								<h4>Task Id:<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $details['task_id'];?><h4>
								<a href="<?php echo site_url('/admin/task/detail/'.$details['task_id']);?>" class="btn btn-warning btn-xs"><i class="fa fa-eye"></i> More Details About Task </a>	
								<a href="<?php echo site_url('/admin/task/edit/'.$details['task_id']);?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit Task </a>
																				
						     </div>
						     
						      <div class="clearfix"></div>
							
							 
                         
                        </div>
                        
                        <div class="col-md-6">
                          <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                              Date :  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> <?php 
                              $date=date_create($details['updated']);
                              echo date_format($date,"D,M d,Y");
                              ?>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

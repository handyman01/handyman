  <style>
	.right_col{
  height: 100% !important;
}
	</style>
     <!-- page content -->
        <div class="right_col" role="main">

          <!-- top tiles -->
          <div class="row tile_count" >
            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Customers</span>
              <div class="count"><?php echo $customers;?></div>
            </div>
            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Handymen</span>
              <div class="count"><?php echo $handymen;?></div>
            </div>
            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Maintenance Tasks</span>
              <div class="count"><?php echo $maintenance_submitted_year;?></div>
            </div>
            


            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Maintenance Collections</span>
              <div class="count"><?php echo number_format($maintenance_amount_year['total_price'],2);?></div>
            </div>
            
             <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Renovation Tasks</span>
              <div class="count"><?php echo $renovation_submitted_year;?></div>
            </div>
            
            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Renovation Collection</span>
              <div class="count"><?php echo number_format($renovation_amount_year['total_price'],2);?></div>
            </div>
          </div>
          <!-- /top tiles -->

          <div class="row" 
            <div class="col-md-12 col-sm-12 col-xs-12" >
				

        
            </div>

          </div>
          <br />

    
        </div>
        <!-- /page content -->


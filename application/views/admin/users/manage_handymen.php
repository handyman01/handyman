<div class="right_col" role="main">
	
	 <div class="title_left">
                <h3>
                      Handymen
                      <small>
                         Listing
                      </small>
                  </h3>

              </div>
              
              
          <div class="">

            <div class="page-title">
			  <div class="col-md-12">
				                     <?php if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
			  </div>

              
	          <!-- top tiles -->
          <div class="row tile_count" >
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/user/filter_handyman/total');?>">
              <span class="count_top"><i class="fa fa-user"></i> Total Handymen</span>
              <div class="count"><?php echo $total_users;?></div>
                </a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/user/filter_handyman/active');?>">
              <span class="count_top"><i class="fa fa-user"></i> Active Handymen</span>
              <div class="count"><?php echo $active_users;?></div>
              </a>
            </div>
            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/user/filter_handyman/inactive');?>">
              <span class="count_top"><i class="fa fa-user"></i> Inactive Handymen</span>
              <div class="count"><?php echo $inactive_users;?></div>
              </a>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			 <a href="<?php echo site_url('/admin/user/filter_handyman/online');?>">
              <span class="count_top"><i class="fa fa-user"></i> Online Handymen</span>
              <div class="count"><?php echo $online_users;?></div>
              </a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/user/filter_handyman/new');?>">
              <span class="count_top"><i class="fa fa-user"></i> New Handymen</span>
              <div class="count"><?php echo $new_users;?></div>
              </a>
            </div>
          </div>
          <!-- /top tiles -->
              <div class="title_right">
				<!--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                    <div class="input text"><input type="text" id="search" placeholder="Search for..." class="form-control" name="key"></div>
					<span class="input-group-btn"><button class="btn btn-default" type="button" onclick = "search_result();">Go!</button></span>
                  </div>
                </div>
               --->
				
              </div>
            </div>
<?php

?>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="">
						
						
                      <table class="table table-striped jambo_table bulk_action" id = "datatable">
                        <thead>
                          <tr class="headings">
                             <th class="column-title">S.No. </th>
                            <th class="column-title">Name </th>
                            <th class="column-title">Email </th>
                           <th class="column-title">is_logged </th>
                            <th class="column-title">Last login </th>
                            <th class="column-title">Total working hours </th>
                            <th class="column-title">Total working amount </th>
							<th class="column-title">Status </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php 
							$count = 1;
							
							foreach($customers as $data){
								
								 ?>
							<tr id ="user_row_<?= $data['id']; ?>">
								<td><?= $count?>.</td>
	
								<td class=" "><?= $data['name']; ?> </td>
								<td class=" "><?= $data['email']; ?> </td>
								<td class=" ">
									
									<?php  if($data['is_login'] == 'Y'){
										echo '<button type="button" class="btn btn-success btn-xs">Yes</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">No</button>';
									} ?>
								</td>
								
								<td class="center">
								
										<?php  if($data['last_login'] == ''){ echo " - ";}
												else{ echo $data['last_login'];} 
										?>
								</td>
								<td class="center">
								
										<?php
										if(isset($arr_handyman_working_hrs[$data['id']]))
										{
										 echo number_format($arr_handyman_working_hrs[$data['id']],2);
									    }else
									    {
											echo '-';
										}
										?>
								</td>
								<td class="center">
								
										<?php
										if(isset($arr_handyman_working_amt[$data['id']]))
										{ echo number_format($arr_handyman_working_amt[$data['id']],2);
										}else
										{
											echo '-';
										}
										?>
								</td>
								<td class=" ">
									<input type="hidden" id="user_status_<?= $data['id'] ?>" value ="<?= $data['status']; ?>" />
									<a href="javascript:void(0)" id="status_id_<?= $data['id']; ?>" onclick="change_user_status(<?php echo $data['id'] ?>)">
									<?php  if($data['status'] == 1){
										echo '<button type="button" class="btn btn-success btn-xs">Active</button>'; 
									}else{
										echo '<button type="button" class="btn btn-danger btn-xs">Deactive</button>';
									} ?></a>
								</td>
								<td class=" last">
									<a href="<?php echo site_url('/admin/user/edit_handyman/'.$data['id']);?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
									
									<?php
									if($data['block_till']!='' && strtotime(date('Y-m-d H:i:s')) < strtotime($data['block_till']))
									{
										?>
								<a href="javascript:void(0);" title="<?= 'Block Till '.$data['block_till']; ?>" class="btn btn-danger btn-xs unblock" id="unblock$$<?= $data['id']; ?>"><i class="fa fa-ban"></i> Unblock </a>		
										<?php										
									}
									else
									{
										?>
										<a href="<?php echo site_url('/admin/user/block_handyman/'.$data['id']);?>" class="btn btn-info btn-xs"><i class="fa fa-ban"></i> Block </a>		
										<?php
									}
									?>
							
								<!--	<a href="#" onclick="delete_user(<?= $data['id'] ?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>-->
									
								</td>
							</tr>
							
							<?php  $count++;
							} ?> 
							
							<?php  if(count($customers) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                      </table>
                     
                     
                     
                    </div>
               
               
               
                  </div>
                </div>
              </div>
            </div>
  
  
          </div>
        </div>
<script>
	$('.unblock').click(function(){
                 var id=$(this).attr('id');
                 var id_parts=id.split("$$");
				jQuery.ajax({ 
				url: '<?php echo site_url('/admin/user/unblock_handyman'); ?>',
				
				data: {'id':id_parts[1]},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
					location.reload();
						
					}
					if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You do not have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
			
	});	
	
	
function delete_user(id){
	bootbox.confirm("Are you sure?", function(result) {
	
		if(result == true){
			jQuery.ajax({ 
				//url: 'delete',
				url: '<?php echo site_url('/admin/user/delete_handyman'); ?>',
				data: {'id':id,'user_type':2},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#user_row_"+id).remove();
						new PNotify({
							  title: 'Success',
							  text: 'Record has been deleted successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You donot have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				},
				error: function (request) {
					new PNotify({
							  title: 'Error',
							  text: 'This record is being referenced in other place. You cannot delete it.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
					
				},
			});
		}
	});
	
}
function change_user_status(id){
	var status = $("#user_status_"+id).val();
	if(status == 1){
		var ques= "Do you want change the status to DEACTIVE";	
		var status = 0;
		var change = '<button type="button" class="btn btn-danger btn-xs">Deactive</button>'
	}else{
		var ques= "Do you want change the status to ACTIVE";
		var status = 1;
		var change = '<button type="button" class="btn btn-success btn-xs">Active</button>';
	}
	
	
	bootbox.confirm(ques, function(result) {
		if(result == true){
			jQuery.ajax({ 
				url: '<?php echo site_url('/admin/user/change_status_handyman'); ?>',
				
				data: {'id':id,'status':status,'user_type':2},
				type: 'POST',
				success: function(data) {
					if(data == 'success'){
						jQuery("#status_id_"+id).html(change);
						jQuery("#user_status_"+id).val(status);
						new PNotify({
							  title: 'Success',
							  text: 'Status changed successfully!',
							  type: 'success',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
					if(data == 'error'){
						
						new PNotify({
							  title: '403 Error',
							  text: 'You do not have permission to access this action.',
							  type: 'error',
							  styling: 'bootstrap3',
							  delay:1200
						  });
						
					}
				}
			});
		}
	});

	
}

</script>

<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>

<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				 <?php echo form_open(site_url('/admin/user/edit_handyman_action'),array('class'=>'form-horizontal form-label-left')); 
				  
					  if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>

                      <span class="section">Edit Handyman</span>
					  

                      <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                      <input type="hidden" name="id" value="<?php echo $details['id'];?>">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					     <?php echo form_input(array('name'=>'name', 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Name",'label' =>false,'required'=>'required',"value"=>$details['name'])); ?>
                         
                        </div>
                      </div>
                      
       
					 
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
						   <?php echo form_input(array('name'=>'email', 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Email",'label' =>false,"type"=>"email",'required'=>'required',"value"=>$details['email'])); ?>
											     
                       
                        </div>
                      </div>
      
                   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php echo form_password(array('name'=>'password', 'class'=>'form-control',"placeholder"=>"Password",'required'=>'required',"value"=>$details['password'])); ?>
                        </div>
                      </div>
                      
                                      
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							 <button type="reset" class="btn btn-primary">Reset</button>
							  <button type="submit" class="btn btn-success">Submit</button>
						
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

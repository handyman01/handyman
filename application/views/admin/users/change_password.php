<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>

<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				 <?php echo form_open(site_url('/admin/user/change_password_action'),array('class'=>'form-horizontal form-label-left')); 
				  
					  if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>

                      <span class="section">Admin Change Password</span>
					  

                      <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                      <input type="hidden" name="id" value="1">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">Old Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					     <?php echo form_password(array('name'=>'old_password', 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Old Password",'label' =>false,'required'=>'required'
					     )); ?>
                         
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">New Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					     <?php echo form_password(array('name'=>'new_password', 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"New Password",'label' =>false,'required'=>'required'
					     )); ?>
                         
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">Retype Old Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					     <?php echo form_password(array('name'=>'retype_password', 'class'=>'form-control col-md-7 col-xs-12',"placeholder"=>"Retype Password",'label' =>false,'required'=>'required'
					     )); ?>
                         
                        </div>
                      </div>
                      

                      
                     
                      
                                      
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							 <button type="reset" class="btn btn-primary">Reset</button>
							  <button type="submit" class="btn btn-success">Submit</button>
						
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


 <link href="<?php echo assets_url('css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css"/> 
<script src="<?php echo assets_url('js/bootstrap-datetimepicker.min.js'); ?>"></script> 

<style type="text/css">
label{padding-left:10px;padding-right:10px;}
</style>

<div class="right_col" role="main">

          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">

				 <?php echo form_open(site_url('/admin/user/block_user_action'),array('class'=>'form-horizontal form-label-left')); 
				  
					  if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>

                      <span class="section">Block User</span>
					  

                      <?php
                  
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                      <input type="hidden" name="id" value="<?php echo $details['id'];?>">
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Block Till <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
					      <div class='input-group date' id='datetimepicker1' >
                    <input type='text' class="form-control" name="block_till" value="<?php echo $details['block_till'];?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                        <script type="text/javascript">
           							$("#datetimepicker1").on("keydown keypress keyup", false);
           	var dateToday = new Date();

                $('#datetimepicker1').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
        	minDate: dateToday

});
            
        </script>
                         
                        </div>
                      </div>
                      
   
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
							 
							  <button type="submit" class="btn btn-success">Submit</button>
						
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

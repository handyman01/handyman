<div class="right_col" role="main">
	<?php
	//echo '<pre>';
	//print_r($tasks);
	//die;
	?>
     <div class="title_left">
                <h3>
                      Customer Payment
                      <small>
                         Listing
                      </small>
                  </h3>
                  
                  <?php
                  $download_link=site_url();
                  if($this->uri->segment(1) !==null)
                  {
					  $download_link.=$this->uri->segment(1);
				  }
                  if($this->uri->segment(2) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(2);
				  }
                  if($this->uri->segment(3) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(3);
				  }
                  if($this->uri->segment(4) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(4);
				  }
                  if($this->uri->segment(5) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(5);
				  }
                  if($this->uri->segment(6) !==null)
                  {
					  $download_link.='/'.$this->uri->segment(6);
				  }
				  $curr_page=$download_link;
				  $download_link.='/download';
                  ?>
           <a href="<?php echo $download_link;?>">
             <span class="count_top pull-right" style="font-weight: bold;font-size: 15px;"><i class="fa fa-download"></i> Download Excel Data</span>      
            </a>
      </div>
                <div class="">

            <div class="page-title">
			  <div class="col-md-12">
				                     <?php if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
			  </div>


          <div class="row tile_count" >
            <!--<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/task/manage_filter/submitted/M');?>">
              <span class="count_top"><i class="fa fa-user"></i> Total Pending</span>
              <div class="count"><?php echo $pending_month;?></div>
                </a>
                <span class="count_bottom">This Month</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/task/manage_filter/completed/M');?>">
              <span class="count_top"><i class="fa fa-user"></i> Total Completed</span>
              <div class="count"><?php echo $completed_month;?></div>
              </a>
               <span class="count_bottom">This Month</span>
            </div>
            
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<a href="<?php echo site_url('/admin/task/manage_filter/active/M');?>">
              <span class="count_top"><i class="fa fa-user"></i> Total Pending</span>
              <div class="count"><?php echo $pending_year;?></div>
              </a>
               <span class="count_bottom">This Year</span>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			 
              <span class="count_top"><i class="fa fa-user"></i> Total Completed</span>
              <div class="count">$ <?php echo $completed_year;?></div>
               <span class="count_bottom">This Year</span>
			</div>
            -->
        

          </div>
          <!-- /top tiles -->
          
             <!--  <div class="col-md-12 col-sm-9 col-xs-12 ">
                      <?php echo form_open(site_url('/admin/payment/searchByDate'),array('class'=>'form-horizontal form-label-left')); 
                      ?>
                      
							<input type="hidden" name="curr_page" id="curr_page" value="<?php echo $curr_page;?>">
							<input type="hidden" name="start_date" id="start_date" value="">
							<input type="hidden" name="end_date" id="end_date" value="">
                          <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                             Search By Date: <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                            <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                      
                          </div>
							<button id="search_bydate" type="submit" style="margin-top:7px;" class="btn btn-success btn-xs pull-right">Search</button>
                        </form>
                      
                 </div>-->

              <div class="title_right">
				<!--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    
                    <div class="input text"><input type="text" id="search" placeholder="Search for..." class="form-control" name="key"></div>
					<span class="input-group-btn"><button class="btn btn-default" type="button" onclick = "search_result();">Go!</button></span>
                  </div>
                </div>
               --->
				
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div id="divLoading"> </div><!--Loading class -->
                  <div class="x_content">

                    <div class="">
						
						
                      <table class="table table-striped jambo_table bulk_action" id = "datatable">
                        <thead>
                          <tr class="headings">
								<th class="column-title">S.No.</th>
								<th class="column-title">Task No(Click)</th>
								<th class="column-title">Customer</th>
								<th class="column-title">Transaction Id</th>
								<th class="column-title">Amount </th>
								<th class="column-title">Status </th>
								<th class="column-title">Action </th>
								
                            </th>
                          
                          </tr>
                        </thead>

                        <tbody>
							<?php 
							$count = 1;
							
							foreach($list as $data){
								if($data['payment_status'] == 'pending') $color= 'btn-danger';
								else $color= 'btn-success';
								
								 ?>
							<tr id ="user_row">
								<td><?= $count?>.</td>
								<td class=" "><?php echo '<a style="text-decoration: underline;" href="'.site_url('/admin/task/detail/'.$data['task_id']).'">'.$data['task_id'].'</a>'; ?> </td>
								<td class=" "><?= $data['first_name'].' '.$data['last_name']; ?>  </td>
								<td class=" "><?= $data['txn_id']; ?></td>
								<td class=" "><?= $data['currency_code']." ".$data['payment_gross'] ?> </td>
								<td class=" "><?php echo '<button type="button" class="btn '.$color.' btn-xs">'.$data['payment_status'].'</button>';?> </td>
								<td class=" ">
								<a href="<?php echo site_url('/admin/payment/detail/'.$data['payment_id']);?>" class="btn btn-warning btn-xs"><i class="fa fa-eye"></i> Details </a>		
								</td>
							</tr>
							<?php  $count++;
							}
							if(count($list) < 1) {
										echo "<tr><th colspan = '6'>No record found</th></tr>";
								   } ?>	

                         </tbody>
                       </table>
                     </div>
				   </div>
                </div>
              </div>
            </div>
		  </div>
         </div>

   <?php

   ?>
        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Task Payment Details:</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="profile_title">
                        <div class="col-md-6">
							
							
							 <div class="col-md-6">
								<h4>Payment ID<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $details[0]['payment_id'];?><h4>
						     </div>
						     
							
							
							
							<div class="col-md-6">
								<h4>Task ID<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php  echo '<a href="'.site_url('/admin/task/detail/'.$details[0]['task_id']).'">'.$details[0]['task_id'].' (Click to view task)</a>';?><h4>
						     </div>
							
							
							 <div class="col-md-6">
								<h4>Task Title<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo ucfirst($details[0]['title']);?><h4>
						     </div>
						     
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> User Name<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo $details[0]['first_name']." ".$details[0]['last_name'];?><h4>
						     </div>
						     
						      <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Transaction Number<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?= $details[0]['txn_id']; ?><h4>
						     </div>
						     
						     
                          <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4> Amount<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php echo "$ ".$details[0]['payment_gross']?><h4>
						     </div>
                          <div class="clearfix"></div>
							 <div class="col-md-6">
								<h4>Payment Status<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?= $details[0]['payment_status']; ?><h4>
						     </div>
						   
							
						     
							 <div class="col-md-6">
								<h4> DateTime<h4>
						     </div>
						     
							 <div class="col-md-6">
								<h4> <?php $date=date_create($details[0]['updated']);
                              echo date_format($date,"M d, Y H:i A");?><h4>
						     </div>
                           </div>
                        
                        </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

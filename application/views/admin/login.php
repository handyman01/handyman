<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Handyman </title>

    <!-- Bootstrap -->
    <link href="<?php echo assets_adminUrl('bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo assets_adminUrl('font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo assets_adminUrl('css/custom.css'); ?>" rel="stylesheet">
  </head>

  <body style="background:#F7F7F7;">
    <div class="">
      <a class="hiddenanchor" id="toregister"></a>
      <a class="hiddenanchor" id="tologin"></a>

      <div id="wrapper">
        <div id="login" class=" form">
          <section class="login_content">
			  
			  <?php
			  
			   if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-danger"> <?= $this->session->flashdata('message') ?> </div>
<?php } ?>

             <?php //echo form_open('/loginAction'); ?>
             <form action="<?php echo site_url('/admin/user/login_action');?>" method="post" accept-charset="utf-8">
              <h1>Login Form</h1>
              <div>
				   <?php echo form_input(array('name'=>'email', 'class'=>'form-control',"placeholder"=>"Email",'required'=>true)); ?>
              </div>
              <div>
				    <?php echo form_password(array('name'=>'password', 'class'=>'form-control',"placeholder"=>"Password",'required'=>true)); ?>

              </div>
              <div>
				  <input type="hidden" value="submitted" name="submitted"/>
				  <button type="submit" class="btn btn-default submit">Log in</button>
              </div>
              <div class="clearfix"></div>
              <div class="separator">

   
                <div class="clearfix"></div>
                <br />
                <div>
                  <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Handyman!</h1>

                  <p>©2017 All Rights Reserved.</p>
                </div>
              </div>

               <?php echo  form_close(); ?>
          </section>
        </div>

        <div id="register" class=" form">
          <section class="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">Submit</a>
              </div>
              <div class="clearfix"></div>
              <div class="separator">

                <p class="change_link">Already a member ?
                  <a href="#tologin" class="to_register"> Log in </a>
                </p>
                <div class="clearfix"></div>
                <br />
                <div>
                  <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1>

                  <p>©2017 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>

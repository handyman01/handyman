    
    <div class="lower-section">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="res-box">
              <div class="login-part">
                <ul>
                  <li><a href="<?php echo site_url('login');?>">Log In</a></li>
                  <li><a href="<?php echo site_url('register');?>">Not Registered ?</a></li>
                </ul>
                <div class="row">
					<?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1" style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"><?php echo validation_errors();?></div>
						   <?php
					   }
                       ?>  
                       
                <?php 
       
                echo form_open(site_url('login_action'),array('class'=>'',"id"=>'login_user')); ?>  
                 <input type="hidden" name="redirect" value="<?php echo (isset($redirect) ? $redirect :'');?>">
                       
                  <div class="col-md-5 col-sm-5">
                    <div class="form-group">
					  <?php echo form_input(array('name'=>'email', 'class'=>'form-control',"placeholder"=>"Email",'required'=>true,'value'=>(($this->session->flashdata('eemail')) ? $this->session->flashdata('eemail'):set_value('email') ))); ?>
										   
                      <i><img src="<?php echo assets_url('images/user-icon.png'); ?>"></i> </div>
                  </div>
                  <div class="col-md-5 col-sm-5">
                    <div class="form-group">
                        <?php echo form_password(array('name'=>'password', 'class'=>'form-control',"placeholder"=>"Password",'required'=>true,'value'=>(($this->session->flashdata('ppass')) ? $this->session->flashdata('ppass'):set_value('password') ))); ?>
                      <i><img src="<?php echo assets_url('images/pass-icon.png'); ?>"></i> </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-lg-2 text-center">
                    <input type="submit" value="Continue">
                  </div>
                  
                  </form>
                  
                  
                  <div class="clear"></div>
                  <div class="forgot_text"><a href="<?php echo site_url('forget_password');?>">Forgot Password ?</a></div>
                  <div class="text-center or-text">OR</div>
                  <div class="row">
                    <div class="col-lg-12 text-center"> <a href="javascript:void(0);" onclick = "return fblogin();" class="sign-up facebook"><i class="fa fa-facebook"></i> Facebook</a> </div>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="https://connect.facebook.net/en_US/sdk.js"></script>

 <script>
	  function fblogin() {
        FB.init({appId: '1268367383242990', status: !0, cookie: !0, xfbml: true, version: 'v2.2'});
        FB.login(function(response) {
            FB.api('/me?fields=id,name,first_name,last_name,email,birthday,picture', function(response) {
                $.ajax({
                    url: "<?php echo site_url('login_fb'); ?>",
                    data: response,
                    type: 'post',
                    success: function(r) {

                        if (r == 'success') {
location.href='<?php echo site_url('do_login');?>';
                          
                        } else {
alert('login failed');
                           
                        }
                    }
                });
            });
        }, {scope: 'public_profile,email'});

    }
</script>
 
 

<style>

.profile_right_blk h2
{
    margin-bottom: 0; 
}
.right_form{
	    background: #fff;
    border: 2px solid #a1a1a1;
    border-radius: 5px;	
}

.right_form_button
{
	    padding-bottom: 30px;
}

</style>


<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>

          <div class="profile_right_blk right_form">
                    
                      
				
				  
                <div class="col-md-12 right_form_content">                
                  
                  
                   <div class="col-md-12" >
					 <?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger " style="text-align: center;"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success " style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger" style="text-align: center;"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                       
					 </div>
					 
					 
					 <?php echo form_open(site_url('switchTo_registerHandyman_action'),array('class'=>'',"id"=>'change_password')); ?>  
					                      
					  <div class="form-group">
							  <div class="col-sm-4"><label>Name: </label></div>
							  
							  <div class="col-sm-7">  
								  <?php echo form_input(array('name'=>'name', 'class'=>'form-control',"placeholder"=>"Name",'label' =>false,'value'=>$user_detail['firstname'].' '.$user_detail['lastname'])); ?>
							  </div>
				      </div>
				      
					  <div class="form-group">
							  <div class="col-sm-4"><label>Email: </label></div>
							  
							  <div class="col-sm-7">  
                                    <?php echo form_input(array('name'=>'email', 'class'=>'form-control',"placeholder"=>"Email",'label' =>false,'readonly'=>'readonly','value'=>$user_detail['email'])); ?>
							  </div>
				      </div>
				      
			
				      
					  <div class="form-group">
							  <div class="col-sm-4"><label>Password: </label></div>
							  
							  <div class="col-sm-7">  
                                  <?php echo form_password(array('name'=>'password', 'class'=>'form-control',"placeholder"=>"Password")); ?>
							  </div>
				      </div>
				  
					  <div class="form-group">
							  <div class="col-sm-4"><label>Company: </label></div>
							  
							  <div class="col-sm-7">  
                                  <?php echo form_input(array('name'=>'company', 'class'=>'form-control',"placeholder"=>"Company",'label' =>false)); ?>
							  </div>
				      </div>
				  
					  <div class="form-group">
							  <div class="col-sm-4"><label>Address: </label></div>
							  
							  <div class="col-sm-7">  
                               <?php echo form_input(array('name'=>'address', 'class'=>'form-control',"placeholder"=>"Address",'label' =>false)); ?>
							  </div>
				      </div>
				  
			             <?php
     
              foreach($services as $service_arr)
              {
				   foreach($service_arr as $service)
				   {
				
              ?>
             
                
               <div class="form-group">
				   <div class="col-md-4"></div>
                  <div class="col-md-8">
					   <input type="checkbox"  id="checkbox-<?php echo $service['id']?>" class="checkbox-custom main_services" name="main_services[]" value="<?php echo $service['id'];?>"/> 				                     
                    <label for="checkbox-<?php echo $service['id'];?>" class="checkbox-custom-label" style="text-align:left;"><?php echo ucfirst($service['name']);?></label>
                  </div>
                </div>
             
              <?php
		         }
             
		     }
              ?>	  
                   
                   
        <div class="profile_bottom_blk pad_btm_0" >
          <div class="row">
			  <!-- 
            <div class="col-lg-12">
              <div class="title-sec text-center wow fadeIn animated animated" style="visibility: visible; animation-name: fadeIn;">
                <h5>All Services</h5>
              </div>
            </div>
            -->
            <?php
            
              foreach($services as $service_arr)
              {
				  	foreach($service_arr as $service)
				   {
				  
             ?>
             

            
              
            <div class="al-serv" style="display:none;" id="subservice_<?php echo $service['id'];?>">
            
            <div class="col-lg-12">
              <div class="title-sec text-center wow fadeIn animated animated" style="visibility: visible; animation-name: fadeIn;">
                <b><?php echo ucfirst($service['name']);?></b>
              </div>
            </div>
            
              <ul>
				  
              <?php
              foreach($service['sub_services'] as $sub)
              {
              ?>

                <li class="wow fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><a href="#">
                  <div>
                    <input id="checkbox-<?php echo $sub['id'];?>" class="checkbox-custom" name="subservices[]" type="checkbox" value="<?php echo $sub['id'];?>">
                    <label for="checkbox-<?php echo $sub['id'];?>" class="checkbox-custom-label"><?php echo ucfirst($sub['name']);?></label>
                  </div>
                  </a>
                 </li>
                <?php
			    }
                ?>
              </ul>
            </div>
         
         <?php
	            }
	            }
	      ?>
         

            
            
            
          </div>
        </div>
      
  
  
          
                  <div class="col-lg-12 text-center right_form_button">
                     <a href="javascript:void(0)" id="change_pass" class="sign-up">Save</a>
                  </div>
                  
                   </form>	
                   
                </div>
              
             
           
          </div>
          

			
  <script>
   $(".main_services").change(function() {
	   idd=$(this).attr('id');
	   parts_id=idd.split("checkbox-");
    if(this.checked) {
		$("#subservice_"+parts_id[1]).show();
        //Do stuff
    }
    else
    {
		$("#subservice_"+parts_id[1]).hide();
	}
});
    
    $('#change_pass').click(function(){
		
		 $("#change_password").submit();
		
		});
    </script>
  
  </script>

<style>

.profile_right_blk h2
{
    margin-bottom: 0; 
}
.right_form{
	    background: #fff;
    border: 2px solid #a1a1a1;
    border-radius: 5px;	
}

.right_form_button
{
	    padding-bottom: 30px;
}
.profile_title_dashboard h4 {
font-size: 20px;
padding-bottom: 15px;
}
.detail_col
{
	color:#31708f;
	font-size: 18px;
	font-weight:300;
}
</style>
<?php

?>
<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>
                      	 <div class="" >

   <a href="<?php echo site_url('check_task_response/'.$task_id);?>" style="margin-bottom: 12px;"  class="pull-right">	<button type="button" class="btn btn-success">Back</button></a>  
   </div>
          <div class="profile_right_blk right_form">
                  		              
               

				
				  
                <div class="col-md-12 right_form_content">  
			
                  
                   <div class="col-md-12" >
					 <?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger " style="text-align: center;"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success " style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger" style="text-align: center;"><?php echo validation_errors();?></div>
						   <?php
					   }
                    
                       ?>  
                       
					 </div>
					 
					 
					
                    <div class="col-md-12 col-sm-12 col-xs-12">

                      <div class="profile_title_dashboard">
						  <?php 
						  if(empty($details))
						  {
							 ?>
							 <div class="col-md-12">
								<div class="alert alert-warning" role="alert">
								<strong>Sorry ! </strong>No associated data found!
								</div>
							</div>
							 <?php 
						  }
						  else
						  {
						  ?>
                       
                        
                        <div class="col-md-12">
							
						 <?php
						 
						 if(!empty($details['offers']))
						 {
						 ?>
								<div class="alert alert-warning" role="alert">
								<strong>Offer!</strong> Tasker has offered you $<?php echo $details['offers']['price'];?>.
								</div>
							
						<?php
					    }
						?>	
							
							 <div class="col-md-6">
								<h4>Handyman Name<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <a target="_blank" style="color:blue;text-decoration:underline;" href="<?php echo site_url('user_profile/h/'.$details['id']);?>"><?php echo ucfirst($details['name']);?></a><h4>
						     </div>
						     
						      <div class="clear"></div>
							 <div class="col-md-6">
								<h4> Email <h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $details['email'];?><h4>
						     </div>
						      <div class="clear"></div>
						     
							 <div class="col-md-6">
								<h4> Phone<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $details['phone'];?><h4>
						     </div>
                          <div class="clear"></div>
							 <div class="col-md-6">
								<h4> Company<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $details['company'];?><h4>
						     </div>
                          <div class="clear"></div>
							 <div class="col-md-6">
								<h4> Address<h4>
						     </div>
						     
							 <div class="col-md-6 detail_col">
								<h4> <?php echo $details['address'];?><h4>
						     </div>
						     
							 <div class="clear"></div>
						     
	
						         
                         
                        </div>
                        <?php
					      }
                        ?>
            
                      </div>

                    </div>
                  
                  
                
                </div>
              
             
           
          </div>
          

			
    <script>
    $('#change_pass').click(function(){
		
		 $("#change_password").submit();
		
		});
    </script>

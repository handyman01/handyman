	<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<style>
	.locate_task{
		vertical-align: baseline;
	}
	
	.locate_tasks_btn {
    background-color: rgb(70, 70, 70);
    border-radius: 5px;
    color: rgb(255, 255, 255) !important;
    display: inline-block;
    font-size: 17px;
    height: 50px;
    line-height: 50px;
    padding: 0 30px;
    text-transform: uppercase;
     margin-bottom: 12px; 
}

</style>


<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>

          <div class="profile_right_blk">
			  <div class="col-md-12" style="margin-bottom: 12px;">				                  
				
                       
                       
				<h2>Task Responses</h2>
				
				<div>
						 <?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger " style="text-align: center;"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success " style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                       
                       
				<a href="<?php echo site_url('posted_task');?>"  data-toggle="modal" class="pull-right">	<button type="button" class="btn btn-success">Back</button></a>
				
				</div>
             </div>
            <div class="table-responsive" style=" padding: 12px;">
				
										
                      <table class="table table-striped jambo_table bulk_action" id = "datatable">
                        <thead>
                          <tr class="headings">
                    <th>Handyman ID</th>
                    <th>Handyman Name</th>
                    <th>Applied On</th>
                    <th>Category</th>
                    <th>View Details</th>
                          
                          </tr>
                        </thead>

                        <tbody>
					<?php
					if(!empty($handy))
					{
						
						$count=0;
						foreach($handy as $detail)
						{
						
						
						?>
						  <tr>
							<td><?php echo $detail['handy_id'];?></td>
							<td><a target="_blank" style="color:blue;text-decoration:underline;" href="<?php echo site_url('user_profile/h/'.$detail['handy_id']);?>"><?php echo $detail['name'];?></a></td>
							<td><?php echo $detail['applied_tasks_created'];?></td>
							<td><?php echo ucfirst($detail['category_name']);?></td>
							
							<td>
					
									<a href="<?php echo site_url('handyman_detail/'.$detail['handy_id'].'/'.$task_id);?>" class="btn btn-warning btn-xs"><i class="fa fa-search"></i> Details </a>		
									
									<?php
									if($detail['task_status']=='S')
									{
									?>
									<a href="<?php echo site_url('hire_handyman/'.$detail['handy_id'].'/'.$task_id);?>" class="btn btn-success btn-xs"> Hire Handyman </a>
									<?php
								    }
								    if($detail['assigned_handyman']==$detail['handy_id'])
								    {
										?>
										
									  <a href="javascript:void(0);" class="btn btn-success btn-xs"> Approved</a>
										<?php
										
									}
								    
								    
									?>	
									
								
							</td>
						  </tr>
					 <?php
						 $count++;
					   }
					  

			       
			       }
			       else
			       {
					   ?>
					<tr ><td colspan='7'>No responses found ! </td></tr>   
					   <?php
				   }
                 ?>

                         </tbody>
                      </table>
                     
           
           
 
            </div>
          </div>
          



		<div class="modal fade" id="myMapModal" >
			<div class="modal-dialog" style="width:90%">
				<div class="modal-content">
					
					<div class="modal-body" style="height: 500px;">
						<div class="container">
							<div class="row">
										<div id="map" style="height: 470px;width: 100%;position: absolute;"></div> 


								<div id="map-canvas" class=""></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
  
 <script>
$(document).ready(function() {
    $('#datatable').DataTable();
} );
</script>

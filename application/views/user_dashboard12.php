	<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<style>
	.locate_task{
		vertical-align: baseline;
	}
	
	.locate_tasks_btn {
    background-color: rgb(70, 70, 70);
    border-radius: 5px;
    color: rgb(255, 255, 255) !important;
    display: inline-block;
    font-size: 17px;
    height: 50px;
    line-height: 50px;
    padding: 0 30px;
    text-transform: uppercase;
     margin-bottom: 12px; 
}
.profile_right_blk h2
{
    margin-bottom: 0; 
}
.gm-style-iw {
	width: 350px !important;
	top: 15px !important;
	left: 0px !important;
	background-color: #fff;
	box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
	border: 1px solid rgba(72, 181, 233, 0.6);
	border-radius: 2px 2px 10px 10px;
}
#iw-container {
	margin-bottom: 10px;
}
#iw-container .iw-title {
	font-family: 'Open Sans Condensed', sans-serif;
	font-size: 22px;
	font-weight: 400;
	padding: 10px;
	background-color: #48b5e9;
	color: white;
	margin: 0;
	border-radius: 2px 2px 0 0;
	line-height: 1;
}
#iw-container .iw-content {
	font-size: 13px;
	line-height: 18px;
	font-weight: 400;
	margin-right: 1px;
	padding: 15px 5px 20px 15px;
	max-height: 140px;
	overflow-y: auto;
	overflow-x: hidden;
}
.iw-content img {
	float: right;
	margin: 0 5px 5px 10px;	
}
.iw-subTitle {
	font-size: 16px;
	font-weight: 700;
	padding: 5px 0;
}
.iw-bottom-gradient {
	position: absolute;
	width: 326px;
	height: 25px;
	bottom: 10px;
	right: 18px;
	background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
}
</style>


<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>

          <div class="profile_right_blk">
			  <div class="col-md-12" style="margin-bottom: 12px;">
				<h2>Posted Tasks</h2>
				
				<div>
				<a href="<?php echo site_url('post_task');?>"  data-toggle="modal" class="pull-right">	<button type="button" class="btn btn-success">Post New Task</button></a>
				<?php
					if(!empty($tasks))
					{ ?>
					
					<a href="#myMapModal"  data-toggle="modal" class="pull-right" style="padding-right: 10px;">
						<button type="button" class="btn btn-info ">Locate My Tasks</button>
					</a>
						
				   
				   <?php } ?>
				</div>
             </div>
            <div class="table-responsive" style=" padding: 12px;">
				
										
                      <table class="table table-striped jambo_table bulk_action" id = "datatable">
                        <thead>
                          <tr class="headings">
                    <th>Task No.</th>
                    <th>Submitted On</th>
                    <th>Handyman</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>View Details</th>
                          
                          </tr>
                        </thead>

                        <tbody>
					<?php
					if(!empty($tasks))
					{
						
						$arr_status=array('S'=>'Submitted','P'=>'Processing','X'=>'Cancelled','C'=>'Completed');
						$arr_status_color=array('S'=>'btn-warning','P'=>'btn-info','X'=>'btn-danger','C'=>'btn-success');
							
						$count=1;
						
						$marker_array = array();
						foreach($tasks as $k=>$task)
						{
						//	print_r($task);die;
							$marker_array[$k] = array('lat'=>$task['lat'],'lng'=>$task['lon'],'category'=>ucfirst($task['category_name']),'total_price'=>$task['total_price'],'created_on'=>$task['created'],'description'=>$task['description'],'address'=>$task['address'],'type'=>($task['is_residential']==1 ? 'Residential':'Commercial'));
						
						?>
						  <tr>
							<td><?php echo $task['id'];?></td>
							<td><?php echo $task['created'];?></td>
							<td><?php 
							if($task['name']!='')
							{
								echo '<span style="color:blue;">'.$task['name'].'</span>';
							}
							else
							{
							   echo 'Not Assigned';	
							}
							
							?> </td>
							<td><?php echo ucfirst($task['category_name']);?></td>
							<td>$<?php echo $task['total_price'];?></td>
							<td>
								<a href="javascript:void(0)" >
									
										<button type="button" class="btn <?php echo $arr_status_color[$task['status']];?> btn-xs"><?php echo $arr_status[$task['status']];?></button>
									</a>
							</td>
							<td>
						<a class="googleMapPopUp" rel="nofollow" href="https://maps.google.com/maps?q=<?php echo $task['lat'];?>,<?php echo $task['lon'];?>" target="_blank">
									<img class="locate_task" src="<?php echo assets_url('images/marker.png');?>" height="30" width="30" />
									</a>
									<a href="<?php echo site_url('check_task_response/'.$task['id']);?>" class="btn btn-warning btn-xs"><i class="fa fa-search"></i> Responses </a>	
									
								<a href="<?php echo site_url('check_task_detail/'.$task['id']);?>"><img src="<?php echo assets_url('/images/view-img.png');?>" alt=""></a>
							</td>
						  </tr>
					 <?php
						 $count++;
					   }
					  $locations=json_encode($marker_array,JSON_NUMERIC_CHECK); 
				
					//$array_final = preg_replace('/"([a-zA-Z]+[a-zA-Z0-9_]*)":/','$1:',$locations);

			       
			       }
			       else
			       {
					   ?>
					<tr ><td colspan='7'>No tasks found ! </td></tr>   
					   <?php
				   }
                 ?>

                         </tbody>
                      </table>
                     
           
           
 
            </div>
          </div>
          



		<div class="modal fade" id="myMapModal" >
			<div class="modal-dialog" style="width:90%">
				<div class="modal-content">
					
					<div class="modal-body" style="height: 500px;">
						<div class="container">
							<div class="row">
										<div id="map" style="height: 470px;width: 100%;position: absolute;"></div> 


								<div id="map-canvas" class=""></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.colorbox.js"></script>   
<script>
	
	
	$('.googleMapPopUp').each(function() {
    var thisPopup = $(this);
    thisPopup.colorbox({
        iframe: true,
        innerWidth: 400,
        innerHeight: 300,
        opacity: 0.7,
        href: thisPopup.attr('href') + '&ie=UTF8&t=h&output=embed'
    });
});

      function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 3,
          center: {lat:  <?php echo $marker_array[0]['lat'] ?>, lng:  <?php echo $marker_array[0]['lng'] ?>}
        });

		 var infowindow = new google.maps.InfoWindow({
			maxWidth: 350
		 }); 
        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
       
       /* var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            label: labels[i % labels.length]
          });
          
          
        });
*/
var stack = [];
   for (var i = 0; i < locations.length; i++) {
                    // obtain the attribues of each marker
                    //console.log(locations);

                    
                      // InfoWindow content
  var content = '<div id="iw-container">' +
                    '<div class="iw-title">Task For '+locations[i].category+' Service</div>' +
                    '<div class="iw-content">' +
                      '<div class="iw-subTitle">Category</div><p>'+locations[i].category+'</p><div class="iw-subTitle">Description</div>' +
                      '<img src="images/vistalegre.jpg" alt="Task image/ doc " height="115" width="83">' +
                      '<p>'+locations[i].description+'</p>' +
                      '<div class="iw-subTitle">Type</div><p>'+locations[i].type+'</p><div class="iw-subTitle">Address</div><p>'+locations[i].address+'</p><div class="iw-subTitle"></div>' +
                      '<p>'+
                      '<br>Phone. <br>e-mail: <br>www: www.test.com<br><a href="#" style="color:blue;"> See Detail</a></p>'+
                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                  '</div>';
                  

		var lat = parseFloat(locations[i].lat);
		var lng = parseFloat(locations[i].lng);
		var marker = new google.maps.Marker({
			position : new google.maps.LatLng(lat, lng),
			map: map,
			title:"This is a marker"
		});    
                    
      google.maps.event.addListener(marker, 'click', function() {
		   infowindow.setContent(content);
		    infowindow.open(map,marker);
	  });
	  google.maps.event.addListener(map, 'click', function() {
		infowindow.close();
	  });
      google.maps.event.addListener(infowindow, 'domready', function() {

			// Reference to the DIV that wraps the bottom of infowindow
			var iwOuter = $('.gm-style-iw');

			/* Since this div is in a position prior to .gm-div style-iw.
			 * We use jQuery and create a iwBackground variable,
			 * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
			*/
			var iwBackground = iwOuter.prev();

			// Removes background shadow DIV
			iwBackground.children(':nth-child(2)').css({'display' : 'none'});

			// Removes white background DIV
			iwBackground.children(':nth-child(4)').css({'display' : 'none'});

			// Moves the infowindow 115px to the right.
			iwOuter.parent().parent().css({left: '115px'});

			// Moves the shadow of the arrow 76px to the left margin.
			iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

			// Moves the arrow 76px to the left margin.
			iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

			// Changes the desired tail shadow color.
			iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});
		   // Reference to the div that groups the close button elements.
			var iwCloseBtn = iwOuter.next();

			// Apply the desired effect to the close button
			iwCloseBtn.css({opacity: '1', right: '38px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

			// If the content of infowindow not exceed the set maximum height, then the gradient is removed.
			if($('.iw-content').height() < 140){
			  $('.iw-bottom-gradient').css({display: 'none'});
			}

			// The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
			iwCloseBtn.mouseout(function(){
			  $(this).css({opacity: '1'});
			});
			iwCloseBtn.css({'display': 'none'});
        });
   
  /*
  
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infowindow.setContent(locations[i].description);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    
                    */
                    
                    stack.push(marker);
                }


        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, stack,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

      }
      
locations=<?php echo $locations;?>;


		$('#myMapModal').on('show.bs.modal', function() {  
		  resizeMap();
		});
	


    </script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCd_Tujq99uHzmtznB4SqOWvjy6KHmDpzQ&callback=initMap">
</script>
<script>
		google.maps.event.addDomListener(window, 'load', initMap);
	google.maps.event.addDomListener(window, "resize", resizingMap());
	function resizeMap() {
	if(typeof map =="undefined") return;
	setTimeout( function(){resizingMap();} , 400);
	}
	function resizingMap() {
	if(typeof map =="undefined") return;
	google.maps.event.trigger(map, "resize");
	}
	
	
	</script>
 <script>
$(document).ready(function() {
    $('#datatable').DataTable();
} );
</script>

<style>
	
#map{
height: 300px;
	width:100%;
	border: 1px solid #fff;
	box-shadow: 0px 0px 0px 8px rgba(234,234,234,0.9);
}

.profile_right_blk h2
{
    margin-bottom: 0; 
}

.right_form{
	    background: #fff;
    border: 2px solid #a1a1a1;
    border-radius: 5px;	
}

.right_form_button
{
	    padding-bottom: 30px;
}
.time_select span{
	    display: block;
    padding-top: 12px;
}

#file
{
	padding-top: 18px;
}

</style>
<?php


?>
<link href="<?php echo assets_url('css/jquery.datetimepicker.css'); ?>" rel="stylesheet" type="text/css"/>


<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>
          <div class="profile_right_blk right_form">
                <div class="col-md-12 right_form_content">
                   <div class="col-md-12" >
					 <?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger " style="text-align: center;"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success " style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                       
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger" style="text-align: center;"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                       
					 </div>
					 
					 
					 <?php echo form_open_multipart(site_url('edit_task_action'),array('class'=>'',"id"=>'change_password')); ?>		
					 <input type="hidden" name="id" id="id" value="<?php echo $details[0]['id'];?>"/> 
                       
					  <div class="form-group">
							  <div class="col-sm-4"><label>Title: </label></div>
							  
							  <div class="col-sm-7">  
								  <?php echo form_input(array('name'=>'title','id'=>'title', 'value'=>$details[0]['title'],'class'=>'form-control',"placeholder"=>"Title",'label' =>false)); ?>
								   <p style="padding-top:8px;font-size:16px;"><span id="count"></span> characters left</p>
							  </div>
				      </div>
				      
					  <div class="form-group">
							  <div class="col-sm-4"><label> </label></div>
							  
							  <div class="col-sm-7">  
			 <input id="is_residential" class="checkbox-custom" name="is_residential" type="checkbox" <?php echo ($details[0]['is_residential']==1 ? 'checked':'');?>>
              <label for="is_residential" class="checkbox-custom-label">Residential</label>
              <input id="is_commercial" class="checkbox-custom" name="is_commercial" type="checkbox" <?php echo ($details[0]['is_commercial']==1 ? 'checked':'');?>>
              <label for="is_commercial" class="checkbox-custom-label">Commercial</label>
							  </div>
				      </div>

				      
					  <div class="form-group">
							  <div class="col-sm-4"><label>Adress: </label></div>							  
							  <div class="col-sm-7">  
								   <div class="col-sm-3">
									    <input type="text" id="address_unit" value="<?php echo $details[0]['address_unit'];?>" class="form-control" name="address_unit" placeholder="Unit #">
								   </div>								  	 
								   <div class="col-sm-9">
									    <input type="text" id="address" class="form-control" name="address" placeholder="Enter address" value="<?php echo $details[0]['address'];?>">
								   </div>								  	 
                                 
                                  
                                   <input type="hidden" name="lat" id="latbox" value="<?php echo $details[0]['lat'];?>"/>
			                       <input type="hidden" name="lon" id="lngbox" value="<?php echo $details[0]['lon'];?>"/> 
							  </div>
				      </div>
				      
					  <div class="form-group">
							  <div class="col-sm-4"><label>	 </label></div>							  
							  <div class="col-sm-7">  								  	 
                              <div id="map"></div>
							  </div>
				      </div>
				      
				      
				  				      
					  <div class="form-group">
							  <div class="col-sm-4"><label>Date: </label></div>							  
							  <div class="col-sm-7">  								  	 
                                   <input type="text" id="datetimepicker2" value="<?php echo $details[0]['task_date'];?>" class="form-control" name="task_date" placeholder="Date"/>   
							  </div>
				      </div>    
				      
					  <div class="form-group">
							  <div class="col-sm-4"><label>Time: </label></div>
							  
							  <div class="col-sm-7">  
								  	  
			
             
                 	<input type="text" id="datetimepicker1"  value="<?php echo $details[0]['task_time'];?>" value="" class="form-control" name="task_time" placeholder="Time"/>
               
                
							  </div>
				      </div>
				      
					  <div class="form-group">
							  <div class="col-sm-4"><label>Estimated Hour(s): </label></div>
							  
							  <div class="col-sm-7 time_select">  
								  	  
			
             
                 	<input type="text" id="estimated_time"  value="<?php echo $details[0]['estimated_time'];?>" class="form-control" name="estimated_time" placeholder="Estimated Time"/>
               <span style="color: green;font-size: 16px;">Estimated total amount is $<?php echo $details[0]['total_price'];?></span>
                 	<input type="hidden" name="hourly_price" id="hourly_price" value="<?php echo $hourly_price;?>"/>
                
							  </div>
				      </div>
				  
				   <div class="form-group">
					  <div class="col-sm-4"><label>Parent Category: </label></div>
					  
					  <div class="col-sm-7">  
						   <?php echo form_dropdown('categ_parent_slug', $parent_categories,$details[0]['categ_parent_slug'], 'class="form-control " id="parent_id" required="required" onchange="populate_subcategory(this.value)"'); ?>
						   

					  </div>
				  </div>
				  
				   <div class="form-group">
					  <div class="col-sm-4"><label>Sub Category: </label></div>
					  
					  <div class="col-sm-7">  
						  <?php echo form_dropdown('categ_id', $sub_category,$details[0]['categ_id'], 'class="form-control" id="subcategory_id" required="required " onclick="getPrice(this.value)"  '); ?>
						  
						
					  </div>
                   </div>
                   
  
  					  <div class="form-group">
							  <div class="col-sm-4"><label>Description: </label></div>
							  
							  <div class="col-sm-7">  
								   <textarea id=""  class="form-control" name="description" rows="3" cols="20"><?php echo $details[0]['description'];?></textarea>
								   
								 
							  </div>
				      </div>
  
  					  <div class="form-group">
							  <div class="col-sm-4"><label>Document/Image: </label></div>
							  
							  <div class="col-sm-7">  
								  
						<?php		  
					    if($details[0]['document']!="")
						{
							
							$ext = array("png", "jpg", "jpeg", "gif", "gif");
							$doc=$details[0]['document'];
							 $file_name=explode(".", $doc);
							
                            if(!in_array(end($file_name), $ext)) 
                            {
   
						?>
						<a href="<?php echo site_url('/admin/task/download_doc/'.$details[0]['document']);?>">
<p><img src="<?php echo site_url('/assets/images/task_doc.png');?>" height="50" width="50"/><br/></p>
<p style="width: 20%;text-align: center;"><span></span></p></a>
<?php
                            }
                            else
                            {
								?>
							<a href="<?php echo site_url('/admin/task/download_doc/'.$details[0]['document']);?>">	<img src="<?php echo site_url('/assets/task/'.$details[0]['document']);?>" height="50" width="50"/><p style="width: 20%;text-align: center;"><span></span></p></a>
								<?php
							}
                         }
                         ?>
											   <input onchange='check_upload()' id="file" type="file" name="file" class="file">
   
								 
							  </div>
				      </div>
                      
                  <div class="col-lg-12 text-center right_form_button">
                     <a href="javascript:void(0)" id="change_pass" class="sign-up">Update</a>
                  </div>
                  
                   </form>	
                   
                </div>
              
             
           
          </div>
          

			
    <script>
    $('#change_pass').click(function(){
		
		 $("#change_password").submit();
		
		});
    </script>
     <script src="<?php echo assets_url('js/jquery.datetimepicker.full.js'); ?>"></script> 
<script>

$('body').bind('cut copy paste', function (e) {
      e.preventDefault();
   });

	var maxchar = 100;
var i = document.getElementById("title");
var c = document.getElementById("count");
c.innerHTML = maxchar;
    
i.addEventListener("keydown",count);

function count(e){
	 var keyCode = e.keyCode;

    var len =  i.value.length;
    if (len >= maxchar){
		 if(keyCode!=8)
         {
             e.preventDefault();
		 }
    } else{
       c.innerHTML = maxchar - len-1;   
    }
}


function check_upload() {
	 $(".alert-danger").hide();
	 var image_file = document.getElementById("file");
	 var size = image_file.files[0].size/1024/1024;
	
    //checking file name and extension here
   // var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif|.jpeg|.pdf|.doc|.docx|.txt|.xls|.xlsx|.odt|.ods|.csv)$");
   var extensions = ["jpg", "jpeg", "txt", "png","gif","pdf","doc","docx","txt","xls","xlsx","odt","ods","csv"];
   var extension = image_file.files[0].name.replace(/.*\./, '').toLowerCase();

	if (extensions.indexOf(extension) < 0) { 
				$("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
		$(".alert-danger").text("Supported extension are jpg, png, gif, jpeg, pdf, doc, docx, txt, xls, xlsx, odt, ods, csv").show();
        $("#file").val('');
        return false;
	}
	else
	{
		 if(size > 2){
			 $("html, body").animate({ scrollTop: $(".tab_part").offset().top }, 1000);
			 $(".alert-danger").text('File size exceed the size of 2MB').show();
			 $("#file").val('');
			 return false;
		
		}
	}

}

	
		function getPrice(id){
		$("#estimated_time").val('');
		$("#hourly_price").val(0);
		if(id !=''){
			jQuery.ajax({ 
				url: '<?php echo site_url('get_price'); ?>',
				type: 'POST',
				data:{'id':id},				
				success: function(data) {
					$("#hourly_price").val(data)					
				}
			});
		}
	}  
	
	   $("#estimated_time").keyup(function() {
        var vall = $('input:text[name=estimated_time]').val();
		  $(".time_select span").html('<img src="http://p.brsoftech.net/handyman/assets/css/images/loading.gif">');

	   	  var amount =vall * $("#hourly_price").val(); 
	   	  //console.log(amount);
           
	      $(".time_select span").text("Estimated total amount is $"+amount.toFixed(2));
    });
    



$('#datetimepicker1').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:5
});

$("#datetimepicker2").on("keydown keypress keyup", false);

	var dateToday = new Date();
	$('#datetimepicker2').datetimepicker({
	timepicker:false,
	format:'Y/m/d',
	minDate: dateToday


});
	function populate_subcategory(slug)
	{		
				jQuery.ajax({ 
				url: '<?php echo site_url('/task/get_subcateg_bySlug'); ?>',
				
				data: {'slug':slug},
				type: 'POST',
				success: function(data) {
					var obj = jQuery.parseJSON ( data );
					console.log(obj);
					
					console.log(obj.msg);
					if(obj.msg=='success'){
						
						var select = $('#subcategory_id');

						select.empty();


						for (var j = 0; j < obj.response.length; j++){
						console.log(obj.response[j].name);
						$("#subcategory_id").append("<option value='" +obj.response[j].id+ "'>" +obj.response[j].name+ "     </option>");
						}
						
						
					}
			
				}
			});
		}
</script>

<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script>
	  function initialize() {
		   var myLatLng = {lat: <?php echo $details[0]['lat'];?>, lng: <?php echo $details[0]['lon'];?>};
		   
        var map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('address');
        var searchBox = new google.maps.places.SearchBox(input);
       // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
        
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
			  $("#latbox").val("");
			  $("#latbox").val("");
            return;
          }
		 
          // Clear out the old markers.
          
          for (var i = 0, marker; marker = markers[i]; i++) {
			marker.setMap(null);
			}
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
             var pinColor = "FE7569";
    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
        new google.maps.Size(21, 34),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));
			  var marker = new google.maps.Marker({
				draggable: true,
				map: map,
				icon: pinImage,
				title: place.name,
				position: place.geometry.location
				
			  });
			  

  
				document.getElementById("latbox").value = marker.getPosition().lat()
				document.getElementById("lngbox").value = marker.getPosition().lng();
				
				google.maps.event.addListener(map, 'click', function(event) {
					
					marker.setPosition(event.latLng);
					document.getElementById("latbox").value = event.latLng.lat();
					document.getElementById("lngbox").value = event.latLng.lng();
					
					
			   });
				
				google.maps.event.addListener(marker, 'dragend', function (event) {
				document.getElementById("latbox").value = event.latLng.lat();
				document.getElementById("lngbox").value = event.latLng.lng();
			
				
				});
				
			  markers.push(marker);
			
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });

  
      }
  google.maps.event.addDomListener(window, 'load', initialize());
  
  document.getElementById('address').onblur = function () {

    google.maps.event.trigger(this, 'focus');
  
};




  
  
  </script>

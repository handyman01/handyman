<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<link href="<?php echo assets_url('css/rating.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo assets_url('js/rating.js'); ?>"></script> 
<style>
	.form-group{
		margin-bottom: 15px;
		}
	.locate_task{
		vertical-align: baseline;
	}
	
	.locate_tasks_btn {
    background-color: rgb(70, 70, 70);
    border-radius: 5px;
    color: rgb(255, 255, 255) !important;
    display: inline-block;
    font-size: 17px;
    height: 50px;
    line-height: 50px;
    padding: 0 30px;
    text-transform: uppercase;
     margin-bottom: 12px; 
}
.profile_right_blk h2
{
    margin-bottom: 0; 
}
.gm-style-iw {
	width: 350px !important;
	top: 15px !important;
	left: 0px !important;
	background-color: #fff;
	box-shadow: 0 1px 6px rgba(178, 178, 178, 0.6);
	border: 1px solid rgba(72, 181, 233, 0.6);
	border-radius: 2px 2px 10px 10px;
}
#iw-container {
	margin-bottom: 10px;
}
#iw-container .iw-title {
	font-family: 'Open Sans Condensed', sans-serif;
	font-size: 22px;
	font-weight: 400;
	padding: 10px;
	background-color: #48b5e9;
	color: white;
	margin: 0;
	border-radius: 2px 2px 0 0;
	line-height: 1;
}
#iw-container .iw-content {
	font-size: 13px;
	line-height: 18px;
	font-weight: 400;
	margin-right: 1px;
	padding: 15px 5px 20px 15px;
	max-height: 140px;
	overflow-y: auto;
	overflow-x: hidden;
}
.iw-content img {
	float: right;
	margin: 0 5px 5px 10px;	
}
.iw-subTitle {
	font-size: 16px;
	font-weight: 700;
	padding: 5px 0;
}
.iw-bottom-gradient {
	position: absolute;
	width: 326px;
	height: 25px;
	bottom: 10px;
	right: 18px;
	background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
}
</style>


<link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" type="text/css"/>

          <div class="profile_right_blk">
			  <div class="col-md-12" style="margin-bottom: 12px;">
				   <?php
				     if ($this->session->flashdata('error')) { ?>
                          <div class="alert alert-danger " style="text-align: center;"> <?= $this->session->flashdata('error') ?> </div>
                       <?php } 
                       
					  if ($this->session->flashdata('success')) { ?>
                          <div class="alert alert-success " style="text-align: center;"> <?= $this->session->flashdata('success') ?> </div>
                       <?php } ?>
                       
                        <?php
                      
                       if(validation_errors())
                       {
						   ?>
						   <div class="alert alert-danger" style="text-align: center;"><?php echo validation_errors();?></div>
						   <?php
					   }
                      
                      
                       ?>  
                       
                       
				<h2>Posted Tasks</h2>
				
				<div>
				<a href="<?php echo site_url('post_task');?>"  data-toggle="modal" class="pull-right">	<button type="button" class="btn btn-success">Post New Task</button></a>
				<?php
					if(!empty($tasks))
					{ ?>
					
					<a href="#myMapModal"  data-toggle="modal" class="pull-right" style="padding-right: 10px;">
						<button type="button" class="btn btn-info ">Locate My Tasks</button>
					</a>
						
				   
				   <?php } ?>
				</div>
             </div>
            
              <div id="completetask" class="modal fade" role="dialog">
				 
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Do you really want to submit task as completed ?</h4>
					  </div>
					
					   
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<a class="btn btn-danger btn-ok">Yes</a>
					  </div>   
						
					
					</div>

				  </div>
				</div>
  
  
               <div id="complete" class="modal fade" role="dialog">
				 
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<form method="post" onsubmit="return false" id="complete_form" >
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Task Complete Request</h4>
					  </div>
					  <div class="modal-body">
					  
						  <div class="form-group">
							 <input type="hidden" id="complete_task_id" name="task_id"/>
							<label for="Message">Working Hour(s): <span class="redcolor">*</span></label>
							<input type="text" name="cust_wrk_hrs" id="cust_wrk_hrs">
							<label for="Message">Working Fee: <span class="redcolor">*</span></label>
							<input type="text" name="cust_wrk_amt" id="cust_wrk_amt">
						  </div>
						  
					   
					    <div class="form-group">
							<button type="submit" class="btn btn-default" >Submit</button>
					   </div>
					   </div>
					  <!-- <div class="modal-footer">
						<button type="submit" class="btn btn-default" >Submit</button>
					  </div>-->   
						 </form>
					
					</div>

				  </div>
				  
				  
				  
				  
				  
				  
				  
				</div>
			
	
	
	
	
            
             <div id="cancel" class="modal fade" role="dialog">
				 
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<form method="post" onsubmit="return false" id="cancel_form" >
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Task cancellation or Complaint form</h4>
					  </div>
					  <div class="modal-body">
					  
						  <div class="form-group">
							<label for="Message">Please enter message here: <span class="redcolor">*</span></label>
							<input type="hidden" id="cancel_task_id" name="task_id"/>
							<textarea name="cancel_reason" id="feedbackform" cols="" rows="" class="form-control messages" required></textarea>
						  </div>
						  
					   
					    <div class="form-group">
							<button type="submit" class="btn btn-default" >Submit</button>
					   </div>
					   </div>
					  <!-- <div class="modal-footer">
						<button type="submit" class="btn btn-default" >Submit</button>
					  </div>-->   
						 </form>
					
					</div>

				  </div>
				  
				  
				  
				  
				  
				  
				  
				</div>
			
			                 
             <div id="rating" class="modal fade" role="dialog">
				 
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Task Rating</h4>
					  </div>
					  <div class="modal-body">
		               
		               <?php echo form_open_multipart(site_url('ratingToHandyman'),array('class'=>'',"id"=>'rating_form')); ?> 			  
		               
						  <div class="form-group">
							 
							  <input name="rating" value="0" id="rating_star" type="hidden" postID="1" />
							  <input name="task_id" value="0" id="rating_task_id" type="hidden" />
							  <input name="user_id" value="0" id="rating_user_id" type="hidden"  />
							  <input name="user_type" value="user" id="" type="hidden"  />
							  <input name="review_by" value="<?php echo $user_detail['id'];?>" id="" type="hidden"  />


							<label for="Message">Your reviews<span class="redcolor">*</span></label>
							<textarea name="review" id="review" cols="" rows="" class="form-control messages" required></textarea>
						  </div>
						  
					   
					    <div class="form-group">
							<button type="button" class="btn btn-default" id="rating_submit" >Submit</button>
					   </div>
					   </form>
					   </div>
					   
					  <!-- <div class="modal-footer">
						<button type="submit" class="btn btn-default" >Submit</button>
					  </div>-->   
						
					
					</div>

				  </div>
			
			
			
				</div>
			
            <div class="table-responsive my_table" style="min-height:300px;padding: 12px;">
				
										
                   <table class="table table-striped jambo_table bulk_action" id = "datatable">
					<thead>
					 <tr class="headings">
                    <th>Task No.</th>
                    <th>Submitted On</th>
                    <th>Handyman</th>
                    <th>Main category</th>
                    <th>Category</th>
                    <th>Total Price</th>
                    <th>Deposit</th>
                    <th>Status</th>
                    <th>View Details</th>
                          
                          </tr>
                        </thead>

                        <tbody>
					<?php
				
					if(!empty($tasks))
					{
					
						$count=1;
						
						$marker_array = array();
						foreach($tasks as $k=>$task)
						{
						//	print_r($task);die;
								$var_document='';
						 if($task['document']!="")
						{
							
							$ext = array("png", "jpg", "jpeg", "gif", "gif");
							$doc=$task['document'];
							$file_name=explode(".", $doc);
							
                            if(!in_array(end($file_name), $ext)) 
                            {
   
   
   $var_document='<a href="'.site_url('/admin/task/download_doc/'.$task['document']).'">
<p><img src="'.site_url('/assets/images/task_doc.png').'" height="115" width="86"/><br/></p>
<p style="width: 20%;text-align: center;"><span></span></p></a>';
   

                            }
                            else
                            {
								$var_document='<a href="'.site_url('/admin/task/download_doc/'.$task['document']).'"><img src="'.site_url('/assets/task/'.$task['document']).'" height="115" width="86"/><p style="width: 20%;text-align: center;"><span></span></p></a>';
								
							}
                         }
                         
                         
                         
							$marker_array[$k] = array('lat'=>$task['lat'],'lng'=>$task['lon'],'category'=>ucfirst($task['category_name']),'total_price'=>$task['total_price'],'created_on'=>$task['created'],'description'=>$task['description'],'address'=>$task['address'],'type'=>($task['is_residential']==1 ? 'Residential':'Commercial'),'email'=>$task['customer_email'],'document'=>$var_document,'title'=>ucfirst($task['title']));
						
						?>
						  <tr>
							<td><?php echo $task['id'];?></td>
							<td><?php echo $task['created'];?></td>
							<td><?php 
							if($task['name']!='')
							{
								echo ' <a target="_blank" style="color:blue;text-decoration:underline;" href="'.site_url('user_profile/h/'.$task['handyman_id']).'">'.$task['name'].'</a>';
							}
							else
							{
							   echo 'Not Assigned';	
							}
							
							?> </td>
							<td>
								<?php 
							if($task['categ_parent_slug']=='home_renovation')
							{
								echo "Home Renovation";
							}
							else {
								echo "Home Maintenance";
							}
							?>
							</td>
							<td><?php echo ucfirst($task['category_name']);?></td>
							<td>$<?php echo $task['total_price'];?></td>
							<td>
							<?php
							if($task['task_type']=='guaranteed' && $task['payment_status']==0 && $task['total_deposit_amt']>0)
							{
								$confirm_url=base_url().'task/buy/'.$task['id'];
								?>
								 <a href="<?php echo $confirm_url;?>" style="margin-bottom: 12px;"  class="">	<button type="button" class="btn btn-success  btn-xs">Pay Now</button></a> 
								<?php
							}
							else if($task['payment_status']==1)
							{
							   echo 'Done';	
							}
							else
							{
								echo '-';
							}
							?>
										
							</td>
							<td>
								<a href="javascript:void(0)" >
									
									<button type="button" class="btn <?php echo $this->config->item('task_status_color')[$task['status']];?> btn-xs"><?php echo $this->config->item('task_status')[$task['status']];?></button>
									
										
									</a>
							</td>
							<td>
							<!--	<div class="btn-group dropup">
								  <button class="btn">Options</button>
								  <button class="btn dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
								  </button>
								<ul class="dropdown-menu pull-right"> -->
								
								
								
								<div class="btn-group">
								  
								  <button class="btn dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu pull-right">
									<li><a class="googleMapPopUp" rel="nofollow" href="https://maps.google.com/maps?q=<?php echo $task['address'];?>" target="_blank">
									View on Map
									</a></li>
									
									<li><a href="<?php echo site_url('check_task_detail/'.$task['id']);?>">Check Details</a></li>
									<?php
									if($task['status']=='S')	echo '<li><a href="'.site_url('edit_task/'.$task['id']).'" >Edit Task </a></li>';
								   
								   
								   //task responses for hme renovation won't be show to user because for renovation tasks handyman will be assined by admin
								   
								    if($task['categ_parent_slug']!='home_renovation' && $task['response_count'] >0 ) echo '<li><a href="'.site_url('check_task_response/'.$task['id']).'">View '.$task['response_count'].' Response(s) </a></li>';
									
									if(($task['status'] == 'S' || $task['status'] == 'P') && $task['complete_request'] == 0)
									{
										echo ' <li><a href="javascript:void(0)" data-toggle="modal" data-target="#cancel"  data-id="'.$task['id'].'" class="cancel_button"> Cancel Request Or Complaint</a></li>';
										
									}
										
									if(($task['status'] == 'P') && $task['complete_request'] == 0)
									{
										echo ' <li><a href="javascript:void(0)" data-toggle="modal" data-target="#complete"  data-id="'.$task['id'].'" class="complete_button">Send Complete Request </a></li>';
										
										//echo ' <li><a href="javascript:void(0)" data-href="'.site_url('task/complete_request/'.$task['id']).'" data-toggle="modal" data-target="#completetask">Send Complete request </a></li>';
									}
									if($task['status']=='C' && $task['is_rated']==0)  echo ' <li><a href="javascript:void(0)" data-toggle="modal" data-target="#rating" data-user="'.$task['handyman_id'].'"  data-id="'.$task['id'].'" class="rating_button">Give Rating </a></li>';
									
									?>
									</ul>
									</div>
									<br/>
									<?php if($task['complete_request'] == 1 && $task['status'] != 'C') 
									echo  '<a href="javascript:void(0)" data-toggle="tooltip" title="You have sent the complete request!"><span class="glyphicon glyphicon-exclamation-sign"></span></a>';
									 ?>
							</td>
						  </tr>
					 <?php
						 $count++;
					   }
					  $locations=json_encode($marker_array,JSON_NUMERIC_CHECK); 
				
					//$array_final = preg_replace('/"([a-zA-Z]+[a-zA-Z0-9_]*)":/','$1:',$locations);

			       
			       }
			       else
			       {
					   $marker_array = array();
					   
					   ?>
					<tr ><td colspan='7'>No tasks found ! </td></tr>   
					   <?php
				   }
                 ?>

                         </tbody>
                      </table>
 
            </div>
        
          </div>
          



		<div class="modal fade" id="myMapModal" >
			<div class="modal-dialog" style="width:90%">
				<div class="modal-content">
					
					<div class="modal-body" style="height: 500px;">
						<div class="container">
							<div class="row">
										<div id="map" style="height: 470px;width: 100%;position: absolute;"></div> 


								<div id="map-canvas" class=""></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.colorbox.js"></script>   
<script>
	$('#completetask').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
           
        });
    
    $(document).on('click',".cancel_button",function(event){

		$("#cancel_task_id").val($(this).attr('data-id'));
	});
	$( "#cancel_form" ).submit(function( event ){
		jQuery.ajax({ 
				url: '<?php echo site_url('/task/cancel_task'); ?>',
				
				data: $(this).serialize(),
				type: 'POST',
				success: function(data) {
					if(data){
						location.reload();
						//$("#cancel_form .modal-body").html('<div class="form-group">Cancellation request has been sent successfully!!</div>');
					}
					
			
				}
			});
		
	});
	
	$(document).on('click',".complete_button",function(event){
		$("#complete_task_id").val($(this).attr('data-id'));
	});
	
	$( "#complete_form" ).submit(function( event ){
		
		if($('#cust_wrk_hrs').val()=='')
		{
			alert('Please enter working hours!');
			return false;
		}
		else if($('#cust_wrk_amt').val()=='')
		{
			alert('Please enter working amount!');
			return false;
		}
		else
		{
					jQuery.ajax({ 
				url: '<?php echo site_url('/task/complete_task_request'); ?>',				
				data: $(this).serialize(),
				type: 'POST',
				success: function(data) {
					if(data){
						location.reload();
						//$("#cancel_form .modal-body").html('<div class="form-group">Cancellation request has been sent successfully!!</div>');
					}
					
			
				}
			});
			
		}
		

		
	});
	$('.googleMapPopUp').each(function() {
    var thisPopup = $(this);
    thisPopup.colorbox({
        iframe: true,
        innerWidth: 400,
        innerHeight: 300,
        opacity: 0.7,
        href: thisPopup.attr('href') + '&ie=UTF8&t=h&output=embed'
    });
});

       function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
           center: new google.maps.LatLng(52.2, 5)
          //center: {lat:  <?php echo $marker_array[0]['lat'] ?>, lng:  <?php echo $marker_array[0]['lng'] ?>}
        });

 var infowindow = new google.maps.InfoWindow();
        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
       
       /* var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            label: labels[i % labels.length]
          });
          
          
        });
*/
var stack = [];
var markers=[];
var contents = [];
var infowindows = [];

   for (var i = 0; i < locations.length; i++)
   {
                    // obtain the attribues of each marker
                    //console.log(locations);

                    
                      // InfoWindow content
                     var content = '<div id="iw-container">' +
                    '<div class="iw-title">'+locations[i].title+'</div>' +
                    '<div class="iw-content">' +
                      '<div class="iw-subTitle">Category</div><p>'+locations[i].category+'</p><div class="iw-subTitle">Description</div>' +
                      locations[i].document+
                      '<p>'+locations[i].description+'</p>' +
                      '<div class="iw-subTitle">Type</div><p>'+locations[i].type+'</p><div class="iw-subTitle">Address</div><p>'+locations[i].address+'</p><div class="iw-subTitle"></div>' +
                      '<p>'+
                      'Email: <br>'+locations[i].email+'<br></p>'+
                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                  '</div>';
                  
                    // A new Info Window is created and set content
                    /*
  var infowindow = new google.maps.InfoWindow({
    content: content,

    // Assign a maximum value for the width of the infowindow allows
    // greater control over the various content elements
    maxWidth: 350
  }); 
  */
  
                      var lat = parseFloat(locations[i].lat);
                    var lng = parseFloat(locations[i].lng);
                   markers[i] = new google.maps.Marker({
                        position : new google.maps.LatLng(lat, lng),
                        map: map,
                        title:"This is a marker"
                    });    
        
                    stack.push(markers[i]);
                     markers[i].index = i;
                      contents[i] = content;
                    
                    infowindows[i] = new google.maps.InfoWindow({
        content: contents[i],
        maxWidth: 350
    });

    google.maps.event.addListener(markers[i], 'click', function() {
        console.log(this.index); // this will give correct index
        console.log(i); //this will always give 10 for you
        infowindows[this.index].open(map,markers[this.index]);
        map.panTo(markers[this.index].getPosition());
    });  
                }


        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, stack,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

      }
locations=<?php echo $locations;?>;


		$('#myMapModal').on('show.bs.modal', function() {  
		  resizeMap();
		});
	


    </script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCd_Tujq99uHzmtznB4SqOWvjy6KHmDpzQ&callback=initMap">
</script>
<script>
		google.maps.event.addDomListener(window, 'load', initMap);
	google.maps.event.addDomListener(window, "resize", resizingMap());
	function resizeMap() {
	if(typeof map =="undefined") return;
	setTimeout( function(){resizingMap();} , 400);
	}
	function resizingMap() {
	if(typeof map =="undefined") return;
	google.maps.event.trigger(map, "resize");
	}
	
	
	</script>
 <script>
$(document).ready(function() {
    $('#datatable').DataTable({
        "order": [[ 1, "desc" ]]
    } );
    
    $(document).on('click',".rating_button",function(){
		$('#rating_task_id').val($(this).data("id"));
		$('#rating_user_id').val($(this).data("user"));
    });
  
    $(document).on('click',"#rating_submit",function(){
		$('#rating_form').submit();
		//location.reload();
	});
	
	
	  $("#rating_star").codexworld_rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: false,
        imageDirectory: 'images/',
        inputAttr: 'postID'
   
});
    
} );

	$(document).on('keypress',"#cust_wrk_hrs,#cust_wrk_amt",function(e){
	 
	 var vall = String.fromCharCode(e.which);

     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57 )) {
            e.preventDefault();
              // return false;
      }

   });
   
</script>

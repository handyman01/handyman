<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth
{
    var $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
        $this->CI->load->helper('url');
    }
    
    function check_access($access, $default_redirect=false, $redirect = false)
    {
        /*
        we could store this in the session, but by accessing it this way
        if an admin's access level gets changed while they're logged in
        the system will act accordingly.
        */
        
        $admin = $this->CI->session->userdata('admin');
        
        $this->CI->db->select('access');
        $this->CI->db->where('id', $admin['id']);
        $this->CI->db->limit(1);
        $result = $this->CI->db->get('admin');
        $result = $result->row();
        
        //result should be an object I was getting odd errors in relation to the object.
        //if $result is an array then the problem is present.
        if(!$result || is_array($result))
        {
            $this->logout();
            return false;
        }
    //  echo $result->access;
        if ($access)
        {
            if ($access == $result->access)
            {
                return true;
            }
            else
            {
                if ($redirect)
                {
                    redirect($redirect);
                }
                elseif($default_redirect)
                {
                    redirect(config_item('admin_folder').'/dashboard/');
                }
                else
                {
                    return false;
                }
            }
            
        }
    }
    
    /*
    this checks to see if the admin is logged in
    we can provide a link to redirect to, and for the login page, we have $default_redirect,
    this way we can check if they are already logged in, but we won't get stuck in an infinite loop if it returns false.
    */
    function is_admin_logged_in()
    {
    
        //var_dump($this->CI->session->userdata('session_id'));

        //$redirect allows us to choose where a customer will get redirected to after they login
        //$default_redirect points is to the login page, if you do not want this, you can set it to false and then redirect wherever you wish.

        $admin = $this->CI->session->userdata('admin');
        
        if (!$admin)
        {
		    redirect('/admin/user/index');    
            return false;
        }
        else
        {
	
            return true;
        }
    }
    function is_user_logged_in($segment=false)
    {
   
        //var_dump($this->CI->session->userdata('session_id'));

        //$redirect allows us to choose where a customer will get redirected to after they login
        //$default_redirect points is to the login page, if you do not want this, you can set it to false and then redirect wherever you wish.

        $user = $this->CI->session->userdata('user');
       
        if (!$user)
        {
		    redirect('login/'.$segment);                       
            return false;
        }
        else
        {
            return true;
        }
    }
    /*
    this function does the logging in.
    */
    function login_admin($email, $password)
    {
        // make sure the username doesn't go into the query as false or 0
        if(!$email)
        {
            return false;
        }

        $this->CI->db->select('*');
        $this->CI->db->where('email', $email);
        $this->CI->db->where('password',  md5($password));
        $this->CI->db->where('user_type',0);
        $this->CI->db->limit(1);
        $result = $this->CI->db->get('users');
        $result = $result->row_array();
        
        if (sizeof($result) > 0)
        {
			
            $admin = array();
            $admin['admin'] = array();
            $admin['admin']['id'] = $result['id'];
            $admin['admin']['firstname'] = $result['first_name'];
            $admin['admin']['lastname'] = $result['last_name'];
            $admin['admin']['email'] = $result['email'];
            
 

            $this->CI->session->set_userdata($admin);
            return true;
        }
        else
        {
		
            return false;
        }
    }
    
    function login_user($email, $password)
    {
        // make sure the username doesn't go into the query as false or 0
        if(!$email)
        {
            return false;
        }

        $this->CI->db->select('*');
        $this->CI->db->where('email', $email);
        $this->CI->db->where('password',  md5($password));
        $this->CI->db->where('status',1);
        $this->CI->db->limit(1);
        $result = $this->CI->db->get('users');
        $result = $result->row_array();
      
        if (sizeof($result) > 0)
        {
			
            $user = array();
            $user['user'] = array();
            $user['user']['id'] = $result['id'];
            $user['user']['firstname'] = $result['first_name'];
            $user['user']['lastname'] = $result['last_name'];
            $user['user']['email'] = $result['email'];            
            $user['user']['user_type'] = 'user';            
            $user['user']['credit'] =  $result['credit'];            
           
            $this->CI->session->set_userdata($user);
            return true;
        }
        else
        {
			$this->CI->db->select('*');
			$this->CI->db->where('email', $email);
			$this->CI->db->where('password',  md5($password));
			$this->CI->db->where('status',1);
			$this->CI->db->limit(1);
			$result = $this->CI->db->get('handyman');
			$result = $result->row_array();
			
			if(sizeof($result) > 0)
            {
				$user = array();
				$user['user'] = array();
				$user['user']['id'] = $result['id'];
				$user['user']['firstname'] = $result['name'];
				$user['user']['lastname'] = '';
				$user['user']['email'] = $result['email'];        
				$user['user']['user_type'] = 'handyman';  
				$user['user']['credit'] =  $result['credit'];                          
				$this->CI->session->set_userdata($user);
				return true;
			}
			else
			{
				 return false;
			}
           
        }
    }
    
    
    function login_user_fb($fb_id)
    {
		$this->CI->db->select('*');
        $this->CI->db->where('fb_id', $fb_id);
        $this->CI->db->where('status',1);
        $this->CI->db->limit(1);
        $result = $this->CI->db->get('users');
        $result = $result->row_array();
      
        if (sizeof($result) > 0)
        {
			
            $user = array();
            $user['user'] = array();
            $user['user']['id'] = $result['id'];
            $user['user']['firstname'] = $result['first_name'];
            $user['user']['lastname'] = $result['last_name'];
            $user['user']['email'] = $result['email'];            
            $user['user']['user_type'] = 'user';            
            $user['user']['credit'] =  $result['credit'];            
           
            $this->CI->session->set_userdata($user);
            return true;
        }
        else
        {
			return false;
		}
	}
    
    private function generateCookie($data, $expire)
    {
        setcookie('GoCartAdmin', $data, $expire, '/', $_SERVER['HTTP_HOST']);
    }

    private function aes256Encrypt($data)
    {
        $key = config_item('encryption_key');
        if(32 !== strlen($key))
        {
            $key = hash('SHA256', $key, true);
        }
        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);
        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16));
    }

    private function aes256Decrypt($data) 
    {
        $key = config_item('encryption_key');
        if(32 !== strlen($key))
        {
            $key = hash('SHA256', $key, true);
        }
        $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16));
        $padding = ord($data[strlen($data) - 1]); 
        return substr($data, 0, -$padding); 
    }

    /*
    this function does the logging out
    */
    function logout()
    {
        $this->CI->session->unset_userdata('admin');
        //force expire the cookie
        $this->generateCookie('[]', time()-3600);
    }
    
    function logout_user()
    {
        $this->CI->session->unset_userdata('user');
        //force expire the cookie
        $this->generateCookie('[]', time()-3600);
    }

    /*
    This function resets the admins password and usernames them a copy
    */
    function reset_password($username)
    {
        $admin = $this->get_admin_by_username($username);
        if ($admin)
        {
            $this->CI->load->helper('string');
            $this->CI->load->library('email');
            
            $new_password       = random_string('alnum', 8);
            $admin['password']  = sha1($new_password);
            $this->save_admin($admin);
            
            $this->CI->email->from(config_item('email'), config_item('site_name'));
            $this->CI->email->to($admin['email']);
            $this->CI->email->subject(config_item('site_name').': Admin Password Reset');
            $this->CI->email->message('Your password has been reset to '. $new_password .'.');
            $this->CI->email->send();
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    /*
    This function gets the admin by their username address and returns the values in an array
    it is not intended to be called outside this class
    */
    private function get_admin_by_username($username)
    {
        $this->CI->db->select('*');
        $this->CI->db->where('username', $username);
        $this->CI->db->limit(1);
        $result = $this->CI->db->get('admin');
        $result = $result->row_array();

        if (sizeof($result) > 0)
        {
            return $result; 
        }
        else
        {
            return false;
        }
    }
    
    
    /*
    This function takes admin array and inserts/updates it to the database
    */
    function save($admin)
    {
        if ($admin['id'])
        {
            $this->CI->db->where('id', $admin['id']);
            $this->CI->db->update('admin', $admin);
        }
        else
        {
            $this->CI->db->insert('admin', $admin);
        }
    }
    
    
    /*
    This function gets a complete list of all admin
    */
    function get_admin_list()
    {
        $this->CI->db->select('*');
        $this->CI->db->order_by('lastname', 'ASC');
        $this->CI->db->order_by('firstname', 'ASC');
        $this->CI->db->order_by('email', 'ASC');
        $this->CI->db->order_by('username', 'ASC');
        $result = $this->CI->db->get('admin');
        $result = $result->result();
        
        return $result;
    }

    /*
    This function gets an individual admin
    */
    function get_admin($id)
    {
        $this->CI->db->select('*');
        $this->CI->db->where('id', $id);
        $result = $this->CI->db->get('admin');
        $result = $result->row();

        return $result;
    }       
    
    function check_id($str)
    {
        $this->CI->db->select('id');
        $this->CI->db->from('admin');
        $this->CI->db->where('id', $str);
        $count = $this->CI->db->count_all_results();
        
        if ($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }   
    }
    
    function check_username($str, $id=false)
    {
        $this->CI->db->select('username');
        $this->CI->db->from('admin');
        $this->CI->db->where('username', $str);
        if ($id)
        {
            $this->CI->db->where('id !=', $id);
        }
        $count = $this->CI->db->count_all_results();
        
        if ($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function delete($id)
    {
        if ($this->check_id($id))
        {
            $admin  = $this->get_admin($id);
            $this->CI->db->where('id', $id);
            $this->CI->db->limit(1);
            $this->CI->db->delete('admin');

            return $admin->firstname.' '.$admin->lastname.' has been removed.';
        }
        else
        {
            return 'The admin could not be found.';
        }
    }
}

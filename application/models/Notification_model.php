<?php
Class Notification_model extends CI_Model
{

    var $CI;

    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
        $this->load->library('session'); 
    }
  
	function notifications(){
		$result = $this->db->query('select * from (SELECT task_id as id,"cancel_request" as type,"Task cancel request" as title,CONCAT ("User send a task cancel request for Task: ",task_id ) as message,updated FROM cancel_tasks where is_seen = 0 union all SELECT payment_id as id,"payment" as type,"Task Payment" as title,CONCAT ("User made a payment for Task: ",task_id ) as message,updated  FROM payments where is_seen = 0 union all SELECT id as id,"complete_request" as type,"Task Completed" as title,CONCAT ("User marked task as completed for Task: ",id ) as message,updated  FROM maintenance_tasks where complete_request = 1) a order by updated desc');
		 return $result->result_array();
	
	}
	function update_notify($id,$field,$table)
	{
		$this->db->set('is_seen', '1');
		$this->db->where($field, $id);
		$this->db->update($table);
	
	}
	function update_notify_complete($id)
	{
		$this->db->set('complete_request', '0');
		$this->db->where('id', $id);
		$this->db->update('maintenance_tasks');
	
	}
           
}

<?php
Class Payment_model extends CI_Model
{

    var $CI;

    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
    
    function getList()
    {
		/* service type will be slug of category either home_maintenance,home_renovation */
		/* id is when task is in edit mode */
	
		$this->db->select('payments.*')->select('users.first_name,users.last_name,maintenance_tasks.title')->from('payments')
		->join('users', 'users.id = payments.user_id', 'LEFT')
		->join('maintenance_tasks', 'maintenance_tasks.id = payments.task_id', 'INNER')
		->order_by('payments.task_id','DESC');
		
		$query = $this->db->get();
		   
		return $query->result_array();
                    
    }
    
    function getDetail($id)
    {
		/* service type will be slug of category either home_maintenance,home_renovation */
		/* id is when task is in edit mode */
	
		$this->db->select('payments.*')->select('users.first_name,users.last_name,maintenance_tasks.title,maintenance_tasks.categ_parent_slug')->from('payments')
		->join('users', 'users.id = payments.user_id', 'LEFT')
		->join('maintenance_tasks', 'maintenance_tasks.id = payments.task_id', 'INNER');
		$this->db->where(array('payment_id'=>$id));
		
		$query = $this->db->get();
		   
		return $query->result_array();
                    
    }
    
    function getTotal($type)
    {
		 /* service type will be slug of category either home_maintenance,home_renovation those are fixed */
		/* $type would be 'Y' and 'M' */
		/* Y for current year M for current month*/
		$where =' AND payment_status ="'.$type.'"';
		
		if($type=='Y')
		{
		  $result = $this->db->query('SELECT * FROM payments WHERE YEAR(updated) = YEAR(CURDATE())'.$where);
		}
		else
		{
			$result = $this->db->query('SELECT * FROM payments WHERE MONTH(updated) = MONTH(CURDATE())'.$where);		
		}			
	    return count($result->result_array());
			
                      
    }
    

	
	 public function insertCredit($data = array())
	 {
        $insert = $this->db->insert('credits',$data);
        
        if($data['payment_status']=='Pending')
        {
			 $result = $this->db->get_where('users', array('id'=>$data['user_id']));

             $res=$result->row_array();


			$this->db->where('id', $data['user_id']);
			$this->db->update('users', array('credit'=>$data['payment_gross']+$res['credit']));
			
			unset($this->session->userdata['user']['credit']);
	        $this->session->userdata['user']['credit']=$data['payment_gross']+$res['credit'];
	    
			return true;				
		}
		else
		{
			return false;
		}
        
        //return $insert?true:false;
    }
    
    
	
    
}

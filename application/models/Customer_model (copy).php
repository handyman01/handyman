<?php
Class Customer_model extends CI_Model
{

    var $CI;

    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
        $this->load->library('session'); 
    }
  
     function email_from_id($id,$table){
		$this->db->select('email');
		$this->db->from($table);
		$this->db->where('id',$id);
		return $this->db->get()->row()->email;
	}
	
	

    function save_customer($data)
    {
        // update or insert
        if(!empty($data))
        {
			$this->db->insert('users',$data);
			$insert_id = $this->db->insert_id();
			return $insert_id;

        } else {
            
            return false;
        }
    }
 
    function save_handyman($data,$user_areas)
    {
        // update or insert
        if(!empty($data))
        {
			
			$this->db->insert('handyman',$data);
			$insert_id = $this->db->insert_id();
			
			$arr['handyman_id']=$insert_id;
			if(!empty($user_areas))
			{
				foreach($user_areas as $val)
				{
					
					$arr['categ_id']=$val;
					$this->db->insert('handyman_areas',$arr);
				}
			}
			return $insert_id;

        } else {
            
            return false;
        }
    }
    
    function get_customers($user_type=false)
    {
        $this->db->order_by('created', 'DESC');
        $result = $this->db->get_where('users', array('user_type'=>$user_type));
        return $result->result_array();
    }
    
    function get_FilteredCustomers($filter_type,$user_type)
    {
		if($user_type=='user')
		{
			$arr=array('user_type !='=>0);
		    if($filter_type=='active')
			{
				$arr['status']=1;
			}
		    else if($filter_type=='inactive')
			{
				$arr['status']=0;
			}
			else if($filter_type=='online')
			{
				$arr['is_login']='Y';
			}
			else if($filter_type=='new')
			{
				$result = $this->db->query('SELECT * FROM users WHERE MONTH(created) = MONTH(CURDATE())');			
			    return $result->result_array();
				
			}
			else
			{

			}
			$this->db->order_by('created', 'DESC');
			$result = $this->db->get_where('users', $arr);
			//echo $this->db->last_query();
			
			return $result->result_array();
        
		}
		else
		{
			$arr=array();
			if($filter_type=='active')
			{
				$arr['status']=1;
			}
			else if($filter_type=='inactive')
			{
				$arr['status']=0;
			}
			else if($filter_type=='online')
			{
				$arr['is_login']='Y';
			}
			else if($filter_type=='new')
			{
				$result = $this->db->query('SELECT * FROM handyman WHERE MONTH(created) = MONTH(CURDATE())');			
			    return $result->result_array();
				
			}
			else
			{

			}
			$this->db->order_by('created', 'DESC');
			$result = $this->db->get_where('handyman', $arr);
			return $result->result_array();
			
		}
		


    }
 
    function get_handyman()
    {
        $this->db->order_by('created', 'DESC');
        $result = $this->db->get('handyman');
        return $result->result_array();
    }
       
        
    function delete_customer($id,$user_type)
    {
        $this->db->where(array('id'=>$id,'user_type'=>$user_type))->delete('users');
        return true;
    }
    
    function delete_handyman($id)
    {
        $this->db->where(array('id'=>$id))->delete('handyman');
        return true;
    }
 
    function get_customer_detail($id)
    {
        $result = $this->db->get_where('users', array('id'=>$id));
        $res=$result->row_array();
        
        $result_reviews = $this->db->get_where('reviews', array('user_id'=>$id,'user_type'=>'handyman'));
        $res_reviews=$result_reviews->result_array();
        $res['reviews']=$res_reviews;
   
        if(!empty($res['reviews']))
        {
		   	foreach($res['reviews'] as $k=>$val)
		   	{
				$res['reviews'][$k]['detail']= $this->getTasks($val['task_id']);
			
			}
		} 
		
        if(count($res)>0)
        {
			return $res;
		}
		else
		{
			return false;
		}
    }
 
     function getTasks($service_type=false,$id=false)
    {
		/* service type will be slug of category either home_maintenance,home_renovation */
		/* id is when task is in edit mode */
		
		$this->db->select('maintenance_tasks.*')->select('users.first_name,users.last_name,handyman.name,categories.name as category_name,COUNT(applied_tasks.id) AS response_count')->from('maintenance_tasks')
                ->join('users', 'users.id = maintenance_tasks.user_id', 'LEFT')
                ->join('handyman', 'handyman.id = maintenance_tasks.handyman_id', 'LEFT')
                ->join('categories', 'categories.id = maintenance_tasks.categ_id', 'LEFT')
                ->join('applied_tasks', 'applied_tasks.task_id = maintenance_tasks.id', 'LEFT')
                ->group_by('maintenance_tasks.id')->order_by('maintenance_tasks.id','DESC');
                if($service_type)
                {
                  $this->db->where('maintenance_tasks.categ_parent_slug',$service_type);
			    }
                if($id)
                {
                  $this->db->where('maintenance_tasks.id',$id);
			    }
                $query = $this->db->get();
               
        return $query->result_array();
                    
    }
       
     function get_category_detail($id)
    {
        $result = $this->db->get_where('categories', array('id'=>$id));
        $res=$result->row_array();
        if(count($res>0))
        {
			return $res;
		}
		else
		{
			return false;
		}
    }
    
    
    function get_handyman_detail($id=false,$task_id=false)
    {
        $result = $this->db->get_where('handyman', array('id'=>$id));
        $res=$result->row_array();

        $result_area = $this->db->get_where('handyman_areas', array('handyman_id'=>$id));
        $res_area=$result_area->result_array();
        $res['work_area']=$res_area;
   
        if(!empty($res['work_area']))
        {
		   	foreach($res['work_area'] as $k=>$val)
		   	{
				$res['work_area'][$k]['detail']= $this->get_category_detail($val['categ_id']);
			
			}
		} 
		
        if(count($res)>0)
        {
			 $this->db->select('*')->from('offers');
			if($id)
			{
			   $this->db->where('offers.handyman_id',$id);
			}
			if($task_id)
			{
			   $this->db->where('offers.task_id',$task_id);
			}
			
			$result1 = $this->db->get();
			
			if($id && $task_id)
			{
				 $res['offers']=$result1->row_array();
			}
			else
			{
				 $res['offers']=$result1->result_array();
			}
        
			return $res;
		}
		else
		{
			return false;
		}
    }
    function admin_hire_handyman($handyman_id=false,$task_id=false)
    {
		 $return_arr = array();
		 $this->db->select('*')->from('offers');
	     $this->db->where('offers.handyman_id',$handyman_id);
	     $this->db->where('offers.task_id',$task_id);
	     $result1 = $this->db->get();
	     $res=$result1->row_array();
	     if(!empty($res))
	     {
		     $return_arr['final_price']=$return_arr['accepted_offer']=$res['price'];		 
		 }
		 else
		 {
				$this->db->select('maintenance_tasks.total_price')->from('maintenance_tasks');
				$this->db->where('maintenance_tasks.id',$task_id);
				$result2 = $this->db->get();
				$res2=$result2->row_array();
				$return_arr['final_price']=$res2['total_price'];
				$return_arr['accepted_offer'] =0;
				// check if already applied
				 $this->db->select('*')->from('applied_tasks');
				 $this->db->where('applied_tasks.user_id',$handyman_id);
				 $this->db->where('applied_tasks.task_id',$task_id);
				 $result3 = $this->db->get();
				 $res3=$result3->row_array();
				 if(empty($res3))
				 {
					 $data['task_id'] = $task_id;
					 $data['user_id'] = $handyman_id;
					 $this->db->insert('applied_tasks',$data);
					 	 
				 }
		}
		return $return_arr;
	
	}
    function hire_handyman($handyman_id=false,$task_id=false)
    {
	    //update status as processing , offer price if handyman offered any price , final_price	
	     $post['status']='P';
	     $post['handyman_id']=$handyman_id;
	     $this->db->select('*')->from('offers');
	     $this->db->where('offers.handyman_id',$handyman_id);
	     $this->db->where('offers.task_id',$task_id);
	     $result1 = $this->db->get();
	     $res=$result1->row_array();
	     if(!empty($res))
	     {
		     $post['final_price']=$post['accepted_offer']=$res['price'];		 
		 }
		 else
		 {
				$this->db->select('maintenance_tasks.total_price')->from('maintenance_tasks');
				$this->db->where('maintenance_tasks.id',$task_id);
	     	    $result2 = $this->db->get();
	            $res2=$result2->row_array();
			    $post['final_price']=$res2['total_price'];
		 }
		
	     
	    $this->db->where('task_id', $task_id);
		$this->db->update('applied_tasks', array('status'=>'X'));
		
	    $this->db->where(array('task_id'=>$task_id,'user_id'=>$handyman_id));
		$this->db->update('applied_tasks', array('status'=>'S'));
		
	    $this->db->where('id', $task_id);
		$this->db->update('maintenance_tasks', $post);
		
		//echo $this->db->last_query();die;		
	    return true;
	}
    
    function update_email_other_table($before_email,$new_email,$table){
		 $this->db->select('id')->from($table);
	     $this->db->where('email',$before_email);
		 $result1 = $this->db->get();
		 $res=$result1->row_array();
	     if(!empty($res))
	     {
			$this->db->where('id', $res['id']);
			$this->db->update($table, array('email'=>$new_email));
		 }
		
	
	}
	function update_customer($post)
	{
          if($this->session->userdata('user'))
	      {
              $post['id']=$this->session->userdata('user')['id'];
		  }
		  
	      	$result = $this->db->get_where('users', array('id'=>$post['id']));
		    $res=$result->row_array();
		   if(!empty($res))
		   {
			   if(isset($post['password']))
			   {
				   if($res['password']!=$post['password'])
				   {
					   $post['password']=md5($post['password']);
				   }
				   else
				   {
					   unset($post['password']);
				   }
				}
				if($res['email'] != $post['email']){
					$this->update_email_other_table($res['email'],$post['email'],$post['other_table']);
					$this->session->userdata['user']['email']=$post['email'];
				} 
				unset($post['other_table']);
				$this->db->where('id', $post['id']);
				$this->db->update('users', $post);
				if($this->session->userdata('user'))
	            {
					unset($this->session->userdata['user']['firstname']);
					unset($this->session->userdata['user']['lastname']);
					unset($this->session->userdata['user']['email']);
					$this->session->userdata['user']['firstname']=$post['first_name'];
					$this->session->userdata['user']['lastname']=$post['last_name'];
					$this->session->userdata['user']['email']=$post['email'];
					//$this->session->userdata('user')['firstname']= 'dfgfdgdgh';
                }
       
				return true;
		   }
		   else
		   {
			   return false;
		   }
	}
	
	
	function login_info()
	{
		
		if($this->session->userdata('user')['user_type']=='user'){
			
			$info  = array('last_login'=>date('Y-m-d H:i:s'),'is_login'=>'Y');
			
			$this->db->where('id', $this->session->userdata('user')['id']);
			$this->db->update('users', $info);
			
		}
		
		else{
		
			
			$info  = array('last_login'=>date('Y-m-d H:i:s'),'is_login'=>'Y');
			$this->db->where('id', $this->session->userdata('user')['id']);
			$this->db->update('handyman', $info);
			
			
			
		}
		return true;
		
         
	}
	
	
	function logut_info()
	{
		
		if($this->session->userdata('user')['user_type']=='user'){
			
			$info  = array('is_login'=>'N');
			
			$this->db->where('id', $this->session->userdata('user')['id']);
			$this->db->update('users', $info);
			
		}
		
		else{
			
			
			$info  = array('is_login'=>'N');
			
			$this->db->where('id', $this->session->userdata('user')['id']);
			$this->db->update('handyman', $info);
			
		}
		return true;
		
         
	}
	
	
	
	function update_password($post)
	{
		
		$capital_letters = range("A", "Z");
		$lowcase_letters = range("a", "z");
		$numbers = range(0, 9);
		$all = array_merge($capital_letters, $lowcase_letters, $numbers);
		$count = count($all);
		$new_pass = "";
		for($i = 0; $i < 6; $i++)
		{
			$key = rand(0, $count);
			$new_pass .= $all[$key];
		}
		$new_encrypt_pass=md5($new_pass);
		$this->db->where('email', $post['email']);
		$this->db->update('users', array('password'=>$new_encrypt_pass));
		$this->db->where('email', $post['email']);
		$this->db->update('handyman', array('password'=>$new_encrypt_pass));
		return $new_pass;
		
	}
	
	function update_isLogin($val=false,$table=false,$user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->update($table, array('is_login'=>$val));
		return true;
	}
	
	function update_handyman($post)
	{
		
		  if($this->session->userdata('user'))
	      {
              $post['id']=$this->session->userdata('user')['id'];
		  }
		  
		   $result = $this->db->get_where('handyman', array('id'=>$post['id']));
		   $res=$result->row_array();
		   if(!empty($res))
		   {
			   if(isset($post['password']))
			   {
				   if($res['password']!=$post['password'])
				   {
					   $post['password']=md5($post['password']);
				   }
				   else
				   {
					   unset($post['password']);
				   }
			   }
			   if($res['email'] != $post['email']){
					$this->update_email_other_table($res['email'],$post['email'],$post['other_table']);
					$this->session->userdata['user']['email']=$post['email'];
				} 
			   unset($post['other_table']);
			   $update_arr['name']=$post['name'];
			   $update_arr['email']=$post['email'];
			   $update_arr['phone']=$post['phone'];
			   $update_arr['company']=$post['company'];
			   $update_arr['address']=$post['address'];
			   $update_arr['about_urself']=$post['about_urself'];
			   $update_arr['education']=$post['education'];
			   $update_arr['speciality']=$post['speciality'];
			   $update_arr['languages']=$post['languages'];
			   $update_arr['workings']=$post['workings'];
			   
			   $update_arr['id']=$post['id'];
			   if($post['image'] !='')
			   {
				   $update_arr['image']=$post['image'];
			   }
				
				$this->db->where('id', $post['id']);
				$this->db->update('handyman', $update_arr);
				
				$arr['handyman_id']=$post['id'];
				
				if(!empty($post['subservices']))
				{
					//delete prev area of work
					 $this->db->where(array('handyman_id'=>$arr['handyman_id']))->delete('handyman_areas');
                    foreach($post['subservices'] as $val)
					{						
						$arr['categ_id']=$val;
						$this->db->insert('handyman_areas',$arr);
					}
				}
			
			    if($this->session->userdata('user'))
	            {
					unset($this->session->userdata['user']['firstname']);
					unset($this->session->userdata['user']['email']);
					$this->session->userdata['user']['firstname']=$post['name'];
					$this->session->userdata['user']['email']=$post['email'];
				}
				return true;
		   }
		   else
		   {
			   return false;
		   }
	}
    
    function change_status_customer($id,$status,$user_type)
	{
				
				$this->db->where(array('id'=>$id,'user_type'=>$user_type));
				$this->db->update('users', array('status'=>$status));
				return true;
		
	}
    function change_status_handyman($id,$status,$user_type)
	{
				
				$this->db->where(array('id'=>$id));
				$this->db->update('handyman', array('status'=>$status));
				return true;
		
	}
	
    function validate_member($email)
    {
        $result = $this->db->get_where('users', array('email'=>$email));
        $res=$result->row_array();
        if(count($res)>0)
        {
			return false;
		}
		else
		{
		         $result1 = $this->db->get_where('handyman', array('email'=>$email));
                 $res1=$result1->row_array();
                 if(count($res1)>0)
                 {
					 return false;
				 }
				 else
				 {
					  return true;
				 }        			   
		}
    }
    
    function validate_handyman($email)
    {
 
		         $result1 = $this->db->get_where('handyman', array('email'=>$email));
                 $res1=$result1->row_array();
                 if(count($res1)>0)
                 {
					 return false;
				 }
				 else
				 {
					  return true;
				 }        			   
		
    }
    
    function validate_customer($email)
    {
 
		         $result1 = $this->db->get_where('users', array('email'=>$email));
                 $res1=$result1->row_array();
                 if(count($res1)>0)
                 {
					 return false;
				 }
				 else
				 {
					  return true;
				 }        			   
		
    }
    
    function change_password($data)
    {
		if($this->session->userdata('user')['user_type']=='user')
		{
				 $result1 = $this->db->get_where('users', array('id'=>$this->session->userdata('user')['id'],'password'=>$data['old_password']));
                 $res1=$result1->row_array();
                 if(count($res1)>0)
                 {
					$this->db->where(array('id'=>$this->session->userdata('user')['id']));
					$this->db->update('users', array('password'=>$data['password']));
					return true;
			
				 }
				 else
				 {
					 return false;
				 }

		}	

		else
		{
				 $result1 = $this->db->get_where('handyman', array('id'=>$this->session->userdata('user')['id'],'password'=>$data['old_password']));
                 $res1=$result1->row_array();
                 if(count($res1)>0)
                 {
					$this->db->where(array('id'=>$this->session->userdata('user')['id']));
					$this->db->update('handyman', array('password'=>$data['password']));
					return true;				 
				 }
				 else
				 {
					 return false;
				 }					 
		}
		
	}
    
    
    function get_totalUsers($user_type)
    {
		if($user_type=='user')
		{
			$result = $this->db->get_where('users', array('user_type !='=>0));
			
			return count($result->result_array());
			
		}
		else
		{
			$result = $this->db->get_where('handyman', array());
			return count($result->result_array());
		}

    }
    
    function get_totalActiveUsers($user_type)
    {
		if($user_type=='user')
		{
			$result = $this->db->get_where('users', array('status'=>1,'user_type !='=>0));
			
			return count($result->result_array());
			
		}
		else
		{
			$result = $this->db->get_where('handyman', array('status'=>1));
			return count($result->result_array());
		}

    }
    
    function get_totalInactiveUsers($user_type)
    {
		if($user_type=='user')
		{
			$result = $this->db->get_where('users', array('status'=>0,'user_type !='=>0));
			
			return count($result->result_array());
			
		}
		else
		{
			$result = $this->db->get_where('handyman', array('status'=>1));
			return count($result->result_array());
		}

    }
    
    function get_onlineUsers($user_type)
    {
		if($user_type=='user')
		{
			$result = $this->db->get_where('users', array('is_login'=>'Y','user_type !='=>0));
			
			return count($result->result_array());
			
		}
		else
		{
			$result = $this->db->get_where('handyman', array('is_login'=>'Y'));
			return count($result->result_array());
		}

    }
    
    function get_newUsers($user_type)
    {
		if($user_type=='user')
		{
			$result = $this->db->query('SELECT * FROM users WHERE MONTH(created) = MONTH(CURDATE())');			
			return count($result->result_array());
			
		}
		else
		{
			$result = $this->db->query('SELECT * FROM handyman WHERE MONTH(created) = MONTH(CURDATE())');
			return count($result->result_array());
		}

    }
    
    
    function is_email_exist($arr_param=false)
    {
		
		if($arr_param['user_type']=='user')
		{
          $result = $this->db->get_where('users', array('email'=>$arr_param['email']));
	    }
	    else
	    {
		   $result = $this->db->get_where('handyman', array('email'=>$arr_param['email']));
		 }
		
        $res=$result->row_array();
       
        if(count($res)>0)
        {
			return $res;
		}
		else
		{
			return false;			      			 
		}
    }
           
}

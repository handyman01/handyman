<?php
Class Service_model extends CI_Model
{
    var $CI;
    function __construct()
    {
        parent::__construct();
        $this->CI = &get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
    
    
    function save_customer($data)
    {
        // update or insert
        if(!empty($data))
        {
			$this->db->insert('users',$data);
			return true;

        } else {
            
            return false;
        }
    }
    
    function get_customers($user_type)
    {
        $this->db->order_by('created', 'DESC');
        $result = $this->db->get_where('users', array('user_type'=>$user_type));
        return $result->result_array();
    }
    
        
    function delete_customer($id,$user_type)
    {
        $this->db->where(array('id'=>$id,'user_type'=>$user_type))->delete('users');
        return true;
    }
 
     function get_customer_detail($id)
    {
        $result = $this->db->get_where('users', array('id'=>$id));
        $res=$result->row_array();
        if(count($res)>0)
        {
			return $res;
		}
		else
		{
			return false;
		}
    }
    
    
	function update_customer($post)
	{
		   $result = $this->db->get_where('users', array('id'=>$post['id']));
		   $res=$result->row_array();
		   if(!empty($res))
		   {
			   if($res['password']!=$post['password'])
			   {
				   $post['password']=md5($post['password']);
			   }
			   else
			   {
				   unset($post['password']);
			   }
				
				$this->db->where('id', $post['id']);
				$this->db->update('users', $post);
				return true;
		   }
		   else
		   {
			   return false;
		   }
	}
    
    function change_status_customer($id,$status,$user_type)
	{
				
				$this->db->where(array('id'=>$id,'user_type'=>$user_type));
				$this->db->update('users', array('status'=>$status));
				return true;
		
	}
	
    function check_service($slug)
    {
        $result = $this->db->get_where('categories', array('slug'=>$slug));
        $res=$result->row_array();
        if(count($res)>0)
        {
			$arr_resp=array();
            $result1 = $this->db->get_where('categories', array('parent_id'=>$res['id']));
            $result2 = $this->db->get_where('categories', array('parent_id'=>$res['id'],'is_popular'=>1));
            $arr_resp['allServices']=$result1->result_array();
            $arr_resp['popularServices']=$result2->result_array();
            return $arr_resp;        			
		}
		else
		{
			return false;
		}
    }
    
    
}

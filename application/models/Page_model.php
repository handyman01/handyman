<?php
Class Page_model extends CI_Model
{

    var $CI;

    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
	function footer_data(){
	$data = array();
	$data['links'] = $this->getListing('front');
	return $data;
	
	}
    function getListing($front= false)
    {
		/* service type will be slug of category either home_maintenance,home_renovation */
		/* id is when task is in edit mode */
		if($front =='front'){
			$this->db->select('name,slug')->from('pages')
			->order_by('pages.name','ASC');
		}else{
			$this->db->select('*')->from('pages')
			->order_by('pages.id','DESC');
		} 
		$query = $this->db->get();
		return $query->result_array();
                    
    }
    function save($data)
    {
        // update or insert
        if(!empty($data))
        {
			$this->db->insert('pages',$data);
			return true;

        } else {
            
            return false;
        }
    }
    
    function change_status($id,$status)
	{
				
		$this->db->where(array('id'=>$id));
		$this->db->update('pages', array('enabled'=>$status));
		return true;
		
	}
     function get_page_detail($id)
    {
        $result = $this->db->get_where('pages', array('id'=>$id));
        $res=$result->row_array();
        if(count($res>0))
        {
			return $res;
		}
		else
		{
			return false;
		}
    }
    function update($post)
	{
		   $result = $this->db->get_where('pages', array('id'=>$post['id']));
		   $res=$result->row_array();
		   if(!empty($res))
		   {

				$this->db->where('id', $post['id']);
				$this->db->update('pages', $post);
				return true;
		   }
		   else
		   {
			   return false;
		   }
	}
    function delete($id)
    {
        $this->db->where(array('id'=>$id))->delete('pages');
        return true;
    }
    function getDescription($slug){
		$result = $this->db->get_where('pages', array('slug'=>$slug));
        $res=$result->row_array();
        if(count($res>0))
        {
			return $res;
		}
		else
		{
			return false;
		}
	
	}
    
 
    
    
	
    
}

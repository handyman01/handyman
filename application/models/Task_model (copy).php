<?php
Class Task_model extends CI_Model
{

    var $CI;

    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
    function cancel_request($data){
	    // update or insert
        $result = $this->db->get_where('cancel_tasks',array('task_id'=>$data['task_id']));
		$res=$result->result_array();
		
		if(empty($res))
		{
		  $this->db->insert('cancel_tasks',$data);			
		  $id=$this->db->insert_id();
		  // update status
			$this->db->set('status', 'CR');
			$this->db->where('id', $data['task_id']);
			$this->db->update('maintenance_tasks');
		  return true;
		  
		}else{
			return false;
		}
		

    }
    
    function save_task($data)
    {
		
        // update or insert
        if(!empty($data))
        {
			$result = $this->db->get_where('maintenance_tasks',array('user_id'=>$data['user_id'],'title'=>$data['title'],'categ_id'=>$data['categ_id'],'categ_parent_slug'=>$data['categ_parent_slug'],'address_unit'=>$data['address_unit'],'address'=>$data['address'],'description'=>$data['description'],'task_date'=>$data['task_date'],'task_time'=>$data['task_time'],'estimated_time'=>$data['estimated_time']));
            $res=$result->result_array();
            
            if(!empty($res))
            {
				return 'redundant';
			}
			else
			{
			  $this->db->insert('maintenance_tasks',$data);			
			  $id=$this->db->insert_id();
			  
				$this->db->set('total_task', 'total_task+1', FALSE);
				$this->db->where('id', $data['user_id']);
				$this->db->update('users');
		     
			  return  $id;
		    }

        } else {
            
            return false;
        }
    }
    
    function save_offer($data)
    {
		
			$this->db->insert('offers',$data);	
			$offer_id=$this->db->insert_id();
			
			$this->db->select('applied_tasks.id')->from('applied_tasks');
		    $this->db->where(array('task_id'=>$data['task_id'],'user_id'=>$data['handyman_id']));
		    $query = $this->db->get();
		    $res=$query->row_array();
		    
		    //add offers in user
		    $this->db->set('total_offers', 'total_offers+1', FALSE);
			$this->db->where('id', $data['user_id']);
			$this->db->update('users');
			
		    //add offers in handyman
		    $this->db->set('total_offers', 'total_offers+1', FALSE);
			$this->db->where('id', $data['handyman_id']);
			$this->db->update('handyman');
			
		    //add offers in task
		    $this->db->set('total_offers', 'total_offers+1', FALSE);
			$this->db->where('id', $data['task_id']);
			$this->db->update('maintenance_tasks');
				
		    if(empty($res))
		    {
				$task_data['task_id']=$data['task_id'];
				$task_data['user_id']=$data['handyman_id'];
				$this->db->insert('applied_tasks',$task_data);	
				
				//update applied
				$this->db->set('total_applied', 'total_applied+1', FALSE);
				$this->db->where('id', $data['handyman_id']);
				$this->db->update('handyman');
				
			
			
			}

			//$this->db->where(array('id'=>$data['id']));
			//$this->db->update('applied_tasks', array(''));
					
			return  $offer_id;
	}
	
    function getUserTasks($arr_param=false)
    {
		
		$this->db->select('maintenance_tasks.*')->select('users.first_name,users.last_name,users.email as customer_email,handyman.name,categories.name as category_name')->from('maintenance_tasks')
                ->join('users', 'users.id = maintenance_tasks.user_id', 'LEFT')
                ->join('handyman', 'handyman.id = maintenance_tasks.handyman_id', 'LEFT')
                ->join('categories', 'categories.id = maintenance_tasks.categ_id', 'LEFT')
                ->group_by('maintenance_tasks.id')->order_by('maintenance_tasks.id','DESC');
                if(isset($arr_param['service_type']) && $arr_param['service_type']!='all')
                {
                  $this->db->where('maintenance_tasks.categ_parent_slug',$arr_param['service_type']);
			    }
                if(isset($arr_param['task_id']))
                {
                  $this->db->where('maintenance_tasks.id',$arr_param['task_id']);
			    }
                if(isset($arr_param['user_id']))
                {
                  $this->db->where('maintenance_tasks.user_id',$arr_param['user_id']);
			    }
			    
                $query = $this->db->get();
                return $query->result_array();
        
    }
    
    function getUserOffers($arr_post=false)
    {
		$this->db->select('offers.*')->from('offers');
		
		if(isset($arr_post['task_id']))
		{
			$this->db->where('task_id',$arr_post['task_id']);
		}
		if(isset($arr_post['handyman_id']))
		{
			$this->db->where('handyman_id',$arr_post['handyman_id']);
		}
		$query = $this->db->get();
		if(isset($arr_post['task_id']) && isset($arr_post['handyman_id']))
		{
		  return $query->row_array();
		}
		else
		{
			return $query->result_array();
		}
		

	}
    
    function getAssignedTasks($arr_param=false)
    {
		
		$this->db->select('maintenance_tasks.*')->select('users.first_name,users.last_name,users.email as customer_email,handyman.name,categories.name as category_name')->from('maintenance_tasks')
                ->join('users', 'users.id = maintenance_tasks.user_id', 'LEFT')
                ->join('handyman', 'handyman.id = maintenance_tasks.handyman_id', 'LEFT')
                ->join('categories', 'categories.id = maintenance_tasks.categ_id', 'LEFT')
                ->group_by('maintenance_tasks.id')->order_by('maintenance_tasks.id','DESC');
                if(isset($arr_param['service_type']) && $arr_param['service_type']!='all')
                {
                  $this->db->where('maintenance_tasks.categ_parent_slug',$arr_param['service_type']);
			    }
                if(isset($arr_param['task_id']))
                {
                  $this->db->where('maintenance_tasks.id',$arr_param['task_id']);
			    }
                if(isset($arr_param['user_id']))
                {
                  $this->db->where('maintenance_tasks.handyman_id',$arr_param['user_id']);
			    }
			    
                $query = $this->db->get();
                return $query->result_array();
        
    }
    function getFollowingTasks($arr_param=false)
    {
		
		$this->db->select('maintenance_tasks.*')->select('users.first_name,users.last_name,handyman.name,categories.name as category_name')->from('maintenance_tasks')
                ->join('users', 'users.id = maintenance_tasks.user_id', 'LEFT')
                ->join('handyman', 'handyman.id = maintenance_tasks.handyman_id', 'LEFT')
                ->join('categories', 'categories.id = maintenance_tasks.categ_id', 'LEFT')
                ->join('followers', 'followers.task_id = maintenance_tasks.id', 'LEFT')
                ->group_by('maintenance_tasks.id')->order_by('maintenance_tasks.id','DESC');
               

                if(isset($arr_param['handyman_id']))
                {
                  $this->db->where('followers.handyman_id',$arr_param['handyman_id']);
			    }
			    
                $query = $this->db->get();
                return $query->result_array();
        
    }
    
    public function insertTransaction($data = array()){
        $insert = $this->db->insert('payments',$data);
        
        if($data['payment_status']=='Pending')
        {
			$this->db->where(array('id'=>$data['task_id']));
			$this->db->update('maintenance_tasks', array('payment_status'=>1));
			return true;				
		}
        
        return $insert?true:false;
    }
    
    public function updateMyTask($data)
    {
        
			$this->db->where(array('id'=>$data['id']));
			$this->db->update('maintenance_tasks', $data);
            return true;		
        
    }
    
     public function getRows($id = ''){
        $this->db->select('id,address,total_price');
        $this->db->from('maintenance_tasks');
        if($id){
            $this->db->where('id',$id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('total_price','asc');
            $query = $this->db->get();
            $result = $query->result_array();
        }
        return !empty($result)?$result:false;
    }
    
    function getTasks($service_type=false,$id=false)
    {
		/* service type will be slug of category either home_maintenance,home_renovation */
		/* id is when task is in edit mode */
		
		$this->db->select('maintenance_tasks.*')->select('users.first_name,users.last_name,handyman.name,categories.name as category_name')->from('maintenance_tasks')
                ->join('users', 'users.id = maintenance_tasks.user_id', 'LEFT')
                ->join('handyman', 'handyman.id = maintenance_tasks.handyman_id', 'LEFT')
                ->join('categories', 'categories.id = maintenance_tasks.categ_id', 'LEFT')
                ->group_by('maintenance_tasks.id')->order_by('maintenance_tasks.id','DESC');
                if($service_type)
                {
                  $this->db->where('maintenance_tasks.categ_parent_slug',$service_type);
			    }
                if($id)
                {
                  $this->db->where('maintenance_tasks.id',$id);
			    }
                $query = $this->db->get();
               
        return $query->result_array();
                    
    }
    
    function getTasksResponses($arr_param=false)
    {

		
		$this->db->select('applied_tasks.*')->select('maintenance_tasks.title,maintenance_tasks.handyman_id as assigned_handyman,maintenance_tasks.lat,maintenance_tasks.lon,maintenance_tasks.address,maintenance_tasks.categ_id,maintenance_tasks.categ_parent_slug,maintenance_tasks.is_residential,maintenance_tasks.is_commercial,maintenance_tasks.address_unit,maintenance_tasks.description,maintenance_tasks.task_date,maintenance_tasks.task_time,task_date,maintenance_tasks.estimated_time,maintenance_tasks.total_price,maintenance_tasks.document,maintenance_tasks.status as task_status,maintenance_tasks.created as task_created,handyman.id as handy_id,handyman.name,categories.name as category_name,users.first_name,users.last_name,users.email as customer_email')->from('applied_tasks')
                ->join('maintenance_tasks', 'maintenance_tasks.id = applied_tasks.task_id', 'LEFT')
                ->join('handyman', 'handyman.id = applied_tasks.user_id', 'LEFT')
                ->join('users', 'users.id = maintenance_tasks.user_id', 'LEFT')
                ->join('categories', 'categories.id = maintenance_tasks.categ_id', 'LEFT');
                
              
                if(isset($arr_param['user_id']))
                {
                  $this->db->where('applied_tasks.user_id',$arr_param['user_id']);
			    }
                if(isset($arr_param['task_id']))
                {
                  $this->db->where('applied_tasks.task_id',$arr_param['task_id']);
			    }
                
			    
                $query = $this->db->get();
                //echo $this->db->last_query();die;
                return $query->result_array();
        
    
	}
	
    function getTasksByCateg($service_type=false,$categ_id=false)
    {
		/* service type will be slug of category either home_maintenance,home_renovation */
		/* id is when task is in edit mode */
		
		$this->db->select('maintenance_tasks.*')->select('users.first_name,users.last_name,handyman.name,users.email as customer_email,categories.name as category_name')->from('maintenance_tasks')
                ->join('users', 'users.id = maintenance_tasks.user_id', 'LEFT')
                ->join('handyman', 'handyman.id = maintenance_tasks.handyman_id', 'LEFT')
                ->join('categories', 'categories.id = maintenance_tasks.categ_id', 'LEFT')
                ->group_by('maintenance_tasks.id')->order_by('maintenance_tasks.id','DESC');
                if($service_type)
                {
                  $this->db->where('maintenance_tasks.categ_parent_slug',$service_type);
			    }
                if($categ_id)
                {
                  $this->db->where('maintenance_tasks.categ_id',$categ_id);
			    }
			    if($service_type=='home_renovation')
			    {
					$this->db->where('maintenance_tasks.is_posted',1);
				}
				//get all task with some status
				$this->db->where('maintenance_tasks.status','S');
                $query = $this->db->get();
               	
        return $query->result_array();
                    
    }
    
    function getFilteredTasks($service_type=false,$type,$flag)
    {
        /* service type will be slug of category either home_maintenance,home_renovation */
        /* flag is Y or M (yealy or monthly )*/
        
		$this->db->select('maintenance_tasks.*')->select('users.first_name,users.last_name,handyman.name,categories.name as category_name')->from('maintenance_tasks')
                ->join('users', 'users.id = maintenance_tasks.user_id', 'LEFT')
                ->join('handyman', 'handyman.id = maintenance_tasks.handyman_id', 'LEFT')
                ->join('categories', 'categories.id = maintenance_tasks.categ_id', 'LEFT')
                ->group_by('maintenance_tasks.id')->order_by('maintenance_tasks.id','DESC');
                
                if($service_type)
                {
                  $this->db->where('maintenance_tasks.categ_parent_slug',$service_type);
			    }
			    
                if($flag=='Y')
                {
					$this->db->where('YEAR(maintenance_tasks.created) = YEAR(CURDATE())');
				
				}
				else
				{
				   $this->db->where('MONTH(maintenance_tasks.created) = MONTH(CURDATE())');
				}
                 if($type=='completed')
				{
                  $this->db->where('maintenance_tasks.status','C');
				}
				else if($type=='active')
				{
                  $this->db->where('maintenance_tasks.status','P');
				}
		
                $query = $this->db->get();
                //echo $this->db->last_query();
                //die;
               return $query->result_array();
                    
    }
    
    function getTotalSubmitted($service_type=false,$type)
    {
		 /* service type will be slug of category either home_maintenance,home_renovation those are fixed */
		/* $type would be 'Y' and 'M' */
		/* Y for current year M for current month*/
		$where='';
		if($service_type)
		{
			$where =' AND categ_parent_slug ="'.$service_type.'"';
		}
		if($type=='Y')
		{
		  $result = $this->db->query('SELECT * FROM maintenance_tasks WHERE YEAR(created) = YEAR(CURDATE())'.$where);
		}
		else
		{
			$result = $this->db->query('SELECT * FROM maintenance_tasks WHERE MONTH(created) = MONTH(CURDATE())'.$where);		
		}			
	    return count($result->result_array());
			
                      
    }
    
    function getTotalCompleted($service_type=false,$type)
    {
	    $where='';
		if($service_type)
		{
			$where =' AND categ_parent_slug ="'.$service_type.'"';
		}
		
		if($type=='Y')
		{
		  $result = $this->db->query("SELECT * FROM maintenance_tasks WHERE YEAR(created) = YEAR(CURDATE()) and status='C' ".$where);
		}
		else
		{
			$result = $this->db->query("SELECT * FROM maintenance_tasks WHERE MONTH(created) = MONTH(CURDATE()) and status='C'".$where);		
		}		

        return count($result->result_array());
                    
    }
    
    function getTotalActive($service_type=false,$type)
    {
		$where='';
		if($service_type)
		{
			$where =' AND categ_parent_slug ="'.$service_type.'"';
		}
		
		if($type=='Y')
		{
		  $result = $this->db->query('SELECT * FROM maintenance_tasks WHERE YEAR(created) = YEAR(CURDATE()) and status="P" '.$where);
		}
		else
		{
			$result = $this->db->query('SELECT * FROM maintenance_tasks WHERE MONTH(created) = MONTH(CURDATE()) and status="P"'.$where);		
		}		

        return count($result->result_array());
                    
    }
    
    function getTotalTaskAmount($service_type=false,$type)
    {
		$where='';
		if($service_type)
		{
			$where =' AND categ_parent_slug ="'.$service_type.'"';
		}
		
		
		if($type=='Y')
		{
		  $result = $this->db->query('SELECT sum(total_price) AS total_price FROM maintenance_tasks WHERE YEAR(created) = YEAR(CURDATE()) and payment_status=1 '.$where);
		}
		else
		{
			$result = $this->db->query('SELECT sum(total_price) AS total_price  FROM maintenance_tasks WHERE MONTH(created) = MONTH(CURDATE()) and payment_status=1 '.$where);		
		}	
		
		
       return $result->row_array();
      
                    
    }
    
    
    function update_task($data){
		
			$this->db->where(array('id'=>$data['id']));
			$this->db->update('maintenance_tasks', $data);
			return true;	
			
	}

    function getTasksDateFilter($arr_param)
    {
				/* service type will be slug of category either home_maintenance,home_renovation */
		/* id is when task is in edit mode */
		
		$this->db->select('maintenance_tasks.*')->select('users.first_name,users.last_name,handyman.name,categories.name as category_name')->from('maintenance_tasks')
                ->join('users', 'users.id = maintenance_tasks.user_id', 'LEFT')
                ->join('handyman', 'handyman.id = maintenance_tasks.handyman_id', 'LEFT')
                ->join('categories', 'categories.id = maintenance_tasks.categ_id', 'LEFT')
                ->group_by('maintenance_tasks.id')->order_by('maintenance_tasks.id','DESC');

                  $this->db->where('maintenance_tasks.categ_parent_slug',$arr_param['service_type']);
				  $this->db->where('maintenance_tasks.task_date >=', $arr_param['start_date']);
				  $this->db->where('maintenance_tasks.task_date <=', $arr_param['end_date']);
              
                  $query = $this->db->get();
                 
                  return $query->result_array();
        
		
	}
	
	
	function getTotalSubmittedDateFilter($arr_param=false)
    {
		 /* service type will be slug of category either home_maintenance,home_renovation those are fixed */
		/* $type would be 'Y' and 'M' */
		/* Y for current year M for current month*/
	
			$where =' categ_parent_slug ="'.$arr_param['service_type'].'" AND maintenance_tasks.task_date >="'. $arr_param['start_date'].'" AND maintenance_tasks.task_date <="'.$arr_param['end_date'].'"';
		
		
		  $result = $this->db->query('SELECT * FROM maintenance_tasks WHERE '.$where);
				
	    return count($result->result_array());
			
                      
    }
    
    function getTotalCompletedDateFilter($arr_param=false)
    {
						$where =' categ_parent_slug ="'.$arr_param['service_type'].'" AND maintenance_tasks.task_date >="'. $arr_param['start_date'].'" AND maintenance_tasks.task_date <="'.$arr_param['end_date'].'"';
		
		
		  $result = $this->db->query("SELECT * FROM maintenance_tasks WHERE status='C' and ".$where);
			

        return count($result->result_array());
                    
    }
    
    function getTotalActiveDateFilter($arr_param=false)
    {
	      			$where =' categ_parent_slug ="'.$arr_param['service_type'].'" AND maintenance_tasks.task_date >="'. $arr_param['start_date'].'" AND maintenance_tasks.task_date <="'.$arr_param['end_date'].'"';
		
		
		  $result = $this->db->query('SELECT * FROM maintenance_tasks WHERE status="P" and '.$where);
			

        return count($result->result_array());
                    
    }
    
    function getTotalTaskAmountDateFilter($arr_param=false)
    {
	
	   			$where =' categ_parent_slug ="'.$arr_param['service_type'].'" AND maintenance_tasks.task_date >="'. $arr_param['start_date'].'" AND maintenance_tasks.task_date <="'.$arr_param['end_date'].'"';
		
		
		  $result = $this->db->query('SELECT sum(total_price) AS total_price FROM maintenance_tasks WHERE payment_status=1 and '.$where);
	
		
		
       return $result->row_array();
      
                    
    }
    
    function applyTask($data)
    {
		$this->db->insert('applied_tasks',$data);			
	    $idd=$this->db->insert_id();
		
		//update applied
		$this->db->set('total_applied', 'total_applied+1', FALSE);
		$this->db->where('id', $data['user_id']);
		$this->db->update('handyman');
		return $idd;
				
	}
	
    function getAppliedTasks($user_id)
    {
		
		$this->db->select('task_id,status');
        $this->db->from('applied_tasks');
        $this->db->where('user_id',$user_id);
        $query = $this->db->get();
        return $query->result_array();

	}
    
    function post_task_status($id,$status)
	{
				
				$this->db->where(array('id'=>$id));
				$this->db->update('maintenance_tasks', array('is_posted'=>$status));
				return true;
		
	}
	
	
	function getFollowingTask($arr_post=false)
    {
		$this->db->select('followers.*')->from('followers');
		
		if(isset($arr_post['task_id']))
		{
			$this->db->where('task_id',$arr_post['task_id']);
		}
		if(isset($arr_post['handyman_id']))
		{
			$this->db->where('handyman_id',$arr_post['handyman_id']);
		}
		$query = $this->db->get();
		if(isset($arr_post['task_id']) && isset($arr_post['handyman_id']))
		{
		  return $query->row_array();
		}
		else
		{
			return $query->result_array();
		}
		

	}
	
	
	function follow_task($data=false)
	{
		    $this->db->select('followers.*')->from('followers');
		    $this->db->where('task_id',$data['task_id']);
		    $this->db->where('handyman_id',$data['handyman_id']);
		     $query = $this->db->get();
		    if(!empty($query->row_array()))
		    {
				//delete from table 
				$this->db->where(array('task_id'=>$data['task_id'],'handyman_id'=>$data['handyman_id']))->delete('followers');
				return 'unfollowed';
			}
			else
			{
			    $this->db->insert('followers',$data);	
			    $id=$this->db->insert_id();
			    return 'followed';
			
			}
			

	}
	
    
}

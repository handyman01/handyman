<?php
Class Category_model extends CI_Model
{

    var $CI;

    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
    
      function get_hourly_price($id){
		
		$this->db->select('hourly_price,deposit_hrs');
		$this->db->from('categories');
		$this->db->where('id', $id );
		$result = $this->db->get();
		$res=$result->row_array();
        if(count($res>0))
        {
			
			return $res;
		}
		else
		{
			return false;
		}


    $query = $this->db->get();
		
		$result = $this->db->get_where('categories', array('id'=>$id));
        $res=$result->row_array();
        if(count($res)>0)
        {
			return $res;
		}
		else
		{
			return false;
		}
	}
    function save($data)
    {
        // update or insert
        if(!empty($data))
        {
			$this->db->insert('categories',$data);
			return true;

        } else {
            
            return false;
        }
    }
    
    function get_categories($parent_id=false,$slug=false)
    {
		
		$arr_condition=array();
		if($parent_id && $parent_id!='')
		{
			$arr_condition['parent_id']=$parent_id;
		}
		
		if($slug && $slug!='')
		{
			$arr_condition['slug']=$slug;
		}
        $this->db->order_by('created', 'DESC');
        $result = $this->db->get_where('categories',$arr_condition);
          //    echo $this->db->last_query();

        return $result->result_array();
    }
    

        
    function delete($id)
    {
        $this->db->where(array('id'=>$id))->delete('categories');
        return true;
    }
 
     function get_category_detail($id)
    {
        $result = $this->db->get_where('categories', array('id'=>$id));
        $res=$result->row_array();
        if(count($res>0))
        {
			return $res;
		}
		else
		{
			return false;
		}
    }
    
     function get_categories_bySlug($slug)
    {
        $result = $this->db->get_where('categories', array('slug'=>$slug));
        $res=$result->row_array();
        if(count($res>0))
        {
			 
			 $result1 = $this->db->get_where('categories', array('parent_id'=>$res['id']));
             $res1=$result1->result_array();
        
			return $res1;
		}
		else
		{
			return false;
		}
    }
    
    
	function update($post)
	{
		   $result = $this->db->get_where('categories', array('id'=>$post['id']));
		   $res=$result->row_array();
		   if(!empty($res))
		   {

				$this->db->where('id', $post['id']);
				$this->db->update('categories', $post);
				return true;
		   }
		   else
		   {
			   return false;
		   }
	}
	
    function get_imageNameById($id)
    {
		   $result = $this->db->select('image')->get_where('categories', array('id'=>$id));
		   return $result->row_array();
		
	}
    function getIconNameById($id)
    {
		   $result = $this->db->select('icon_big')->get_where('categories', array('id'=>$id));
		   return $result->row_array();
		
	}
	
    function change_status($id,$status)
	{
				
				$this->db->where(array('id'=>$id));
				$this->db->update('categories', array('status'=>$status));
				return true;
		
	}
    function change_popularity($id,$status)
	{
				
				$this->db->where(array('id'=>$id));
				$this->db->update('categories', array('is_popular'=>$status));
				return true;
		
	}
	
	
    function check_service($slug)
    {
        $result = $this->db->get_where('categories', array('slug'=>$slug));
        $res=$result->row_array();
        if(count($res)>0)
        {
			$arr_resp=array();
            $result1 = $this->db->get_where('categories', array('parent_id'=>$res['id']));
            $result2 = $this->db->get_where('categories', array('parent_id'=>$res['id'],'is_popular'=>1));
            $arr_resp['allServices']=$result1->result_array();
            $arr_resp['popularServices']=$result2->result_array();
            
            //get child categories for each category
            foreach($arr_resp['allServices'] as $val)
            {
				
			}
            
            return $arr_resp;        			
		}
		else
		{
			return false;
		}
    }
    
}

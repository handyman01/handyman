<?php
Class Message_model extends CI_Model
{

    var $CI;

    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
        $this->load->library('session'); 
    }
    function get_template($for){
		
		$this->db->select('*');
		$this->db->where('type', $for);

		$query = $this->db->get('email_template');
		$res=$query->row_array();
        if(count($res)>0)
        {
			return $res;
		}
		else
		{
			return false;
		}
	
	}  
    
   
}

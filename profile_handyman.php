<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>:: Handyman ::</title>
<link href="<?php echo assets_url('css/bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/animate.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/styles.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo assets_url('css/responsive.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo assets_url('js/jquery.min.js'); ?>"></script> 
<link href="<?php echo assets_url('css/rating.css'); ?>" rel="stylesheet" type="text/css"/>
<script>
	var rating_imgg='<?php echo assets_url('css/images'); ?>';
</script>
<script src="<?php echo assets_url('js/rating.js'); ?>"></script> 

<style>
.codexworld_rating_widget
{
pointer-events: none;
padding-bottom: 15px;
}
.rating_blkk
{
	    padding-bottom: 40px;
}
.tabbable-line > .nav-tabs > li.active
{
	border:none;
}
	</style>
</head>
<body class="time-body">
	<div class="main-div">

			
 <?php if($header) echo $header ;?>

  <div class="profile_middle_container">
    <div class="container">
      <div class="profile_blk">
        <div class="cover_blk">
          <div class="img_blk"> <img src="<?php echo site_url('/assets/users/'.$details['image']);?>" alt="user">  </div>
        </div>
        <div class="profile_detail_blk">
          <div class="content_blk">
            <div class="col-md-6">
              <div class="info_blk">
                <h2>
				<?php
                if(isset($details['first_name']))
                {
					echo $details['first_name'].' '.$details['last_name'];
				}
				else
				{
					echo $details['name'];
				}
                ?>
                </h2>
                
                <p>Last online 8 hours ago</p>
                <?php
                if(isset($details['address']))
                {
                ?>
                <p><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo  $details['address'];?></p>
                <?php
			     }
                ?>
                <p>Member since  <?php echo date('M d,Y',strtotime($details['created']));?></p>
                <!-- <span><i class="fa fa-flag-o" aria-hidden="true"></i><a href="#">Report this Member</a></span> -->
                
                 </div>
            </div>
            <div class="col-md-6">
              <div class="info_blk align_right">
                <div class="tabbable-panel">
                  <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                      <li class="active"><a href="#tab_default_1" data-toggle="tab"></a></li>
                     
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_default_1">
                      	<span class="rating_blk">
							 <input name="rating" value="" id="rating_star2" class="rating_star" type="hidden" postID="2" /> 

							</span>
                        <p><?php echo $details['avg_rating'];?> Star - <?php echo count($details['reviews']);?> Review(s)</p>
                       <!--  <p>66% Completion Rate</p>-->
                        <span><?php echo $details['total_completed'];?> completed task(s)</span>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
   		 <?php
          if($details['about_urself'] && $details['about_urself']!="")
          {
          ?>       
          <div class="content_blk">

          <h3>About</h3>

            	<div class="about_blk">
					<ul>
					<li><?php
                	echo $details['about_urself'];
                	?></li>
					</ul>
                	
                </div>

          </div>
                       <?php
                
			}
			?>   

    	 <?php
          if($details['education']!="" || $details['speciality']!="" || $details['languages']!="" || $details['workings']!="")
          {
          ?>   
 
          <div class="content_blk">
          <h3>Skills</h3>
          <div class="row">
          	<div class="col-md-3">
            	<div class="skills_blk">
                <h4>Education</h4>
                  <ul>
					  <li>
							<?php
							echo ($details['education']!='' ? $details['education']:'-');
							?>
					  </li>
				  </ul>
                	
                </div>
            </div>
            <div class="col-md-3">
            	<div class="skills_blk">
                <h4>Specialities</h4>
                     <ul>
					  <li>
							<?php
							echo ($details['speciality']!='' ? $details['speciality']:'-');
							?>
					  </li>
				  </ul>

                </div>
            </div>
            <div class="col-md-3">
            	<div class="skills_blk">
                <h4>Languages</h4>
                      <ul>
					  <li>
							<?php
							echo ($details['languages']!='' ? $details['languages']:'-');
							?>
					  </li>
				  </ul>
				  
                	
                </div>
            </div>
            <div class="col-md-3">
            	<div class="skills_blk">
                <h4>Work</h4>
                      <ul>
					  <li>
							<?php
							echo ($details['workings']!='' ? $details['workings']:'-');
							?>
					  </li>
				  </ul>
                </div>
            </div>
           </div>
          </div>
          
          
          
       <?php
	  }
	  ?>
          <div class="content_blk">
          	<h3>Reviews</h3>
            <div class="row">
            <div class="col-md-12 rating_blkk">
  
                    
                	<div class="info_blk align_left">
						 <input name="rating" value="" id="rating_star1" class="rating_star" type="hidden" postID="1" /> 
						
                        <p><?php echo $details['avg_rating'];?> Star -  <?php echo count($details['reviews']);?> Review(s)</p>
                       <!--  <p>66% Completion Rate</p> -->
                        <span> <?php echo $details['total_completed'];?> completed task(s)</span>
                    </div>
                    
               
             
                </div>
            </div>
            <div class="row">

              
              
              
				<?php 

				$count=0;
				foreach($details['reviews'] as $val)
				{
					
					?>
				<div class="col-md-6">
                	<div class="media">
                      <div class="media-left">
                        <a href="#">
                       					<?php			
						if($val['detail'][0]['document']!="")
						{
							
							$ext = array("png", "jpg", "jpeg", "gif", "gif");
							$imgg=$val['detail'][0]['document'];
							 $file_name=explode(".", $imgg);
							
                            if(in_array(end($file_name), $ext)) 
                            {
								?>
								 <img src="<?php echo site_url('/assets/task/'.$imgg);?>" height='80' width='80' class="" alt="">
								<?php
					
                            }
                            else
                            {
								?>
								  <img src="<?php echo site_url('/assets/images/notask.png');?>" class="img-responsive" alt="">
								<?php
							
							
							}
                         }
                         else
                         {
							 ?>
							   <img src="<?php echo site_url('/assets/images/notask.png');?>" class="img-responsive" alt="">
							 <?php
						 }     
						
                         ?>
                        
                        
                        </a>
                      </div>
                      <div class="media-body" style="color:#758389;">
                      <span class="rating_blk"> <input name="rating" value="" id="task_rating_star<?php echo $count;?>" class="rating_star" type="hidden" postID="1" /> </span>
                      <span class="time_txt">13 hours ago</span>
                        <h4 class="media-heading"><?php echo $val['detail'][0]['title'];?></h4>
                     <span style="color:#008fb6;"> <?php echo $val['detail'][0]['first_name'];?></span> said "<?php echo $val['review'];?>
                      </div>
                    </div>
                </div>
                
	
                
                
					<?php
					$count++;
				}
				?>

               
               

           
           
            </div>
       
       
       
       
          </div>
        </div>
      </div>
    </div>
  </div>
 
	     
		     
		     <?php if($footer) echo $footer ;?>
  </div>
 
 

   
         
         
         
</body>

<script src="<?php echo assets_url('js/bootstrap.min.js'); ?>"></script> 
<script src="<?php echo assets_url('js/wow.min.js'); ?>"></script> 
<script>
	
	
(function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);

var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})

	var total_task='<?php echo count($details['reviews']);?>';
	var tasks=<?php echo json_encode($details['reviews']);?>;
 new WOW().init();
   
    $("#rating_star1").codexworld_rating_widget({
        starLength: '5',
        initialValue: '<?php echo $details['avg_rating'];?>',
        callbackFunctionName: false,
        imageDirectory: 'images/',
        inputAttr: 'postID'
   
});
    $("#rating_star2").codexworld_rating_widget({
        starLength: '5',
        initialValue: '<?php echo $details['avg_rating'];?>',
        callbackFunctionName: false,
        imageDirectory: 'images/',
        inputAttr: 'postID'
   
});

 for (var key in tasks) {
	
	   
	 	$("#task_rating_star"+key).codexworld_rating_widget({
        starLength: '5',
        initialValue: tasks[key].rating,
        callbackFunctionName: false,
        imageDirectory: 'images/',
        inputAttr: 'postID'
	});
        
 }


$(function(){

  $('#new-review').autosize({append: "\n"});

  var reviewBox = $('#post-review-box');
  var newReview = $('#new-review');
  var openReviewBtn = $('#open-review-box');
  var closeReviewBtn = $('#close-review-box');
  var ratingsField = $('#ratings-hidden');

  openReviewBtn.click(function(e)
  {
    reviewBox.slideDown(400, function()
      {
        $('#new-review').trigger('autosize.resize');
        newReview.focus();
      });
    openReviewBtn.fadeOut(100);
    closeReviewBtn.show();
  });

  closeReviewBtn.click(function(e)
  {
    e.preventDefault();
    reviewBox.slideUp(300, function()
      {
        newReview.focus();
        openReviewBtn.fadeIn(200);
      });
    closeReviewBtn.hide();
    
  });

  $('.starrr').on('starrr:change', function(e, value){
    ratingsField.val(value);
  });
});

</script>
</body>
</html>

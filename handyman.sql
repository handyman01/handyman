-- --------------------------------------------------------
-- Host:                         45.33.20.186
-- Server version:               5.5.49-0+deb8u1 - (Debian)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table handyman.admin_usercredits
CREATE TABLE IF NOT EXISTS `admin_usercredits` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_gross` float(10,2) NOT NULL,
  `currency_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `payer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_seen` tinyint(4) NOT NULL DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='admin store credit in user account';

-- Dumping data for table handyman.admin_usercredits: ~5 rows (approximately)
/*!40000 ALTER TABLE `admin_usercredits` DISABLE KEYS */;
INSERT INTO `admin_usercredits` (`payment_id`, `user_id`, `task_id`, `txn_id`, `payment_gross`, `currency_code`, `payer_email`, `payment_status`, `is_seen`, `updated`) VALUES
	(5, 31, 71, '4C029180B8014033P', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-12 08:34:18'),
	(6, 31, 71, '4B0989664B0241237', 4.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-12 08:35:52'),
	(7, 31, 71, '4NC75809WE127934B', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-12 09:16:11'),
	(8, 31, 94, '84J15135J9478813P', 0.60, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-17 08:36:26'),
	(9, 47, 95, '7HJ97288S50222748', 2.40, 'USD', 'ramswaroop.jat@brsoftech.com', 'Pending', 0, '2017-01-17 10:34:11');
/*!40000 ALTER TABLE `admin_usercredits` ENABLE KEYS */;


-- Dumping structure for table handyman.applied_tasks
CREATE TABLE IF NOT EXISTS `applied_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` enum('A','X','S') DEFAULT 'A' COMMENT 'A=>APPLIED,X=>TASK ALLOTED TO SOMEONE, S=>ASSIGNED TO THIS USER',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.applied_tasks: ~49 rows (approximately)
/*!40000 ALTER TABLE `applied_tasks` DISABLE KEYS */;
INSERT INTO `applied_tasks` (`id`, `task_id`, `user_id`, `status`, `created`) VALUES
	(31, 20, 16, 'A', '2016-12-31 04:59:16'),
	(32, 21, 16, 'S', '2016-12-31 05:02:43'),
	(33, 20, 18, 'A', '2016-12-31 05:09:24'),
	(34, 21, 18, 'X', '2016-12-31 05:11:27'),
	(35, 22, 18, 'S', '2016-12-31 05:11:52'),
	(37, 23, 18, 'A', '2016-12-31 06:46:11'),
	(38, 23, 17, 'A', '2016-12-31 06:52:25'),
	(39, 23, 16, 'A', '2016-12-31 06:54:41'),
	(40, 26, 20, 'A', '2017-01-03 17:09:09'),
	(41, 26, 13, 'A', '2017-01-03 17:19:56'),
	(42, 25, 13, 'S', '2017-01-03 17:34:28'),
	(43, 24, 20, 'A', '2017-01-03 17:37:52'),
	(44, 25, 20, 'X', '2017-01-03 19:01:11'),
	(45, 1, 23, 'A', '2017-01-03 23:33:08'),
	(46, 28, 23, 'A', '2017-01-03 23:33:11'),
	(47, 29, 23, 'A', '2017-01-04 11:33:23'),
	(48, 63, 15, 'A', '2017-01-10 11:28:08'),
	(49, 71, 16, 'A', '2017-01-12 08:24:01'),
	(50, 62, 25, 'A', '2017-01-13 06:25:59'),
	(52, 72, 1, 'A', '2017-01-13 06:37:06'),
	(53, 74, 25, 'A', '2017-01-13 07:18:10'),
	(54, 74, 23, 'A', '2017-01-13 08:39:56'),
	(55, 77, 25, 'A', '2017-01-13 09:43:21'),
	(56, 81, 25, 'A', '2017-01-13 12:09:41'),
	(57, 86, 13, 'A', '2017-01-13 16:38:30'),
	(58, 62, 1, 'A', '2017-01-16 05:21:05'),
	(59, 89, 27, 'S', '2017-01-16 06:45:44'),
	(60, 91, 27, 'S', '2017-01-16 07:04:19'),
	(62, 73, 17, 'A', '2017-01-16 08:30:45'),
	(63, 88, 27, 'A', '2017-01-16 10:14:44'),
	(64, 62, 28, 'A', '2017-01-16 10:19:07'),
	(65, 78, 28, 'S', '2017-01-16 10:19:44'),
	(66, 82, 28, 'A', '2017-01-16 10:20:16'),
	(67, 44, 28, 'A', '2017-01-16 10:22:55'),
	(68, 32, 28, 'A', '2017-01-16 10:25:16'),
	(69, 92, 28, 'S', '2017-01-16 10:27:56'),
	(70, 87, 27, 'A', '2017-01-16 10:56:44'),
	(71, 31, 28, 'A', '2017-01-17 11:53:35'),
	(72, 65, 28, 'A', '2017-01-17 12:00:53'),
	(73, 67, 28, 'A', '2017-01-17 12:02:45'),
	(74, 35, 17, 'A', '2017-01-17 12:54:06'),
	(75, 96, 17, 'A', '2017-01-17 12:54:18'),
	(76, 32, 17, 'A', '2017-01-17 12:59:04'),
	(77, 82, 17, 'A', '2017-01-18 05:41:01'),
	(78, 50, 17, 'A', '2017-01-18 05:41:38'),
	(79, 84, 17, 'A', '2017-01-18 05:41:47'),
	(82, 98, 25, 'A', '2017-01-18 10:26:41'),
	(83, 73, 23, 'A', '2017-01-18 23:12:28'),
	(84, 97, 30, 'A', '2017-01-18 23:26:39');
/*!40000 ALTER TABLE `applied_tasks` ENABLE KEYS */;


-- Dumping structure for table handyman.cancel_tasks
CREATE TABLE IF NOT EXISTS `cancel_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `cancel_reason` varchar(255) NOT NULL,
  `status` enum('S','A','D') NOT NULL DEFAULT 'S',
  `amount_refund` enum('Y','N') NOT NULL DEFAULT 'N',
  `penalty_charges` float NOT NULL DEFAULT '0',
  `is_seen` tinyint(4) NOT NULL DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.cancel_tasks: ~9 rows (approximately)
/*!40000 ALTER TABLE `cancel_tasks` DISABLE KEYS */;
INSERT INTO `cancel_tasks` (`id`, `task_id`, `cancel_reason`, `status`, `amount_refund`, `penalty_charges`, `is_seen`, `updated`) VALUES
	(4, 22, 'date needs to be extend.', 'S', 'N', 0, 1, '2016-12-31 05:32:02'),
	(5, 20, 'please cancel my task', 'S', 'N', 0, 1, '2016-12-31 06:00:18'),
	(6, 21, 'tretre', 'S', 'N', 0, 1, '2016-12-31 06:03:08'),
	(7, 17, 'test', 'S', 'N', 0, 1, '2017-01-06 20:39:01'),
	(8, 70, 'dont want to get it done', 'S', 'N', 0, 1, '2017-01-10 11:29:55'),
	(9, 63, 'pls cancel', 'S', 'N', 0, 1, '2017-01-10 11:33:29'),
	(11, 75, 'i want to cancel it.', 'S', 'N', 0, 1, '2017-01-13 08:44:40'),
	(12, 45, 'cancel', 'S', 'N', 0, 0, '2017-01-17 11:37:15'),
	(13, 99, '7777', 'S', 'N', 0, 1, '2017-01-18 23:17:45');
/*!40000 ALTER TABLE `cancel_tasks` ENABLE KEYS */;


-- Dumping structure for table handyman.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(250) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `icon_big` varchar(250) DEFAULT NULL,
  `is_popular` int(11) DEFAULT '0',
  `hourly_price` float DEFAULT '0',
  `deposit_hrs` int(11) DEFAULT '1',
  `overtime_charge` float DEFAULT '0',
  `status` int(11) DEFAULT '1',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.categories: ~8 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `parent_id`, `name`, `slug`, `image`, `icon_big`, `is_popular`, `hourly_price`, `deposit_hrs`, `overtime_charge`, `status`, `created`) VALUES
	(7, -1, 'Home Maintenances', 'home_maintenance', '1481258617_44873697-Builder-handyman-with-construction-tools-House-renovation-background--Stock-Photo.jpg', '1480511271_cat-4.png', 1, 0, 1, 0, 1, '2016-11-24 18:22:38'),
	(10, -1, 'Home Renovation', 'home_renovation', '1480058060_about_img.jpg', '1480511243_cat-2.png', 1, 5, 1, 0, 1, '2016-11-25 12:44:20'),
	(12, 10, 'Renovation service 1', NULL, '1483608251_22724331-Handyman-with-a-tool-belt-House-renovation-service--Stock-Photo.jpg', '1481258642_cat-3.png', 0, 20, 1, 10, 1, '2016-11-25 13:46:18'),
	(17, 7, 'Moving Help', NULL, '1480511710_images(2).jpg', '1480511710_cat-3.png', 1, 2, 1, 0, 1, '2016-11-30 13:15:10'),
	(18, 7, 'Garden Maintence', NULL, '1480511722_images.jpg', '1480511722_cat-3.png', 1, 12, 1, 0, 1, '2016-11-30 13:15:22'),
	(19, 7, 'House Cleaning', NULL, '1480511740_22724331-Handyman-with-a-tool-belt-House-renovation-service--Stock-Photo.jpg', '1480511740_cat-4.png', 1, 10, 1, 0, 1, '2016-11-30 13:15:40'),
	(20, 7, 'Electrical', NULL, '1481258673_images.jpg', '1481258673_cat-3.png', 1, 24, 2, 0, 1, '2016-11-30 13:16:00'),
	(21, 10, 'abc', NULL, '1482323544_g3.png', '1483608317_cat-4.png', 1, 20, 1, 5, 1, '2016-12-21 12:32:24');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;


-- Dumping structure for table handyman.credits
CREATE TABLE IF NOT EXISTS `credits` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_gross` float(10,2) NOT NULL,
  `currency_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `payer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_seen` tinyint(4) NOT NULL DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user store credit in his own account';

-- Dumping data for table handyman.credits: ~21 rows (approximately)
/*!40000 ALTER TABLE `credits` DISABLE KEYS */;
INSERT INTO `credits` (`payment_id`, `user_id`, `txn_id`, `payment_gross`, `currency_code`, `payer_email`, `payment_status`, `is_seen`, `updated`) VALUES
	(1, 31, '5FB89802N3921772V', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-09 07:14:24'),
	(2, 31, '6XC51122UK1186405', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-09 07:28:39'),
	(3, 31, '9PL19154BD222914H', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-09 08:18:05'),
	(4, 31, '4AU42120T9275563T', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-09 08:28:41'),
	(5, 31, '0VD083298G310030C', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-09 08:38:52'),
	(6, 31, '41K38341EG954645S', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-09 08:46:08'),
	(7, 31, '8V5192152M363103F', 10.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 04:26:05'),
	(8, 31, '6F804370D8053743B', 10.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 04:31:55'),
	(9, 31, '45D324007Y3350015', 10.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 04:34:27'),
	(10, 31, '8WY56106E83683344', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 04:35:51'),
	(11, 31, '0PS07431G0298391F', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 05:14:49'),
	(12, 31, '123', 10.00, 'USD', 'sd@sdsf.com', 'Pending', 0, '2017-01-10 05:19:50'),
	(13, 31, '64E58567B9780471P', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 05:39:55'),
	(14, 31, '6PE75176R5541991V', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 05:41:19'),
	(15, 31, '0EA0783611491240J', 3.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 05:41:25'),
	(16, 31, '0H5427138W811664H', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 05:51:35'),
	(17, 31, '0EP28294UF773125F', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 05:52:39'),
	(18, 31, '55704750MA386272V', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 05:54:08'),
	(19, 34, '1FW75745GS465092G', 10.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 09:24:24'),
	(20, 46, '5JM92598ET351973R', 3.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-12 12:44:08'),
	(21, 47, '9V5811786G105802P', 10.00, 'USD', 'ramswaroop.jat@brsoftech.com', 'Pending', 0, '2017-01-13 06:04:34');
/*!40000 ALTER TABLE `credits` ENABLE KEYS */;


-- Dumping structure for table handyman.email_template
CREATE TABLE IF NOT EXISTS `email_template` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table handyman.email_template: ~9 rows (approximately)
/*!40000 ALTER TABLE `email_template` DISABLE KEYS */;
INSERT INTO `email_template` (`id`, `type`, `subject`, `content`) VALUES
	(2, 'offer', 'Offer for task {task_name}.', '<p>You have received an offer for your task "{task_name}" <br><br>Offer Detail-<br/><br/>Offer Amount: ${amount}<br>From: {handy_man}<br/>Email: {handyman_email}<br/>Contact No: {handyman_phone}<br/><br/>Below find the task detail- <br/><br/>Task Timing: {task_timing}<br/>Estimated hours: {task_hours}<br/>Task Description: {task_description}<br/>Task Total amount: ${task_price}</p>'),
	(3, 'cancel_request', 'Task cancel request', '<p>Your task cancellation request has been initiated. for the task {task_name}</p>'),
	(4, 'forget_password', 'Forget password mail', '<p>Your New password is {new_password}<br/><br/>Remember!! This password will be applicable for your both account(s).</p>'),
	(5, 'hire_handyman', 'You are hired for a task', '<p>Congratulations, You are hired for the task "{task_name}"<br/><br/>Below find the task detail- <br/><br/>Customer Name: {customer_name}<br/>Task Timing: {task_timing}<br/>Estimated hours: {task_hours}.<br/>Total Amount: ${total_amount}<br/>Category: {category_name}<br/>Task Description: {task_description}<br/>Address: {address}</p>'),
	(6, 'renovation_task_posted', 'Your Renovation task is posted on website', '<p>Congratulations, Your renovation task {task_name} has been posted on website.</p>'),
	(7, 'task_cancelled', 'Task Cancelled', '<p>Your task "{task_name}" has been cancelled.<br/><br/>Below find the task detail- <br/><br/>Task Timing: {task_timing}<br/>Estimated hours: {task_hours}<br/>Task Description: {task_description}</p>'),
	(8, 'task_completed', 'Task Completed', '<p>Your task "{task_name}" has been COMPLETED.<br/><br/>Below find the task detail- <br/><br/>Task Timing: {task_timing}<br/>Estimated hours: {task_hours}<br/>Task Description: {task_description}</p>'),
	(9, 'guranteed_task_posted', 'A guranteed task is submitted on website', '<p>A guranteed task {title} has been submitted on website.<br/><br/>Below find the task detail- <br/><br/>Customer Name: {customer_name}<br/><br/>Task Title: {title}<br/><br/>Task Description: {description}<br/>\r\n<br/>Task Total Price:$ {total_price}<br/><br/>Task Deposit Amount:$ {total_deposit_amt}<br/></p>'),
	(10, 'create_user', 'You are registered on handyman', '<p>You are successfully registered on handyman. You can login using following credentials <br/><br/>Email Id:{email}<br/><br/>Password:{password}</p>');
/*!40000 ALTER TABLE `email_template` ENABLE KEYS */;


-- Dumping structure for table handyman.followers
CREATE TABLE IF NOT EXISTS `followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handyman_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.followers: ~6 rows (approximately)
/*!40000 ALTER TABLE `followers` DISABLE KEYS */;
INSERT INTO `followers` (`id`, `handyman_id`, `task_id`, `created`) VALUES
	(21, 20, 5, '2017-01-03 18:39:28'),
	(22, 25, 74, '2017-01-13 07:18:15'),
	(23, 1, 62, '2017-01-16 05:21:19'),
	(26, 27, 91, '2017-01-16 07:04:35'),
	(28, 25, 32, '2017-01-17 12:43:36'),
	(29, 17, 82, '2017-01-18 05:41:05');
/*!40000 ALTER TABLE `followers` ENABLE KEYS */;


-- Dumping structure for table handyman.handyman
CREATE TABLE IF NOT EXISTS `handyman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `company` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `ip_address` varchar(250) DEFAULT NULL,
  `total_offers` int(11) DEFAULT '0',
  `total_applied` int(11) DEFAULT '0',
  `total_completed` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '1',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_login` enum('Y','N') DEFAULT 'N',
  `last_login` datetime DEFAULT NULL,
  `image` varchar(250) DEFAULT 'user.jpg',
  `about_urself` text,
  `education` text,
  `speciality` text,
  `languages` text,
  `workings` text,
  `avg_rating` float DEFAULT '0',
  `credit` float DEFAULT '0',
  `block_till` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.handyman: ~27 rows (approximately)
/*!40000 ALTER TABLE `handyman` DISABLE KEYS */;
INSERT INTO `handyman` (`id`, `name`, `email`, `phone`, `password`, `company`, `address`, `ip_address`, `total_offers`, `total_applied`, `total_completed`, `status`, `created`, `is_login`, `last_login`, `image`, `about_urself`, `education`, `speciality`, `languages`, `workings`, `avg_rating`, `credit`, `block_till`) VALUES
	(1, 'eddy', 'eddy@gmail.com', 5657657, 'e10adc3949ba59abbe56e057f20f883e', 'eddy', 'eddy', NULL, NULL, NULL, 1, 1, '2016-11-30 18:02:36', 'N', '2017-01-16 10:00:16', 'user.jpg', NULL, NULL, NULL, NULL, NULL, 4, 0, NULL),
	(2, 'tasker1', 'tasker1@gmail.com', 465654646, 'e10adc3949ba59abbe56e057f20f883e', 'company', 'address', NULL, NULL, NULL, 0, 1, '2016-11-30 12:59:25', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(4, 'tasker356', 'tasker356@gmail.com', 78687686782, '827ccb0eea8a706c4c34a16891f84e7b', 'company1', '1address', '111.93.195.78', NULL, NULL, 0, 1, '2016-12-06 12:41:42', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(5, 'tasker from admin100', 'tasker_admin@gmail.com', 42354355446, 'e10adc3949ba59abbe56e057f20f883e', 'company', 'address', NULL, NULL, NULL, 0, 1, '2016-12-06 12:45:28', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(6, 'Test Handyman', 'testhandyman@mailinator.com', 4161010101, 'e10adc3949ba59abbe56e057f20f883e', 'Aurora', '500 Alden Rd', NULL, NULL, NULL, 0, 1, '2016-12-16 19:30:41', 'Y', '2016-12-16 20:35:27', 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(7, 'v v', 'vandana.pareek1@brsoftech.org', 5645646456, 'e10adc3949ba59abbe56e057f20f883e', 'fdsgdf', 'gdfg', '117.247.227.77', NULL, NULL, 0, 1, '2016-12-19 13:10:32', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(8, 'test test', 'test.p@brsoftech.org', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'yry', 'rty', '111.93.195.78', NULL, NULL, 0, 1, '2016-12-21 12:15:43', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(10, 'robert downy', 'robert@gmail.com', 678768, 'e10adc3949ba59abbe56e057f20f883e', 'test', 'australia', '111.93.195.78', NULL, NULL, 1, 1, '2016-12-22 12:48:13', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(11, 'qqq aaa', 'qqq1223@mailinator.com', NULL, '8c7618934c9f6f51e11317aa410a1116', 'auroratd', '500 Alden Rd, Markham, ON', '99.238.201.125', NULL, NULL, 0, 1, '2016-12-23 19:20:21', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(12, 'qqq', 'qqq2016@mailinator.com', 12345646, '96e79218965eb72c92a549dd5a330112', 'auroratd', '500 Alden Rd, Markham, ON', '99.238.201.125', NULL, NULL, 0, 1, '2016-12-23 19:25:44', 'N', '2017-01-03 16:54:32', 'user.jpg', NULL, NULL, NULL, NULL, NULL, 3, 0, NULL),
	(13, 'wwww', 'qqq2015@mailinator.com', 2314242134, '96e79218965eb72c92a549dd5a330112', 'Aurora', '505 Alden rd, Markham, ON', '99.238.201.125', NULL, NULL, 1, 1, '2016-12-23 20:51:40', 'Y', '2017-01-13 22:05:32', 'user.jpg', '', '', '', '', '', 0, 0, NULL),
	(15, 'manesh dhakad', 'manesh.kumar@brsoftech.org', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'brsoft', 'triveni nagar', '111.93.195.78', 1, 2, 2, 1, '2016-12-30 05:02:30', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 4, 0, NULL),
	(16, 'kajal chaturvedi', 'kajal.chaturvedi@brsoftech.org', 123456, 'e10adc3949ba59abbe56e057f20f883e', 'brsoftech', '123, triveni nagar', '117.247.227.77', 3, 3, 3, 1, '2016-12-31 04:47:36', 'N', '2016-12-31 06:42:31', '1483169125attractions3.jpg', 'Qualified Locksmith Taking Pride Commeded To My Workmanship.\r\n- Lock out & making keys to locks.\r\n- Installation / Upgrade Security.\r\n- Safe Installation & service.\r\n- Free Security advice/Quots/Senior Discounts.\r\n- All Work Garentee & Security Licence with over 11 years experance in the field.\r\n- Strata & Fire regaltions Cert.', 'Masterlocksmith / Security Licence / Over 11 years in the field.', 'Locksmithing, Handyman Skills, Door Frams', 'English & Arabic & Little Polynesian', '2 yrs', 3, 0, NULL),
	(17, 'dffdf', 'vandana.pareek@brsoftech.org', 6546464646, 'e10adc3949ba59abbe56e057f20f883e', 'yhrtyry', 'trytry', '111.93.195.78', 4, 9, 0, 1, '2016-12-31 04:51:40', 'Y', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(18, 'Privacy Policy', 'privacy@gmail.com', 23232, 'e10adc3949ba59abbe56e057f20f883e', 'brsoftech', '2323, gayatri nagar', '111.93.195.78', 4, 4, 1, 1, '2016-12-31 05:08:36', 'Y', '2016-12-31 05:55:36', 'user.jpg', NULL, NULL, NULL, NULL, NULL, 5, 0, NULL),
	(19, 'sss', 'qqq0103-2@mailinator.com', 12345646, 'e10adc3949ba59abbe56e057f20f883e', 'Auroratd', '500 Alden Rd', '99.238.201.125', 0, 0, 0, 1, '2017-01-03 16:16:02', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(20, 'vvvvv', 'qqq2017@mailinator.com', 1326546, 'e10adc3949ba59abbe56e057f20f883e', 'ARtd', '600 Alden Rd', '99.238.201.125', 1, 3, 0, 1, '2017-01-03 16:55:44', 'N', '2017-01-06 20:51:46', 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(21, 'yiheng', 'cyh50071208@gmail.com', 6464646464, '409d4e4cde50b005366224e19fdbe7fa', 'Aurora Technology Development Inc.', '500 alden rd', '99.238.201.125', 0, 0, 0, 1, '2017-01-03 18:45:47', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(22, 'asd', 'qqq2018@mailinator.com', 12321654, 'e10adc3949ba59abbe56e057f20f883e', 'aaa', '345 alden rd', NULL, 0, 0, 0, 0, '2017-01-03 20:46:18', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(23, 'admin admin', 'admin@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'Aurora Technology Development Inc.', '500 alden rd', '99.238.201.125', 0, 4, 1, 1, '2017-01-03 23:31:41', 'Y', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(24, 'dssdsd', 'sdss@qwqw.com', 23232, 'e10adc3949ba59abbe56e057f20f883e', 'wewe', 'wew', '117.247.227.77', 0, 0, 0, 1, '2017-01-04 12:06:54', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(25, 'handy', 'handy@mailinator.com', 25659858, 'e10adc3949ba59abbe56e057f20f883e', 'Handy Corp.', 'sodala jaipur.', '111.93.195.78', 2, 4, 2, 1, '2017-01-13 06:16:43', 'Y', '2017-01-18 15:59:41', '148430183510-1204x768.jpg', '', '', '', '', '', 5, 0, '2017-01-13 16:30:19'),
	(26, '3545', 'task@mailinator.com', NULL, 'c20ad4d76fe97759aa27a0c99bff6710', NULL, NULL, '111.93.195.78', 0, 0, 0, 1, '2017-01-13 10:55:28', 'N', '2017-01-13 16:50:01', '1484306231images(6).jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(27, 'Amit', 'testbr0007@gmail.com', 1, 'e10adc3949ba59abbe56e057f20f883e', 'BR Soft', 'Jaipur Rajasthan', '111.93.195.78', 1, 2, 3, 1, '2017-01-16 06:40:38', 'N', '2017-01-16 17:35:26', '1484555390Desktop-Wallpaper-HD2.jpg', 'Passoinate', 'B.tech', 'Electrician and Gardener.', 'English', '3 yr', 4, 0, NULL),
	(28, 'Vandana Pareek', 'vandana.pareek42@gmail.com', NULL, '89b34a69ebd955a3bfd4528f3b87984c', 'utu', 'tyu', '111.93.195.78', 1, 6, 1, 1, '2017-01-16 08:11:45', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 4, 0, NULL),
	(29, 'Tesker', 'a2@mailinator.com', 123456789, 'e10adc3949ba59abbe56e057f20f883e', 'BR Soft', 'Jaipur', '111.93.195.78', 0, 0, 0, 1, '2017-01-16 12:52:46', 'Y', '2017-01-16 12:53:24', 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
	(30, 'mengfei', 'mengfei0714@gmail.com', 1112223333, '96e79218965eb72c92a549dd5a330112', '111111', '111111', '99.238.201.125', 0, 0, 0, 1, '2017-01-18 22:19:43', 'N', NULL, 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
/*!40000 ALTER TABLE `handyman` ENABLE KEYS */;


-- Dumping structure for table handyman.handyman_areas
CREATE TABLE IF NOT EXISTS `handyman_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categ_id` int(11) DEFAULT NULL,
  `handyman_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.handyman_areas: ~82 rows (approximately)
/*!40000 ALTER TABLE `handyman_areas` DISABLE KEYS */;
INSERT INTO `handyman_areas` (`id`, `categ_id`, `handyman_id`) VALUES
	(1, 16, 1),
	(2, 16, 2),
	(3, 12, 2),
	(4, 8, 2),
	(5, 20, 3),
	(6, 19, 3),
	(7, 20, 4),
	(8, 18, 4),
	(9, 17, 4),
	(10, 12, 4),
	(11, 19, 7),
	(12, 17, 7),
	(13, 20, 8),
	(14, 20, 9),
	(17, 20, 10),
	(18, 19, 10),
	(19, 17, 10),
	(20, 20, 11),
	(21, 20, 12),
	(22, 19, 12),
	(23, 18, 12),
	(24, 17, 12),
	(29, 19, 14),
	(30, 18, 14),
	(31, 17, 14),
	(32, 20, 15),
	(33, 19, 15),
	(34, 18, 15),
	(35, 17, 15),
	(36, 12, 15),
	(43, 20, 17),
	(44, 19, 17),
	(45, 20, 18),
	(46, 19, 18),
	(47, 18, 18),
	(48, 17, 18),
	(61, 20, 16),
	(62, 19, 16),
	(63, 18, 16),
	(64, 17, 16),
	(65, 21, 16),
	(66, 12, 16),
	(67, 20, 19),
	(68, 19, 19),
	(69, 18, 19),
	(70, 17, 19),
	(71, 20, 13),
	(72, 19, 13),
	(73, 18, 13),
	(74, 17, 13),
	(75, 21, 13),
	(76, 12, 13),
	(77, 21, 20),
	(78, 12, 20),
	(79, 19, 21),
	(80, 18, 21),
	(81, 17, 21),
	(82, 19, 23),
	(83, 17, 23),
	(84, 12, 23),
	(85, 19, 24),
	(92, 20, 25),
	(93, 19, 25),
	(94, 18, 25),
	(95, 17, 25),
	(96, 21, 25),
	(97, 12, 25),
	(104, 20, 26),
	(105, 19, 26),
	(106, 18, 26),
	(107, 17, 26),
	(108, 21, 26),
	(109, 12, 26),
	(115, 20, 28),
	(116, 20, 27),
	(117, 19, 27),
	(118, 18, 27),
	(119, 17, 27),
	(120, 21, 27),
	(121, 19, 29),
	(122, 17, 29),
	(123, 17, 30);
/*!40000 ALTER TABLE `handyman_areas` ENABLE KEYS */;


-- Dumping structure for table handyman.maintenance_tasks
CREATE TABLE IF NOT EXISTS `maintenance_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `categ_id` int(11) NOT NULL,
  `categ_parent_slug` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `handyman_id` int(11) DEFAULT '0',
  `is_residential` int(11) DEFAULT '0',
  `is_commercial` int(11) DEFAULT '0',
  `address_unit` varchar(100) DEFAULT '0',
  `address` varchar(250) DEFAULT NULL,
  `description` text,
  `task_date` date DEFAULT NULL,
  `task_time` time DEFAULT NULL,
  `estimated_time` varchar(50) DEFAULT NULL,
  `total_price` float DEFAULT '0',
  `total_deposit_amt` float DEFAULT '0',
  `deposited_amt` float DEFAULT '0',
  `is_credit` int(11) DEFAULT '0',
  `credit_used` float DEFAULT '0',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  `lon` varchar(150) DEFAULT NULL,
  `document` varchar(255) DEFAULT NULL,
  `status` enum('S','P','X','C','CR') DEFAULT 'S' COMMENT 's=>submit, p=>processing,cr=>cancel_request,x=>cancelled,c=>completed',
  `payment_status` int(11) DEFAULT '0',
  `accepted_offer` float DEFAULT '0',
  `final_price` float DEFAULT '0',
  `total_offers` int(11) DEFAULT '0',
  `is_posted` int(11) DEFAULT '0' COMMENT 'this field is only for renovation tasks when admin finally post to website',
  `complete_request` tinyint(4) DEFAULT '0',
  `cust_wrk_hrs` float DEFAULT '0',
  `cust_wrk_amt` float DEFAULT '0',
  `handy_wrk_hrs` float DEFAULT '0',
  `handy_wrk_amt` float DEFAULT '0',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL,
  `task_type` varchar(50) DEFAULT NULL,
  `admin_note` text,
  `store_credit_percent` float DEFAULT '0',
  `store_credit_amount` float DEFAULT '0',
  `store_credit_status` int(11) DEFAULT '0',
  `diff_hrs` float DEFAULT '0',
  `diff_amt` float DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.maintenance_tasks: ~100 rows (approximately)
/*!40000 ALTER TABLE `maintenance_tasks` DISABLE KEYS */;
INSERT INTO `maintenance_tasks` (`id`, `title`, `categ_id`, `categ_parent_slug`, `user_id`, `handyman_id`, `is_residential`, `is_commercial`, `address_unit`, `address`, `description`, `task_date`, `task_time`, `estimated_time`, `total_price`, `total_deposit_amt`, `deposited_amt`, `is_credit`, `credit_used`, `first_name`, `last_name`, `lat`, `lon`, `document`, `status`, `payment_status`, `accepted_offer`, `final_price`, `total_offers`, `is_posted`, `complete_request`, `cust_wrk_hrs`, `cust_wrk_amt`, `handy_wrk_hrs`, `handy_wrk_amt`, `created`, `updated`, `task_type`, `admin_note`, `store_credit_percent`, `store_credit_amount`, `store_credit_status`, `diff_hrs`, `diff_amt`) VALUES
	(1, 'Clean a Chimney Flue', 20, 'home_maintenance', 20, 0, 1, 0, '73', 'Usha Vihar, Jaipur, Rajasthan, India', ' check for creosote, shine the light near the top of the firebox, in the smoke chamber and around the damper. And check the flue too', '2016-12-20', '10:00:00', '3', 15, 0, 0, 0, 0, NULL, NULL, '26.8680238', '75.7756008', '', 'S', 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, '2016-12-14 05:14:05', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(2, 'roof, flashing and downspouts inspection.', 12, 'home_renovation', 20, 0, 1, 0, '45', 'Usha Vihar, Jaipur, Rajasthan, India', 'roof, flashing and downspouts inspection.roof, flashing and downspouts inspection.', '2016-12-27', '11:10:00', '20', 400, 0, 0, 0, 0, NULL, NULL, '26.8680238', '75.7756008', '148169379444873697-Builder-handyman-with-construction-tools-House-renovation-background--Stock-Photo.jpg', 'S', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, '2016-12-14 05:36:34', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(3, 'test task', 20, 'home_maintenance', 20, 0, 1, 0, '44', 'Usha Vihar, Jaipur, Rajasthan, India', 'test', '2016-12-21', '13:55:00', '3', 15, 0, 0, 0, 0, NULL, NULL, '26.8680238', '75.7756008', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-14 08:14:45', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(4, 'ret', 12, 'home_renovation', 20, 0, 1, 0, '34', 'treetrete', 'retert', '2016-12-20', '14:45:00', '4', 80, 0, 0, 0, 0, NULL, NULL, '', '', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-14 09:08:01', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(5, 'ert', 20, 'home_maintenance', 20, 0, 1, 0, 'rt', 'retete', 'tetet', '2016-12-28', '15:20:00', '4', 20, 0, 0, 0, 0, NULL, NULL, '', '', '', 'S', 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, '2016-12-14 09:41:16', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(6, 'test', 20, 'home_maintenance', 20, 0, 1, 0, '22', 'Usha Vihar, Jaipur, Rajasthan, India', 'test', '2016-12-27', '16:50:00', '2', 10, 0, 0, 0, 0, NULL, NULL, '26.8680238', '75.7756008', '', 'S', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, '2016-12-14 10:59:23', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(7, 'test', 20, 'home_maintenance', 20, 0, 1, 0, '43', 'Usha Vihar, Jaipur, Rajasthan, India', 'test', '2016-12-20', '16:50:00', '1', 5, 0, 0, 0, 0, NULL, NULL, '26.8680238', '75.7756008', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-14 11:14:22', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(8, 'test', 20, 'home_maintenance', 20, 7, 1, 0, '45', 'Usha Vihar, Jaipur, Rajasthan, India', 'test', '2016-12-15', '17:00:00', '1', 5, 0, 0, 0, 0, 'vandana pareek', NULL, '26.8680238', '75.7756008', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-14 11:17:13', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(9, 'Test Test', 12, 'home_renovation', 23, 6, 1, 0, '210', '500 Alden Road, Markham, ON, Canada', 'Test Test', '2016-12-17', '15:50:00', '2', 40, 0, 0, 0, 0, 'XC Test', NULL, '43.8356582', '-79.33352459999998', '1481920117a36dcfbf6c81800ac02fe908b83533fa838b47a7.jpg', 'P', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-16 20:28:37', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(10, 'Test Test', 19, 'home_maintenance', 23, 6, 1, 0, '210', '500 Alden Road, Markham, ON, Canada', 'Test Test', '2016-12-30', '15:50:00', '3', 24, 0, 0, 0, 0, 'XC Test', NULL, '43.8356582', '-79.33352459999998', '1481920632f4cfdcc2356a1684a10f0a18d6d2a9c91ce9d61b.jpg', 'P', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-16 20:37:12', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(11, 'test', 20, 'home_maintenance', 25, 9, 1, 0, '45', 'Mohan Nagar, Ghaziabad, Uttar Pradesh, India', 'test', '2016-12-20', '18:45:00', '2', 10, 0, 0, 0, 0, 'v v', NULL, '28.672818', '77.38628360000007', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-12-19 13:09:02', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(12, 'renovation service test', 12, 'home_renovation', 27, 10, 1, 0, 'test', 'Robert Street, Rozelle, New South Wales, Australia', 'renovation service test renovation service testrenovation service testrenovation service testrenovation service test', '2016-12-27', '18:25:00', '2', 40, 0, 0, 0, 0, 'emma watson', NULL, '-33.8657031', '151.17760240000007', '1482410609images(2).jpg', 'C', 0, 50, 50, 0, 1, 0, 0, 0, 0, 0, '2016-12-22 12:43:29', '2016-12-29 13:18:16', NULL, NULL, 0, 0, 0, 0, 0),
	(13, 'renovation service test', 20, 'home_maintenance', 27, 9, 1, 0, '34', 'Robert Street, Rozelle, New South Wales, Australia', 'renovation service test renovation service testrenovation service testrenovation service testrenovation service test', '2016-12-26', '17:35:00', '2', 10, 0, 0, 0, 0, NULL, NULL, '-33.8657031', '151.17760240000007', '1482410677images(2).jpg', 'P', 0, 1, 1, 3, 0, 0, 0, 0, 0, 0, '2016-12-22 12:44:37', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(14, 'Test', 20, 'home_maintenance', 29, 12, 1, 0, '', '500 Alden Road, Markham, ON, Canada', 'Test Test Test Test Test Test Test Test ', '2016-12-26', '14:50:00', '2', 10, 0, 0, 0, 0, 'qqq aaa', NULL, '43.8356582', '-79.33352459999998', '1482521698Screenshot_2016-12-14-15-36-45.png', 'C', 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, '2016-12-23 19:34:58', NULL, NULL, NULL, 20, 2, 0, 0, 0),
	(15, 'Test 2', 19, 'home_maintenance', 29, 12, 1, 0, '', '505 Alden Road, Markham, ON, Canada', 'Test 2Test 2Test 2Test 2Test 2Test 2Test 2', '2016-12-28', '14:00:00', '4', 32, 0, 0, 0, 0, 'qqq aaa', NULL, '43.8363218', '-79.3320506', '1482525057Screenshot_2016-12-14-15-36-45.png', 'P', 0, 32, 32, 1, 0, 0, 0, 0, 0, 0, '2016-12-23 20:30:57', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(16, 'Test 3', 18, 'home_maintenance', 29, 12, 1, 0, '', '555 Alden Road, Markham, ON, Canada', 'Test 3Test 3Test 3Test 3Test 3Test 3', '2016-12-24', '16:00:00', '5', 60, 0, 0, 0, 0, 'qqq aaa', NULL, '43.836627', '-79.3309112', '', 'P', 0, 2, 2, 1, 0, 0, 0, 0, 0, 0, '2016-12-23 20:40:01', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(17, 'Moving', 17, 'home_maintenance', 29, 13, 1, 0, '', '511 Alden Rd, Markham, ON, Canada', 'MovingMovingMovingMovingMovingMoving', '2016-12-27', '16:00:00', '6', 12, 0, 0, 0, 0, 'qqq aaa', NULL, '43.8363218', '-79.3320506', '', 'X', 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, '2016-12-23 20:52:58', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(18, 'Home cleaning', 19, 'home_maintenance', 27, 15, 1, 0, '12', 'Gayatri Nagar, Jaipur, Rajasthan, India', '3 rooms, 1 kitchen, 1 big hall , dusting cleaning', '2016-12-31', '10:40:00', '5', 40, 0, 0, 0, 0, 'emma watson', NULL, '26.903466179095222', '75.7723245248535', '', 'C', 0, 42, 42, 1, 0, 0, 0, 0, 0, 0, '2016-12-30 05:00:39', '2016-12-30 05:10:35', NULL, NULL, 0, 0, 0, 0, 0),
	(19, 'testing task', 20, 'home_maintenance', 30, 14, 1, 0, '12', 'Triveni Nagar, Jaipur, Rajasthan, India', 'testing tasktesting tasktesting task testing tasktesting tasktesting task testing tasktesting tasktesting task', '2016-12-30', '11:05:00', '3', 15, 0, 0, 0, 0, NULL, NULL, '26.868967336235386', '75.77653560157478', '', 'P', 0, 17, 17, 2, 0, 0, 0, 0, 0, 0, '2016-12-30 05:13:16', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(20, 'room and kitchen dusting and cleaning', 19, 'home_maintenance', 31, 16, 1, 0, '43', 'mohan nagar, near ridhi sidhi sweets, jaipur', 'have one room to be clean and mop', '2017-01-01', '10:35:00', '2', 16, 0, 0, 0, 0, 'vandana pareek', NULL, '26.873479', '75.77599310000005', '1483160307images.jpg', 'C', 0, 18, 18, 2, 0, 0, 0, 0, 0, 0, '2016-12-31 04:58:27', '2016-12-31 06:08:00', NULL, NULL, 0, 0, 0, 0, 0),
	(21, 'electric wire fitting in kitchen chimney', 20, 'home_maintenance', 31, 16, 1, 0, '22', 'near panchwati circle raja park jaipur', 'electric wire fitting in kitchen chimney', '2017-01-02', '10:55:00', '7', 35, 0, 0, 0, 0, 'vandana pareek', NULL, '26.8946909', '75.83026040000004', '148316049444873697-Builder-handyman-with-construction-tools-House-renovation-background--Stock-Photo.jpg', 'C', 0, 33, 33, 2, 0, 0, 0, 0, 0, 0, '2016-12-31 05:01:34', '2016-12-31 06:09:49', NULL, NULL, 0, 0, 0, 0, 0),
	(22, 'Fixing electrical circuits and fixing wiring in basement area and first floor', 20, 'home_maintenance', 32, 18, 1, 0, '22', 'Malviya Nagar Industrial Area, Jaipur, Rajasthan, India', 'Fixing electrical circuits and fixing wiring in basement area and first floor\r\nFixing electrical circuits and fixing wiring in basement area and first floor\r\nFixing electrical circuits and fixing wiring in basement area and first floor', '2017-01-03', '11:00:00', '12', 60, 0, 0, 0, 0, 'manesh kumar', NULL, '26.8573126', '75.82870100000002', '148316098122724331-Handyman-with-a-tool-belt-House-renovation-service--Stock-Photo.jpg', 'C', 0, 48, 48, 1, 0, 0, 0, 0, 5, 9, '2016-12-31 05:09:41', '2016-12-31 07:19:26', NULL, NULL, 0, 0, 0, 0, 0),
	(23, 'A Complete day house cleaning', 19, 'home_maintenance', 34, 0, 1, 0, '21', 'Mansarovar, Jaipur, Rajasthan, India', '4 rooms, 1 hall , 1 kitchen,, mob, utensils include all claning stuff', '2017-01-02', '12:35:00', '15', 120, 0, 0, 0, 0, NULL, NULL, '26.848151236249716', '75.73142079707031', '', 'S', 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, '2016-12-31 06:44:47', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(24, 'test', 12, 'home_renovation', 29, 0, 1, 1, '', '510 Alden Road, Markham, ON, Canada', 'pi3M4pi3M4pi3M4pi3M4', '2017-01-04', '11:50:00', '2', 40, 0, 0, 0, 0, NULL, NULL, '43.8360296', '-79.33233559999996', '', 'S', 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, '2017-01-03 16:39:14', '2017-01-14 01:37:48', NULL, NULL, 0, 0, 0, 0, 0),
	(25, 'test123', 20, 'home_maintenance', 29, 13, 1, 0, '', '510 Alden Road, Markham, ON, Canada', 'test123test123test123test123test123test123', '2017-01-05', '12:05:00', '4', 20, 0, 0, 0, 0, 'qqq aaa', NULL, '43.8360296', '-79.33233559999996', '', 'C', 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, '2017-01-03 16:43:56', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(26, 'ForTest', 21, 'home_renovation', 29, 13, 1, 0, '', '510 Alden Road, Markham, ON, Canada', 'ForTestForTestForTestForTestForTest', '2017-01-06', '11:55:00', '3', 60, 0, 0, 0, 0, 'qqq aaa', NULL, '43.8360296', '-79.33233559999996', '', 'P', 0, 0, 60, 1, 1, 0, 0, 0, 0, 0, '2017-01-03 16:49:58', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(27, 'cleaning', 19, 'home_maintenance', 1, 0, 1, 1, '', '221 Glendora Avenue, North York, ON, Canada', 'gfdfdfdfdfdfdfd', '2017-01-05', '13:55:00', '3', 24, 0, 0, 0, 0, NULL, NULL, '43.7621441', '-79.39551589999996', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-03 18:44:18', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(28, 'cleaninAS', 20, 'home_maintenance', 1, 0, 1, 0, '', '221 Glendora Avenue, North York, ON, Canada', 'ASASASASDAFFDFD', '2017-01-04', '17:05:00', '1', 5, 0, 0, 0, 0, NULL, NULL, '43.7621441', '-79.39551589999996', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-03 22:38:16', NULL, NULL, NULL, 0, 0, 0, 0, 0),
	(29, 'test', 20, 'home_maintenance', 31, 23, 1, 0, '45', 'jaipur', 'test', '2017-01-16', '03:00:00', '1', 5, 0, 0, 0, 0, 'vandana pareek', NULL, '26.9124336', '75.78727090000007', '', 'C', 0, 0, 5, 0, 0, 0, 0, 0, 6, 12, '2017-01-04 10:54:51', NULL, NULL, NULL, 0, 0, 0, 6, 12),
	(30, 'test guaranteed with amt', 20, 'home_maintenance', 31, 0, 1, 0, '3', '345 George Street, Sydney, New South Wales, Australia', 'test guaranteed with amt', '2017-01-30', '18:30:00', '12', 12, 1, 1, 1, 1, NULL, NULL, '-33.8679205', '151.20674740000004', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 12:49:29', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(31, 'test guaranteed with amt11', 19, 'home_maintenance', 31, 28, 1, 0, '3', '345 George Street, Sydney, New South Wales, Australia', '1test guaranteed with amt', '2017-01-30', '18:40:00', '1', 1, 1, 1, 0, 0, 'vandana pareek', NULL, '-33.8679205', '151.20674740000004', '', 'P', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 12:50:39', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(32, 'ytuyu', 20, 'home_maintenance', 31, 0, 1, 0, '6', '56 Pitt Street, Sydney, New South Wales, Australia', 'uyuy', '2017-01-24', '18:30:00', '1', 1, 1, 1, 0, 0, NULL, NULL, '-33.8638903', '151.209205', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 12:55:29', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(33, '78iuyi', 20, 'home_maintenance', 31, 0, 1, 0, '56', '88 Pitt Street, Sydney, New South Wales, Australia', 'uyiuyiuyi', '2017-01-24', '18:45:00', '6', 6, 1, 1, 0, 0, NULL, NULL, '-33.8669828', '151.20880250000005', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 13:00:34', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(34, 'try', 20, 'home_maintenance', 31, 0, 1, 0, '54', '45 George Street, The Rocks, New South Wales, Australia', 'tryrty', '2017-01-17', '18:40:00', '5', 5, 1, 1, 0, 0, NULL, NULL, '-33.85774810000001', '151.20858320000002', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 13:06:14', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(35, 'rt', 20, 'home_maintenance', 31, 0, 1, 0, '435', '50 Pitt Street, Sydney, New South Wales, Australia', 'et', '2017-01-16', '18:40:00', '4', 4, 1, 1, 0, 0, NULL, NULL, '-33.8633547', '151.20927389999997', '', 'S', 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, '2017-01-09 13:07:14', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(36, 'ert', 20, 'home_maintenance', 31, 0, 1, 0, '554', '45 Oxford Street, Surry Hills, New South Wales, Australia', 'ertetr', '2017-01-16', '04:00:00', '4', 4, 1, 1, 1, 1, NULL, NULL, '-33.8780691', '151.21337829999993', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 13:21:40', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(37, '56', 20, 'home_maintenance', 31, 0, 1, 0, '56', '55 George Street, The Rocks, New South Wales, Australia', '5656', '2017-01-16', '19:00:00', '5', 5, 1, 1, 1, 1, NULL, NULL, '-33.8581087', '151.20873299999994', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 13:23:07', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(38, '45', 19, 'home_maintenance', 31, 0, 1, 0, '44', '456 Pitt Street, Haymarket, New South Wales, Australia', 'rtrytry', '2017-01-23', '04:00:00', '3', 3, 1, 1, 1, 1, NULL, NULL, '-33.8838351', '151.20601280000005', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 13:27:33', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(39, '45ret', 19, 'home_maintenance', 31, 0, 1, 0, '44', '400 Elizabeth Street, Surry Hills, New South Wales, Australia', 'rtrytrytet', '2017-01-17', '04:15:00', '4', 4, 1, 1, 1, 1, NULL, NULL, '-33.8859853', '151.20853160000001', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 13:28:04', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(40, '45retret', 19, 'home_maintenance', 31, 0, 1, 0, '44r', '3 King Street, Newtown, New South Wales, Australia', 'rtrytrytetrtret', '2017-01-17', '04:15:00', '4', 4, 1, 1, 1, 1, NULL, NULL, '-33.89167719999999', '151.18766900000003', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 13:28:44', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(41, 'ytu', 19, 'home_maintenance', 31, 0, 1, 0, '56', '65 Pitt Street, Sydney, New South Wales, Australia', 'tyu', '2017-01-16', '19:10:00', '5', 5, 1, 1, 1, 1, NULL, NULL, '-33.8634022', '151.20890110000005', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 13:30:06', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(42, 'newtest', 20, 'home_maintenance', 29, 0, 1, 1, '', '500 Alden Road, Markham, ON, Canada', 'newtestnewtestnewtestnewtestnewtestnewtestnewtestnewtest', '2017-01-10', '16:00:00', '15', 360, 1, 0, 0, 0, NULL, NULL, '43.8356582', '-79.33352459999998', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-09 20:53:28', '2017-01-14 01:26:15', 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(43, 'test', 19, 'home_maintenance', 31, 0, 1, 0, '54', '540 George Street, Sydney, New South Wales, Australia', 'test', '2017-01-30', '11:40:00', '30', 30, 1, 1, 1, 1, NULL, NULL, '-33.8733328', '151.2073828', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 05:55:01', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(44, '545 testt', 20, 'home_maintenance', 31, 0, 1, 0, '4', '45 George Street, The Rocks, New South Wales, Australia', 'test', '2017-01-30', '11:35:00', '5', 5, 1, 1, 1, 1, NULL, NULL, '-33.85774810000001', '151.20858320000002', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 05:57:46', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(45, 'test', 18, 'home_maintenance', 31, 0, 1, 0, '44', '44 Oxford Street, Paddington, New South Wales, Australia', 'tst', '2017-01-23', '11:40:00', '3', 36, 12, 12, 1, 12, NULL, NULL, '-33.8822479', '151.22038010000006', '', 'CR', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 05:59:07', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(46, 'testt', 18, 'home_maintenance', 31, 0, 1, 0, '4', '44 Pitt Street, Sydney, New South Wales, Australia', 'est', '2017-01-17', '11:40:00', '12', 144, 12, 11, 1, 1, NULL, NULL, '-33.8630226', '151.20926039999995', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 06:03:24', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(47, 'test', 18, 'home_maintenance', 31, 0, 1, 0, '4', '55 George Street, The Rocks, New South Wales, Australia', 'Fall garden cleanup can make spring gardening a treat instead of a chore. Garden clean up can also prevent pests, weed seeds and diseases from overwintering and causing problems when temperatures warm. Cleaning out the garden for winter also allows you to spend more time on the fun aspects of gardening in spring and provides a clean slate for perennials and vegetables to grow. Cleaning Out the Garden for Winter One of the key aspects of fall cleanup is the removal of potentially problem pests and disease. When you rake up old leaves and debris, you are removing a hiding place for overwintering insects and pests. The old plant material left behind is a perfect refuge for diseases such as fungal spores, which can infect fresh new plants in spring. Garden clean up should also include maintenance of the compost pile and proper practices to prevent mold and seed bloom. Empty and spread the compost pile to protect tender perennial plants and add a layer of nutrient and weed prevention over the beds. Any compost that was not finished goes back into the pile along with the leaves and debris you raked up. Cleaning up garden vegetable beds will allow you to till in some of the compost and begin to amend them for spring. The perennial garden can be raked, weeded and cut back in most zones. Zones below USDA plant hardiness zone 7 can leave the debris as protective cover for tender perennials. All other areas will benefit from fall clean up, both visually and as a time saver in spring. Cleaning up garden perennials allows you to catalogue your plants as you make plans for ordering and acquiring new items. Cleaning Gardens Schedule The novice gardener may wonder exactly when to do each project. It is common sense in most cases. As soon as vegetables stop producing, pull the plant. When a perennial fails to bloom anymore, cut it back. Garden clean up includes the weekly chores of raking, compost duties and weeding. When cleaning gardens don’t forget bulbs and tender plants. Any plant that will not survive winter in your zone needs to be dug up and transplanted. Then they are put in the basement or garage where they will not freeze. Bulbs that cannot overwinter are dug up, cut back the foliage, dry them for a few days and then place them in paper bags. Let them rest in a dry area until spring. Pruning Practices When Cleaning Up the Garden As everything else in the landscape gets tidy, it’s hard to resist shaping and pruning hedges, topiaries and other plants. This isn’t a good idea, as it encourages the formation of new growth that is more sensitive to cooler temperatures. Wait until they are dormant or early spring for most evergreen and broad leaf evergreen plants. Do not cut spring flowering plants until after they have bloomed. Cleaning up garden plants with dead or broken plant material is done at any time of the year. Print This Article This article was last updated on 06/27/16 73 3 185 1 5349 Additional Help & Information Didn\'t find the answer to your question? Ask one of our friendly gardening experts. Do you know anything about gardening? Help answer someone\'s gardening question. Read more articles about Gardening Tips & Information. Search for more information Use the search box below to find more gardening information on Gardening Know How: Related Articles "Turning Your Compost Heap - How To Aerate A Compost Pile "Compost Smells Bad: How To Fix Bad Smelling Compost "How To Make Your Own Topiary Newest Articles Fertilizer For Water Grown Plants – How To Fertilize Plants In Water Japanese Snowball Care: Learn About Japanese Snowball Trees Zone 5 Vegetables – When To Plant Zone 5 Vegetable Gardens Matilija Poppy Care: Tips On Growing Matilija Poppy Plants Kangaroo Deterrents: How To Control Kangaroos In The Garden Learn More About… Testing Garden Soil – Why Test Soil In A Garden Storing Chili Peppers – How To Dry Hot Peppers Understanding Plant Dormancy: How To Put A Plant Into Dormancy Drying Fresh Basil: How To Dry Basil From Your Garden On The Blog What Is A Yellow Pear Tomato Leafscape Espoma Houseplant Care Prize Pack Giveaway Read more at Gardening Know How: Cleaning Up Garden: How To Prepare Your Garden For Winter https://www.gardeningknowhow.com/garden-how-to/info/cleaning-up-garden.htm', '2017-01-17', '12:30:00', '3', 36, 12, 2, 1, 10, 'vandana pareek', NULL, '-33.8581087', '151.20873299999994', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 06:51:20', NULL, 'guaranteed', NULL, 10, 3.6, 0, 0, 0),
	(48, 'test task 124', 18, 'home_maintenance', 31, 0, 1, 0, '43', '431 Pitt Street, Haymarket, New South Wales, Australia', 'test task 124', '2017-01-30', '13:00:00', '2', 24, 12, 12, 0, 0, NULL, NULL, '-33.8790948', '151.20690409999997', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 07:10:09', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(49, 'tet', 18, 'home_maintenance', 31, 0, 1, 0, '44', '44 Pitt Street, Sydney, New South Wales, Australia', 'tet', '2017-01-17', '13:00:00', '3', 36, 12, 2, 1, 10, NULL, NULL, '-33.8630226', '151.20926039999995', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 07:24:05', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(50, 'tyu', 20, 'home_maintenance', 31, 0, 1, 0, '23', '23 George Street, The Rocks, New South Wales, Australia', 'test', '2017-01-17', '13:00:00', '5', 5, 1, 1, 1, 1, NULL, NULL, '-33.85694669999999', '151.2081263', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 07:27:42', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(51, 'test', 20, 'home_maintenance', 31, 0, 1, 0, '66', '66 George Street, The Rocks, New South Wales, Australia', 'test', '2017-01-31', '13:10:00', '3', 3, 1, 1, 0, 0, NULL, NULL, '-33.857165', '151.2087669', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 07:28:55', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(52, 'test', 18, 'home_maintenance', 31, 0, 1, 0, '99', '99 George Street, The Rocks, New South Wales, Australia', 'test', '2017-01-24', '13:10:00', '5', 60, 12, 3, 1, 9, NULL, NULL, '-33.8594989', '151.20858299999998', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 07:30:04', NULL, 'guaranteed', NULL, 10, 6, 0, 0, 0),
	(53, 'tyu', 17, 'home_maintenance', 31, 0, 1, 0, '66', '66 Oxford Street, Paddington, New South Wales, Australia', 'test', '2017-01-24', '13:50:00', '5', 10, 2, 0, 0, 0, NULL, NULL, '-33.8827034', '151.22063079999998', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 08:09:10', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(54, 'test', 17, 'home_maintenance', 31, 0, 1, 0, '45', '540 George Street, Sydney, New South Wales, Australia', 'test', '2017-01-17', '13:45:00', '1', 2, 2, 0, 0, 0, NULL, NULL, '-33.8733328', '151.2073828', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 08:11:45', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(55, 'test', 19, 'home_maintenance', 31, 0, 1, 0, '65', '65 George Street, The Rocks, New South Wales, Australia', 'test', '2017-01-18', '13:55:00', '4', 4, 1, 0, 0, 0, NULL, NULL, '-33.8581354', '151.20874019999997', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 08:19:37', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(56, '54', 19, 'home_maintenance', 31, 0, 1, 0, '55', '555 George Street, Sydney, New South Wales, Australia', '54', '2017-01-17', '14:05:00', '4', 4, 1, 0, 0, 0, NULL, NULL, '-33.8762037', '151.20599330000005', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 08:36:24', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(57, '54', 19, 'home_maintenance', 31, 0, 1, 0, '5', '540 George Street, Sydney, New South Wales, Australia', '54', '2017-01-24', '14:15:00', '4', 4, 1, 0, 0, 0, NULL, NULL, '-33.8733328', '151.2073828', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 08:40:46', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(58, 'testt', 18, 'home_maintenance', 31, 0, 1, 0, '55', '54 Pitt Street, Sydney, New South Wales, Australia', 'test', '2017-01-11', '14:15:00', '3', 36, 12, 0, 0, 0, NULL, NULL, '-33.8633335', '151.2092854', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 08:42:09', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(59, '56', 18, 'home_maintenance', 31, 0, 1, 0, '56', '76 Pitt Street, Sydney, New South Wales, Australia', '567', '2017-01-17', '14:25:00', '5', 60, 12, 0, 0, 0, NULL, NULL, '-33.8665144', '151.20910019999997', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 08:43:33', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(60, 'rty', 19, 'home_maintenance', 31, 0, 1, 0, 'try', 'truck stop', 'rty', '2017-01-17', '14:20:00', '4', 4, 1, 1, 0, 0, NULL, NULL, '-33.897534', '151.0212901', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 08:44:50', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(61, 'ytry', 20, 'home_maintenance', 31, 0, 1, 0, '55', '55 George Street, The Rocks, New South Wales, Australia', 'rty', '2017-01-17', '14:30:00', '5', 5, 1, 1, 0, 0, NULL, NULL, '-33.8581087', '151.20873299999994', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 08:52:59', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(62, '34', 20, 'home_maintenance', 31, 0, 1, 0, '3', '46 Pitt Street, Sydney, New South Wales, Australia', '43', '2017-01-23', '14:35:00', '3', 3, 1, 1, 0, 0, NULL, NULL, '-33.8633335', '151.2092854', '', 'S', 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, '2017-01-10 08:56:22', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(63, '45', 20, 'home_maintenance', 31, 15, 1, 0, '55', '55 George Street, The Rocks, New South Wales, Australia', '54', '2017-01-16', '04:05:00', '4', 4, 1, 1, 0, 0, 'vandana pareek', NULL, '-33.8581087', '151.20873299999994', '', 'C', 1, 0, 4, 0, 0, 0, 5, 9, 0, 0, '2017-01-10 08:59:39', '2017-01-17 16:53:37', 'guaranteed', NULL, 0, 0, 0, 5, 9),
	(64, 'dfdsfds', 19, 'home_maintenance', 34, 0, 1, 0, '1', '231, R K Colony Road, R K Colony, Pulgaon, Maharashtra, India', 'sdfsdfsf', '2017-02-10', '15:05:00', '1', 1, 1, 1, 1, 1, NULL, NULL, '20.7167647', '78.3248165', '', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 09:26:58', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(65, 'dss', 20, 'home_maintenance', 34, 28, 1, 0, '1', '231 Sussex Street, Sydney, New South Wales, Australia', 'dsdsds', '2017-03-10', '15:25:00', '2', 2, 1, 1, 0, 0, 'kajal chaturvedi', NULL, '-33.871805211706636', '151.20357850100845', '', 'P', 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 09:28:12', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(66, 'free add', 12, 'home_renovation', 34, 0, 1, 0, '12', 'Anand Vihar, New Delhi, Delhi, India', ' free add free add free add', '2017-02-10', '15:20:00', '1', 20, 0, 0, 0, 0, NULL, NULL, '28.636136791948765', '77.2819485515356', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 09:31:00', NULL, 'free', NULL, 0, 0, 0, 0, 0),
	(67, 'gurantte tak', 19, 'home_maintenance', 34, 28, 1, 0, '12', 'Lakshmi Nagar, Chennai, Tamil Nadu, India', 'free addfree addfree addfree add free addfree add free add', '2017-01-12', '15:15:00', '1', 1, 1, 1, 1, 1, 'kajal chaturvedi', NULL, '13.034422066277017', '80.1768047735095', '', 'P', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 09:33:09', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(68, 'dfsdf', 18, 'home_maintenance', 34, 0, 1, 0, '22', '233 Indura Street, Mount Martha, Victoria, Australia', 'dfsdsf', '2017-01-18', '15:20:00', '1', 12, 12, 4, 1, 8, NULL, NULL, '-38.248253', '145.0362837', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 09:34:15', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(69, 'dfsdf', 19, 'home_maintenance', 34, 0, 1, 0, '22', '125 George Street, The Rocks, New South Wales, Australia', 'dfsdsf', '2017-02-18', '15:20:00', '3', 3, 1, 1, 0, 0, NULL, NULL, '-33.860071', '151.20839019999994', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 09:35:15', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(70, 'testtesttesttest', 20, 'home_maintenance', 31, 0, 1, 0, '65', '65 Pitt Street, Sydney, New South Wales, Australia', 'testtesttest', '2017-01-16', '16:25:00', '3', 3, 1, 1, 0, 0, 'vandana pareek', NULL, '-33.8634022', '151.20890110000005', '', 'X', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-10 10:56:51', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(71, 'test', 20, 'home_maintenance', 31, 16, 1, 0, '4', '46 Pitt Street, Sydney, New South Wales, Australia', 'test', '2017-01-10', '18:20:00', '4', 200, 2, 2, 0, 0, 'vandana pareek', NULL, '-33.8633335', '151.2092854', '', 'C', 1, 0, 4, 0, 0, 0, 5, 4, 3, 5, '2017-01-10 12:43:58', NULL, 'guaranteed', 'task 71', 1, 2, 1, 0, 0),
	(72, 'house cleaning 2 room kitchen', 19, 'home_maintenance', 46, 1, 1, 0, '44', 'Alberta Street, Sydney, New South Wales, Australia', 'house cleaning 2 room kitchen', '2017-01-31', '18:30:00', '12', 12, 1, 1, 1, 1, 'Vandana Pareek', NULL, '-33.8780887', '151.21062359999996', '', 'C', 1, 0, 12, 0, 0, 1, 4, 5, 0, 0, '2017-01-12 12:45:34', '2017-01-16 09:58:16', 'guaranteed', NULL, 0, 0, 0, 4, 5),
	(73, 'clean floor', 18, 'home_maintenance', 47, 0, 1, 0, '1', 'BR Softech Pvt. Ltd. ( Website and Mobile Application Development Company ), Jaipur, Rajasthan, India', 'demo..', '2017-01-13', '11:30:00', '3', 36, 12, 12, 0, 0, 'test 123', NULL, '26.867936', '75.77604099999996', '1484287101john.jpg', 'S', 1, 10, 10, 1, 0, 0, 0, 0, 0, 0, '2017-01-13 05:58:21', NULL, 'guaranteed', NULL, 10, 3.6, 0, 0, 0),
	(74, 'Decoration', 19, 'home_maintenance', 47, 0, 1, 1, '2', 'Sodala, Jaipur, Rajasthan, India', 'just decorate floor to party.', '2017-01-13', '15:10:00', '3', 3, 1, 0, 0, 0, NULL, NULL, '26.9064744', '75.77280139999993', '14842918122.jpeg', 'S', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, '2017-01-13 07:16:52', '2017-01-13 14:02:02', 'free', NULL, 0, 0, 0, 0, 0),
	(75, 'garden cleaning', 18, 'home_maintenance', 47, 0, 1, 1, '2', 'Devi Nagar, Jaipur, Rajasthan, India', 'Fall garden cleanup can make spring gardening a treat instead of a chore. Garden clean up can also prevent pests, weed seeds and diseases from overwintering and causing problems when temperatures warm. Cleaning out the garden for winter also allows you to spend more time on the fun aspects of gardening in spring and provides a clean slate for perennials and vegetables to grow. Cleaning Out the Garden for Winter One of the key aspects of fall cleanup is the removal of potentially problem pests and disease. When you rake up old leaves and debris, you are removing a hiding place for overwintering insects and pests. The old plant material left behind is a perfect refuge for diseases such as fungal spores, which can infect fresh new plants in spring. Garden clean up should also include maintenance of the compost pile and proper practices to prevent mold and seed bloom. Empty and spread the compost pile to protect tender perennial plants and add a layer of nutrient and weed prevention over the beds. Any compost that was not finished goes back into the pile along with the leaves and debris you raked up. Cleaning up garden vegetable beds will allow you to till in some of the compost and begin to amend them for spring. The perennial garden can be raked, weeded and cut back in most zones. Zones below USDA plant hardiness zone 7 can leave the debris as protective cover for tender perennials. All other areas will benefit from fall clean up, both visually and as a time saver in spring. Cleaning up garden perennials allows you to catalogue your plants as you make plans for ordering and acquiring new items. Cleaning Gardens Schedule The novice gardener may wonder exactly when to do each project. It is common sense in most cases. As soon as vegetables stop producing, pull the plant. When a perennial fails to bloom anymore, cut it back. Garden clean up includes the weekly chores of raking, compost duties and weeding. When cleaning gardens don’t forget bulbs and tender plants. Any plant that will not survive winter in your zone needs to be dug up and transplanted. Then they are put in the basement or garage where they will not freeze. Bulbs that cannot overwinter are dug up, cut back the foliage, dry them for a few days and then place them in paper bags. Let them rest in a dry area until spring. Pruning Practices When Cleaning Up the Garden As everything else in the landscape gets tidy, it’s hard to resist shaping and pruning hedges, topiaries and other plants. This isn’t a good idea, as it encourages the formation of new growth that is more sensitive to cooler temperatures. Wait until they are dormant or early spring for most evergreen and broad leaf evergreen plants. Do not cut spring flowering plants until after they have bloomed. Cleaning up garden plants with dead or broken plant material is done at any time of the year. Print This Article This article was last updated on 06/27/16 73 3 185 1 5349 Additional Help & Information Didn\'t find the answer to your question? Ask one of our friendly gardening experts. Do you know anything about gardening? Help answer someone\'s gardening question. Read more articles about Gardening Tips & Information. Search for more information Use the search box below to find more gardening information on Gardening Know How: Related Articles "Turning Your Compost Heap - How To Aerate A Compost Pile "Compost Smells Bad: How To Fix Bad Smelling Compost "How To Make Your Own Topiary Newest Articles Fertilizer For Water Grown Plants – How To Fertilize Plants In Water Japanese Snowball Care: Learn About Japanese Snowball Trees Zone 5 Vegetables – When To Plant Zone 5 Vegetable Gardens Matilija Poppy Care: Tips On Growing Matilija Poppy Plants Kangaroo Deterrents: How To Control Kangaroos In The Garden Learn More About… Testing Garden Soil – Why Test Soil In A Garden Storing Chili Peppers – How To Dry Hot Peppers Understanding Plant Dormancy: How To Put A Plant Into Dormancy Drying Fresh Basil: How To Dry Basil From Your Garden On The Blog What Is A Yellow Pear Tomato Leafscape Espoma Houseplant Care Prize Pack Giveaway\r\n\r\nRead more at Gardening Know How: Cleaning Up Garden: How To Prepare Your Garden For Winter https://www.gardeningknowhow.com/garden-how-to/info/cleaning-up-garden.htm', '2017-01-13', '15:30:00', '2', 24, 12, 0, 0, 0, NULL, NULL, '26.8824899', '75.76476609999997', '14842966923_alexandrpopkov-morning_butterfly1.jpg', 'CR', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-13 08:38:12', '2017-01-13 14:13:17', 'free', NULL, 0, 0, 0, 0, 0),
	(76, 'test address task', 20, 'home_maintenance', 1, 0, 1, 0, '44', 'Sodala, Jaipur, Rajasthan, India', 'test address task', '2017-01-18', '14:25:00', '3', 3, 2, 0, 0, 0, NULL, NULL, '26.9064744', '75.77280139999993', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-13 08:43:22', NULL, 'free', NULL, 0, 0, 0, 0, 0),
	(77, 'make new electricity fittings.', 20, 'home_maintenance', 47, 25, 1, 0, '1', 'Sodala Police Station, Jaipur, Rajasthan, India', 'make new electricity fittings.', '2017-01-14', '15:30:00', '5', 5, 2, 0, 0, 0, 'test 123', NULL, '26.9018936', '75.77633179999998', '14843002573ccfc82f7e7d9bf39efc515be68d70a2.jpg', 'C', 0, 0, 5, 0, 0, 1, 3, 10, 5, 25, '2017-01-13 09:37:37', '2017-01-13 15:43:28', 'free', 'dispute', 2, 0.1, 0, 2, 15),
	(78, 'test', 20, 'home_maintenance', 1, 28, 1, 0, '55', '55 Parramatta Road, Annandale, New South Wales, Australia', 'test', '2017-01-25', '15:20:00', '4', 4, 2, 0, 0, 0, NULL, NULL, '-33.887391', '151.17021499999998', '', 'C', 0, 6, 6, 1, 0, 1, 4, 7, 0, 0, '2017-01-13 09:47:38', '2017-01-16 17:57:31', 'free', NULL, 0, 0, 0, 4, 7),
	(79, 'testt', 20, 'home_maintenance', 1, 0, 1, 0, '44', '44 Oxford Street, Paddington, New South Wales, Australia', 'est', '2017-01-25', '04:15:00', '4', 4, 2, 2, 0, 0, NULL, NULL, '-33.8822479', '151.22038010000006', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-13 10:40:12', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(80, 'work of decoration', 20, 'home_maintenance', 47, 0, 1, 1, '1', 'Sector 7 Mansarovar, Mansarovar, Jaipur, Rajasthan, India', 'decoration lighting.', '2018-01-14', '17:30:00', '5', 5, 2, 2, 1, 2, NULL, NULL, '26.8580109', '75.7696684', '14843084071.jpg', 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-13 11:53:27', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(81, 'decoration', 20, 'home_maintenance', 47, 25, 1, 0, '2', 'Sector 8 Mansarovar, Jaipur, Rajasthan, India', 'decoration.... 123 testing message', '2017-01-14', '17:35:00', '12', 12, 2, 2, 1, 2, 'test 123', NULL, '26.8536731', '75.76476609999997', '14843085041.jpeg', 'C', 1, 0, 12, 0, 0, 0, 3, 10, 3, 10, '2017-01-13 11:55:04', '2017-01-13 17:56:32', 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(82, 'fitting', 20, 'home_maintenance', 47, 0, 1, 1, '2', 'Sector 9 Mansarovar, Mansarovar, Jaipur, Rajasthan, India', 'fitting in house.', '2017-01-21', '17:50:00', '6', 6, 2, 2, 0, 0, NULL, NULL, '26.8513854', '75.77057949999994', '14843087141.jpg', 'S', 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, '2017-01-13 11:58:34', NULL, 'guaranteed', NULL, 10, 0.6, 0, 0, 0),
	(83, 'old wires change', 20, 'home_maintenance', 47, 0, 1, 1, '2', 'Malviya Nagar, Sector 10, Jaipur, Rajasthan, India', 'old wires change.', '2017-01-14', '18:25:00', '3', 0, 0, 0, 0, 0, NULL, NULL, '26.8476849', '75.81199349999997', '148431111951b451a37900336010.jpg', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-13 12:38:39', NULL, 'free', NULL, 0, 0, 0, 0, 0),
	(84, 'ttttt', 20, 'home_maintenance', 51, 0, 1, 0, '', '500 Alden Road, Markham, ON, Canada', 'tttttesttttttesttttttesttttttest', '2017-01-14', '10:35:00', '5', 0, 0, 0, 0, 0, NULL, NULL, '43.8356582', '-79.33352459999998', '', 'S', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, '2017-01-13 15:28:15', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(85, '2354235', 19, 'home_maintenance', 51, 0, 1, 0, '', '500 Alden Road, Markham, ON, Canada', '23465234562346', '2017-01-14', '10:55:00', '5', 50, 10, 10, 0, 0, NULL, NULL, '43.8356582', '-79.33352459999998', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-13 15:41:16', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(86, '324562345', 12, 'home_renovation', 29, 0, 1, 1, '', '500 Alden Road, Markham, ON, Canada', 'erterterter', '2017-01-16', '11:50:00', '6', 120, 20, 0, 0, 0, NULL, NULL, '43.8356582', '-79.33352459999997', '', 'S', 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, '2017-01-13 16:37:30', '2017-01-14 01:37:02', 'free', NULL, 0, 0, 0, 0, 0),
	(87, 'Cut the plants and Clean garden and Maintain all the trees and plants.bnbnbnbnbnbnbnbnbnbnmbnmbnbnmb', 18, 'home_maintenance', 53, 27, 1, 0, '1212', 'Jaipur, Rajasthan 302033, India', 'dhjdfhdkjf dfdfjdkfjd fjdfjdflkjdl dfjdkfj erew r ty ty t try try tr yt r tew   c x c zxc zx cz czx c xzc x czx c zxc x czx c zxc zx c zxc zxc x', '2015-02-16', '11:25:00', '2', 24, 12, 0, 0, 0, 'Amit User', NULL, '26.803474431139', '75.80141376703978', '1484545716HD-Wallpapers1.jpeg', 'P', 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, '2017-01-16 05:48:36', NULL, 'free', NULL, 0, 0, 0, 0, 0),
	(88, 'Testing Task', 20, 'home_maintenance', 53, 27, 1, 0, '121', 'Pratap Plaza, Pratap Nagar, Jaipur, Rajasthan, India', 'It is testing Task please ignore it.', '2015-03-16', '11:45:00', '3', 72, 48, 48, 0, 0, 'Amit User', NULL, '26.802269', '75.80884919999994', '1484546822invoice_204.pdf', 'C', 1, 0, 72, 0, 0, 1, 2, 100, 0, 0, '2017-01-16 06:07:02', '2017-01-16 18:14:29', 'guaranteed', NULL, 0, 0, 0, 2, 100),
	(89, 'Garden Cleaning', 18, 'home_maintenance', 53, 27, 1, 1, '1123', 'Jawahar Circle Garden, Jaipur, Rajasthan, India', 'Need to clean the garden.', '2017-03-17', '12:05:00', '4', 48, 12, 0, 0, 0, NULL, NULL, '26.83901833795309', '75.8004850925904', '1484548002images01.jpg', 'C', 0, 50, 50, 1, 0, 1, 4, 50, 0, 0, '2017-01-16 06:26:42', '2017-01-16 12:36:37', 'free', NULL, 0, 0, 0, 4, 50),
	(90, 'Road Light Complaint.', 20, 'home_maintenance', 47, 0, 1, 0, '2', 'Devi Nagar, Jaipur, Rajasthan, India', 'Road Light Complaint.', '2017-01-17', '12:10:00', '1', 24, 48, 0, 0, 0, NULL, NULL, '26.8824899', '75.76476609999997', '1484548209womens-aconcagua-jacket-AMUW_128_hero.jpg', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-16 06:30:09', NULL, 'free', NULL, 0, 0, 0, 0, 0),
	(91, 'Lighting Work in the temple.', 20, 'home_maintenance', 53, 27, 1, 0, '222', 'Birla Mandir, Tilak Nagar, Jaipur, Rajasthan, India', 'Tesing work', '2017-02-25', '12:20:00', '3', 72, 48, 0, 0, 0, NULL, NULL, '26.8921609', '75.81552959999999', '1484548496index1.jpeg', 'C', 0, 0, 72, 0, 0, 1, 3, 72, 3, 72, '2017-01-16 06:34:56', '2017-01-16 14:59:47', 'free', NULL, 0, 0, 0, 0, 0),
	(92, 'test 1111111111111111111', 20, 'home_maintenance', 46, 28, 1, 0, '55', '55 Parramatta Road, Annandale, New South Wales, Australia', 'test', '2017-01-17', '14:55:00', '4', 96, 0, 0, 0, 0, NULL, NULL, '-33.887391', '151.17021499999998', '', 'P', 0, 0, 96, 0, 0, 0, 0, 0, 0, 0, '2017-01-16 09:16:30', '2017-01-16 15:57:30', 'free', NULL, 0, 0, 0, 0, 0),
	(93, 'jdkdfjdsf fjdskfjds fjlkdfjdsl jflkdsfjl', 20, 'home_maintenance', 53, 0, 1, 0, '45', 'Birla Mandir, Tilak Nagar, Jaipur, Rajasthan, India', 'kdsfkdf jdkfjdkf fjkdjflkjflkd fjkdjfdslkjf dfjkdsljfdslkjfds dfkdjflksdjfkdsjfks dfjlkdj djfkdfsj dfdsf d fds fd fds fd fd sfd f dsf ds df dfdf df d f df d fd f df d fd f df df d fd f d fd ', '2017-01-17', '17:45:00', '3', 72, 0, 0, 0, 0, 'Amit User', NULL, '26.891969527567262', '75.8184478434082', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-16 11:56:54', NULL, 'free', NULL, 5, 3.6, 0, 0, 0),
	(94, 'test', 19, 'home_maintenance', 31, 0, 1, 0, '55', '55 George Street, The Rocks, New South Wales, Australia', 'test', '2017-01-24', '12:10:00', '6', 60, 0, 0, 0, 0, NULL, NULL, '-33.8581087', '151.20873299999994', '', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-17 06:31:47', NULL, 'free', NULL, 1, 0.6, 1, 0, 0),
	(95, 'qqqqq', 20, 'home_maintenance', 47, 0, 1, 0, '1', 'Sodala, Jaipur, Rajasthan, India', 'qqqq', '2017-01-17', '16:00:00', '1', 24, 0, 0, 0, 0, NULL, NULL, '26.9064744', '75.77280139999993', '14846484191_alexandrpopkov-morning_snail.jpg', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-17 10:20:19', NULL, 'free', NULL, 10, 2.4, 1, 0, 0),
	(96, 'FFFFFFFFFFFFF FFFFFF FFFFFFFF FFFFFFFFF FFFFFFFFF FFFFFFFFFFFFF FFFFFFFF FFFFFFFFFFFFFFFFFF FFFFFFFFFFF FFFFFFFFFFS SSSSSSS SSSSSSSS SSSSSSSSS SSSSSS SSSSSSSS  SSS SSSSSSSSSSSSSSSS SSSSSSSSS SSSSSSSS SSSSSSSSS            SSSSSSSS SSSSSSSS RRRRR RRRRR', 20, 'home_maintenance', 47, 0, 1, 0, '1', 'Sodala, Jaipur, Rajasthan, India', 'dfsdf', '2017-01-17', '17:50:00', '1', 24, 0, 0, 0, 0, 'test 123', NULL, '26.9064744', '75.77280139999993', '14846545152_alexandrpopkov-morning_grasses.jpg', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-17 12:01:55', NULL, 'free', NULL, 0, 0, 0, 0, 0),
	(97, 'asdas', 20, 'home_maintenance', 47, 30, 1, 0, '1', 'Sodala, Jaipur, Rajasthan, India', 'dsadsdsad', '2017-01-17', '19:05:00', '1', 24, 0, 0, 0, 0, 'test 123', NULL, '26.9064744', '75.77280139999993', '1484658848Deutsche_Bank_logo_without_wordmark-300x300.png', 'P', 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, '2017-01-17 13:14:08', NULL, 'free', NULL, 0, 0, 0, 0, 0),
	(98, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddtttttttttttttttttttttttt', 20, 'home_maintenance', 47, 25, 1, 0, '1', 'Sodala, Jaipur, Rajasthan, India', 'aaaaa', '2017-01-19', '14:45:00', '1', 24, 0, 0, 0, 0, 'test 123', NULL, '26.9064744', '75.77280139999993', '1484729728dzire2012_2.JPG', 'C', 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, '2017-01-18 08:55:28', NULL, 'free', NULL, 0, 0, 0, 0, 0),
	(99, 'testing how it works', 18, 'home_maintenance', 1, 0, 1, 0, '', '500 Alden Rd, Markham, ON, Canada', 'not sure', '2017-02-19', '18:05:00', '10', 120, 12, 12, 0, 0, 'admin admin', NULL, '43.8356582', '-79.33352459999998', '1484780389ausoshare2-02.jpg', 'X', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-18 22:59:49', NULL, 'guaranteed', NULL, 0, 0, 0, 0, 0),
	(100, '88888888888888888', 20, 'home_maintenance', 19, 0, 1, 0, '', '500 George Street, Sydney, New South Wales, Australia', '898888888888', '2017-01-27', '18:45:00', '9', 216, 0, 0, 0, 0, NULL, NULL, '-33.8723996', '151.2071178', '1484782473ausoshare2-01.jpg', 'S', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-01-18 23:34:33', NULL, 'free', NULL, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `maintenance_tasks` ENABLE KEYS */;


-- Dumping structure for table handyman.offers
CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `handyman_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.offers: ~16 rows (approximately)
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` (`id`, `task_id`, `handyman_id`, `user_id`, `price`, `created`) VALUES
	(20, 20, 16, 31, 18, '2016-12-31 04:59:16'),
	(21, 21, 16, 31, 33, '2016-12-31 05:02:43'),
	(22, 20, 18, 31, 14, '2016-12-31 05:09:24'),
	(23, 21, 18, 31, 34, '2016-12-31 05:11:27'),
	(24, 22, 18, 32, 48, '2016-12-31 05:11:52'),
	(25, 23, 18, 34, 130, '2016-12-31 06:46:11'),
	(26, 23, 17, 34, 134, '2016-12-31 06:52:25'),
	(27, 23, 16, 34, 122, '2016-12-31 06:54:41'),
	(28, 26, 20, 29, 2, '2017-01-03 17:09:09'),
	(30, 74, 25, 47, 1, '2017-01-13 07:18:10'),
	(31, 62, 1, 31, 1, '2017-01-16 05:21:05'),
	(32, 89, 27, 53, 50, '2017-01-16 06:52:49'),
	(33, 78, 28, 1, 6, '2017-01-16 10:22:34'),
	(34, 82, 17, 47, 4, '2017-01-18 05:41:01'),
	(35, 35, 17, 31, 11, '2017-01-18 05:42:06'),
	(36, 84, 17, 51, 66666, '2017-01-18 05:43:51');
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;


-- Dumping structure for table handyman.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `enabled` varchar(1) NOT NULL DEFAULT 'Y',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.pages: ~3 rows (approximately)
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `name`, `slug`, `description`, `enabled`, `created`, `modified`) VALUES
	(1, 'Privacy Policy', 'privacy-policy', '<h2>What Personal information Does Handyman Collect?</h2>\r\n<p>Your phone number. <br>Your location information from time to time. <br>Any personal information that you choose to provide such as name, email, gender. <br>Information about your device such as hardware information, operating system version, carrier info if available.</p>\r\n<h2>Why is this information collected?</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam molestie, orci vitae interdum mattis, eros neque egestas odio, ut blandit libero sapien a dui. Donec vitae magna hendrerit augue pulvinar rhoncus accumsan ac orci. Sed pellentesque, ante eget consectetur blandit, nibh nulla vulputate lectus.</p>\r\n<p>Nulla in sem sodales, fringilla diam eu, fermentum lacus. Maecenas vestibulum porta mi, at porta augue blandit non. Phasellus porttitor lorem ut venenatis pulvinar. In egestas leo eros, at tristique justo fermentum eu. Quisque vel augue vitae magna cursus congue. Aenean rhoncus suscipit odio nec sodales. Aliquam lectus est, condimentum sit amet laoreet non, bibendum in dui. Vivamus placerat ornare turpis vel consequat. Fusce sed urna et tortor suscipit placerat.</p>\r\n<h2>How is My Information Shared?</h2>\r\n<p>Maecenas sem risus, volutpat vitae nisi id, gravida mattis lacus. In in leo sapien. Mauris tempor ut orci eget posuere. Etiam dapibus pellentesque mi sit amet elementum. Mauris commodo sem id enim auctor, in efficitur ipsum consectetur. Aenean dignissim ultricies est in efficitur.</p>\r\n<h2>Friends and Followers:</h2>\r\n<p>Nulla in sem sodales, fringilla diam eu, fermentum lacus. Maecenas vestibulum porta mi, at porta augue blandit non. Phasellus porttitor lorem ut venenatis pulvinar. In egestas leo eros, at tristique justo fermentum eu. Quisque vel augue vitae magna cursus congue. Aenean rhoncus suscipit odio nec sodales. Aliquam lectus est, condimentum sit amet laoreet non, bibendum in dui. Vivamus placerat ornare turpis vel consequat. Fusce sed urna et tortor suscipit placerat.</p>\r\n<h2>Determining your location.</h2>\r\n<p>Nulla in sem sodales, fringilla diam eu, fermentum lacus. Maecenas vestibulum porta mi, at porta augue blandit non. Phasellus porttitor lorem ut venenatis pulvinar. In egestas leo eros, at tristique justo fermentum eu. Quisque vel augue vitae magna cursus congue. Aenean rhoncus suscipit odio nec sodales. Aliquam lectus est, condimentum sit amet laoreet non, bibendum in dui. Vivamus placerat ornare turpis vel consequat. Fusce sed urna et tortor suscipit placerat. Nulla in sem sodales, fringilla diam eu, fermentum lacus. Maecenas vestibulum porta mi, at porta augue blandit non. Phasellus porttitor lorem ut venenatis pulvinar. In egestas leo eros, at tristique justo fermentum eu. Quisque vel augue vitae magna cursus congue. Aenean rhoncus suscipit odio nec sodales. Aliquam lectus est, condimentum sit amet laoreet non, bibendum in dui. Vivamus placerat ornare turpis vel consequat. Fusce sed urna et tortor suscipit placerat.</p>\r\n<h2>Sharing Location with the Handyman.</h2>\r\n<p>Nulla in sem sodales, fringilla diam eu, fermentum lacus. Maecenas vestibulum porta mi, at porta augue blandit non. Phasellus porttitor lorem ut venenatis pulvinar. In egestas leo eros, at tristique justo fermentum eu. Quisque vel augue vitae magna cursus congue. Aenean rhoncus suscipit odio nec sodales. Aliquam lectus est, condimentum sit amet laoreet non, bibendum in dui. Vivamus placerat ornare turpis vel consequat. Fusce sed urna et tortor suscipit placerat. Nulla in sem sodales, fringilla diam eu, fermentum lacus. Maecenas vestibulum porta mi, at porta augue blandit non. Phasellus porttitor lorem ut venenatis pulvinar. In egestas leo eros, at tristique justo fermentum eu.</p>\r\n<p>Quisque vel augue vitae magna cursus congue. Aenean rhoncus suscipit odio nec sodales. Aliquam lectus est, condimentum sit amet laoreet non, bibendum in dui. Vivamus placerat ornare turpis vel consequat. Fusce sed urna et tortor suscipit placerat.</p>\r\n<p>Maecenas sem risus, volutpat vitae nisi id, gravida mattis lacus. In in leo sapien. Mauris tempor ut orci eget posuere. Etiam dapibus pellentesque mi sit amet elementum. Mauris commodo sem id enim auctor, in efficitur ipsum consectetur. Aenean dignissim ultricies est in efficitur.</p>\r\n<h2>Your Location History</h2>\r\n<p>We store your location history to provide you better quality deals. This information is treated as private under any and all circumstances and is NEVER shared.</p>', 'Y', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'Terms and Conditions', 'terms-and-conditions', '<h2>Your Acceptance</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam molestie, orci vitae interdum mattis, eros neque egestas odio, ut blandit libero sapien a dui. Donec vitae magna hendrerit augue pulvinar rhoncus accumsan ac orci. Sed pellentesque, ante eget consectetur blandit, nibh nulla vulputate lectus, pulvinar viverra lacus massa eu lorem. Sed vehicula purus vel dui pharetra, in placerat nunc elementum. Proin fermentum porttitor dolor, eget vehicula enim dictum ut. Praesent fermentum tortor a neque hendrerit, a posuere libero ornare. Pellentesque ligula tortor, bibendum et volutpat id, tempus ac magna.</p>\r\n<h2>Handyman Service</h2>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam molestie, orci vitae interdum mattis, eros neque egestas odio, ut blandit libero sapien a dui. Donec vitae magna hendrerit augue pulvinar rhoncus accumsan ac orci. Sed pellentesque, ante eget consectetur blandit, nibh nulla vulputate lectus.</p>\r\n<h2>Handyman  Access</h2>\r\n<p class="marginL5"><strong>1.</strong> Maecenas sem risus, volutpat vitae nisi id, gravida mattis lacus. In in leo sapien. Mauris tempor ut orci eget posuere. Etiam dapibus pellentesque mi sit amet elementum. Mauris commodo sem id enim auctor, in efficitur ipsum consectetur. Aenean dignissim ultricies est in efficitur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum rhoncus diam nec gravida commodo. Maecenas congue, elit non faucibus commodo, ex diam scelerisque sapien, in porttitor metus justo sed nunc. Donec finibus sapien a lectus cursus, id dignissim lacus maximus. Nam porta ligula sed urna suscipit porttitor.</p>\r\n<p class="marginL5"><strong>2.</strong> Maecenas sem risus, volutpat vitae nisi id, gravida mattis lacus. In in leo sapien. Mauris tempor ut orci eget posuere. Etiam dapibus pellentesque mi sit amet elementum. Mauris commodo sem id enim auctor, in efficitur ipsum consectetur. Aenean dignissim ultricies est in efficitur.</p>\r\n<p class="marginL5"><strong>3.</strong> Maecenas sem risus, volutpat vitae nisi id, gravida mattis lacus. In in leo sapien. Mauris tempor ut orci eget posuere. Etiam dapibus pellentesque mi sit amet elementum. Mauris commodo sem id enim auctor, in efficitur ipsum consectetur. Aenean dignissim ultricies est in efficitur.</p>\r\n<h2>Intellectual Property Rights</h2>\r\n<p>Nulla in sem sodales, fringilla diam eu, fermentum lacus. Maecenas vestibulum porta mi, at porta augue blandit non. Phasellus porttitor lorem ut venenatis pulvinar. In egestas leo eros, at tristique justo fermentum eu. Quisque vel augue vitae magna cursus congue. Aenean rhoncus suscipit odio nec sodales. Aliquam lectus est, condimentum sit amet laoreet non, bibendum in dui. Vivamus placerat ornare turpis vel consequat. Fusce sed urna et tortor suscipit placerat.</p>', 'Y', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 'How its work', 'how-its-work', '<p><span xss=removed><strong><span class="bold">HandyMan</span></strong> is easy to use, and Home Owners can post their projects for free. Simply post your project and <span class="bold">HandyMan</span> will send you verified and qualified contractors within minutes . Compare Contractor by looking at their profiles, work history, reviews from previous customers, insurance information and membership to reputable associations like the Better Business Bureau and others . You will then decide who to speak to and get quotes from. After the project is completed, leave your feedback review so the next home owners can research on your experience.</span></p>', 'Y', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;


-- Dumping structure for table handyman.payments
CREATE TABLE IF NOT EXISTS `payments` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_gross` float(10,2) NOT NULL,
  `currency_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `payer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_seen` tinyint(4) NOT NULL DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table handyman.payments: ~20 rows (approximately)
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` (`payment_id`, `user_id`, `task_id`, `txn_id`, `payment_gross`, `currency_code`, `payer_email`, `payment_status`, `is_seen`, `updated`) VALUES
	(1, 20, 3, '6CN787999X181552U', 15.00, 'USD', 'akshay.bhargav@brsoftech.org', 'Pending', 1, '2016-12-11 11:11:00'),
	(2, 20, 4, '43J6999940556122T', 80.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2016-12-21 11:11:00'),
	(3, 20, 5, '4S506919JL970672R', 20.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2016-12-22 11:11:00'),
	(4, 20, 6, '7V75141022355572R', 10.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2016-12-13 11:11:00'),
	(5, 20, 7, '756443600M821983T', 5.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2016-12-10 11:11:00'),
	(6, 20, 8, '8JB75962KF482825P', 5.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2016-12-11 11:11:00'),
	(7, 31, 33, '7VU97116LD655681K', 6.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2017-01-09 13:06:01'),
	(8, 31, 31, '7FU07505SL823273R', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-09 13:06:11'),
	(9, 31, 32, '687638629Y3853701', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-09 13:06:18'),
	(10, 31, 35, '940169348D3200314', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2017-01-09 13:07:43'),
	(11, 31, 47, '5NS50737CU2583641', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2017-01-10 06:54:10'),
	(12, 31, 49, '6AA20379DP664305C', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2017-01-10 07:24:42'),
	(13, 31, 51, '6F046976E9554880A', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 07:29:25'),
	(14, 31, 52, '4RB06578UD5839936', 3.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2017-01-10 07:30:26'),
	(15, 31, 63, '9G492885MM929034F', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 0, '2017-01-10 08:59:59'),
	(16, 31, 62, '4BU65221AG278383X', 1.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2017-01-10 09:59:39'),
	(17, 31, 71, '02U66831GN997925D', 2.00, 'USD', 'pushpendravivek1991@gmail.com', 'Pending', 1, '2017-01-10 12:44:22'),
	(18, 47, 73, '8E108680P2828831L', 12.00, 'USD', 'ramswaroop.jat@brsoftech.com', 'Pending', 1, '2017-01-13 05:59:51'),
	(19, 47, 82, '29B35910GC9559413', 2.00, 'USD', 'ramswaroop.jat@brsoftech.com', 'Pending', 1, '2017-01-13 12:03:39'),
	(20, 53, 88, '5MX094249G8494440', 48.00, 'USD', 'ramswaroop.jat@brsoftech.com', 'Pending', 0, '2017-01-16 06:11:13');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;


-- Dumping structure for table handyman.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(50) DEFAULT NULL COMMENT 'who is giving rating',
  `user_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `review_by` int(11) DEFAULT NULL,
  `review` text,
  `rating` float DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.reviews: ~21 rows (approximately)
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` (`id`, `user_type`, `user_id`, `task_id`, `review_by`, `review`, `rating`, `created`) VALUES
	(15, 'user', 16, 21, 31, 'good', 4, '2016-12-31 07:15:32'),
	(16, 'user', 16, 20, 31, 'average', 1, '2016-12-31 07:16:51'),
	(17, 'user', 18, 22, 32, 'very good', 5, '2016-12-31 07:20:48'),
	(18, 'handyman', 31, 20, 16, 'good', 5, '2017-01-05 08:28:36'),
	(19, 'handyman', 31, 21, 16, 'good', 3, '2017-01-05 08:42:58'),
	(20, 'user', 12, 14, 29, '', 3, '2017-01-06 20:38:36'),
	(21, 'handyman', 47, 77, 25, 'good job done.', 5, '2017-01-13 10:17:54'),
	(22, 'user', 25, 77, 47, 'good job', 5, '2017-01-13 10:24:19'),
	(23, 'handyman', 47, 81, 25, 'review submitted by handy.. ', 5, '2017-01-13 12:54:45'),
	(24, 'user', 1, 72, 46, 'rating by user to handyman test', 4, '2017-01-16 04:28:36'),
	(25, 'handyman', 46, 72, 1, 'rating by handy to userr', 3, '2017-01-16 04:30:31'),
	(26, 'handyman', 53, 89, 27, 'Good', 4, '2017-01-16 07:09:45'),
	(27, 'user', 27, 89, 53, 'Good Job Buddy', 4, '2017-01-16 08:24:11'),
	(28, 'user', 27, 91, 53, 'Nice Job', 4, '2017-01-16 09:30:08'),
	(29, 'handyman', 53, 91, 27, '', 4, '2017-01-16 12:08:22'),
	(33, 'user', 0, 0, 31, 'good', 4, '2017-01-17 11:23:55'),
	(34, 'user', 0, 0, 31, 'good', 4, '2017-01-17 11:24:13'),
	(35, 'user', 15, 63, 31, 'test', 4, '2017-01-17 11:36:26'),
	(36, 'handyman', 31, 29, 23, '', 5, '2017-01-18 06:17:13'),
	(37, 'user', 25, 98, 47, 'rating to check tme..', 5, '2017-01-18 10:28:06'),
	(38, 'handyman', 47, 98, 25, '', 5, '2017-01-18 10:30:02');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;


-- Dumping structure for table handyman.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0=>admin,1=>user,2=>handyman',
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT 'user.jpg',
  `address` text,
  `phone_number` varchar(100) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lon` varchar(255) DEFAULT NULL,
  `ip_address` varchar(200) DEFAULT NULL,
  `total_offers` int(11) DEFAULT '0',
  `total_task` int(11) DEFAULT '0',
  `total_submitted` int(11) DEFAULT '0',
  `total_completed` int(11) DEFAULT '0',
  `total_cancelled` int(11) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `fb_id` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `is_login` enum('Y','N') DEFAULT 'N',
  `about_urself` text,
  `education` text,
  `speciality` text,
  `languages` text,
  `workings` text,
  `avg_rating` float DEFAULT '0',
  `credit` float DEFAULT '0',
  `block_till` datetime DEFAULT NULL,
  `raw_password` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

-- Dumping data for table handyman.users: ~31 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `user_type`, `email`, `first_name`, `last_name`, `password`, `image`, `address`, `phone_number`, `lat`, `lon`, `ip_address`, `total_offers`, `total_task`, `total_submitted`, `total_completed`, `total_cancelled`, `last_login`, `fb_id`, `created`, `modified`, `status`, `is_login`, `about_urself`, `education`, `speciality`, `languages`, `workings`, `avg_rating`, `credit`, `block_till`, `raw_password`) VALUES
	(1, 0, 'admin@gmail.com', 'admin', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-19 10:28:49', NULL, NULL, NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(10, 1, 'admddin@gmail.comyuyu', 'handy1', 'handy1', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-24 16:15:15', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(17, 1, 'test_user1@gmail.com', 'test1', 'user1', '827ccb0eea8a706c4c34a16891f84e7b', 'user.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-28 09:46:22', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(18, 1, 'hemendra@gmail.com', 'hememndra', 'k', '25d55ad283aa400af464c76d713c07ad', 'user.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-30 16:22:21', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(19, 1, 'mengfei0714@gmail.com', 'mengfei', 'peng', '96e79218965eb72c92a549dd5a330112', 'user.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-19 04:58:04', NULL, '2016-11-30 16:27:04', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(22, 1, 'user2@brsoftech.org', 'user233', 'user233', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '111.93.195.78', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-12-06 12:27:41', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(23, 1, 'xctest@mailinator.com', 'XC', 'Test', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '99.238.201.125', NULL, NULL, NULL, NULL, NULL, '2016-12-16 19:26:54', NULL, '2016-12-16 19:26:47', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(24, 1, 'testcustomer@gmail.com', 'Test', 'Customer', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-13 18:09:11', NULL, '2016-12-16 19:30:00', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(26, 1, 'test.p@brsoftech.org', 'test', 'test', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '111.93.195.78', NULL, NULL, NULL, NULL, NULL, '2016-12-21 12:14:32', NULL, '2016-12-21 12:14:22', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(28, 1, 'robert@gmail.com', 'robert', 'downy', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '117.247.227.77', NULL, NULL, NULL, NULL, NULL, '2016-12-23 12:59:17', NULL, '2016-12-22 12:40:52', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(29, 1, 'qqq1223@mailinator.com', 'qqq', 'aaa', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '99.238.201.125', NULL, NULL, NULL, NULL, NULL, '2017-01-14 00:59:51', NULL, '2016-12-23 18:40:18', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
	(31, 1, 'vandana.pareek@brsoftech.org', 'vandana', 'pareek', 'e10adc3949ba59abbe56e057f20f883e', '1483166626images(1).jpg', NULL, NULL, NULL, NULL, '117.247.227.77', 6, 39, 0, 5, 1, '2017-01-19 10:00:00', NULL, '2016-12-31 04:49:27', NULL, 1, 'Y', '', '', '', '', '', 4, 8.6, NULL, NULL),
	(32, 1, 'manesh.kumar@brsoftech.org', 'manesh', 'kumar', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '111.93.195.78', 1, 1, 0, 1, 0, '2016-12-31 07:19:18', NULL, '2016-12-31 05:07:16', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(33, 1, 'privacy@gmail.com', 'Privacy Policy', 'Privacy Policy', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '111.93.195.78', 0, 0, 0, 0, 0, '2016-12-31 06:45:25', NULL, '2016-12-31 06:24:29', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(34, 1, 'kajal.chaturvedi@brsoftech.org', 'kajal', 'chaturvedi', 'e10adc3949ba59abbe56e057f20f883e', '1483167962attractions3.jpg', NULL, NULL, NULL, NULL, '117.247.227.77', 3, 7, 0, 0, 0, '2017-01-10 09:23:11', NULL, '2016-12-31 06:42:41', NULL, 1, 'Y', '', '', '', '', '', 0, 0, NULL, NULL),
	(35, 1, 'qqq0103@mailinator.com', 'aaa', 'sss', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '99.238.201.125', 0, 0, 0, 0, 0, '2017-01-03 20:31:30', NULL, '2017-01-03 16:13:02', NULL, 0, 'N', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(36, 1, 'qqq0103-2@mailinator.com', 'www', 'eeee', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '99.238.201.125', 0, 0, 0, 0, 0, '2017-01-03 16:52:41', NULL, '2017-01-03 16:14:48', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(37, 1, 'qqq0103-3@mailinator.com', 'ddd', 'xxx', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '99.238.201.125', 0, 0, 0, 0, 0, '2017-01-03 20:33:12', NULL, '2017-01-03 16:17:11', NULL, 0, 'N', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(38, 1, 'qqq0103-4@mailinator.com', 'aaa', 'bbb', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2017-01-09 21:29:17', NULL, '2017-01-03 20:34:10', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(39, 1, 'fgh@fd.com', 'ghfgh', 'fghfgh', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '111.93.195.78', 0, 0, 0, 0, 0, NULL, NULL, '2017-01-04 10:49:05', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(40, 1, 'sdsd@gmail.com', 'xdsd', 'sdsds', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '117.247.227.77', 0, 0, 0, 0, 0, NULL, NULL, '2017-01-04 12:06:24', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(41, 1, 'qqq0106@mailinator.com', 'wwwww', 'wwwww', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '99.238.201.125', 0, 0, 0, 0, 0, NULL, NULL, '2017-01-06 19:49:57', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(42, 1, 'sampleverma@gmail.com', 'Sample', 'Verma', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2017-01-12 11:18:21', '356284601419542', '2017-01-12 11:05:45', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '123456'),
	(46, 1, 'vandana.pareek42@gmail.com', 'Vandana', 'Pareek', '89b34a69ebd955a3bfd4528f3b87984c', '1484225434images(2).jpg', NULL, NULL, NULL, NULL, NULL, 0, 2, 0, 1, 0, '2017-01-16 16:40:56', '1395939350450963', '2017-01-12 12:24:13', NULL, 1, 'Y', '', '', '', '', '', 3, 2, '2017-01-17 16:47:14', '530323'),
	(47, 1, 'test123@mailinator.com', 'test', '123', 'e10adc3949ba59abbe56e057f20f883e', '14842915512_alexandrpopkov-morning_grasses.jpg', NULL, NULL, NULL, NULL, '111.93.195.78', 3, 13, 0, 2, 0, '2017-01-18 14:33:57', NULL, '2017-01-13 05:54:13', NULL, 1, 'Y', '', '', '', '', '', 5, 8.4, '2017-01-17 16:53:42', NULL),
	(48, 1, 'handy@mailnator.com', '1234', '4545', 'cdeee02fa37ce77eb43ccbd8e7a63ba8', 'user.jpg', NULL, NULL, NULL, NULL, '111.93.195.78', 0, 0, 0, 0, 0, '2017-01-13 16:08:09', NULL, '2017-01-13 10:28:28', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(49, 1, 'handy1@mailinator.com', 'handyy', 'one one', '25d55ad283aa400af464c76d713c07ad', '', NULL, NULL, NULL, NULL, '111.93.195.78', 0, 0, 0, 0, 0, '2017-01-13 16:52:18', NULL, '2017-01-13 10:43:09', NULL, 1, 'N', 'Handyman.com helps you find an experienced, professional Handyman contractor in your local area, FREE, NO OBLIGATIONS. Handyman.com is the industry leading portal for the home improvement, home repair and remodeling industry. Our free tools and services help both homeowners and contractors facilitate the process to accomplish your home repair and remodeling needs. Serving the Home and Contractor market.', 'B.tech', 'Handyman.com helps you find an experienced, professional Handyman contractor in your local area, FREE, NO OBLIGATIONS. Handyman.com is the industry leading portal for the home improvement, home repair and remodeling industry. Our free tools and services help both homeowners and contractors facilitate the process to accomplish your home repair and remodeling needs. Serving the Home and Contractor market.', 'Hindi, English, Punjabi', '20 years', 0, 0, NULL, NULL),
	(50, 1, 'neha.handicrunch@gmail.com', 'Amy', 'Zak', 'eaeb99d866a84210eb2d4dbd88e79161', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/14100360_235498200184429_6683999861873272950_n.jpg?oh=112aae636e7f95552c39d2d3bf13b533&oe=59231D83', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2017-01-13 16:53:15', '319568761777372', '2017-01-13 11:23:15', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '164856'),
	(51, 1, 'qc.qing.chen@gmail.com', 'Wei', 'Chen', 'e91da9c5c055ff7d20087b6a1a4cbdd5', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c15.0.50.50/p50x50/10354686_10150004552801856_220367501106153455_n.jpg?oh=978df650af5b925f321fe4050af2869f&oe=5911542F', NULL, NULL, NULL, NULL, NULL, 1, 2, 0, 0, 0, '2017-01-13 21:18:30', '113974599110079', '2017-01-13 15:26:50', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '632572'),
	(52, 1, 'qqq1224@mailinator.com', 'qqqqqqq', 'wwwwww', 'e10adc3949ba59abbe56e057f20f883e', 'user.jpg', NULL, NULL, NULL, NULL, '99.238.201.125', 0, 0, 0, 0, 0, '2017-01-13 21:33:34', NULL, '2017-01-13 16:03:12', NULL, 1, 'N', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL),
	(53, 1, 'amittester009@ gmail.com', 'Amit', 'User', '62aff5e676a5c89f4203ecd2ae6478c8', 'user.jpg', NULL, NULL, NULL, NULL, '111.93.195.78', 1, 5, 0, 3, 0, '2017-01-16 17:57:47', NULL, '2017-01-16 05:32:10', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

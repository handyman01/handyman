// Custom Js 
$(document).ready(function(){
	$(".custom-select").each(function(){
     $(this).wrap( "<span class='select-wrapper'></span>" );
      $(this).after("<span class='holder'></span>");
    });
	$(".custom-select").change(function(){
     var selectedOption = $(this).find(":selected").text();
      $(this).next(".holder").text(selectedOption);
    }).trigger('change');
})

//date slider

$('.responsive').slick({
  dots: false,
  arrows: true,
  infinite: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow:2,
        slidesToScroll: 1,
        infinite: true,
		dots: false,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
		dots: false,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
		dots: false,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});